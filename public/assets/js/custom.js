/* Add here all your JS customizations */
$(document).ready(function () {
	$("#inmueble-publicado").addClass("hidden");
	$("#publicado-error").addClass("hidden");

	$(".price_input").on({
		"focus": function (event) {
			$(event.target).select();
		},
		"keyup": function (event) {
			$(event.target).val(function (index, value) {
				return value.replace(/\D/g, "")
				// .replace(/([0-9])([0-9]{2})$/, "$1.$2")
				.replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
			});
		}
	});
});

$(document).ready(function(){
	$("#modalInicio").modal('show');
});

function openFollowingsSubmenu(event, id) {
	event.preventDefault();

	if ($("#" + id).hasClass("hidden")) {
		$("#" + id).removeClass("hidden");
	} else {
		$("#" + id).addClass("hidden");
	}
}

var input = $('input[type="file"]');

input.on("change", function (event) {
	var upl = event.currentTarget;
	var max = 20000000;

	if (upl.files[0].size > max) {
		alert("Archivo demasiado grande, por favor ingrese un archivo de máximo 20MB!");
		upl.value = "";
	}
});

// $(".lightgallery").lightGallery({
// 	mode: 'lg-slide',
// 	thumbnail: true,
// 	selector: '.item',
// 	toogleThumb: true,
// 	zoom: false
// });

var viewer = null;

if ($("#viewer").length) {
	viewer = null;
	$(".nav-tabs a").on("shown.bs.tab", function (e) {
		if ($(e.target).attr("href") == "#mult") {
			var iterator = 0;
			viewer = new PhotoSphereViewer.Viewer({
				container: document.querySelector("#viewer"),
				panorama: $("#imagen_360_0").val(),
				navbar: [
					"autorotate",
					"zoom",
					{
						id: "my-button",
						content: "<<",
						title: "Imagen anterior",
						className: "custom-button",
						onClick: () => {
							if (iterator >= 1) {
								iterator = iterator - 1;
								changeImage($("#imagen_360_" + iterator).val());
							}
						}
					},
					{
						id: "my-button",
						content: ">>",
						title: "Imagen siguiente",
						className: "custom-button",
						onClick: () => {
							;
							if (iterator <= $("#imagesLenght").val()) {
								iterator = iterator + 1;
								changeImage($("#imagen_360_" + iterator).val());
							}
						}
					},
					"caption",
					"fullscreen"
				]
			});

			function changeImage(image) {
				viewer.setPanorama(image);
			}
		} else {
			viewer.destroy();
		}
	});
}
