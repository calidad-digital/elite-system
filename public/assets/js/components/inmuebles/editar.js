(function () {

    var app = angular.module("app", [], function ($interpolateProvider) {
        $interpolateProvider.startSymbol("<%");
        $interpolateProvider.endSymbol("%>");
    });

    angular.module("app").directive("selectNgFiles", function () {
        return {
            require: "ngModel",
            link: function postLink(scope, elem, attrs, ngModel) {
                elem.on("change", function (e) {
                    var files = elem[0].files;
                    ngModel.$setViewValue(files);
                })
            }
        }
    });

    app.filter("cletter", function () {
        return function (input, scope) {
            if (input != null) {
                input = input.toLowerCase();
                return input.substring(0, 1).toUpperCase() + input.substring(1);
            } else {
                return input
            }
        }
    });

    app.filter("replacespacios", function () {
        return function (input) {
            if (input != null) {
                input = input.replace(/\s/g, "-");
                return input;
            }
        }
    });

    app.directive("repeatEnd", function () {
        return {
            restrict: "A",
            link: function (scope, element, attrs) {
                if (scope.$last) {
                    scope.$eval(attrs.repeatEnd);
                }
            }
        };
    });

    //Cálculos de precio
    app.controller("Formulario", function ($scope, $http, $timeout) {
        $scope.tipo_inmueble = "";
        $scope.gestion = "";
        $scope.arriendo = 0;
        $scope.administracion = 0;
        $scope.venta = 0;
        $scope.total = 0;

        $("#formlario-cargando").hide();
        $("#formlario-enviado").hide();
        $("#formlario-error").hide();

        $scope.setValorInm = function (gestion, venta, arriendo, administracion) {
            var total = (gestion == 1 ? (parseInt(arriendo) + parseInt(administracion)) : venta);

            $scope.total = total.toString().replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
        }

        $scope.setTotal = function () {
            var arriendo = $scope.arriendo.toString().replace(/,/g, "");
            var administracion = $scope.administracion.toString().replace(/,/g, "");
            var venta = $scope.venta.toString().replace(/,/g, "");

            var total = 0;

            if ($scope.arriendo != "" && $scope.arriendo != 0) {
                total = (parseInt(arriendo) + parseInt(administracion));
            } else {
                total = parseInt(venta);
            }

            $scope.total = total.toString().replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
        }

        //Úbicación
        $scope.departamento = "";
        $scope.ciudad = "";
        $scope.zona = "";
        $scope.localidad = "";
        $scope.barrio = "";

        //Ciudades
        $scope.ciudades = function () {
            var departamento = $scope.departamento;
            $http.get("/alt/ciudades/" + departamento)
            .then(function (response) {
                $scope.cities = response.data;
            })
            .catch(function (response) {
                console.log(response);
            });
        }

        //Localidades
        $scope.localidades = function () {
            var ciudad = $scope.ciudad;
            $http.get("/alt/localidades/" + ciudad)
            .then(function (response) {
                $scope.localities = response.data;
            })
            .catch(function (response) {
                console.log(response);
            });
        }

        //Barrios
        $scope.barrios = function () {
            var localidad = $scope.localidad;
            $http.get("/alt/barrios/" + localidad)
            .then(function (response) {
                $scope.neighborhoods = response.data;
            })
            .catch(function (response) {
                console.log(response);
            });
        }

        $scope.barrioName = "";
        $scope.selectBarrio = function () {
            var barrio = $("#barrio_id option:selected").text().toUpperCase().trim().replaceAll(" ", "%20");
            $scope.barrioName = " " + barrio.replaceAll("%20", " ");
        }

        //Agentes
        $scope.sucursal = "";
        $scope.asesores = function () {
            var sucursal = $scope.sucursal;
            $http.get("/alt/agentes/" + sucursal)
            .then(function (response) {
                $scope.agents = response.data;
            })
            .catch(function (response) {
                console.log(response);
            });
        }

        //Caracteristicas
        $scope.mostrarCaracteristicas = function () {
            var tipo_inmueble = $scope.tipo_inmueble;
            $http.get("/alt/caracteristicas/" + tipo_inmueble)
            .then(function (response) {
                $scope.amenities = response.data;
            })
            .catch(function (response) {
                console.log(response);
            });
        }

        //Dirección
        $scope.dir1 = "1";
        $scope.dir2 = "";
        $scope.dir3 = "1";
        $scope.dir4 = "1";
        $scope.dir5 = "1";
        $scope.dir6 = "1";
        $scope.dir7 = "";
        $scope.dir8 = "1";
        $scope.dir9 = "";
        $scope.dir10 = "1";
        $scope.complemento = "";
        $scope.complemento2 = "";
        $scope.direccion = "";
        $scope.campodireccion = "";
        $scope.latitude = "";
        $scope.longitude = "";
        $scope.isCompleteAddress = true;

        let map;
        var position = new google.maps.LatLng(parseFloat(4.643165), parseFloat(-74.105575));

        map = new google.maps.Map(document.getElementById("mapa"), {
            center: { lat: parseFloat(4.643165), lng: parseFloat(-74.105575) },
            zoom: 12,
        });

        var marker = new google.maps.Marker({
            position: position,
            title: "Inmueble",
            map: map
        });

        $scope.esperarDireccion = function () {
            if (!$scope.isCompleteAddress) {
                $timeout(function () {
                    $scope.armarDireccion();
                    $scope.buscardireccion();
                }, 2000);
            }
        }

        $scope.armarDireccion = function (e = null, time) {
            if (e != null) {
                e.preventDefault();
            }

            // Utilizando las variables
            var dir1 = $("#dir1 option:selected").text().toUpperCase().trim();
            var dir2 = $("#dir2").val();
            var dir3 = $("#dir3 option:selected").val() != 1 ? $("#dir3 option:selected").text().toUpperCase().trim() : "";
            var dir4 = $("#dir4 option:selected").val() != 1 ? $("#dir4 option:selected").text().toUpperCase().trim() : "";
            var dir5 = $("#dir5 option:selected").val() != 1 ? $("#dir5 option:selected").text().toUpperCase().trim() : "";
            var dir6 = $("#dir6 option:selected").val() != 1 ? $("#dir6 option:selected").text().toUpperCase().trim() : "";
            var dir7 = $("#dir7").val();
            var dir8 = $("#dir8 option:selected").val() != 1 ? $("#dir8 option:selected").text().toUpperCase().trim() : "";
            var dir9 = $("#dir9").val();
            var dir10 = $("#dir10 option:selected").val() != 1 ? $("#dir10 option:selected").text().toUpperCase().trim() : "";

            var direccion = $scope.direccion;

            direccion = dir1 + " "
            + dir2 + " "
            + dir3 + " "
            + dir4 + " "
            + dir5 + " "
            + dir6 + " "
            + dir7 + " "
            + dir8 + " "
            + dir9 + " "
            + dir10 + " ";

            $scope.direccion = direccion;
            $("#direccion").val(direccion);

            var barrio = $("#barrio_id option:selected").text().toLowerCase().trim().replaceAll(" ", "%20");
            var ciudad = $("#ciudad option:selected").text().toLowerCase().trim().replaceAll(" ", "%20");
            var direccion_completa = $("#direccion_completa").val();

            if (!$scope.isCompleteAddress) {
                $scope.campodireccion = direccion;
            } else {
                $scope.campodireccion = direccion_completa;
            }

            if (barrio != "seleccione%20barrio") {
                $scope.campodireccion += " " + barrio.replaceAll("%20", " ");
            }

            if (ciudad != "seleccione%20una%20ciudad") {
                $scope.campodireccion += " " + ciudad.replaceAll("%20", " ");
            }

            $scope.campodireccion = $scope.campodireccion.trim();
        }

        $scope.buscardireccion = function (e = null) {

            if (e != null) {
                e.preventDefault();
            }

            var url = "https://geocode.arcgis.com"
            + "/arcgis/rest/services/World/GeocodeServer/findAddressCandidates?f=pjson"
            + "&SingleLine=" + $scope.campodireccion.trim().replaceAll(" ", "%20").replaceAll("#", "")
            + "%20,Colombia"
            + "&outFields=Match_addr%2CAddr_type";

            $http.get(url)
            .then(function (response) {
                var latitud = response.data.candidates[0].location.y;
                var longitud = response.data.candidates[0].location.x;

                $scope.latitude = latitud;
                $scope.longitude = longitud;

                marker.setMap(null);

                marker = new google.maps.Marker({
                    position: position,
                    title: $scope.campodireccion,
                    map: map,
                    draggable: true,
                    zoom: 12
                });

                position = new google.maps.LatLng(parseFloat(latitud), parseFloat(longitud));
                map.setCenter(position);
                $http.get(url)
                .then(function (response) {
                    var latitud = response.data.candidates[0].location.y;
                    var longitud = response.data.candidates[0].location.x;

                    $scope.latitude = latitud;
                    $scope.longitude = longitud;

                    marker.setMap(null);

                    marker = new google.maps.Marker({
                        position: position,
                        title: $scope.campodireccion,
                        map: map,
                        draggable: true,
                        zoom: 12
                    });

                    position = new google.maps.LatLng(parseFloat(latitud), parseFloat(longitud));
                    map.setCenter(position);
                    // $timeout(function () { $("#searchMap").click(); }, 1000);
                })
                .catch(function (response) {
                    console.log(response);
                });
            })
            .catch(function (response) {
                console.log(response);
            });




            // $scope.buscardireccion();
        }

        $scope.caracteriticasSeleccionadas = [];
        $scope.caracteriticasEliminadas = [];
        $scope.setAlreatySelectedAmenities = function (amenities) {
            $scope.caracteriticasSeleccionadas = JSON.parse(amenities);
            $scope.caracteriticasEliminadas = JSON.parse(amenities);
        }

        $scope.deleteAmenity = function (id) {

        }

        $scope.submitData = function (url) {

            $("#formlario-cargando").show();
            $("#formlario-enviado").hide();
            $("#formlario-error").hide();

            var formData = $("#formulario-crear-inmueble").serializeArray();

            var data = {};
            $(formData).each(function (index, obj) {
                data[obj.name] = obj.value;

            });

            $http.post(url, data)
            .then(function (response) {
                let finalUrl = "/admin/inmuebles/editar/" + response.data.id + "/imagenes"
                console.log(finalUrl)
                $("#formlario-cargando").hide();
                $("#formlario-enviado").show();

                if (data['parq1_chip'] != '125AA') {
                    window.location.href = finalUrl;
                }

            })
            .catch(function (response) {
                console.log(response);
                $("#formlario-cargando").hide();
                $("#formlario-error").html(response.data.mensaje);
                $("#formlario-error").show();
            });
        }
    });

    app.factory('generalfactory', function () {
        var generalfactory = function () { };


        generalfactory.previousPage = function () {
            window.history.back();
        }

        return generalfactory;
    })

    app.controller("FormImage", function ($scope, $http, $timeout, generalfactory) {
        $scope.generalfactory = generalfactory;
        $scope.initDropZone = function (token, url, id, imagenes) {

            imagenes = JSON.parse(imagenes);

            if (window.document.documentMode) {
                alert("This website doesn't work on Internet Explorer, you should use modern browsers like Chrome or Safari instead");
            }

            var myImageUpload = new ImageUpload({
                imageUploadZoneId: 'iu-image-upload-zone',
                imageGalleryId: 'iu-gallery',
                dragAndDropFeature: false,
                dictUploadImageNote: 'Agregar fotos',

                //Enviando al api
                sendRequestToServer: true,
                headers: { "X-CSRF-TOKEN": token },
                saveImageRoute: url + "/save-image",
                imageBelongsTo: { "property": id, "is_360": false },
                saveImageOrderRoute: url + "/save-image-order",
                deleteImageRoute: url + "/delete-image",

                //Efectos y demás
                showUploadingLoader: function (imagePlaceholder, showUploadedPercentComplete) {
                    var progressBar = document.createElement('div');
                    progressBar.className = 'iu-progress-bar';
                    var percentBar = document.createElement('div');
                    percentBar.className = 'iu-percent-bar';
                    if (showUploadedPercentComplete === true) {
                        percentBar.innerHTML = '0%';
                    }
                    progressBar.appendChild(percentBar);
                    imagePlaceholder.appendChild(progressBar);
                },

                updateUploadingLoader: function (percentComplete, imagePlaceholder, showUploadedPercentComplete) {
                    var percentComplete = Math.floor(percentComplete);
                    var percentBar = imagePlaceholder.getElementsByClassName('iu-percent-bar')[0];

                    if (percentBar != null) {
                        percentBar.style.width = percentComplete + "%";
                        if (showUploadedPercentComplete === true) {
                            percentBar.innerHTML = percentComplete + "%";
                        }
                    }
                },

                removeUploadingLoader: function (imagePlaceholder, showUploadedPercentComplete) {
                    var progressBar = imagePlaceholder.getElementsByClassName('iu-progress-bar')[0];
                    var fadeEffect = setInterval(function () {
                        if (!progressBar.style.opacity) {
                            progressBar.style.opacity = 1;
                        }
                        if (progressBar.style.opacity > 0) {
                            progressBar.style.opacity -= 0.1;
                        } else {
                            clearInterval(fadeEffect);
                            progressBar.remove();
                        }
                    }, 300);
                },

                addFlashBox: function (showedAlertString, showedTime, backgroundColor) {
                    var oldFlashBox = document.getElementsByClassName('iu-flash-box')[0];
                    if (oldFlashBox) {
                        oldFlashBox.remove();
                    }
                    var flashBox = document.createElement('div');
                    flashBox.className = 'iu-flash-box';
                    if (backgroundColor) {
                        flashBox.style.backgroundColor = backgroundColor;
                    }
                    flashBox.innerHTML = showedAlertString;
                    document.body.appendChild(flashBox);
                    setTimeout(function () {
                        fadeEffect(flashBox);
                    }, showedTime);

                    function fadeEffect(elmnt) {
                        var fadeEffect = setInterval(function () {
                            if (!elmnt.style.opacity) {
                                elmnt.style.opacity = 1;
                            }
                            if (elmnt.style.opacity > 0) {
                                elmnt.style.opacity -= 0.1;
                            } else {
                                clearInterval(fadeEffect);
                                elmnt.remove();
                            }
                        }, 50);
                    }
                },

            });

            imagenes.forEach((item, i) => {
                if (item.is_360 == 0) {
                    myImageUpload.appendServerImage(item.url, item.orden.toString(), item.id);
                }
            });

        }
    });

    app.controller("FormImage360", function ($scope, $http, $timeout) {
        $scope.initDropZone = function (token, url, id, imagenes) {

            imagenes = JSON.parse(imagenes);

            if (window.document.documentMode) {
                alert("This website doesn't work on Internet Explorer, you should use modern browsers like Chrome or Safari instead");
            }

            var myImageUpload = new ImageUpload({
                imageUploadZoneId: 'iu-image-upload-zone-360',
                imageGalleryId: 'iu-gallery-360',
                dragAndDropFeature: false,
                dictUploadImageNote: 'Agregar fotos 360',

                //Enviando al api
                sendRequestToServer: true,
                headers: { "X-CSRF-TOKEN": token },
                saveImageRoute: url + "/save-image",
                imageBelongsTo: { "property": id, "is_360": true },
                saveImageOrderRoute: url + "/save-image-order",
                deleteImageRoute: url + "/delete-image",

                //Efectos y demás
                showUploadingLoader: function (imagePlaceholder, showUploadedPercentComplete) {
                    var progressBar = document.createElement('div');
                    progressBar.className = 'iu-progress-bar';
                    var percentBar = document.createElement('div');
                    percentBar.className = 'iu-percent-bar';
                    if (showUploadedPercentComplete === true) {
                        percentBar.innerHTML = '0%';
                    }
                    progressBar.appendChild(percentBar);
                    imagePlaceholder.appendChild(progressBar);
                },

                updateUploadingLoader: function (percentComplete, imagePlaceholder, showUploadedPercentComplete) {
                    var percentComplete = Math.floor(percentComplete);
                    var percentBar = imagePlaceholder.getElementsByClassName('iu-percent-bar')[0];

                    if (percentBar != null) {
                        percentBar.style.width = percentComplete + "%";
                        if (showUploadedPercentComplete === true) {
                            percentBar.innerHTML = percentComplete + "%";
                        }
                    }
                },

                removeUploadingLoader: function (imagePlaceholder, showUploadedPercentComplete) {
                    var progressBar = imagePlaceholder.getElementsByClassName('iu-progress-bar')[0];
                    var fadeEffect = setInterval(function () {
                        if (!progressBar.style.opacity) {
                            progressBar.style.opacity = 1;
                        }
                        if (progressBar.style.opacity > 0) {
                            progressBar.style.opacity -= 0.1;
                        } else {
                            clearInterval(fadeEffect);
                            progressBar.remove();
                        }
                    }, 300);
                },

                addFlashBox: function (showedAlertString, showedTime, backgroundColor) {
                    var oldFlashBox = document.getElementsByClassName('iu-flash-box')[0];
                    if (oldFlashBox) {
                        oldFlashBox.remove();
                    }
                    var flashBox = document.createElement('div');
                    flashBox.className = 'iu-flash-box';
                    if (backgroundColor) {
                        flashBox.style.backgroundColor = backgroundColor;
                    }
                    flashBox.innerHTML = showedAlertString;
                    document.body.appendChild(flashBox);
                    setTimeout(function () {
                        fadeEffect(flashBox);
                    }, showedTime);

                    function fadeEffect(elmnt) {
                        var fadeEffect = setInterval(function () {
                            if (!elmnt.style.opacity) {
                                elmnt.style.opacity = 1;
                            }
                            if (elmnt.style.opacity > 0) {
                                elmnt.style.opacity -= 0.1;
                            } else {
                                clearInterval(fadeEffect);
                                elmnt.remove();
                            }
                        }, 50);
                    }
                },

            });

            imagenes.forEach((item, i) => {
                if (item.is_360 == 1) {
                    myImageUpload.appendServerImage(item.url, item.orden.toString(), item.id);
                }
            });
        }
    });

    app.controller("Propietarios", function ($scope, $http, $timeout) {
        $("#formlario-cargando").hide();
        $("#formlario-enviado").hide();
        $("#formlario-error").hide();
        $("#formulario-cargando").hide();
        $("#formulario-enviado").hide();
        $("#formulario-error").hide();

        $scope.propietarios = [];

        $scope.nombre = "";
        $scope.lista = function (token, pagina = 1, event = null) {
            if (event != null) {
                event.preventDefault();
            }
            var nombre = $scope.nombre;
            $http.get("/api/clientes?name=" + nombre + "&page=" + pagina, {
                headers: {
                    "Authorization": token
                }
            })
            .then(function (response) {
                $scope.contactos = response.data;
            })
            .catch(function (response) {
                console.log(response);
            });
        }

        $scope.agregarPropietario = function (propietario, event = null) {
            if (event != null) {
                event.preventDefault();
            }

            if ($scope.propietarios.length > 0 && $scope.propietarios[0] != null) {
                var hasMatch = false;

                for (var i = 0; i < $scope.propietarios.length; i++) {
                    var item = $scope.propietarios[i];
                    if (item != null && item.id == propietario.id) {
                        hasMatch = true;
                        break;
                    }

                    if (item == null) {
                        delete $scope.propietarios[i];
                    }
                }

                if (!hasMatch) {
                    $scope.propietarios.push(propietario);
                }
            } else {
                $scope.propietarios.push(propietario);
            }
        }

        $scope.eliminarPropietario = function (index, event = null) {
            if (event != null) {
                event.preventDefault();
            }

            var aBorrar = "";
            if (typeof ($("#borrados").val()) !== "undefined") {
                aBorrar + $("#borrados").val();
            }

            if (typeof ($scope.propietarios[index].inmuebles) !== "undefined") {
                aBorrar += $scope.propietarios[index].inmuebles[0].id + ",";
            }

            $("#borrados").val(aBorrar);

            $scope.propietarios.splice(index, 1);
        }

        $scope.agregarPropietariosYaExistentes = function (propietarios) {
            owners = JSON.parse(propietarios);

            owners.forEach((propietario, i) => {
                $scope.propietarios.push(propietario);
            });
        }

        $scope.creatPropietario = function (url, token) {

            $("#formlario-cargando").show();
            $("#formlario-enviado").hide();
            $("#formlario-error").hide();

            var formData = $("#formulario-crear-propietario").serializeArray();


            var data = {
                archivo_documento: $("#archivo_documento").prop("files")[0]
            };

            $(formData).each(function (index, obj) {
                data[obj.name] = obj.value;
            });


            var configs = {
                headers: {
                    "Content-Type": undefined,
                    "Authorization": token
                },
                transformRequest: function (data) {
                    var formData = new FormData();
                    angular.forEach(data, function (value, key) {
                        formData.append(key, value);
                    });
                    return formData;
                }
            };

            $http.post(url, data, configs)
            .then(function (response) {
                console.log(response);
                $scope.propietarios.push(response.data);

                $scope.lista(token);

                $("#formlario-cargando").hide();
                $("#formlario-enviado").show();
            })
            .catch(function (response) {
                console.log(response);
                $("#formlario-cargando").hide();
                $("#formlario-error").html(JSON.stringify(response.data));
                $("#formlario-error").show();
            });
        }

        $scope.asociarPropietarios = function (url, token) {
            $("#formulario-cargando").show();
            $("#formulario-enviado").hide();
            $("#formulario-error").hide();

            var formData = $("#formulario-asociar-propietario").serializeArray();

            var data = {};
            $(formData).each(function (index, obj) {
                data[obj.name] = obj.value;
            });

            $http.post(url, data, {
                headers: {
                    "Authorization": token
                }
            })
            .then(function (response) {
                console.log(response);

                window.location.href = "/admin/inmuebles/editar/" + response.data[0].inmueble_id + "/documentos";

                $("#formulario-cargando").hide();
                $("#formulario-enviado").show();
            })
            .catch(function (response) {
                console.log(response);
                $("#formulario-cargando").hide();
                $("#formulario-error").html(JSON.stringify(response.data));
                $("#formulario-error").show();
            });
        }
    });

    app.controller("FormDocumentos", function ($scope, $http, $timeout) {
        $scope.file_1 = "";
        $scope.file_2 = "";
        $scope.file_3 = "";
        $scope.file_4 = "";
        $scope.file_5 = "";

        $scope.uploadDocusign = function () {
            $scope.file_1 = "lleno";
        }
    });
})();
