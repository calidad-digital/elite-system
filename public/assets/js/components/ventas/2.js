(function () {

    var app = angular.module("app", [], function ($interpolateProvider) {
        $interpolateProvider.startSymbol("<%");
        $interpolateProvider.endSymbol("%>");
    });

    angular.module("app").directive("selectNgFiles", function () {
        return {
            require: "ngModel",
            link: function postLink(scope, elem, attrs, ngModel) {
                elem.on("change", function (e) {
                    var files = elem[0].files;
                    ngModel.$setViewValue(files);
                })
            }
        }
    });

    app.filter("cletter", function () {
        return function (input, scope) {
            if (input != null) {
                input = input.toLowerCase();
                return input.substring(0, 1).toUpperCase() + input.substring(1);
            } else {
                return input
            }
        }
    });

    app.filter("replacespacios", function () {
        return function (input) {
            if (input != null) {
                input = input.replace(/\s/g, "-");
                return input;
            }
        }
    });

    app.directive("repeatEnd", function () {
        return {
            restrict: "A",
            link: function (scope, element, attrs) {
                if (scope.$last) {
                    scope.$eval(attrs.repeatEnd);
                }
            }
        };
    });

    //Cálculos del formulario
    app.controller("Ventas2Controller", function ($scope, $http, $timeout) {
        $scope.plan_agente_captador = 0;
        $scope.plan_agente_promotor = 0;
        $scope.comision_pactada = 3;
        $scope.precio_cierre = "";
        $scope.caso = "1";

        $scope.comision_venta_mls = 0;
        $scope.ingresos_terceros = 0;
        $scope.ingresos_terceros2 = 0;
        $scope.subtotal_factura = 0;
        $scope.iva_oficina = 0;
        $scope.total_factura = 0;
        $scope.agente1 = {};
        $scope.agente2 = {};

        $scope.captador = "";
        $scope.promotor = "";

        $scope.calcular = function (event) {
            event.preventDefault();

            if ($scope.caso == "1") {
                var precio_cierre = parseInt($scope.precio_cierre.toString().replaceAll(",", ""));

                $scope.ingresos_terceros = (precio_cierre * ($scope.comision_pactada / 100)) * ($scope.plan_agente_captador / 100);
                $scope.comision_venta_mls = (precio_cierre * ($scope.comision_pactada / 100)) - $scope.ingresos_terceros;

                $scope.subtotal_factura = Math.round(($scope.ingresos_terceros + $scope.comision_venta_mls));
                $scope.iva_oficina = $scope.comision_venta_mls * (19 / 100);
                $scope.total_factura = Math.round(($scope.subtotal_factura + $scope.iva_oficina));
            }

            if ($scope.caso == "2") {
                var precio_cierre = parseInt($scope.precio_cierre.toString().replaceAll(",", ""));

                $scope.ingresos_terceros = (precio_cierre * (($scope.comision_pactada / 100) / 2)) * ($scope.plan_agente_captador / 100);
                $scope.comision_venta_mls = (precio_cierre * (($scope.comision_pactada / 100) / 2)) - $scope.ingresos_terceros;

                $scope.subtotal_factura = Math.round(($scope.ingresos_terceros + $scope.comision_venta_mls));
                $scope.iva_oficina = $scope.comision_venta_mls * (19 / 100);
                $scope.total_factura = Math.round(($scope.subtotal_factura + $scope.iva_oficina));
            }

            if ($scope.caso == "3") {
                var precio_cierre = parseInt($scope.precio_cierre.toString().replaceAll(",", ""));

                $scope.ingresos_terceros = precio_cierre * ($scope.comision_pactada / 100) / 2 * ($scope.plan_agente_captador / 100);
                $scope.ingresos_terceros_2 = precio_cierre * ($scope.comision_pactada / 100) / 2 * ($scope.plan_agente_promotor / 100);
                $scope.comision_venta_mls = precio_cierre * ($scope.comision_pactada / 100) - ($scope.ingresos_terceros + $scope.ingresos_terceros_2);

                $scope.subtotal_factura = Math.round(($scope.ingresos_terceros + $scope.comision_venta_mls + $scope.ingresos_terceros_2));
                $scope.iva_oficina = $scope.comision_venta_mls * (19 / 100);
                $scope.total_factura = Math.round(($scope.subtotal_factura + $scope.iva_oficina));
            }

            if ($scope.caso == "4") {
                var precio_cierre = parseInt($scope.precio_cierre.toString().replaceAll(",", ""));

                $scope.ingresos_terceros = precio_cierre * (($scope.comision_pactada / 100) / 2) * ($scope.plan_agente_captador / 100);
                $scope.comision_venta_mls = precio_cierre * ($scope.comision_pactada / 100) - $scope.ingresos_terceros;

                $scope.subtotal_factura = Math.round(($scope.ingresos_terceros + $scope.comision_venta_mls));
                $scope.iva_oficina = $scope.comision_venta_mls * (19 / 100);
                $scope.total_factura = Math.round(($scope.subtotal_factura + $scope.iva_oficina));
            }
        }

        $scope.setCaptador = function (agente, nombre, apellido, doc) {
            console.log(agente);
            $scope.captador = agente;
            $scope.agente1.nombres = nombre;
            $scope.agente1.apellidos = apellido;
            $scope.agente1.documento = doc;
            $scope.agente2.nombres = nombre;
            $scope.agente2.apellidos = apellido;
            $scope.agente2.documento = doc;
        }

        $scope.setPlanCaptador = function (agentes) {
            agentes = JSON.parse(agentes);

            agentes.forEach((agente, i) => {
                if (agente.id == $scope.captador) {
                    $scope.plan_agente_captador = agente.plan;
                    $scope.agente1.nombres = agente.nombres;
                    $scope.agente1.apellidos = agente.apellidos;
                    $scope.agente1.documento = agente.documento;
                }
            });
        }

        $scope.setPlanPromotor = function (agentes) {
            agentes = JSON.parse(agentes);

            agentes.forEach((agente, i) => {
                if (agente.id == $scope.promotor) {
                    console.log(agente);
                    $scope.plan_agente_promotor = agente.plan;
                    $scope.agente2.nombres = agente.nombres;
                    $scope.agente2.apellidos = agente.apellidos;
                    $scope.agente2.documento = agente.documento;
                }
            });
        }
    });
})();
