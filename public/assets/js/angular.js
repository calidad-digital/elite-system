(function () {
    var app = angular.module(
        "app",
        ["angularSuperGallery"],
        function ($interpolateProvider) {
            $interpolateProvider.startSymbol("<%");
            $interpolateProvider.endSymbol("%>");
        }
    );

    angular.module("app").directive("selectNgFiles", function () {
        return {
            require: "ngModel",
            link: function postLink(scope, elem, attrs, ngModel) {
                elem.on("change", function (e) {
                    var files = elem[0].files;
                    ngModel.$setViewValue(files);
                });
            },
        };
    });

    app.filter("cletter", function () {
        return function (input, scope) {
            if (input != null) {
                input = input.toLowerCase();
                return input.substring(0, 1).toUpperCase() + input.substring(1);
            } else {
                return input;
            }
        };
    });

    app.filter("num", function () {
        return function (input) {
            return parseInt(input, 10);
        };
    });

    app.filter("replacespacios", function () {
        return function (input) {
            if (input != null) {
                input = input.replace(/\s/g, "-");
                return input;
            }
        };
    });
    app.directive("format", [
        "$filter",
        function ($filter) {
            return {
                require: "?ngModel",
                link: function (scope, elem, attrs, ctrl) {
                    if (!ctrl) return;

                    ctrl.$formatters.unshift(function (a) {
                        return $filter(attrs.format)(ctrl.$modelValue);
                    });

                    elem.bind("blur", function (event) {
                        var plainNumber = elem
                        .val()
                        .replace(/[^\d|\-+|\.+]/g, "");
                        elem.val($filter(attrs.format)(plainNumber));
                    });
                },
            };
        },
    ]);

    app.directive('formatC', ['$filter', function ($filter) {
        return {
            require: '?ngModel',
            link: function (scope, elem, attrs, ctrl) {
                if (!ctrl) return;

                ctrl.$formatters.unshift(function (a) {
                    return $filter(attrs.format)(ctrl.$modelValue)
                });

                elem.bind('blur', function (event) {
                    var plainNumber = elem.val().replace(/[^\d|\-+|\.+]/g, '');
                    elem.val($filter(attrs.format)(plainNumber));
                });
            }
        };
    }]);

    app.directive("repeatEnd", function () {
        return {
            restrict: "A",
            link: function (scope, element, attrs) {
                if (scope.$last) {
                    scope.$eval(attrs.repeatEnd);
                }
            },
        };
    });

    app.directive("lightGallery", function () {
        return {
            restrict: "A",
            scope: {
                galleryOptions: "=",
            },
            controller: function ($scope) {
                this.initGallery = function () {
                    $scope.elem.lightGallery($scope.galleryOptions);
                };
            },
            link: function (scope, element, attr) {
                scope.elem = element;
            },
        };
    });

    app.directive("lightGalleryItem", function () {
        return {
            restrict: "A",
            require: "^lightGallery",
            link: function (scope, element, attrs, ctrl) {
                if (scope.$last) {
                    ctrl.initGallery();
                }
            },
        };
    });

    // factpry para inyeccion de dependencias
    app.factory("generalfactory", function ($timeout) {
        var generalfactory = function () { };

        generalfactory.openModal = function (inmueble) {
            generalfactory.inmuebleActual = inmueble;
            $timeout(function () {
                $(".forcedgallery .gp").jqPhotoSwipe({
                    forceSingleGallery: true,
                });
                console.log(inmueble);
            }, 1000);
            console.log(inmueble);
        };

        generalfactory.modalPortales = function (codigo, id) {
            generalfactory.inmueblePortales = {};
            generalfactory.inmueblePortales.id = id;
            generalfactory.inmueblePortales.codigo = codigo;
            $("#portales-cargando").hide();
            $("#portales-exito").hide();
        };

        generalfactory.previousPage = function () {
            console.log("anterior");
            window.history.back();
        };

        return generalfactory;
    });

    //Metodos relacionados con los arriendos
    app.controller(
        "Arriendos",
        function ($scope, generalfactory, $http, $filter) {
            $scope.generalfactory = generalfactory;
            $scope.codeudores = [1];
            $scope.cantidad_codeudores = 1;
            $scope.ocupantes = [1];
            $scope.cantidad_ocupantes = 1;
            $scope.ocupantes_array = {};
            $scope.codeudores_array = {};

            $scope.modalCorreos = function (correos) {
                generalfactory.correosArriendo = correos;
                var aprobaciones = [];

                correos.forEach((item, i) => {
                    if (item.aprobado == 1) {
                        aprobaciones.push(item);
                    }
                });

                generalfactory.aprobacionesObtenidas = aprobaciones;
            }

            $scope.cancelarArriendo = function (id) {
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Cancelar arriendo',
                    text: "¿Estás seguro que deseas cancelar el proceso de arriendo?",
                    icon: 'warning',
                    showCancelButton: true,
                    showLoaderOnConfirm: true,
                    confirmButtonText: 'Si, terminar proceso',
                    cancelButtonText: 'No, cancelar',
                    reverseButtons: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        swalWithBootstrapButtons.fire({
                            allowOutsideClick: false,
                            title: 'Eliminando...',
                            showCancelButton: false,
                            showConfirmButton: false,
                            html:
                            '<img src="/assets/img/loading.gif">',
                        })
                        $http
                        .get("/admin/arriendos/cerrar/" + id)
                        .then(function (response) {
                            Swal.close()
                            window.location.href =
                            "/admin/arriendos/lista";
                        })
                        .catch(function (response) {
                            Swal.close();
                        });
                    }
                })
            }

            $scope.addCodeudor = function () {
                if ($scope.cantidad_codeudores == 5) {
                    alert("Solo pueden ser un maximo de 5 codeudores");
                } else {
                    $scope.cantidad_codeudores++;
                    $scope.codeudores.push($scope.cantidad_codeudores);
                }
            };
            $scope.removeCodeudor = function () {
                $scope.cantidad_codeudores--;
                $scope.codeudores.splice(-1);
            };
            $scope.removeOcupante = function () {
                $scope.cantidad_ocupantes--;
                $scope.ocupantes.splice(-1);
            };
            $scope.addOcupante = function () {
                if ($scope.cantidad_codeudores == 10) {
                    alert("Solo pueden ser un maximo de 10 ocupantes");
                } else {
                    $scope.cantidad_ocupantes++;
                    $scope.ocupantes.push($scope.cantidad_ocupantes);
                }
            };
            $scope.cambiarArriendo = function () {
                var pre_total = parseInt($scope.replaceAll0($scope.canon), 10) + parseInt($scope.replaceAll0($scope.admon), 10);
                $scope.total_alquiler = $filter("currency")(
                    (pre_total).toString(), '', '0');
                }

                $scope.replaceAll0 = function (text) {
                    while (text.toString().indexOf(",") != -1)
                    text = text.toString().replace(",", "");
                    while (text.toString().indexOf(".") != -1)
                    text = text.toString().replace(".", "");
                    text = text.toString().replace("$", "");
                    if (text == "") {
                        return "0";
                    } else {
                        return text;
                    }
                };

                $scope.setFecha = function (variable, valor) {
                    var tempDate = new Date(valor);
                    tempDate.setSeconds(0, 0);
                    $scope[variable] = tempDate;
                }

                $scope.initOcupantes = function (array) {
                    $scope.ocupantes = [];
                    $scope.cantidad_ocupantes = array.length;
                    angular.forEach(array, function (value, key) {
                        $scope.ocupantes.push(key + 1);
                        $scope.ocupantes_array[key + 1] = {};
                        $scope.ocupantes_array[key + 1].doc = value.cedula;
                        $scope.ocupantes_array[key + 1].nombre = value.nombre;
                        $scope.ocupantes_array[key + 1].id = value.id;

                    });
                };

                $scope.initCodeudores = function (array) {
                    $scope.codeudores = [];
                    $scope.cantidad_codeudores = array.length;
                    angular.forEach(array, function (value, key) {
                        $scope.codeudores.push(key + 1);
                        $scope.codeudores_array[key + 1] = {};
                        $scope.codeudores_array[key + 1].doc = value.cedula;
                        $scope.codeudores_array[key + 1].nombre = value.nombre;
                        $scope.codeudores_array[key + 1].direccion = value.direccion;
                        $scope.codeudores_array[key + 1].email = value.email;
                        $scope.codeudores_array[key + 1].celular = value.celular;
                        $scope.codeudores_array[key + 1].id = value.id;
                        $scope.codeudores_array[key + 1].url_doc = value.url_cedula;
                        $scope.codeudores_array[key + 1].copropiedad = value.copropiedad;
                        $scope.codeudores_array[key + 1].direccion_comercial = value.direccion_comercial;

                    });
                };
            }
        );
        // Metodos relacionados con las ventas
        app.controller("Ventas", function ($scope, generalfactory, $http, $filter, $timeout) {
            $scope.generalfactory = generalfactory;
            $scope.total = 0;
            $scope.pagos = 0;
            $scope.vendedores = [1];
            $scope.cantidad_vendedores = 1;
            $scope.vendedores_array = {};
            $scope.compradores = [1];
            $scope.cantidad_compradores = 1;
            $scope.compradores_array = {};
            $scope.pagosTotales = [1];
            $scope.cantidad_pagos = 1;
            $scope.pagos_array = {};

            $scope.replaceAll0 = function (text) {
                while (text.toString().indexOf(",") != -1)
                text = text.toString().replace(",", "");
                while (text.toString().indexOf(".") != -1)
                text = text.toString().replace(".", "");
                text = text.toString().replace("$", "");
                if (text == "") {
                    return "0";
                } else {
                    return text;
                }
            };

            $scope.updateSaldoPendiente = function () {
                $scope.total = parseInt(
                    $scope.replaceAll0($scope.negociacion_precio),
                    10
                );

                var pagos = 0;

                for (var i = 1; i <= 8; i++) {
                    if (typeof $scope.pagos_array[i].valor_pago !== "undefined" && $scope.pagos_array[i].valor_pago != 0 && $scope.pagos_array[i].valor_pago != null) {
                        pagos = pagos + parseInt($scope.replaceAll0($scope.pagos_array[i].valor_pago), 10);
                    }
                }

                $scope.pagos = pagos;

                // console.log($scope.total);
                console.log(pagos);
                $scope.negociacion_saldo_pendiente = $filter("currency") (
                    ($scope.total - $scope.pagos).toString()
                );
                $scope.negociacion_number = $scope.total - $scope.pagos;
                $scope.negocionVal = $scope.negociacion_number < 0 ? true : false;
            };

            $scope.detalleVentas = function (venta) {
                generalfactory.ventaDetail = venta;
                $timeout(function () {
                    $(".forcedgallery .gp").jqPhotoSwipe({
                        forceSingleGallery: true,
                    });
                }, 1000);
                console.log(venta);
            };

            $scope.getallUsers = function ($user) {
                $http
                .get("/alt/allUsers")
                .then(function (response) {
                    $scope.usuarios_encargado = response.data;
                    $scope.usuario_encargado = $user;
                })
                .catch(function (response) {
                    console.log(response);
                });
            };

            $scope.getSeguimientosList = function (id) {
                $http
                .get("/admin/ventas/seguimiento/lista/" + id)
                .then(function (response) {
                    $scope.listaSeguimientos = response.data;
                })
                .catch(function (response) {
                    console.log(response);
                });
            };

            $scope.getEstadosSeguimientos = function () {
                $http
                .get("/admin/ventas/seguimiento/listaEstados")
                .then(function (response) {
                    $scope.listaEstadosSeguimientos = response.data;
                })
                .catch(function (response) {
                    console.log(response);
                });
            };

            $scope.addVendedor = function () {
                if ($scope.cantidad_vendedores == 5) {
                    alert("Solo pueden ser un maximo de 5 vendedores");
                } else {
                    $scope.cantidad_vendedores++;
                    $scope.vendedores.push($scope.cantidad_vendedores);
                }
            };

            $scope.removeVendedor = function () {
                $scope.cantidad_vendedores--;
                $scope.vendedores.splice(-1);
            };

            $scope.initVendedores = function (array) {
                $scope.vendedores = [];
                $scope.cantidad_vendedores = array.length;
                angular.forEach(array, function (value, key) {
                    $scope.vendedores.push(key + 1);
                    $scope.vendedores_array[key + 1] = {};
                    $scope.vendedores_array[key + 1].id = value.id;
                    $scope.vendedores_array[key + 1].ciudad = value.ciudad;
                    $scope.vendedores_array[key + 1].tipo_documento = value.tipo_documento_id;

                    if (value.nombres != null) {
                        $scope.vendedores_array[key + 1].nombre = value.nombres + " " + value.apellidos;
                    }

                    if (value.nombre != null) {
                        $scope.vendedores_array[key + 1].nombre = value.nombre;
                    }

                    if (value.documento != null) {
                        $scope.vendedores_array[key + 1].doc = value.documento;
                    }

                    if (value.numero_documento != null) {
                        $scope.vendedores_array[key + 1].doc = value.numero_documento;
                    }

                    if (value.expedicion != null) {
                        $scope.vendedores_array[key + 1].expedicion = value.expedicion;
                    }

                    if (value.lugar_expedicion_documento != null) {
                        $scope.vendedores_array[key + 1].expedicion = value.lugar_expedicion_documento;
                    }

                    $scope.vendedores_array[key + 1].estado_civil = value.estado_civil;
                    $scope.vendedores_array[key + 1].subestado_civil_id = value.subestado_civil_id;
                    $scope.vendedores_array[key + 1].telefono = value.telefono;
                    $scope.vendedores_array[key + 1].celular = value.celular;
                    $scope.vendedores_array[key + 1].email = value.email;
                    $scope.vendedores_array[key + 1].direccion = value.direccion;
                    $scope.vendedores_array[key + 1].url_documento = value.url_documento;

                    $scope.vendedores_array[key + 1].apoderado = value.apoderados != null ? "apoderado" : "vendedor";

                    if (value.apoderados != null) {
                        $scope.vendedores_array[key + 1].apoderados = value.apoderados;
                    }

                });
            };

            $scope.addCompradores = function () {
                if ($scope.cantidad_compradores == 5) {
                    alert("Solo pueden ser un maximo de 5 compradores");
                } else {
                    $scope.cantidad_compradores++;
                    $scope.compradores.push($scope.cantidad_compradores);
                }
            };

            $scope.removeCompradores = function () {
                $scope.cantidad_compradores--;
                $scope.compradores.splice(-1);
            };

            $scope.initCompradores = function (array) {
                $scope.compradores = [];
                $scope.cantidad_compradores = array.length;
                angular.forEach(array, function (value, key) {
                    $scope.compradores.push(key + 1);
                    $scope.compradores_array[key + 1] = {};
                    $scope.compradores_array[key + 1].id = value.id;
                    $scope.compradores_array[key + 1].ciudad = value.ciudad;
                    $scope.compradores_array[key + 1].tipo_documento = value.tipo_documento_id;
                    $scope.compradores_array[key + 1].nombre = value.nombre;
                    $scope.compradores_array[key + 1].doc = value.numero_documento;
                    $scope.compradores_array[key + 1].expedicion = value.lugar_expedicion_documento;
                    $scope.compradores_array[key + 1].estado_civil = value.estado_civil;
                    $scope.compradores_array[key + 1].subestado_civil_id = value.subestado_civil_id;
                    $scope.compradores_array[key + 1].telefono = value.telefono;
                    $scope.compradores_array[key + 1].celular = value.celular;
                    $scope.compradores_array[key + 1].email = value.email;
                    $scope.compradores_array[key + 1].direccion = value.direccion;
                    $scope.compradores_array[key + 1].url_documento = value.url_documento;

                    $scope.compradores_array[key + 1].apoderado = value.apoderados != null ? "apoderado" : "vendedor";

                    if (value.apoderados != null) {
                        $scope.compradores_array[key + 1].apoderados = value.apoderados;
                    }

                });
            };

            $scope.addPago = function () {
                if ($scope.cantidad_pagos == 8) {
                    alert("Solo pueden ser un maximo de 8 pagos");
                } else {
                    $scope.cantidad_pagos++;
                    $scope.pagosTotales.push($scope.cantidad_pagos);
                }
            };

            $scope.removePago = function () {
                $scope.cantidad_pagos--;
                $scope.pagosTotales.splice(-1);
            };

            $scope.initPagos = function (array) {
                $scope.pagosTotales = [];

                if (array.pago_1_valor_pago != "" && array.pago_1_valor_pago != null) {
                    $scope.pagosTotales.push(1);
                }

                if (array.pago_2_valor_pago != "" && array.pago_2_valor_pago != null) {
                    $scope.pagosTotales.push(2);
                }

                if (array.pago_3_valor_pago != "" && array.pago_3_valor_pago != null) {
                    $scope.pagosTotales.push(3);
                }

                if (array.pago_4_valor_pago != "" && array.pago_4_valor_pago != null) {
                    $scope.pagosTotales.push(4);
                }

                if (array.pago_5_valor_pago != "" && array.pago_5_valor_pago != null) {
                    $scope.pagosTotales.push(5);
                }

                if (array.pago_6_valor_pago != "" && array.pago_6_valor_pago != null) {
                    $scope.pagosTotales.push(6);
                }

                if (array.pago_7_valor_pago != "" && array.pago_7_valor_pago != null) {
                    $scope.pagosTotales.push(7);
                }

                if (array.pago_8_valor_pago != "" && array.pago_8_valor_pago != null) {
                    $scope.pagosTotales.push(8);
                }

                $scope.cantidad_pagos = $scope.pagosTotales.length;

                $scope.pagos_array[1] = {
                    cheque_transferencia: array?.pago_1_cheque_transferencia,
                    fecha_pago: array.pago_1_fecha_pago,
                    medio_pago: array.pago_1_medio_pago,
                    notaria: array.pago_1_notaria,
                    origen_recurso: array.pago_1_origen_recurso,
                    valor_pago: array.pago_1_valor_pago ? array.pago_1_valor_pago.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",") : null
                };

                $scope.pagos_array[2] = {
                    cheque_transferencia: array?.pago_2_cheque_transferencia,
                    fecha_pago: array.pago_2_fecha_pago,
                    medio_pago: array.pago_2_medio_pago,
                    origen_recurso: array.pago_2_origen_recurso,
                    valor_pago: array.pago_2_valor_pago ? array.pago_2_valor_pago.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",") : null,
                    pago_favor: array.pago_2_pago_favor
                };

                $scope.pagos_array[3] = {
                    cheque_transferencia: array?.pago_3_cheque_transferencia,
                    fecha_pago: array.pago_3_fecha_pago,
                    medio_pago: array.pago_3_medio_pago,
                    origen_recurso: array.pago_3_origen_recurso,
                    valor_pago: array.pago_3_valor_pago ? array.pago_3_valor_pago.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",") : null,
                    pago_favor: array.pago_3_pago_favor
                };

                $scope.pagos_array[4] = {
                    cheque_transferencia: array?.pago_4_cheque_transferencia,
                    fecha_pago: array.pago_4_fecha_pago,
                    medio_pago: array.pago_4_medio_pago,
                    origen_recurso: array.pago_4_origen_recurso,
                    valor_pago: array.pago_4_valor_pago ? array.pago_4_valor_pago.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",") : null,
                    pago_favor: array.pago_4_pago_favor
                };

                $scope.pagos_array[5] = {
                    cheque_transferencia: array?.pago_5_cheque_transferencia,
                    fecha_pago: array.pago_5_fecha_pago,
                    medio_pago: array.pago_5_medio_pago,
                    origen_recurso: array.pago_5_origen_recurso,
                    valor_pago: array.pago_5_valor_pago ? array.pago_5_valor_pago.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",") : null,
                    pago_favor: array.pago_5_pago_favor
                };

                $scope.pagos_array[6] = {
                    cheque_transferencia: array?.pago_6_cheque_transferencia,
                    fecha_pago: array.pago_6_fecha_pago,
                    medio_pago: array.pago_6_medio_pago,
                    origen_recurso: array.pago_6_origen_recurso,
                    valor_pago: array.pago_6_valor_pago ? array.pago_4_valor_pago.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",") : null,
                    pago_favor: array.pago_6_pago_favor
                };

                $scope.pagos_array[7] = {
                    cheque_transferencia: array?.pago_7_cheque_transferencia,
                    fecha_pago: array.pago_7_fecha_pago,
                    medio_pago: array.pago_7_medio_pago,
                    origen_recurso: array.pago_7_origen_recurso,
                    valor_pago: array.pago_7_valor_pago ? array.pago_4_valor_pago.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",") : null,
                    pago_favor: array.pago_7_pago_favor
                };

                $scope.pagos_array[8] = {
                    cheque_transferencia: array?.pago_8_cheque_transferencia,
                    fecha_pago: array.pago_8_fecha_pago,
                    medio_pago: array.pago_8_medio_pago,
                    origen_recurso: array.pago_8_origen_recurso,
                    valor_pago: array.pago_8_valor_pago ? array.pago_4_valor_pago.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",") : null,
                    pago_favor: array.pago_8_pago_favor
                };

                console.log($scope.pagos_array);
            };
        });

        // Metodos relacionados con inmuebles
        app.controller("Inmuebles", function ($scope, generalfactory, $http) {
            $scope.generalfactory = generalfactory;

            // metodo de re publicacion en portales
            $scope.portales = function (id, usuario) {
                $("#portales-cargando").show();
                $http
                .post(
                    "/alt/actualizar-portales",
                    { id: id, usuario: usuario },
                    {
                        headers: {
                            Authorization: usuario.token,
                        },
                    }
                )
                .then(function (response) {
                    $("#portales-cargando").hide();
                    $("#portales-exito").show();
                })
                .catch(function (response) {
                    console.log(response);
                });
            };
            // metodo de cosulta detalle de inmueble (pro-detail)

            $scope.detalle = function (code, id) {
                generalfactory.detalleInmueble = {};
                $http
                .get("/admin/mls/getPorpertyDetail/" + code + "/" + id)
                .then(function (response) {
                    console.log(response.data.data);
                    generalfactory.detalleInmueble = response.data.data[0];
                })
                .catch(function (response) {
                    console.log(response);
                });
            };

            $scope.activar = function () {
                $("#lightgallery").unbind().removeData();

                // $timeout after
                $timeout(function () {
                    $("#lightgallery").lightGallery();
                }, 100);
            };
        });

        //Cálculos de precio
        app.controller("Formulario", function ($scope, $http, $timeout) {
            $scope.tipo_inmueble = "";
            $scope.gestion = "";
            $scope.arriendo = 0;
            $scope.administracion = 0;
            $scope.venta = 0;
            $scope.total = 0;
            $scope.estado_inmueble = "";
            $scope.sub_estado_inmueble = "";
            $scope.orden = "";
            $scope.agente_inmuebles = "";

            $("#formlario-cargando").hide();
            $("#formlario-enviado").hide();
            $("#formlario-error").hide();

            $scope.setTotal = function () {
                var arriendo = $scope.arriendo.toString().replace(/,/g, "");
                var administracion = $scope.administracion
                .toString()
                .replace(/,/g, "");
                var venta = $scope.venta.toString().replace(/,/g, "");

                var total = 0;

                if ($scope.arriendo != "" && $scope.arriendo != 0) {
                    total = parseInt(arriendo) + parseInt(administracion);
                } else {
                    total = parseInt(venta);
                }

                $scope.total = total
                .toString()
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
            };

            //Úbicación
            $scope.departamento = "";
            $scope.ciudad = "";
            $scope.zona = "";
            $scope.localidad = "";
            $scope.barrio = "";

            //Ciudades
            $scope.ciudades = function () {
                var departamento = $scope.departamento;

                if ($scope.departamento != "") {
                    $http
                    .get("/alt/ciudades/" + departamento)
                    .then(function (response) {
                        $scope.cities = response.data;
                    })
                    .catch(function (response) {
                        console.log(response);
                    });
                }
            };

            //Localidades
            $scope.localidades = function () {
                var ciudad = $scope.ciudad;
                $http
                .get("/alt/localidades/" + ciudad)
                .then(function (response) {
                    $scope.localities = response.data;
                })
                .catch(function (response) {
                    console.log(response);
                });
            };

            //Barrios
            $scope.barrios = function () {
                var localidad = $scope.localidad;
                $http
                .get("/alt/barrios/" + localidad)
                .then(function (response) {
                    $scope.neighborhoods = response.data;
                })
                .catch(function (response) {
                    console.log(response);
                });
            };

            $scope.barrioName = "";
            $scope.selectBarrio = function () {
                var barrio = $("#barrio_id option:selected")
                .text()
                .toUpperCase()
                .trim()
                .replaceAll(" ", "%20");
                $scope.barrioName = " " + barrio.replaceAll("%20", " ");
            };

            //Agentes
            $scope.sucursal = "";
            $scope.asesores = function () {
                var sucursal = $scope.sucursal;
                $http
                .get("/alt/agentes/" + sucursal)
                .then(function (response) {
                    $scope.agents = response.data;
                })
                .catch(function (response) {
                    console.log(response);
                });
            };

            //Caracteristicas
            $scope.mostrarCaracteristicas = function () {
                var tipo_inmueble = $scope.tipo_inmueble;
                $http
                .get("/alt/caracteristicas/" + tipo_inmueble)
                .then(function (response) {
                    $scope.amenities = response.data;
                })
                .catch(function (response) {
                    console.log(response);
                });
            };

            //Dirección
            $scope.dir1 = "1";
            $scope.dir2 = "";
            $scope.dir3 = "1";
            $scope.dir4 = "1";
            $scope.dir5 = "1";
            $scope.dir6 = "1";
            $scope.dir7 = "";
            $scope.dir8 = "1";
            $scope.dir9 = "";
            $scope.dir10 = "1";
            $scope.complemento = "";
            $scope.complemento2 = "";
            $scope.direccion = "";
            $scope.campodireccion = "";
            $scope.latitude = "";
            $scope.longitude = "";
            $scope.isCompleteAddress = false;

            let map;
            var position = new google.maps.LatLng(
                parseFloat(4.643165),
                parseFloat(-74.105575)
            );

            if ($("#mapa").length) {
                $scope.latitude = "4.643165";
                $scope.longitude = "-74.105575";

                map = new google.maps.Map(document.getElementById("mapa"), {
                    center: {
                        lat: parseFloat(4.643165),
                        lng: parseFloat(-74.105575),
                    },
                    zoom: 12,
                });

                var marker = new google.maps.Marker({
                    position: position,
                    title: "Inmueble",
                    map: map,
                    draggable: true,
                });

                google.maps.event.addListener(marker, "dragend", function (marker) {
                    var latLng = marker.latLng;

                    $("#latitude").val(latLng.lat());
                    $("#longitude").val(latLng.lng());
                });
            }


            $scope.armarDireccion = function (e = null) {
                if (e != null) {
                    e.preventDefault();
                }
                //Utilizando las variables
                var dir1 = $("#dir1 option:selected").text().toUpperCase().trim();
                var dir2 = $scope.dir2;
                var dir3 =
                $("#dir3 option:selected").val() != 1
                ? $("#dir3 option:selected").text().toUpperCase().trim()
                : "";
                var dir4 =
                $("#dir4 option:selected").val() != 1
                ? $("#dir4 option:selected").text().toUpperCase().trim()
                : "";
                var dir5 =
                $("#dir5 option:selected").val() != 1
                ? $("#dir5 option:selected").text().toUpperCase().trim()
                : "";
                var dir6 =
                $("#dir6 option:selected").val() != 1
                ? $("#dir6 option:selected").text().toUpperCase().trim()
                : "";
                var dir7 = $scope.dir7;
                var dir8 =
                $("#dir8 option:selected").val() != 1
                ? $("#dir8 option:selected").text().toUpperCase().trim()
                : "";
                var dir9 = $scope.dir9;
                var dir10 =
                $("#dir10 option:selected").val() != 1
                ? $("#dir10 option:selected").text().toUpperCase().trim()
                : "";
                var direccion = $scope.direccion;

                direccion =
                dir1 +
                " " +
                dir2 +
                " " +
                dir3 +
                " " +
                dir4 +
                " " +
                dir5 +
                " " +
                dir6 +
                " " +
                dir7 +
                " " +
                dir8 +
                " " +
                dir9 +
                " " +
                dir10 +
                " ";

                $scope.direccion = direccion;

                var barrio = $("#barrio_id option:selected")
                .text()
                .toLowerCase()
                .trim()
                .replaceAll(" ", "%20");
                var ciudad = $("#ciudad option:selected")
                .text()
                .toLowerCase()
                .trim()
                .replaceAll(" ", "%20");

                $scope.campodireccion = direccion;

                if (barrio != "seleccione%20barrio") {
                    $scope.campodireccion += " " + barrio.replaceAll("%20", " ");
                }

                if (ciudad != "seleccione%20una%20ciudad") {
                    $scope.campodireccion += " " + ciudad.replaceAll("%20", " ");
                }

                $scope.campodireccion = $scope.campodireccion.trim();
            };

            $scope.buscardireccion = function (e) {
                e.preventDefault();

                var url =
                "https://geocode.arcgis.com" +
                "/arcgis/rest/services/World/GeocodeServer/findAddressCandidates?f=pjson" +
                "&SingleLine=" +
                $scope.campodireccion
                .trim()
                .replaceAll(" ", "%20")
                .replaceAll("#", "") +
                "%20,Colombia" +
                "&outFields=Match_addr%2CAddr_type";

                $http
                .get(url)
                .then(function (response) {
                    var latitud = response.data.candidates[0].location.y;
                    var longitud = response.data.candidates[0].location.x;

                    $scope.latitude = latitud;
                    $scope.longitude = longitud;

                    marker.setMap(null);

                    marker = new google.maps.Marker({
                        position: position,
                        title: $scope.campodireccion,
                        map: map,
                        zoom: 12,
                        draggable: true,
                    });

                    position = new google.maps.LatLng(
                        parseFloat(latitud),
                        parseFloat(longitud)
                    );
                    map.setCenter(position);

                    google.maps.event.addListener(
                        marker,
                        "dragend",
                        function (marker) {
                            var latLng = marker.latLng;

                            $("#latitude").val(latLng.lat());
                            $("#longitude").val(latLng.lng());
                        }
                    );
                })
                .catch(function (response) {
                    console.log(response);
                });
            };

            $scope.submitData = function (url) {
                $("#formlario-cargando").show();
                $("#formlario-enviado").hide();
                $("#formlario-error").hide();

                var formData = $("#formulario-crear-inmueble").serializeArray();

                var data = {};
                $(formData).each(function (index, obj) {
                    data[obj.name] = obj.value;
                });

                $http
                .post(url, data)
                .then(function (response) {
                    console.log(response.data.id);
                    $("#formlario-cargando").hide();
                    $("#formlario-enviado").show();

                    window.location.href =
                    "/admin/inmuebles/crear/" +
                    response.data.id +
                    "/imagenes";
                })
                .catch(function (response) {
                    console.log(response);
                    $("#formlario-cargando").hide();
                    $("#formlario-error").html(response.data.mensaje);
                    $("#formlario-error").show();
                });
            };
        });

        app.controller("DemoController", [
            "$rootScope",
            function ($rootScope) {
                this.gallery = {
                    nature: true,
                    abstracts: true,
                    portraits: true,
                    logos: true,
                };

                this.options1 = {
                    debug: true,
                    baseUrl: "https://images.unsplash.com/",
                    selected: 0,
                    fields: {
                        source: {
                            modal: "link",
                            image: "medium",
                            panel: "thumbnail",
                        },
                    },
                    loadingImage: "preload.svg",
                    preloadNext: true,
                    preloadDelay: 420,
                    autoplay: {
                        enabled: false,
                        delay: 3200,
                    },
                    theme: "darkblue",
                    thumbnail: {
                        height: 64,
                        index: true,
                    },
                    modal: {
                        caption: {
                            visible: true,
                            position: "bottom",
                        },
                        header: {
                            enabled: true,
                            dynamic: false,
                        },
                        transition: "rotateLR",
                        title: "Angular Super Gallery Demo",
                        subtitle: "Nature Wallpapers Full HD",
                        thumbnail: {
                            height: 77,
                            index: true,
                        },
                    },
                    panel: {
                        click: {
                            select: false,
                            modal: true,
                        },
                        hover: {
                            select: true,
                        },
                        items: {
                            class: "",
                        },
                        item: {
                            class: "custom",
                            title: false,
                        },
                    },
                    image: {
                        height: 580,
                        click: {
                            modal: true,
                        },
                        transition: "zlideLR",
                        placeholder: "panel",
                    },
                };

                var imagesBackGroundColor = "black";

                this.files1 = [
                    {
                        source: {
                            modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                        },
                        title: "Tindfjöll, Iceland",
                        description: "Gissur Steinarsson",
                        color: imagesBackGroundColor,
                    },
                    {
                        source: {
                            modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                        },
                        title: "Ky Quan San (Bạch Mộc Lương Tử), Bát Xát, Lào Cai, Việt Nam",
                        description: "Dương Trần Quốc",
                        color: imagesBackGroundColor,
                    },
                    {
                        source: {
                            modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                        },
                        title: "Arhavi, Artvin, Turkey",
                        description: "Emre Öztürk",
                        color: imagesBackGroundColor,
                    },

                    {
                        source: {
                            modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                        },
                        title: "Grand Canyon North Rim",
                        description: "Stephen Walker",
                        color: imagesBackGroundColor,
                    },
                    {
                        source: {
                            modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                        },
                        title: "Paris, France",
                        description: "Nicolas Prieto",
                        color: imagesBackGroundColor,
                    },

                    {
                        source: {
                            modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                        },
                        title: "Ales Krivec",
                        description: "",
                        color: imagesBackGroundColor,
                    },

                    {
                        source: {
                            modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                        },
                        title: "Kluane National Park and Reserve of Canada, Canada",
                        description: "Kalen Emsley",
                        color: imagesBackGroundColor,
                    },

                    {
                        source: {
                            modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                        },
                        title: "Playa de la Misericordia, Spain",
                        description: "Quino Al",
                        color: imagesBackGroundColor,
                    },
                    {
                        source: {
                            modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                        },
                        title: "Francesco Ungaro",
                        description: "",
                        color: imagesBackGroundColor,
                    },
                    {
                        source: {
                            modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                        },
                        title: "Heping Island Park, Keelung City, Taiwan",
                        description: "Andy Wang",
                        color: imagesBackGroundColor,
                    },

                    {
                        source: {
                            modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                        },
                        title: "Sun rays a game only on 3-6 minutes…",
                        description: "Valeriy Andrushko",
                        color: imagesBackGroundColor,
                    },
                ];

                this.add1 = function () {
                    $rootScope.$broadcast("ASG-gallery-edit", {
                        id: "nature",
                        selected: -1,
                        add: [
                            {
                                source: {
                                    modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                                    image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                                    panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                                },
                                title: "Seychelles",
                                description: "Nenad Radojčić",
                                color: imagesBackGroundColor,
                            },
                        ],
                    });
                };

                this.update1options = function () {
                    $rootScope.$broadcast("ASG-gallery-edit", {
                        id: "nature",
                        options: this.options1,
                    });
                };

                this.update1selected = function () {
                    $rootScope.$broadcast("ASG-gallery-edit", {
                        id: "nature",
                        selected: this.options1.selected,
                    });
                };

                this.update1 = function () {
                    var newGalleryImages = [
                        {
                            source: {
                                modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                                image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                                panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            },
                            title: "North Shore, Waialua, United States",
                            description: "Sean O.",
                            color: imagesBackGroundColor,
                        },
                        {
                            source: {
                                modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                                image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                                panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            },
                            title: "Beach, Fuerteventura, Spain",
                            description: "Uwe Jelting",
                            color: imagesBackGroundColor,
                        },
                    ];

                    this.options1.selected = 1;

                    $rootScope.$broadcast("ASG-gallery-edit", {
                        id: "nature",
                        update: newGalleryImages,
                    });
                };

                this.delete1 = function () {
                    $rootScope.$broadcast("ASG-gallery-edit", {
                        id: "nature",
                        delete: null, // index number or null for delete selected image
                    });
                };

                this.reset1 = function () {
                    this.options1 = angular.copy(this.options1Backup);
                    this.files1 = angular.copy(this.files1Backup);

                    $rootScope.$broadcast("ASG-gallery-edit", {
                        id: "nature",
                        selected: 0,
                        update: this.files1,
                        options: this.options1,
                    });
                };

                this.options1Backup = angular.copy(this.options1);
                this.files1Backup = angular.copy(this.files1);

                //-----------------------------

                this.options2 = {
                    debug: false,
                    loadingImage: "preload.svg",
                    preloadNext: true,
                    preloadDelay: 1200,
                    baseUrl: "https://images.unsplash.com/",
                    hashUrl: false,
                    autoplay: {
                        enabled: true,
                        delay: 4200,
                    },
                    theme: "darkred",
                    modal: {
                        title: "",
                        transition: "fadeInOut",
                        transitionSpeed: 1,
                        titleFromImage: true,
                        subtitleFromImage: true,
                        caption: {
                            visible: false,
                            position: "bottom",
                        },
                        thumbnail: {
                            height: 90,
                            index: false,
                            dynamic: true,
                        },
                    },
                    panel: {
                        visible: false,
                        item: {
                            class: "col-md-4 float-left",
                            caption: true,
                            index: true,
                        },
                        click: {
                            select: true,
                            modal: false,
                        },
                    },
                    image: {
                        heightAuto: {
                            initial: true,
                            onresize: true,
                        },
                        transition: "zoomInOut",
                        transitionSpeed: 0.9,
                        placeholder: "panel",
                    },
                };

                this.files2 = [
                    {
                        source: {
                            modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                        },
                        title: "Lucas Benjamin",
                        description: "",
                    },
                    {
                        source: {
                            modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                        },
                        title: "Fairhaven, Bellingham, United States",
                        description: "Adrien Converse",
                    },
                    {
                        source: {
                            modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                        },
                        title: "Red Vein",
                        description: "Lucas Benjamin",
                    },
                    {
                        source: {
                            modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                        },
                        title: "Paweł Czerwiński",
                        description: "",
                    },
                    {
                        source: {
                            modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                        },
                        title: "The Titanic Memorial Garden, Belfast, United Kingdom",
                        description: "Yaniv Knobel",
                    },
                    {
                        source: {
                            modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                        },
                        title: "Paweł Czerwiński",
                        description: "",
                    },
                    {
                        source: {
                            modal: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            image: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                            panel: "https://englishlive.ef.com/es-mx/blog/wp-content/uploads/sites/8/2019/03/partes-de-la-casa-en-ingles.jpg",
                        },
                        title: "Petersen Automotive Museum, Los Angeles, United States",
                        description: "Denys Nevozhai",
                    },
                ];
            },
        ]);

        app.controller(
            "FormImage",
            function ($scope, $http, $timeout, generalfactory) {
                $scope.generalfactory = generalfactory;
                $scope.initDropZone = function (token, url, id) {
                    if (window.document.documentMode) {
                        alert(
                            "This website doesn't work on Internet Explorer, you should use modern browsers like Chrome or Safari instead"
                        );
                    }

                    var myImageUpload = new ImageUpload({
                        imageUploadZoneId: "iu-image-upload-zone",
                        imageGalleryId: "iu-gallery",
                        dragAndDropFeature: false,
                        dictUploadImageNote: "Agregar fotos",

                        //Enviando al api
                        sendRequestToServer: true,
                        headers: { "X-CSRF-TOKEN": token },
                        saveImageRoute: url + "/save-image",
                        imageBelongsTo: { property: id, is_360: false },
                        saveImageOrderRoute: url + "/save-image-order",
                        deleteImageRoute: url + "/delete-image",

                        //Efectos y demás
                        showUploadingLoader: function (
                            imagePlaceholder,
                            showUploadedPercentComplete
                        ) {
                            var progressBar = document.createElement("div");
                            progressBar.className = "iu-progress-bar";
                            var percentBar = document.createElement("div");
                            percentBar.className = "iu-percent-bar";
                            if (showUploadedPercentComplete === true) {
                                percentBar.innerHTML = "0%";
                            }
                            progressBar.appendChild(percentBar);
                            imagePlaceholder.appendChild(progressBar);
                        },

                        updateUploadingLoader: function (
                            percentComplete,
                            imagePlaceholder,
                            showUploadedPercentComplete
                        ) {
                            var percentComplete = Math.floor(percentComplete);
                            var percentBar =
                            imagePlaceholder.getElementsByClassName(
                                "iu-percent-bar"
                            )[0];

                            if (percentBar != null) {
                                percentBar.style.width = percentComplete + "%";
                                if (showUploadedPercentComplete === true) {
                                    percentBar.innerHTML = percentComplete + "%";
                                }
                            }
                        },

                        removeUploadingLoader: function (
                            imagePlaceholder,
                            showUploadedPercentComplete
                        ) {
                            var progressBar =
                            imagePlaceholder.getElementsByClassName(
                                "iu-progress-bar"
                            )[0];
                            var fadeEffect = setInterval(function () {
                                if (!progressBar.style.opacity) {
                                    progressBar.style.opacity = 1;
                                }
                                if (progressBar.style.opacity > 0) {
                                    progressBar.style.opacity -= 0.1;
                                } else {
                                    clearInterval(fadeEffect);
                                    progressBar.remove();
                                }
                            }, 300);
                        },

                        addFlashBox: function (
                            showedAlertString,
                            showedTime,
                            backgroundColor
                        ) {
                            var oldFlashBox =
                            document.getElementsByClassName("iu-flash-box")[0];
                            if (oldFlashBox) {
                                oldFlashBox.remove();
                            }
                            var flashBox = document.createElement("div");
                            flashBox.className = "iu-flash-box";
                            if (backgroundColor) {
                                flashBox.style.backgroundColor = backgroundColor;
                            }
                            flashBox.innerHTML = showedAlertString;
                            document.body.appendChild(flashBox);
                            setTimeout(function () {
                                fadeEffect(flashBox);
                            }, showedTime);

                            function fadeEffect(elmnt) {
                                var fadeEffect = setInterval(function () {
                                    if (!elmnt.style.opacity) {
                                        elmnt.style.opacity = 1;
                                    }
                                    if (elmnt.style.opacity > 0) {
                                        elmnt.style.opacity -= 0.1;
                                    } else {
                                        clearInterval(fadeEffect);
                                        elmnt.remove();
                                    }
                                }, 50);
                            }
                        },
                    });
                };
            }
        );

        app.controller("FormImage360", function ($scope, $http, $timeout) {
            $scope.initDropZone = function (token, url, id) {
                if (window.document.documentMode) {
                    alert(
                        "This website doesn't work on Internet Explorer, you should use modern browsers like Chrome or Safari instead"
                    );
                }

                var myImageUpload = new ImageUpload({
                    imageUploadZoneId: "iu-image-upload-zone-360",
                    imageGalleryId: "iu-gallery-360",
                    dragAndDropFeature: false,
                    dictUploadImageNote: "Agregar fotos 360",

                    //Enviando al api
                    sendRequestToServer: true,
                    headers: { "X-CSRF-TOKEN": token },
                    saveImageRoute: url + "/save-image",
                    imageBelongsTo: { property: id, is_360: true },
                    saveImageOrderRoute: url + "/save-image-order",
                    deleteImageRoute: url + "/delete-image",

                    //Efectos y demás
                    showUploadingLoader: function (
                        imagePlaceholder,
                        showUploadedPercentComplete
                    ) {
                        var progressBar = document.createElement("div");
                        progressBar.className = "iu-progress-bar";
                        var percentBar = document.createElement("div");
                        percentBar.className = "iu-percent-bar";
                        if (showUploadedPercentComplete === true) {
                            percentBar.innerHTML = "0%";
                        }
                        progressBar.appendChild(percentBar);
                        imagePlaceholder.appendChild(progressBar);
                    },

                    updateUploadingLoader: function (
                        percentComplete,
                        imagePlaceholder,
                        showUploadedPercentComplete
                    ) {
                        var percentComplete = Math.floor(percentComplete);
                        var percentBar =
                        imagePlaceholder.getElementsByClassName(
                            "iu-percent-bar"
                        )[0];

                        if (percentBar != null) {
                            percentBar.style.width = percentComplete + "%";
                            if (showUploadedPercentComplete === true) {
                                percentBar.innerHTML = percentComplete + "%";
                            }
                        }
                    },

                    removeUploadingLoader: function (
                        imagePlaceholder,
                        showUploadedPercentComplete
                    ) {
                        var progressBar =
                        imagePlaceholder.getElementsByClassName(
                            "iu-progress-bar"
                        )[0];
                        var fadeEffect = setInterval(function () {
                            if (!progressBar.style.opacity) {
                                progressBar.style.opacity = 1;
                            }
                            if (progressBar.style.opacity > 0) {
                                progressBar.style.opacity -= 0.1;
                            } else {
                                clearInterval(fadeEffect);
                                progressBar.remove();
                            }
                        }, 300);
                    },

                    addFlashBox: function (
                        showedAlertString,
                        showedTime,
                        backgroundColor
                    ) {
                        var oldFlashBox =
                        document.getElementsByClassName("iu-flash-box")[0];
                        if (oldFlashBox) {
                            oldFlashBox.remove();
                        }
                        var flashBox = document.createElement("div");
                        flashBox.className = "iu-flash-box";
                        if (backgroundColor) {
                            flashBox.style.backgroundColor = backgroundColor;
                        }
                        flashBox.innerHTML = showedAlertString;
                        document.body.appendChild(flashBox);
                        setTimeout(function () {
                            fadeEffect(flashBox);
                        }, showedTime);

                        function fadeEffect(elmnt) {
                            var fadeEffect = setInterval(function () {
                                if (!elmnt.style.opacity) {
                                    elmnt.style.opacity = 1;
                                }
                                if (elmnt.style.opacity > 0) {
                                    elmnt.style.opacity -= 0.1;
                                } else {
                                    clearInterval(fadeEffect);
                                    elmnt.remove();
                                }
                            }, 50);
                        }
                    },
                });
            };
        });

        app.controller(
            "Propietarios",
            function ($scope, $http, $timeout, generalfactory) {
                $("#formlario-cargando").hide();
                $("#formlario-enviado").hide();
                $("#formlario-error").hide();
                $("#formulario-cargando").hide();
                $("#formulario-enviado").hide();
                $("#formulario-error").hide();

                $scope.generalfactory = generalfactory;

                $scope.propietarios = [];

                $scope.nombre = "";

                $scope.lista = function (token, pagina = 1, event = null) {
                    if (event != null) {
                        event.preventDefault();
                    }
                    var nombre = $scope.nombre;
                    $http
                    .get("/api/clientes?name=" + nombre + "&page=" + pagina, {
                        headers: {
                            Authorization: token,
                        },
                    })
                    .then(function (response) {
                        $scope.contactos = response.data;
                    })
                    .catch(function (response) {
                        console.log(response);
                    });
                };

                $scope.agregarPropietario = function (propietario, event = null) {
                    if (event != null) {
                        event.preventDefault();
                    }

                    if (
                        $scope.propietarios.length > 0 &&
                        $scope.propietarios[0] != null
                    ) {
                        var hasMatch = false;

                        for (var i = 0; i < $scope.propietarios.length; i++) {
                            var item = $scope.propietarios[i];
                            if (item != null && item.id == propietario.id) {
                                hasMatch = true;
                                break;
                            }

                            if (item == null) {
                                delete $scope.propietarios[i];
                            }
                        }

                        if (!hasMatch) {
                            $scope.propietarios.push(propietario);
                        }
                    } else {
                        $scope.propietarios.push(propietario);
                    }
                };

                $scope.eliminarPropietario = function (index, event = null) {
                    if (event != null) {
                        event.preventDefault();
                    }

                    $scope.propietarios.splice(index, 1);
                };

                $scope.agregarPropietariosYaExistentes = function (propietarios) {
                    owners = JSON.parse(propietarios);

                    owners.forEach((propietario, i) => {
                        $scope.propietarios.push(propietario);
                    });
                };

                $scope.creatPropietario = function (url, token) {
                    $("#formlario-cargando").show();
                    $("#formlario-enviado").hide();
                    $("#formlario-error").hide();

                    var formData = $(
                        "#formulario-crear-propietario"
                    ).serializeArray();

                    var data = {
                        archivo_documento: $("#archivo_documento").prop("files")[0],
                    };

                    $(formData).each(function (index, obj) {
                        if (obj.name != "archivo_documento") {
                            data[obj.name] = obj.value;
                        }
                    });

                    var configs = {
                        headers: {
                            "Content-Type": undefined,
                            Authorization: token,
                        },
                        transformRequest: function (data) {
                            var formData = new FormData();
                            angular.forEach(data, function (value, key) {
                                formData.append(key, value);
                            });
                            return formData;
                        },
                    };

                    $http
                    .post(url, data, configs)
                    .then(function (response) {
                        console.log(response);
                        $scope.propietarios.push(response.data);

                        $scope.lista(token);

                        $("#formlario-cargando").hide();
                        $("#formlario-enviado").show();
                    })
                    .catch(function (response) {
                        console.log(response);
                        $("#formlario-cargando").hide();
                        $("#formlario-error").html(
                            JSON.stringify(response.data)
                        );
                        $("#formlario-error").show();
                    });
                };

                $scope.asociarPropietarios = function (url, token) {
                    $("#formulario-cargando").show();
                    $("#formulario-enviado").hide();
                    $("#formulario-error").hide();

                    var formData = $(
                        "#formulario-asociar-propietario"
                    ).serializeArray();

                    var data = {};
                    $(formData).each(function (index, obj) {
                        data[obj.name] = obj.value;
                    });

                    $http
                    .post(url, data, {
                        headers: {
                            Authorization: token,
                        },
                    })
                    .then(function (response) {
                        console.log(response);

                        window.location.href =
                        "/admin/inmuebles/crear/" +
                        response.data[0].inmueble_id +
                        "/documentos";

                        $("#formulario-cargando").hide();
                        $("#formulario-enviado").show();
                    })
                    .catch(function (response) {
                        console.log(response);
                        $("#formulario-cargando").hide();
                        $("#formulario-error").html(
                            JSON.stringify(response.data)
                        );
                        $("#formulario-error").show();
                    });
                };
            }
        );

        app.controller("FormDocumentos", function ($scope, $http, $timeout) {
            $scope.file_1 = "";
            $scope.file_2 = "";
            $scope.file_3 = "";
            $scope.file_4 = "";
            $scope.file_5 = "";

            $scope.uploadDocusign = function () {
                $scope.file_5 = "lleno";
            };
        });

        app.controller("FormSeguimiento", function ($scope, $http, $timeout) {
            //Estados de inmueble
            $scope.estado = "";

            //Sub estados de inmueble
            $scope.subestadosList = function () {
                var estado = $scope.estado;

                if ($scope.estado != "") {
                    $http
                    .get("/alt/sub-estados/" + estado)
                    .then(function (response) {
                        $scope.subestados = response.data;
                    })
                    .catch(function (response) {
                        console.log(response);
                    });
                }
            };
        });

        app.controller(
            "PublicarInmuebleController",
            function ($scope, $http, $timeout) {
                $("#inmueble-publicado").addClass("hidden");
                $("#publicado-error").addClass("hidden");

                $scope.publicar = function (url, token) {
                    $http
                    .get(url, {
                        headers: {
                            Authorization: token,
                        },
                    })
                    .then(function (response) {
                        console.log(response);

                        $("#publicado-error").addClass("hidden");
                        $("#inmueble-publicado").removeClass("hidden");
                    })
                    .catch(function (response) {
                        console.log(response);
                        var respuesta = JSON.stringify(
                            response.data
                        ).replaceAll('"', "");
                        respuesta = respuesta.replaceAll("{", "");
                        respuesta = respuesta.replaceAll("}", "");
                        respuesta = respuesta.replaceAll("[", "");
                        respuesta = respuesta.replaceAll("]", "");
                        respuesta = respuesta.replaceAll(",", "<br />");
                        respuesta = respuesta.replaceAll(":", " -> ");

                        $("#inmueble-publicado").addClass("hidden");
                        $("#publicado-error").html(respuesta);
                        $("#publicado-error").removeClass("hidden");
                    });
                };
            }
        );

        app.controller("MapaLista", function ($scope, $http, $timeout) {
            $scope.lista = function (filtro, token) {
                filtro = JSON.parse(filtro);

                $.each(filtro, function (key, value) {
                    if (value === "" || value === null) {
                        delete filtro[key];
                    }
                });

                //Consultando el API interna
                $http
                .get(
                    "/api/inmuebles?" + new URLSearchParams(filtro).toString(),
                    {
                        headers: {
                            Authorization: token,
                        },
                    }
                )
                .then(function (response) {
                    var filtrin = response.data.data;

                    markers = [];

                    if (filtrin.length != 0) {
                        var lati = filtrin[0].latitud;
                        var long = filtrin[0].longitud;
                    } else {
                        var lati = 4.704622;
                        var long = -74.052306;
                    }

                    var uluru = {
                        lat: parseFloat(lati),
                        lng: parseFloat(long),
                    };
                    var mapa = new google.maps.Map(
                        document.getElementById("mapaLista"),
                        {
                            zoom: 12,
                            center: uluru,
                        }
                    );

                    for (i in filtrin) {
                        var latx = filtrin[i].latitud;
                        var longx = filtrin[i].longitud;

                        var latitude = parseFloat(latx);
                        var longitude = parseFloat(longx);

                        var uluru = { lat: latitude, lng: longitude };

                        var infowindow = new google.maps.InfoWindow({
                            content: "",
                            maxWidth: 500,
                        });

                        var image = {
                            url: "/assets/img/map.png",
                            size: new google.maps.Size(30, 30),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(30, 30),
                            scaledSize: new google.maps.Size(30, 30),
                        };

                        var marker = new google.maps.Marker({
                            position: uluru,
                            map: mapa,
                            title: "Inmueble",
                            // icon: image,
                            codigo: filtrin[i].codigo,
                            id: filtrin[i].id,
                        });

                        markers.push(marker);

                        google.maps.event.addListener(
                            marker,
                            "click",
                            (function (marker, infowindow) {
                                return function () {
                                    mapa.setCenter(marker.getPosition());
                                    infowindow.setContent(
                                        "Espere un momento..."
                                    );
                                    infowindow.open(mapa, marker);

                                    var respuesta = filtrin[i];

                                    // var redir = "/inmuebles/" + respuesta.tipos_inmueble.nombre.toLowerCase() + "-en-" + respuesta.gestiones.nombre.toLowerCase() + "-en-" + respuesta.barrio_comun.toLowerCase() + "/" + respuesta.codigo + "/" + respuesta.id;
                                    var redir = "#modalForm" + respuesta.id;
                                    var imagen =
                                    respuesta.imagenes.length > 0
                                    ? respuesta.imagenes[0].url
                                    : "/assets/img/logo/logo.png";
                                    var urlinspacios = redir.replace(
                                        /\s/g,
                                        "-"
                                    );

                                    var contentString =
                                    '<div id="content" class="map-content">' +
                                    '<div class="row">' +
                                    '<div class="col-md-6">' +
                                    '<a href="' +
                                    urlinspacios +
                                    '" class="modal-with-form">' +
                                    '<img src="' +
                                    imagen +
                                    '" alt="" style="width: 100%; height: 150px; object-fit: cover;" />' +
                                    "</a>" +
                                    "</div>" +
                                    '<div class="col-md-6">' +
                                    '<strong class="capitalized">' +
                                    respuesta.tipos_inmueble.nombre.toLowerCase() +
                                    " en " +
                                    respuesta.gestiones.nombre.toLowerCase() +
                                    " en " +
                                    respuesta.barrio_comun.toLowerCase() +
                                    "</strong>" +
                                    "<p> Precio " +
                                    respuesta.price_format +
                                    "<br />" +
                                    "Ciudad " +
                                    respuesta.ciudades.nombre +
                                    "<br />" +
                                    "Área " +
                                    respuesta.area_construida +
                                    "</p>" +
                                    '<a role="button" href="' +
                                    urlinspacios +
                                    '" class="btn btn-default modal-with-form">Ver más</a>' +
                                    "</div>" +
                                    "</div>" +
                                    "</div>" +
                                    "</div>";

                                    // $("#modalForm" + respuesta.id).modal("show");

                                    infowindow.setContent(contentString);
                                    infowindow.open(mapa, marker);
                                };
                            })(marker, infowindow)
                        );

                        google.maps.event.addListener(
                            mapa,
                            "click",
                            (function (infowindow) {
                                return function () {
                                    infowindow.close();
                                };
                            })(infowindow)
                        );
                    }

                    var markerCluster = new MarkerClusterer(mapa, markers, {
                        imagePath:
                        "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m",
                    });
                })
                .catch(function (response) {
                    console.log(response);
                });
            };
        });

        app.controller("HacerSeguimientos", function ($scope, $http, $timeout) {
            $scope.openModal = function (inmueble, estado, subestado, mensaje, usuario) {
                var dialog = confirm(mensaje);

                if (dialog) {
                    $scope.hacerSeguimiento(inmueble, estado, subestado, usuario);
                } else {
                    console.log("No se realizó la operación");
                }
            };

            $scope.hacerSeguimiento = function (inmueble, estado, subestado, usuario) {
                var datos = {
                    property: inmueble,
                    status: estado,
                    substatus: subestado,
                    description: "Seguimiento de sistema realizado por " + usuario.nombres + " " + usuario.apellidos
                };

                $("#seguimiento-cargando").removeClass("hidden");

                $http.post("/api/seguimientos", datos, {
                    headers: {
                        Authorization: usuario.token,
                    },
                }
            )
            .then(function (response) {
                $("#seguimiento-cargando").addClass("hidden");
                $("#seguimiento-exito").removeClass("hidden");

                setTimeout("location.reload(true);", 500);
            })
            .catch(function (response) {
                console.log(response);
            });
        };
    });
})();
