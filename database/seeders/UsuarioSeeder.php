<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Imports\UsuarioImport;
use Maatwebsite\Excel\Facades\Excel;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new UsuarioImport, "usuarios.xlsx");
    }
}
