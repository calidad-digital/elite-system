<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TutorialSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        DB::table("tutorials")->insert([
            "url" => "https://youtu.be/uTyowEeJe5k",
            "icono" => "youtube",
            "titulo" => "Ingeso a Elite System",
            "subtitulo" => "Inmuebles"
        ]);
        
        DB::table("tutorials")->insert([
            "url" => "https://youtu.be/pr0Jj30pt6Q",
            "icono" => "youtube",
            "titulo" => "Subir inmueble",
            "subtitulo" => "Inmuebles"
        ]);

        DB::table("tutorials")->insert([
            "url" => "https://youtu.be/ejRKXfTFOiY",
            "icono" => "youtube",
            "titulo" => "Cambios básicos",
            "subtitulo" => "Documentos"
        ]);

        DB::table("tutorials")->insert([
            "url" => "https://youtu.be/G8c4X0wz4ZE",
            "icono" => "youtube",
            "titulo" => "Cierre de Ventas",
            "subtitulo" => "Documentos"
        ]);

        DB::table("tutorials")->insert([
            "url" => "https://youtu.be/WgucVtAYxcs",
            "icono" => "youtube",
            "titulo" => "Wetrasnfer y Fotos",
            "subtitulo" => "Fotos y archivos"
        ]);

        DB::table("tutorials")->insert([
            "url" => "https://www.youtube.com/watch?v=CYekWguyzs8",
            "icono" => "youtube",
            "titulo" => "Toma de fotos - Afydi",
            "subtitulo" => "Documentos"
        ]);

        DB::table("tutorials")->insert([
            "url" => "https://youtu.be/6GwfDDj8JA4",
            "icono" => "youtube",
            "titulo" => "Benchmarketing de Afydi",
            "subtitulo" => "Documentos"
        ]);

        DB::table("tutorials")->insert([
            "url" => "https://youtu.be/qpMJ3pnfxfY",
            "icono" => "youtube",
            "titulo" => "Tutorial Actualizar inmuebles",
            "subtitulo" => "Documentos"
        ]);
    }
}
