<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Imports\SubEstadoInmuebleImport;
use Maatwebsite\Excel\Facades\Excel;

class SubEstadoInmuebleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new SubEstadoInmuebleImport, "subestado_inm.xlsx");
    }
}
