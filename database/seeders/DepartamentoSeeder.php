<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Imports\DepartamentoImport;
use Maatwebsite\Excel\Facades\Excel;

class DepartamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new DepartamentoImport, "departamento_empresa.xlsx");
    }
}
