<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Imports\RolImport;
use Maatwebsite\Excel\Facades\Excel;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new RolImport, "roles.xlsx");
    }
}
