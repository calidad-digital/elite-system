<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Imports\EstadoUbicacionImport;
use Maatwebsite\Excel\Facades\Excel;

class EstadoUbicacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new EstadoUbicacionImport, "departamentos.xlsx");
    }
}
