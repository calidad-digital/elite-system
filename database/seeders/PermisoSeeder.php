<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Imports\PermisoImport;
use Maatwebsite\Excel\Facades\Excel;

class PermisoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new PermisoImport, "permisos.xlsx");
    }
}
