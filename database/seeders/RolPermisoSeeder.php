<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Imports\RolPermisoImport;
use Maatwebsite\Excel\Facades\Excel;

class RolPermisoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new RolPermisoImport, "rol_permisos.xlsx");
    }
}
