<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubestadoCivil extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // php artisan db:seed --class=SubestadoCivil
       DB::table('subestado_civiles')->truncate();

        DB::table('subestado_civiles')->insert([
            'id' => '1',
            'nombre' => 'Sin unión marital de hecho',
            'descripcion' => '',
            'estado_civil' => 'soltero',
        ]);
        DB::table('subestado_civiles')->insert([
            'id' => '2',
            'nombre' => 'Con unión marital de hecho',
            'descripcion' => '',
            'estado_civil' => 'soltero',
        ]);
        DB::table('subestado_civiles')->insert([
            'id' => '3',
            'nombre' => 'Con sociedad conyugal vigente',
            'descripcion' => '',
            'estado_civil' => 'casado',
        ]);
        DB::table('subestado_civiles')->insert([
            'id' => '4',
            'nombre' => 'Con sociedad conyugal disuelta y liquidada',
            'descripcion' => '',
            'estado_civil' => 'casado',
        ]);
    }
}