<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PreliminarySeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {

        //1. Estados
        DB::table("estados")->insert([
            "nombre" => "Activo",
            "is_active" => true
        ]);

        DB::table("estados")->insert([
            "nombre" => "Inactivo",
            "is_active" => false
        ]);

        DB::table("estados")->insert([
            "nombre" => "No publicado",
            "is_active" => false
        ]);

        DB::table("estados")->insert([
            "nombre" => "En revisión",
            "is_active" => false
        ]);

        DB::table("estados")->insert([
            "nombre" => "Devuelto",
            "is_active" => false
        ]);

        DB::table("estados")->insert([
            "nombre" => "Publicado",
            "is_active" => true
        ]);

        //2. Empresas
        DB::table("empresas")->insert([
            "nombre" => "Elite inmobiliaria",
            "nit" => "900541111",
            "razon_social" => "INVERSIONES ELITE GROUP SAS",
            "email" => "comercial@eliteinmobiliaria.com.co",
            "telefono1" => "7448205",
            "telefono2" => "7448205",
            "celular" => "",
            "whatsapp" => "",
            "facebook" => "",
            "instagram" => "",
            "twitter" => "",
            "linkedin" => "",
            "pagina_web" => "https://www.eliteinmobiliaria.com.co/",
            "logo" => "",
            "estado_id" => 1
        ]);

        $this->call([
            //3. Roles
            RolSeeder::class,
        ]);

        //4. Módulos
        DB::table("modulos")->insert([
            "nombre" => "Inmuebles",
            "descripcion" => "Creación de inmuebles"
        ]);

        $this->call([
            //5. Permisos
            PermisoSeeder::class,
            //6. Permisos de roles
            RolPermisoSeeder::class,
            //7. Departamentos
            DepartamentoSeeder::class,
        ]);

        //8. Tipos de documento
        DB::table("tipo_documentos")->insert([
            "nombre" => "Cédula"
        ]);

        DB::table("tipo_documentos")->insert([
            "nombre" => "NIT"
        ]);

        DB::table("tipo_documentos")->insert([
            "nombre" => "Cédula de extranjería"
        ]);

        $this->call([
            //9. Estados (departamentos)
            EstadoUbicacionSeeder::class,
            // 10. Ciudades
            CiudadSeeder::class,
        ]);

        //11. Zonas
        DB::table("zonas")->insert([
            "nombre" => "Norte"
        ]);

        DB::table("zonas")->insert([
            "nombre" => "Centro"
        ]);

        DB::table("zonas")->insert([
            "nombre" => "Sur"
        ]);

        DB::table("zonas")->insert([
            "nombre" => "Este"
        ]);

        DB::table("zonas")->insert([
            "nombre" => "Oeste"
        ]);

        $this->call([
            //12. Localidades
            LocalidadSeeder::class,
            //13. Barrios
            BarrioSeeder::class,
            //14. Estratos
            EstratoSeeder::class,
        ]);

        //15. Oficinas
        DB::table("oficinas")->insert([
            "nombre" => "Elite inmobiliaria",
            "email" => "comercial@eliteinmobiliaria.com.co",
            "telefono" => "7448205",
            "celular" => "",
            "whatsapp" => "",
            "domus_id" => 520,
            "empresa_id" => 1,
            "ciudad_id" => 1
        ]);

        $this->call([
            //16. Usuarios
            UsuarioSeeder::class,
            //17. Estado de inmueble
            EstadoInmuebleSeeder::class,
            //18. Sub Estado de inmuebles
            SubEstadoInmuebleSeeder::class,
        ]);

        //18. Clientes
        DB::table("clientes")->insert([
            "username" => "12345",
            "password" => "12345",
            "nombres" => "Prueba",
            "apellidos" => "Prueba",
            "email" => "prueba@prueba.com",
            "telefono" => "12456789",
            "celular" => "123456789",
            "whatsapp" => "123456789",
            "documento" => "1234567",
            "foto" => "prueba.png",
            "comentario" => "Prueba",
            "ciudad" => "Prueba",
            "profesion" => "Prueba",
            "fecha_nacimiento" => "2020-03-30",
            "empresa_id" => 1,
            "oficina_id" => 1,
            "estado_ubicacions_id" => 1,
            "ciudad_id" => 1,
            "tipo_documento_id" => 1,
            "estado_id" => 1,
            "rol_id" => 1,
            "create_user_id" => 1,
            "update_user_id" => 1
        ]);

        //19. Gestiones
        DB::table("gestions")->insert([
            "nombre" => "Arriendo",
        ]);

        DB::table("gestions")->insert([
            "nombre" => "Venta",
        ]);

        DB::table("gestions")->insert([
            "nombre" => "Arriendo/Venta",
        ]);

        //20. Tipos de inmueble
        $this->call([
            TipoInmuebleSeeder::class,
        ]);

        //21. Destinos
        DB::table("destinos")->insert([
            "nombre" => "Vivienda",
        ]);

        DB::table("destinos")->insert([
            "nombre" => "Comercial",
        ]);

        DB::table("destinos")->insert([
            "nombre" => "Mixto",
        ]);

        //21. Tipos de característica
        DB::table("tipo_caracteristicas")->insert([
            "nombre" => "Interior",
        ]);

        DB::table("tipo_caracteristicas")->insert([
            "nombre" => "Exterior",
        ]);

        DB::table("tipo_caracteristicas")->insert([
            "nombre" => "Del Sector",
        ]);

        DB::table("tipo_caracteristicas")->insert([
            "nombre" => "Generales",
        ]);

        $this->call([
            //22. Características
            CaracteristicaSeeder::class,
            //23. Inmuebles
            InmuebleSeeder::class,
            //24. Dirs
            DirSeeder::class,
        ]);

        //25. Tipos de documento para inmuebles
        DB::table("tipo_documento_inmuebles")->insert([
            "nombre" => "CLT",
        ]);

        DB::table("tipo_documento_inmuebles")->insert([
            "nombre" => "Cédula de propietario",
        ]);

        DB::table("tipo_documento_inmuebles")->insert([
            "nombre" => "Contrato firmado",
        ]);

        DB::table("tipo_documento_inmuebles")->insert([
            "nombre" => "Cuenta de cobro de administración",
        ]);

        DB::table("tipo_documento_inmuebles")->insert([
            "nombre" => "Docusign",
        ]);

        //Estado de ventas
        DB::table("estado_ventas")->insert([
            "nombre" => "Creada",
        ]);

        DB::table("estado_ventas")->insert([
            "nombre" => "Pendiente agente",
        ]);

        DB::table("estado_ventas")->insert([
            "nombre" => "Pendiente revisión",
        ]);

        DB::table("estado_ventas")->insert([
            "nombre" => "Vendido",
        ]);
    }
}
