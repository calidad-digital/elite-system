<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Imports\TipoInmuebleImport;
use Maatwebsite\Excel\Facades\Excel;

class TipoInmuebleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new TipoInmuebleImport, "tipoinm.xlsx");
    }
}
