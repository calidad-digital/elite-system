<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class TipoDocumentoInmueble extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

     // php artisan db:seed --class=TipoDocumentoInmueble
    public function run()
    {
        // DB::table('tipo_documento_inmuebles')->truncate();

        // DB::table('tipo_documento_inmuebles')->insert([
        //     'id' => '1',
        //     'nombre' => 'CLT',
        //     'created_at' => Carbon::now(),
        //     'updated_at' => Carbon::now()
        // ]);
        // DB::table('tipo_documento_inmuebles')->insert([
        //     'id' => '2',
        //     'nombre' => 'Cédula de propietario',
        //     'created_at' => Carbon::now(),
        //     'updated_at' => Carbon::now()
        // ]);
        // DB::table('tipo_documento_inmuebles')->insert([
        //     'id' => '3',
        //     'nombre' => 'Contrato firmado',
        //     'created_at' => Carbon::now(),
        //     'updated_at' => Carbon::now()
        // ]);
        // DB::table('tipo_documento_inmuebles')->insert([
        //     'id' => '4',
        //     'nombre' => 'Cuenta de cobro de administración',
        //     'created_at' => Carbon::now(),
        //     'updated_at' => Carbon::now()
        // ]);
        // DB::table('tipo_documento_inmuebles')->insert([
        //     'id' => '5',
        //     'nombre' => 'Docusign',
        //     'created_at' => Carbon::now(),
        //     'updated_at' => Carbon::now()
        // ]);
        
        DB::table('tipo_documento_inmuebles')->insert([
            'id' => '6',
            'nombre' => 'CLT parqueadero',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tipo_documento_inmuebles')->insert([
            'id' => '7',
            'nombre' => 'CLT parqueadero 2',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tipo_documento_inmuebles')->insert([
            'id' => '8',
            'nombre' => 'CLT parqueadero 3',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tipo_documento_inmuebles')->insert([
            'id' => '9',
            'nombre' => 'CLT deposito',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tipo_documento_inmuebles')->insert([
            'id' => '10',
            'nombre' => 'CLT deposito 2',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tipo_documento_inmuebles')->insert([
            'id' => '11',
            'nombre' => 'Entrega de Pre-Inventario',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tipo_documento_inmuebles')->insert([
            'id' => '12',
            'nombre' => 'Mandato de administración del inmueble',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tipo_documento_inmuebles')->insert([
            'id' => '13',
            'nombre' => 'Recibo del Predial del año en curso',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tipo_documento_inmuebles')->insert([
            'id' => '14',
            'nombre' => 'Facturas de servicios públicos',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tipo_documento_inmuebles')->insert([
            'id' => '15',
            'nombre' => 'Cuenta de cobro de administración',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tipo_documento_inmuebles')->insert([
            'id' => '16',
            'nombre' => 'Escritura pública de los linderos del inmueble',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tipo_documento_inmuebles')->insert([
            'id' => '17',
            'nombre' => 'Aprobación de la aseguradora',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tipo_documento_inmuebles')->insert([
            'id' => '18',
            'nombre' => 'Entrega de inventario para amoblados',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tipo_documento_inmuebles')->insert([
            'id' => '19',
            'nombre' => 'Certificado de Cámara de Comercio',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('tipo_documento_inmuebles')->insert([
            'id' => '20',
            'nombre' => 'Leasing habitacional',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tipo_documento_inmuebles')->insert([
            'id' => '21',
            'nombre' => 'Escritura pública de constitución de usufructo',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('tipo_documento_inmuebles')->insert([
            'id' => '22',
            'nombre' => 'RUT',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}