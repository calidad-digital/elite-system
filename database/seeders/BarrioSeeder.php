<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Imports\BarrioImport;
use Maatwebsite\Excel\Facades\Excel;

class BarrioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new BarrioImport, "barrios.xlsx");
    }
}
