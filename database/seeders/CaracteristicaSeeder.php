<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Imports\CaracteristicaImport;
use Maatwebsite\Excel\Facades\Excel;

class CaracteristicaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new CaracteristicaImport, "caracteristicas.xlsx");
    }
}
