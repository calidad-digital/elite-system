<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DirSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Dir 1
        DB::table("dir1s")->insert([
            "nombre" => "Av Calle"
        ]);

        DB::table("dir1s")->insert([
            "nombre" => "Av Carrera"
        ]);

        DB::table("dir1s")->insert([
            "nombre" => "Calle"
        ]);

        DB::table("dir1s")->insert([
            "nombre" => "Carrera"
        ]);

        DB::table("dir1s")->insert([
            "nombre" => "Diagonal"
        ]);

        DB::table("dir1s")->insert([
            "nombre" => "Transversal"
        ]);

        DB::table("dir1s")->insert([
            "nombre" => "Via"
        ]);

        DB::table("dir1s")->insert([
            "nombre" => "Avenida"
        ]);

        //Dir 2
        DB::table("dir2s")->insert([
            "nombre" => "N/A"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "A"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "B"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "C"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "D"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "E"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "F"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "G"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "H"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "I"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "J"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "K"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "L"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "M"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "N"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "O"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "P"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "Q"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "R"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "S"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "T"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "U"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "V"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "W"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "X"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "Y"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "Z"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "AN"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "AE"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "BE"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "BN"
        ]);

        DB::table("dir2s")->insert([
            "nombre" => "CN"
        ]);

        //Dir 3
        DB::table("dir3s")->insert([
            "nombre" => "N/A"
        ]);

        DB::table("dir3s")->insert([
            "nombre" => "BIS"
        ]);

        //Dir 4
        DB::table("dir4s")->insert([
            "nombre" => "N/A"
        ]);

        DB::table("dir4s")->insert([
            "nombre" => "SUR"
        ]);

        DB::table("dir4s")->insert([
            "nombre" => "ESTE/ORIENTE"
        ]);

        DB::table("dir4s")->insert([
            "nombre" => "NORTE"
        ]);

        DB::table("dir4s")->insert([
            "nombre" => "OESTE/OCCIDENTE"
        ]);

        //Dir 5
        DB::table("dir5s")->insert([
            "nombre" => "Bodega"
        ]);

        DB::table("dir5s")->insert([
            "nombre" => "Apto"
        ]);

        DB::table("dir5s")->insert([
            "nombre" => "Agrupación"
        ]);
    }
}
