<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstratoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("estratos")->insert([
            "nombre" => "1",
            "localidad_id" => 1,
            "ciudad_id" => 1
        ]);

        DB::table("estratos")->insert([
            "nombre" => "2",
            "localidad_id" => 2,
            "ciudad_id" => 1
        ]);

        DB::table("estratos")->insert([
            "nombre" => "3",
            "localidad_id" => 3,
            "ciudad_id" => 1
        ]);

        DB::table("estratos")->insert([
            "nombre" => "4",
            "localidad_id" => 4,
            "ciudad_id" => 1
        ]);

        DB::table("estratos")->insert([
            "nombre" => "5",
            "localidad_id" => 5,
            "ciudad_id" => 1
        ]);

        DB::table("estratos")->insert([
            "nombre" => "6",
            "localidad_id" => 6,
            "ciudad_id" => 1
        ]);
    }
}
