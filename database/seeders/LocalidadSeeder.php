<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Imports\LocalidadImport;
use Maatwebsite\Excel\Facades\Excel;

class LocalidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new LocalidadImport, "localidades.xlsx");
    }
}
