<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Imports\EstadoInmuebleImport;
use Maatwebsite\Excel\Facades\Excel;

class EstadoInmuebleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new EstadoInmuebleImport, "estado_inm.xlsx");
    }
}
