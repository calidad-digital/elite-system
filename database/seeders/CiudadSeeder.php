<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Imports\CiudadImport;
use Maatwebsite\Excel\Facades\Excel;

class CiudadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::import(new CiudadImport, "ciudades.xlsx");
    }
}
