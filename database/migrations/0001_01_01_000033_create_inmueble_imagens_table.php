<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInmuebleImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inmueble_imagens', function (Blueprint $table) {
            $table->id();
            $table->foreignId("inmueble_id")->references("id")->on("inmuebles");
            $table->boolean("is_360");
            $table->string("url");
            $table->string("thumb_url");
            $table->integer("orden");
            $table->string("comentario");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inmueble_imagens');
    }
}
