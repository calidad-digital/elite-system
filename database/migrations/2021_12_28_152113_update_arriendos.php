<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateArriendos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arriendos', function (Blueprint $table) {
            $table->string('parqueadero3',100)->default(0)->after("parqueadero2");
            $table->string('deposito2',100)->default(0)->after("deposito");
            $table->string('numero_contrato',100)->default('')->after("notas_seguros");
            $table->datetime('fecha_inicio_contrato')->nullable()->after("notas_seguros");
            $table->datetime('fecha_final_entrega')->nullable()->after("notas_seguros");
            $table->string('notas_precio',500)->default('')->after("notas_seguros");
            $table->string('notas_servicios',500)->default('')->after("notas_seguros");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arriendos', function (Blueprint $table) {
            $table->dropColumn('parqueadero3')->change();
            $table->dropColumn('deposito2')->change();
            $table->dropColumn('numero_contrato')->change();
            $table->dropColumn('fecha_inicio_contrato')->change();
            $table->dropColumn('fecha_final_entrega')->change();
            $table->dropColumn('notas_precio')->change();
            $table->dropColumn('notas_servicios')->change();
        });
    }
}