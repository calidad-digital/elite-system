<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("empresas", function (Blueprint $table) {
            $table->id();
            $table->string("nombre", 100);
            $table->string("nit", 50);
            $table->string("razon_social", 100);
            $table->string("email", 100);
            $table->string("telefono1", 20);
            $table->string("telefono2", 20);
            $table->string("celular", 20);
            $table->string("whatsapp", 20);
            $table->string("facebook", 100);
            $table->string("instagram", 100);
            $table->string("twitter", 100);
            $table->string("linkedin", 100);
            $table->string("pagina_web", 100);
            $table->string("logo", 150);
            $table->foreignId("estado_id")->references("id")->on("estados");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("empresas");
    }
}
