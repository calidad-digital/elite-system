<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("venta1s", function(Blueprint $table) {
            $table->integer("cantidad_veces_que_se_genera_el_contrato")->default(0)->nullable();
        });

        Schema::table("arriendos", function(Blueprint $table) {
            $table->integer("cantidad_veces_que_se_genera_el_contrato")->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("venta1s", function(Blueprint $table) {
            $table->dropColumn("cantidad_veces_que_se_genera_el_contrato")->change();
        });

        Schema::table("arriendos", function(Blueprint $table) {
            $table->dropColumn("cantidad_veces_que_se_genera_el_contrato")->change();
        });
    }
};
