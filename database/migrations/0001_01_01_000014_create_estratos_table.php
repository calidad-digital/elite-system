<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("estratos", function (Blueprint $table) {
            $table->id();
            $table->string("nombre");
            $table->foreignId("localidad_id")->references("id")->on("localidads");
            $table->foreignId("ciudad_id")->references("id")->on("ciudads");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("estratos");
    }
}
