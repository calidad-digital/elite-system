<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateClientesArriendos extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::table("clientes", function (Blueprint $table) {
            $table->string("copropiedad")->default("")->nullable()->after("estado_civil");
            $table->string("direccion_comercial", 100)->default("")->nullable()->after("direccion");
        });

        Schema::table("codeudores", function (Blueprint $table) {
            $table->string("copropiedad")->default("")->nullable()->after("url_cedula");
            $table->string("direccion_comercial", 100)->default("")->nullable()->after("direccion");
        });

        Schema::table("arriendos", function (Blueprint $table) {
            $table->datetime("fecha_pago_canon")->default(null)->nullable()->after("canon");
            $table->datetime("fecha_pago_admon")->default(null)->nullable()->after("admon");
            $table->datetime("fecha_pago_caldera")->default(null)->nullable()->after("caldera");
            $table->datetime("fecha_pago_parqueadero")->default(null)->nullable()->after("parqueadero");
            $table->datetime("fecha_pago_lavanderia")->default(null)->nullable()->after("lavanderia");
            $table->string("nota_pagos_mensuales", 500)->default("")->nullable()->after("notas_seguros");
            $table->string("objeto_condiciones_contrato", 500)->default("")->nullable()->after("notas_seguros");
            $table->string("notas_servicio_energia", 500)->default("")->nullable()->after("notas_servicios");
            $table->string("notas_servicio_gas", 500)->default("")->nullable()->after("notas_servicios");
        });
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::table("clientes", function (Blueprint $table) {
            $table->dropColumn("copropiedad")->change();
        });

        Schema::table("codeudores", function (Blueprint $table) {
            $table->dropColumn("copropiedad")->change();
        });

        Schema::table("arriendos", function (Blueprint $table) {
            $table->dropColumn("fecha_pago_canon")->change();
            $table->dropColumn("fecha_pago_admon")->change();
            $table->dropColumn("fecha_pago_caldera")->change();
            $table->dropColumn("fecha_pago_parqueadero")->change();
            $table->dropColumn("fecha_pago_lavanderia")->change();
            $table->dropColumn("nota_pagos_mensuales")->change();
            $table->dropColumn("objeto_condiciones_contrato")->change();
            $table->dropColumn("notas_servicio_energia")->change();
            $table->dropColumn("notas_servicio_gas")->change();
        });
    }
}
