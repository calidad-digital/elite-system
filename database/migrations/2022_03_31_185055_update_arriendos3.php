<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateArriendos3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("arriendos", function (Blueprint $table) {
            $table->string("texto_fecha_entrega")->default(null)->nullable()->after("fecha_entrega");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("arriendos", function (Blueprint $table) {
            $table->dropColumn("texto_fecha_entrega")->change();
        });
    }
}
