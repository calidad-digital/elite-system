<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOcupantes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocupantes', function (Blueprint $table) {
            $table->id();
            $table->string("nombre", 100)->nullable();
            $table->string("cedula", 100)->nullable();
            $table->foreignId("arriendo_id")->references("id")->on("arriendos");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ocupantes');
    }
}