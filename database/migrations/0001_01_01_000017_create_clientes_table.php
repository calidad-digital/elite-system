<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("clientes", function (Blueprint $table) {
            $table->id();
            $table->string("username", 100);
            $table->string("password", 100);
            $table->string("nombres", 100);
            $table->string("apellidos", 100);
            $table->string("email", 100);
            $table->string("documento", 50);
            $table->string("telefono", 20);
            $table->string("celular", 20);
            $table->string("whatsapp", 20);
            $table->mediumText("comentario", 255);
            $table->string("foto", 255)->nullable();
            $table->string("url_documento", 255)->nullable();
            $table->string("ciudad", 50);
            $table->string("profesion", 100);
            $table->string("estado_civil", 100)->nullable()->default(null);
            $table->date("fecha_nacimiento");
            $table->foreignId("tipo_documento_id")->references("id")->on("tipo_documentos");
            $table->foreignId("estado_ubicacions_id")->references("id")->on("estado_ubicacions");
            $table->foreignId("ciudad_id")->references("id")->on("ciudads");
            $table->foreignId("empresa_id")->references("id")->on("empresas");
            $table->foreignId("oficina_id")->references("id")->on("oficinas");
            $table->foreignId("estado_id")->references("id")->on("estados");
            $table->foreignId("rol_id")->references("id")->on("rols");
            $table->foreignId("create_user_id")->references("id")->on("usuarios");
            $table->foreignId("update_user_id")->references("id")->on("usuarios");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("clientes");
    }
}
