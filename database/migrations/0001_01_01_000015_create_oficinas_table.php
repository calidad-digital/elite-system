<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOficinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("oficinas", function (Blueprint $table) {
            $table->id();
            $table->string("nombre", 100);
            $table->string("email", 100);
            $table->string("telefono", 20);
            $table->string("celular", 20);
            $table->string("whatsapp", 20);
            $table->integer("domus_id");
            $table->foreignId("empresa_id")->references("id")->on("empresas");
            $table->foreignId("ciudad_id")->references("id")->on("ciudads");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("oficinas");
    }
}
