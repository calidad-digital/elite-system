<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateClienteVentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("cliente_ventas", function (Blueprint $table) {
            $table->string("lugar_expedicion_documento")->nullable()->after("fecha_expedicion_documento");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("cliente_ventas", function (Blueprint $table) {
            $table->dropColumn("lugar_expedicion_documento")->change();
        });
    }
}
