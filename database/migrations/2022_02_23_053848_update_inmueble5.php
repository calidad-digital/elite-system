<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateInmueble5 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('inmueble_direccions', function (Blueprint $table) {
            $table->string('complemento3')->default(0)->nullable()->after("complemento2");
        });
         Schema::table('inmuebles', function (Blueprint $table) {
            $table->string('numero_parqueadero_1')->default(0)->nullable()->after("matricula_parqueadero_1");
            $table->string('numero_parqueadero_2')->default(0)->nullable()->after("matricula_parqueadero_2");
            $table->string('numero_parqueadero_3')->default(0)->nullable()->after("matricula_parqueadero_3");
            $table->string('numero_deposito_1')->default(0)->nullable()->after("matricula_deposito_1");
            $table->string('numero_deposito_2')->default(0)->nullable()->after("matricula_deposito_2");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inmueble_direccions', function (Blueprint $table) {
            $table->dropColumn('complemento3')->change();
        });
        Schema::table('inmueble_direccions', function (Blueprint $table) {
            $table->dropColumn('numero_parqueadero_1')->change();
            $table->dropColumn('numero_parqueadero_2')->change();
            $table->dropColumn('numero_parqueadero_3')->change();
            $table->dropColumn('numero_deposito_1')->change();
            $table->dropColumn('numero_deposito_2')->change();
        });
    }
}