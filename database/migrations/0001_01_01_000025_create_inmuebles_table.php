<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInmueblesTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create("inmuebles", function (Blueprint $table) {
            $table->id();

            //Parte básica
            $table->integer("codigo");
            $table->string("codigoMLS")->nullable();
            $table->string("referencia", 20)->nullable();
            $table->foreignId("tipo_inmueble_id")->references("id")->on("tipo_inmuebles");
            $table->foreignId("gestion_id")->references("id")->on("gestions");
            $table->foreignId("destino_id")->references("id")->on("destinos");
            $table->bigInteger("canon");
            $table->bigInteger("administracion");
            $table->bigInteger("venta");
            $table->date("fecha_consignacion");

            //Ubicación
            $table->foreignId("estado_ubicacion_id")->references("id")->on("estado_ubicacions");
            $table->foreignId("ciudad_id")->references("id")->on("ciudads");
            $table->foreignId("zona_id")->references("id")->on("zonas");
            $table->foreignId("localidad_id")->references("id")->on("localidads");
            $table->foreignId("barrio_id")->references("id")->on("barrios");
            $table->foreignId("estrato_id")->references("id")->on("estratos");
            $table->string("barrio_comun", 100);
            $table->string("direccion", 100);
            $table->string("latitud", 100);
            $table->string("longitud", 100);

            //Comodidades
            $table->integer("habitaciones");
            $table->integer("banios");
            $table->integer("parqueaderos");
            $table->integer("parqueaderos_cubiertos");
            $table->integer("parqueaderos_descubiertos");
            $table->integer("parqueaderos_dobles");
            $table->integer("parqueaderos_sencillos");
            $table->integer("area_lote");
            $table->integer("area_construida");
            $table->integer("area_privada");
            $table->integer("frente");
            $table->integer("fondo");
            $table->integer("anio_construccion");
            $table->integer("nivel");

            //Adicionales
            $table->string("video");
            $table->string("tour_virtual");
            $table->foreignId("oficina_id")->references("id")->on("oficinas");
            $table->boolean("aviso_ventana");
            $table->boolean("destacado");
            $table->boolean("exclusivo");
            $table->integer("anio_remodelacion");
            $table->integer("valor_renta");
            $table->integer("comision");
            $table->string("matricula", 50);
            $table->foreignId("asesor_id")->references("id")->on("usuarios");

            //Descripciones
            $table->text("descripcion");
            $table->text("descripcion_metro_cuadrado");
            $table->text("descripcion_mls");
            $table->string("restricciones");
            $table->string("comentario");
            $table->string("comentario2");
            $table->string("chip", 50)->nullable();
            $table->string("cedula_catastral", 50)->nullable();

            //Final
            $table->boolean("publicado")->nullable();
            $table->foreignId("estado_id")->references("id")->on("estado_inmuebles");
            $table->foreignId("sub_estado_id")->references("id")->on("sub_estado_inmuebles");
            $table->foreignId("create_user_id")->references("id")->on("usuarios");
            $table->foreignId("update_user_id")->references("id")->on("usuarios");
            $table->foreignId("empresa_id")->references("id")->on("empresas");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists("inmuebles");
    }
}
