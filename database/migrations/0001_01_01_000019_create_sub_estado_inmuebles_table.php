<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubEstadoInmueblesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("sub_estado_inmuebles", function (Blueprint $table) {
            $table->id();
            $table->foreignId("estado_id")->references("id")->on("estado_inmuebles");
            $table->string("nombre");
            $table->string("id_notificacion_asesores");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("sub_estado_inmuebles");
    }
}
