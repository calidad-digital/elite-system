<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("venta1s", function(Blueprint $table) {
            $table->boolean("mostrar_paragrafo_unico")->default(false)->nullable()->after("negociacion_arras_favor");
            $table->string("notas_escrituracion", 4294967295)->default(null)->nullable()->change();
            $table->datetime("fecha_firma_promesa")->default(null)->nullable()->after("escritura_fecha_publica");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("venta1s", function(Blueprint $table) {
            $table->dropColumn("mostrar_paragrafo_unico")->change();
            $table->string("notas_escrituracion", 255)->change();
            $table->dropColumn("fecha_firma_promesa")->change();
        });
    }
};
