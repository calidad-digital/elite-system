<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateClientes3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

        Schema::table('clientes', function (Blueprint $table) {
            $table->string('expedicion',200)->nullable()->after("documento");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('clientes', function (Blueprint $table) {
            $table->dropColumn('expedicion')->change();
        });
    }
}