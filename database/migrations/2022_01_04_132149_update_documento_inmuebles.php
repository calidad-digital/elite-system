<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDocumentoInmuebles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('documento_inmuebles', function (Blueprint $table) {
            $table->string('url',500)->change();
        });
         Schema::table('venta1s', function (Blueprint $table) {
            $table->string('comprador_1_cedula_url',500)->change();
            $table->string('comprador_2_cedula_url',500)->change();
            $table->string('comprador_3_cedula_url',500)->change();
            $table->string('comprador_4_cedula_url',500)->change();
            $table->string('clt_url',500)->change();
            $table->string('oferta_compra_url',500)->change();
            $table->string('escritura_publica_url',500)->change();
            $table->string('promesa_firmada_url',500)->change();
            $table->string('clt_parqueadero_1_url',500)->change();
            $table->string('clt_parqueadero_2_url',500)->change();
            $table->string('camara_comercio_url',500)->change();
            $table->string('clt_deposito_1_url',500)->change();
            $table->string('clt_deposito_2_url',500)->change();
            $table->string('poder_copia_diligenciada_url',500)->change();
        });
        
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documento_inmuebles', function (Blueprint $table) {
            $table->string('url',255)->change();
        });
        Schema::table('venta1s', function (Blueprint $table) {
            $table->string('comprador_1_cedula_url',200)->change();
            $table->string('comprador_2_cedula_url',200)->change();
            $table->string('comprador_3_cedula_url',200)->change();
            $table->string('comprador_4_cedula_url',200)->change();
            $table->string('clt_url',200)->change();
            $table->string('oferta_compra_url',200)->change();
            $table->string('escritura_publica_url',200)->change();
            $table->string('promesa_firmada_url',200)->change();
            $table->string('clt_parqueadero_1_url',200)->change();
            $table->string('clt_parqueadero_2_url',200)->change();
            $table->string('camara_comercio_url',200)->change();
            $table->string('clt_deposito_1_url',200)->change();
            $table->string('clt_deposito_2_url',200)->change();
            $table->string('poder_copia_diligenciada_url',200)->change();
        });
    }
}