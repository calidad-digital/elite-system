<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInmuebleDireccionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("inmueble_direccions", function (Blueprint $table) {
            $table->id();
            $table->foreignId("inmueble_id")->references("id")->on("inmuebles");
            $table->foreignId("c1_id")->references("id")->on("dir1s");
            $table->integer("c2");
            $table->foreignId("c3_id")->references("id")->on("dir2s");
            $table->foreignId("c4_id")->references("id")->on("dir3s");
            $table->foreignId("c5_id")->references("id")->on("dir2s");
            $table->foreignId("c6_id")->references("id")->on("dir4s");
            $table->integer("c7");
            $table->foreignId("c8_id")->references("id")->on("dir2s");
            $table->integer("c9");
            $table->foreignId("c10_id")->references("id")->on("dir4s");
            $table->string("complemento");
            $table->string("complemento2");
            $table->foreignId("create_user_id")->references("id")->on("usuarios");
            $table->foreignId("update_user_id")->references("id")->on("usuarios");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("inmueble_direccions");
    }
}
