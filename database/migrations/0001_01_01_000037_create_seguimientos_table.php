<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeguimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("seguimientos", function (Blueprint $table) {
            $table->id();
            $table->foreignId("inmueble_id")->references("id")->on("inmuebles");
            $table->foreignId("estado_inmueble_id")->references("id")->on("estado_inmuebles");
            $table->foreignId("sub_estado_inmueble_id")->references("id")->on("sub_estado_inmuebles");
            $table->boolean("is_active");
            $table->string("comentario");
            $table->dateTime("fecha_registro");
            $table->foreignId("create_user_id")->references("id")->on("usuarios");
            $table->foreignId("update_user_id")->references("id")->on("usuarios");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("seguimientos");
    }
}
