<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVenta1sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("venta1s", function (Blueprint $table) {
            $table->id();

            //Garaje
            $table->string("matricula_garaje", 30)->nullable();
            $table->string("chip_garaje", 30)->nullable();
            $table->string("cedula_catastral_garaje", 30)->nullable();

            //Depósito
            $table->string("matricula_deposito", 30)->nullable();
            $table->string("chip_deposito", 30)->nullable();
            $table->string("cedula_catastral_deposito", 30)->nullable();

            //Comprador 1
            $table->string("comprador_1_nombre", 100)->nullable();
            $table->string("comprador_1_cedula", 50)->nullable();
            $table->string("comprador_1_estado_civil", 100)->nullable();
            $table->string("comprador_1_direccion", 100)->nullable();
            $table->string("comprador_1_telefono", 50)->nullable();
            $table->string("comprador_1_email", 100)->nullable();
            $table->string("comprador_1_cedula_url", 200)->nullable();

            //Comprador 2
            $table->string("comprador_2_nombre", 100)->nullable();
            $table->string("comprador_2_cedula", 50)->nullable();
            $table->string("comprador_2_estado_civil", 100)->nullable();
            $table->string("comprador_2_direccion", 100)->nullable();
            $table->string("comprador_2_telefono", 50)->nullable();
            $table->string("comprador_2_email", 100)->nullable();
            $table->string("comprador_2_cedula_url", 200)->nullable();

            //Comprador 3
            $table->string("comprador_3_nombre", 100)->nullable();
            $table->string("comprador_3_cedula", 50)->nullable();
            $table->string("comprador_3_estado_civil", 100)->nullable();
            $table->string("comprador_3_direccion", 100)->nullable();
            $table->string("comprador_3_telefono", 50)->nullable();
            $table->string("comprador_3_email", 100)->nullable();
            $table->string("comprador_3_cedula_url", 200)->nullable();

            //Comprador 4
            $table->string("comprador_4_nombre", 100)->nullable();
            $table->string("comprador_4_cedula", 50)->nullable();
            $table->string("comprador_4_estado_civil", 100)->nullable();
            $table->string("comprador_4_direccion", 100)->nullable();
            $table->string("comprador_4_telefono", 50)->nullable();
            $table->string("comprador_4_email", 100)->nullable();
            $table->string("comprador_4_cedula_url", 200)->nullable();

            //Negociacion 1
            $table->integer("negociacion_precio")->nullable();
            $table->integer("negociacion_pagado_promesa")->nullable();
            $table->integer("negociacion_saldo_pendiente")->nullable();
            $table->integer("negociacion_arras")->nullable();
            $table->string("negociacion_arras_favor", 100)->nullable();

            //Pago 1
            $table->string("pago_1_fecha_promesa_pago", 100)->nullable();
            $table->string("pago_1_notaria", 100)->nullable();
            $table->string("pago_1_medio_pago", 100)->nullable();
            $table->integer("pago_1_valor_pago")->nullable();
            $table->string("pago_1_cheque_transferencia", 100)->nullable();
            $table->string("pago_1_origen_recurso", 100)->nullable();

            //Pago 2
            $table->string("pago_2_fecha_pago", 50)->nullable();
            $table->string("pago_2_medio_pago", 100)->nullable();
            $table->string("pago_2_pago_favor", 100)->nullable();
            $table->integer("pago_2_valor_pago")->nullable();
            $table->string("pago_2_cheque_transferencia", 100)->nullable();
            $table->string("pago_2_origen_recurso", 100)->nullable();

            //Pago 3
            $table->string("pago_3_fecha_pago", 50)->nullable();
            $table->string("pago_3_medio_pago", 100)->nullable();
            $table->string("pago_3_pago_favor", 100)->nullable();
            $table->integer("pago_3_valor_pago")->nullable();
            $table->string("pago_3_cheque_transferencia", 100)->nullable();
            $table->string("pago_3_origen_recurso", 100)->nullable();

            //Pago 4
            $table->string("pago_4_fecha_pago", 50)->nullable();
            $table->string("pago_4_medio_pago", 100)->nullable();
            $table->string("pago_4_pago_favor", 100)->nullable();
            $table->integer("pago_4_valor_pago")->nullable();
            $table->string("pago_4_cheque_transferencia", 100)->nullable();
            $table->string("pago_4_origen_recurso", 100)->nullable();

            //Escrituracion
            $table->string("escritura_fecha_publica", 100)->nullable();
            $table->string("escritura_notaria", 100)->nullable();
            $table->string("escritura_fecha_entrega_inmueble", 100)->nullable();
            $table->string("escritura_predial_asumido", 100)->nullable();
            $table->string("escritura_gasto_notarial_asumido", 100)->nullable();
            $table->string("escritura_beneficiado_instrumento_publico", 100)->nullable();
            $table->string("escritura_negociacion_adicional", 100)->nullable();
            $table->string("escritura_pago_comision_elite", 100)->nullable();
            $table->string("escritura_observacion", 100)->nullable();

            //Documentos
            $table->string("clt_url", 200)->nullable();
            $table->string("oferta_compra_url", 200)->nullable();
            $table->string("escritura_publica_url", 200)->nullable();
            $table->string("promesa_firmada_url", 200)->nullable();
            $table->string("clt_parqueadero_1_url", 200)->nullable();
            $table->string("clt_parqueadero_2_url", 200)->nullable();
            $table->string("camara_comercio_url", 200)->nullable();
            $table->string("clt_deposito_1_url", 200)->nullable();
            $table->string("clt_deposito_2_url", 200)->nullable();
            $table->string("poder_copia_diligenciada_url", 200)->nullable();

            //Relaciones
            $table->foreignId("inmueble_id")->references("id")->on("inmuebles");
            $table->foreignId("estado_venta_id")->references("id")->on("estado_ventas");
            $table->foreignId("create_user_id")->references("id")->on("usuarios");
            $table->foreignId("update_user_id")->references("id")->on("usuarios");

            //Otros
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("venta1s");
    }
}
