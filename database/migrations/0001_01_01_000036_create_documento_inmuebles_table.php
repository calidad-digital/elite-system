<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentoInmueblesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("documento_inmuebles", function (Blueprint $table) {
            $table->id();
            $table->foreignId("inmueble_id")->references("id")->on("inmuebles");
            $table->foreignId("tipo_documento_id")->references("id")->on("tipo_documento_inmuebles");
            $table->boolean("is_active");
            $table->string("url");
            $table->string("comentario");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("documento_inmuebles");
    }
}
