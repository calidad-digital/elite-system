<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("venta_correos", function (Blueprint $table) {
            $table->id();
            $table->string("usuario_tipo", 100)->nullable();
            $table->string("usuario_nombre", 100)->nullable();
            $table->string("usuario_correo", 100)->nullable();
            $table->integer("usuario_id")->nullable();
            $table->boolean("aprobado")->default(false);
            $table->longText("comentario")->nullable();
            $table->string("url_aprobacion")->nullable();
            $table->dateTime("ultima_visita")->nullable();
            $table->foreignId("venta_id")->references("id")->on("venta1s");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("venta_correos");
    }
};
