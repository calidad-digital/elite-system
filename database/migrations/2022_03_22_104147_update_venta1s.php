<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateVenta1s extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("venta1s", function (Blueprint $table) {
            $table->longText("clausulas")->nullable()->after("poder_copia_diligenciada_url");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("venta1s", function (Blueprint $table) {
            $table->dropColumn("clausulas")->change();
        });
    }
}
