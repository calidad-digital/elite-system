<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCodeudores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('codeudores', function (Blueprint $table) {
            $table->id();
            $table->string("nombre", 100)->nullable();
            $table->string("cedula", 100)->nullable();
            $table->string("direccion", 100)->nullable();
            $table->string("email", 100)->nullable();
            $table->string("celular", 100)->nullable();
            $table->string("url_cedula", 100)->nullable();
            $table->foreignId("arriendo_id")->references("id")->on("arriendos");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('codeudores');
    }
}