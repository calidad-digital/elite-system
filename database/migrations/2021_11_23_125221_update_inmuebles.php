<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateInmuebles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('inmuebles', function (Blueprint $table) {
            $table->string('restricciones',500)->change();
            $table->string('comentario',500)->change();
            $table->string('comentario2',500)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inmuebles', function (Blueprint $table) {
            $table->string('restricciones',255)->change();
            $table->string('comentario',255)->change();
            $table->string('comentario2',255)->change();
        });
    }
}