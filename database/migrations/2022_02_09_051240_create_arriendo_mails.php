<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArriendoMails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arriendo_correos', function (Blueprint $table) {
            $table->id();
            $table->string("usuario_tipo", 100)->nullable();
            $table->string("usuario_nombre", 100)->nullable();
            $table->string("usuario_correo", 100)->nullable();
            $table->boolean("aprobado")->default(0);
            $table->string("commentario",1000)->default(0);
            $table->dateTime("ultima_visita")->nullable();
            $table->foreignId("arriendo_id")->references("id")->on("arriendos");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arriendo_correos');
    }
}