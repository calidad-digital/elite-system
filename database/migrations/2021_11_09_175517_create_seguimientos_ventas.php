<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeguimientosVentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("seguimiento_ventas", function (Blueprint $table) {
            $table->id();
            $table->foreignId("venta_id")->references("id")->on("venta1s");
            $table->foreignId("estado_venta_id")->references("id")->on("estado_seguimientos_ventas");
            $table->string("documento_soporte_url");
            $table->string("comentario");
            $table->dateTime("fecha_seguimiento");
            $table->foreignId("usuario_encargado")->references("id")->on("usuarios");
            $table->foreignId("create_user_id")->references("id")->on("usuarios");
            $table->foreignId("update_user_id")->references("id")->on("usuarios");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("seguimientos");
    }
}