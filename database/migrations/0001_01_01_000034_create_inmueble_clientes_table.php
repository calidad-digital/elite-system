<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInmuebleClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("inmueble_clientes", function (Blueprint $table) {
            $table->id();
            $table->foreignId("inmueble_id")->references("id")->on("inmuebles");
            $table->foreignId("cliente_id")->references("id")->on("clientes");
            $table->foreignId("rol_id")->references("id")->on("rols");
            $table->integer("orden");
            $table->integer("porcentaje");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("inmueble_clientes");
    }
}
