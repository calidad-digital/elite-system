<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRevisionArriendos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revision_arriendos', function (Blueprint $table) {
            $table->id();
            $table->morphs('personable');
            $table->boolean("aprobado")->default(0);
            $table->string("comentario", 1000)->nullable();
            $table->foreignId("arriendo_id")->references("id")->on("arriendos");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revision_arriendos');
    }
}