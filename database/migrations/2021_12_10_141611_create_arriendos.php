<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArriendos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arriendos', function (Blueprint $table) {
            $table->id();
            $table->integer("coordinadora")->nullable();
            $table->string("solicitud_aseguradora", 100)->nullable();
            $table->biginteger("valor_total")->nullable();
            $table->biginteger("canon")->nullable();
            $table->integer("admon")->nullable();
            $table->integer("admon_no_desc")->nullable();
            $table->integer("caldera")->nullable();
            $table->integer("parqueadero")->nullable();
            $table->integer("lavanderia")->nullable();
            $table->string("parqueadero1", 100)->nullable();
            $table->string("parqueadero2", 100)->nullable();
            $table->string("deposito", 100)->nullable();
            $table->string("contrato_acueducto", 100)->nullable();
            $table->string("contrato_energia", 100)->nullable();
            $table->string("contrato_gas", 100)->nullable();
            $table->string("nombre_conjunto", 100)->nullable();
            $table->string("uso_suelo_permitido", 100)->nullable();
            $table->string("incremento_anual", 100)->nullable();
            $table->string("uso_suelo_inmueble", 100)->nullable();
            $table->datetime("fecha_entrega")->nullable();
            $table->boolean("amparo_basico")->nullable();
            $table->boolean("amparo_hogar")->nullable();
            $table->boolean("amparo_integral")->nullable();
            $table->string("notas_seguros", 500)->nullable();
            $table->string("contrato_captacion_url", 300)->nullable();
            $table->string("clt_url", 300)->nullable();
            $table->string("clt_parqueadero_url", 300)->nullable();
            $table->string("clt_parqueadero2_url", 300)->nullable();
            $table->string("clt_deposito_url", 300)->nullable();
            $table->string("clt_deposito2_url", 300)->nullable();
            $table->string("entrega_inventario_url", 300)->nullable();
            $table->string("mandanto_admon_url", 300)->nullable();
            $table->string("recibo_predial_url", 300)->nullable();
            $table->string("servicios_publicos_url", 300)->nullable();
            $table->string("cuenta_cobro_admon_url", 300)->nullable();
            $table->string("escritura_linderos_url", 300)->nullable();
            $table->string("aprobacion_aseguradora_url", 300)->nullable();
            $table->string("entrega_amoblados_url", 300)->nullable();
            $table->string("camara_comercio_url", 300)->nullable();
            $table->string("leasing_url", 300)->nullable();
            $table->string("usufructo_url", 300)->nullable();
            $table->string("rut_url", 300)->nullable();
            $table->foreignId("inmueble_id")->references("id")->on("inmuebles");
            $table->foreignId("arrendatario_id")->references("id")->on("clientes");
            $table->foreignId("creado_por")->references("id")->on("usuarios");
            $table->foreignId("actualizado_por")->references("id")->on("usuarios");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arriendos');
    }
}