<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("usuarios", function (Blueprint $table) {
            $table->id();
            $table->string("codigo_interno", 50)->unique();
            $table->string("codigo_interno_mls", 50)->unique();
            $table->string("nombres", 255);
            $table->string("apellidos", 255);
            $table->string("nombre_usuario", 50)->unique();
            $table->string("ingreso", 100);
            $table->string("token", 255)->unique();
            $table->string("email", 200)->unique();
            $table->string("telefono", 50);
            $table->string("celular", 50);
            $table->string("whatsapp", 50);
            $table->string("documento", 50);
            $table->string("foto", 255);
            $table->string("domus_token", 255);
            $table->integer("domus_id")->nullable();
            $table->mediumText("comentario", 255);
            $table->mediumText("comentario2", 255);
            $table->string("ciudad", 50);
            $table->string("departamento", 50);
            $table->integer("orden");
            $table->date("fecha_nacimiento");
            $table->timestamp("fecha_ingreso")->useCurrent();
            $table->integer("plan")->default(1);
            $table->timestamp("fecha_ultimo_cambio_plan")->useCurrent();
            $table->foreignId("departamento_id")->references("id")->on("departamentos");
            $table->foreignId("estado_ubicacions_id")->references("id")->on("estado_ubicacions");
            $table->foreignId("ciudad_id")->references("id")->on("ciudads");
            $table->foreignId("tipo_documento_id")->references("id")->on("tipo_documentos");
            $table->foreignId("empresa_id")->references("id")->on("empresas");
            $table->foreignId("oficina_id")->references("id")->on("oficinas");
            $table->foreignId("estado_id")->references("id")->on("estados");
            $table->foreignId("rol_id")->references("id")->on("rols");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("usuarios");
    }
}
