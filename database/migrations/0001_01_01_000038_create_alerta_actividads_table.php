<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlertaActividadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("alerta_actividads", function (Blueprint $table) {
            $table->id();
            $table->string("tipo_actividad");
            $table->integer("actividad_id");
            $table->integer("estado_actividad_id");
            $table->string("comentario");
            $table->dateTime("fecha_asignada");
            $table->dateTime("fecha_vencimiento");
            $table->boolean("fue_notificado_email");
            $table->boolean("fue_notificado_sms");
            $table->foreignId("usuario_creador_id")->references("id")->on("usuarios");
            $table->foreignId("usuario_asignado_id")->references("id")->on("usuarios");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("alerta_actividads");
    }
}
