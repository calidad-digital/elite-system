<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateInmuebles2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inmuebles', function (Blueprint $table) {
            $table->string('comentarios_documentos',500)->nullable()->after("comentario2");
            $table->string('matricula_parqueadero_1',100)->nullable()->after("cedula_catastral");
            $table->string('chip_parqueadero_1',100)->nullable()->after("cedula_catastral");
            $table->string('cedula_parqueadero_1',100)->nullable()->after("cedula_catastral");
            $table->string('matricula_parqueadero_2',100)->nullable()->after("cedula_catastral");
            $table->string('chip_parqueadero_2',100)->nullable()->after("cedula_catastral");
            $table->string('cedula_parqueadero_2',100)->nullable()->after("cedula_catastral");
            $table->string('matricula_parqueadero_3',100)->nullable()->after("cedula_catastral");
            $table->string('chip_parqueadero_3',100)->nullable()->after("cedula_catastral");
            $table->string('cedula_parqueadero_3',100)->nullable()->after("cedula_catastral");
            $table->string('matricula_deposito_1',100)->nullable()->after("cedula_catastral");
            $table->string('chip_deposito_1',100)->nullable()->after("cedula_catastral");
            $table->string('cedula_deposito_1',100)->nullable()->after("cedula_catastral");
            $table->string('matricula_deposito_2',100)->nullable()->after("cedula_catastral");
            $table->string('chip_deposito_2',100)->nullable()->after("cedula_catastral");
            $table->string('cedula_deposito_2',100)->nullable()->after("cedula_catastral");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inmuebles', function (Blueprint $table) {
            $table->dropColumn('comentarios_documentos')->change();
            $table->dropColumn('matricula_parqueadero_1')->change();
            $table->dropColumn('chip_parqueadero_1')->change();
            $table->dropColumn('cedula_parqueadero_1')->change();
            $table->dropColumn('matricula_parqueadero_2')->change();
            $table->dropColumn('chip_parqueadero_2')->change();
            $table->dropColumn('cedula_parqueadero_2')->change();
            $table->dropColumn('matricula_parqueadero_3')->change();
            $table->dropColumn('chip_parqueadero_3')->change();
            $table->dropColumn('cedula_parqueadero_3')->change();
            $table->dropColumn('matricula_deposito_1')->change();
            $table->dropColumn('chip_deposito_1')->change();
            $table->dropColumn('cedula_deposito_1')->change();
            $table->dropColumn('matricula_deposito_2')->change();
            $table->dropColumn('chip_deposito_2')->change();
            $table->dropColumn('cedula_deposito_2')->change();
        });
    }
}