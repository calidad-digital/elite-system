<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("venta1s", function (Blueprint $table) {
            $table->boolean("mostrar_paragrafo_segundo")->default(false)->nullable()->after("mostrar_paragrafo_unico");
            $table->longText("paragrafo_segundo_texto")->nullable()->after("mostrar_paragrafo_unico");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("venta1s", function (Blueprint $table) {
            $table->dropColumn("mostrar_paragrafo_segundo")->change();
            $table->dropColumn("paragrafo_segundo_texto")->change();
        });
    }
};
