<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateArriendoCorreos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("arriendo_correos", function (Blueprint $table) {
           $table->string("url_aprobacion")->default("")->nullable()->after("commentario");
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("arriendo_correos", function (Blueprint $table) {
            $table->dropColumn("url_aprobacion")->change();
        });
    }
}
