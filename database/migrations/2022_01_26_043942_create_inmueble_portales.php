<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInmueblePortales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inmueble_portales', function (Blueprint $table) {
            $table->id();
            $table->string("nombre_portal", 100)->nullable();
            $table->string("codigo_portal", 100)->nullable();
            $table->string("url_portal", 500)->nullable();
            $table->datetime("fecha_actualizacion")->nullable();
            $table->datetime("fecha_subida")->nullable();
            $table->string("estado")->nullable();
            $table->foreignId("inmueble_id")->references("id")->on("inmuebles");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inmueble_portales');
    }
}