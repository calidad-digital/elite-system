<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("venta1s", function(Blueprint $table) {
            $table->renameColumn("pago_1_fecha_promesa_pago", "pago_1_fecha_pago");
            $table->string("notaria_circulo")->default(null)->nullable()->after("escritura_notaria");
            $table->string("hora_escrituracion")->default(null)->nullable()->after("escritura_fecha_publica");
            $table->string("notas_escrituracion")->default(null)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("venta1s", function(Blueprint $table) {
            $table->renameColumn("pago_1_fecha_pago", "pago_1_fecha_promesa_pago");
            $table->dropColumn("notaria_circulo")->change();
            $table->dropColumn("hora_escrituracion")->change();
            $table->dropColumn("notas_escrituracion")->change();
        });
    }
};
