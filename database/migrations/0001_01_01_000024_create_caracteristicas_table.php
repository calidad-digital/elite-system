<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaracteristicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("caracteristicas", function (Blueprint $table) {
            $table->id();
            $table->integer("domus_id");
            $table->string("nombre");
            $table->integer("orden");
            $table->string("tipo");
            $table->string("tipo_inmueble");
            $table->foreignId("tipo_caracteristica_id")->references("id")->on("tipo_caracteristicas");
            $table->foreignId("tipo_inmueble_id")->references("id")->on("tipo_inmuebles");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("caracteristicas");
    }
}
