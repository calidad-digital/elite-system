<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create("venta1_extended", function (Blueprint $table) {
            $table->id();
            $table->string("pago_5_fecha_pago")->default(null)->nullable();
            $table->string("pago_5_medio_pago")->default(null)->nullable();
            $table->string("pago_5_pago_favor")->default(null)->nullable();
            $table->integer("pago_5_valor_pago")->default(null)->nullable();
            $table->text("pago_5_cheque_transferencia")->default(null)->nullable();
            $table->string("pago_5_origen_recurso")->default(null)->nullable();
            $table->string("pago_6_fecha_pago")->default(null)->nullable();
            $table->string("pago_6_medio_pago")->default(null)->nullable();
            $table->string("pago_6_pago_favor")->default(null)->nullable();
            $table->integer("pago_6_valor_pago")->default(null)->nullable();
            $table->text("pago_6_cheque_transferencia")->default(null)->nullable();
            $table->string("pago_6_origen_recurso")->default(null)->nullable();
            $table->string("pago_7_fecha_pago")->default(null)->nullable();
            $table->string("pago_7_medio_pago")->default(null)->nullable();
            $table->string("pago_7_pago_favor")->default(null)->nullable();
            $table->integer("pago_7_valor_pago")->default(null)->nullable();
            $table->text("pago_7_cheque_transferencia")->default(null)->nullable();
            $table->string("pago_7_origen_recurso")->default(null)->nullable();
            $table->string("pago_8_fecha_pago")->default(null)->nullable();
            $table->string("pago_8_medio_pago")->default(null)->nullable();
            $table->string("pago_8_pago_favor")->default(null)->nullable();
            $table->integer("pago_8_valor_pago")->default(null)->nullable();
            $table->text("pago_8_cheque_transferencia")->default(null)->nullable();
            $table->string("pago_8_origen_recurso")->default(null)->nullable();
            $table->boolean("mostrar_clausula_covid")->default(true)->nullable();
            $table->boolean("mostrar_firma_electronica")->default(false)->nullable();
            $table->foreignId("venta_id")->references("id")->on("venta1s");

            $table->timestamps();
        });
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists("venta1_extended");
    }
};
