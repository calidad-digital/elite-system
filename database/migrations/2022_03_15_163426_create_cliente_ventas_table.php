<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClienteVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("cliente_ventas", function (Blueprint $table) {
            $table->id();
            $table->string("nombre")->nullable();
            $table->string("ciudad")->nullable();
            $table->string("numero_documento")->nullable();
            $table->datetime("fecha_expedicion_documento")->nullable();
            $table->datetime("fecha_nacimiento")->nullable();
            $table->string("estado_civil")->nullable();
            $table->string("telefono", 20)->nullable();
            $table->string("celular", 20)->nullable();
            $table->string("email", 100)->nullable();
            $table->string("direccion")->nullable();
            $table->string("tipo_cliente")->nullable();
            $table->string("url_documento", 255)->nullable();
            $table->string("profesion")->nullable();
            $table->foreignId("tipo_documento_id")->nullable()->unsigned()->references("id")->on("tipo_documentos");
            $table->foreignId("subestado_civil_id")->nullable()->unsigned()->references("id")->on("subestado_civiles");
            $table->foreignId("inmueble_id")->nullable()->unsigned()->references("id")->on("inmuebles");
            $table->foreignId("venta1_id")->nullable()->unsigned()->references("id")->on("venta1s");
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("cliente_ventas");
    }
}
