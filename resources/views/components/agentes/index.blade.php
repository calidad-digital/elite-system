<section role="main" class="content-body">
    <header class="page-header">
        <h2>Agentes</h2>

        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>Agentes</span></li>
                <li><span>Lista&nbsp;&nbsp;&nbsp;</span></li>
            </ol>

            <a class="" data-open=""></a>
        </div>
    </header>

    {{-- Mensajes --}}
    @if (session("mensaje"))
    <div class="row alert alert-success">
        <strong>Éxito!</strong> {{ session("mensaje") }}
    </div>
    @endif

    @if (session("error"))
    <div class="row alert alert-danger">
        <strong>Ha ocurrido un error!</strong>

        @php
        $error = session("error");

        if (!is_array($error)) {
        $jsonError = json_decode($error, true);
        } else {
        $jsonError = $error;
        }
        @endphp

    </div>

    @endif
    {{-- Final de mensajes --}}

    <!-- start: page -->
    <div class="row">
        <a class="modal-with-form btn btn-default" href="#modalForm">Crear agente</a>
    </div>

    <div class="row">

        <div class="col">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Agentes</h2>
                </header>
                <div class="card-body">
                    <form action="" method="get" ng-controller="Formulario">
                        <div class="row">

                            <div class="form-group col-md-3">
                                <label for="departamento" class="sr-only">Tipo</label>
                                <select class="form-control" name="rol_id">
                                    <option value="">Tipo</option>
                                    @foreach ($roles as $rol)
                                    <option value="{{ $rol->id }}"
                                        {{ isset($request["rol_id"]) && $request["rol_id"] == $rol->id ? "selected" : "" }}>
                                        {{ $rol->nombre }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-3">
                                <label for="departamento" class="sr-only">Estado</label>
                                <select class="form-control" name="status">
                                    <option value="1">Activos</option>
                                    <option value="0"
                                        {{ isset($request["status"]) && $request["status"] == "0" ? "selected" : "" }}>
                                        Todos</option>
                                </select>
                            </div>

                            <div class="col-lg-3">
                                <input class="form-control mb-3" name="name" type="text"
                                    placeholder="Nombre y/o apellido"
                                    value="{{ isset($request["name"]) ? $request["name"] : "" }}">
                            </div>

                            <div class="col-lg-3">
                                <input class="form-control mb-3" name="code" type="text" placeholder="Código interno"
                                    value="{{ isset($request["code"]) ? $request["code"] : "" }}">
                            </div>

                            <div class="col-lg-3">
                                <input class="form-control mb-3" name="email" type="text" placeholder="E-mail"
                                    value="{{ isset($request["email"]) ? $request["email"] : "" }}">
                            </div>

                            <div class="col-lg-3">
                                <input class="form-control mb-3" name="phone" type="text" placeholder="Teléfono"
                                    value="{{ isset($request["phone"]) ? $request["phone"] : "" }}">
                            </div>

                            <div class="col-lg-6">
                                <button class="btn btn-primary">Buscar</button>

                            </div>
                        </div>
                    </form>
                    <table class="table table-bordered table-striped mb-0" id="datatable-default">
                        <thead>
                            <tr>
                                <th>Código interno</th>
                                <th>Tipo</th>
                                <th>Nombres</th>
                                <th>Apellidos</th>
                                <th>E-mail</th>
                                <th>Teléfono</th>
                                <th>Acción</th>
                                <th class="d-lg-none">Engine version</th>
                                <th class="d-lg-none">CSS grade</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($agentes as $agente)
                            <tr>
                                <td>{{ $agente->codigo_interno }}</td>
                                <td>{{ $agente->roles->nombre }}</td>
                                <td>{{ $agente->nombres }}</td>
                                <td>{{ $agente->apellidos }}</td>
                                <td>{{ $agente->email }}</td>
                                <td>{{ $agente->telefono }}</td>
                                <td>
                                    <a class="modal-with-form btn-default" href="#modalEdit_{{ $agente->id }}">
                                        <i class="el el-pencil"></i>
                                    </a>
                                    &nbsp;
                                    <a class="modal-with-form btn-default" href="#modalView_{{ $agente->id }}">
                                        <i class="el el-eye-open"></i>
                                    </a>
                                </td>
                                <td class="center d-lg-none">4</td>
                                <td class="center d-lg-none">X</td>
                            </tr>

                            {{-- Ver Usuario --}}
                            <div id="modalView_{{ $agente->id }}" class="modal-block modal-block-primary mfp-hide">
                                <section class="card">
                                    <header class="card-header">
                                        <h2 class="card-title">Vista rápida agente</h2>
                                    </header>
                                    <div class="card-body">
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label for="username">Código interno</label>
                                                <br><b>{{ $agente->codigo_interno }}</b>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="username">Nombre de usuario</label>
                                                <br><b>{{ $agente->nombre_usuario }}</b>
                                            </div>
                                            <div class="form-group col-md-6 mb-3 mb-lg-0">
                                                <label for="password">Clave</label>
                                                <br><b>{{ $agente->ingreso }}</b>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label for="ciudad">Ciudad</label>
                                                <br><b>{{ $agente->ciudad }}</b>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="estado">Estado</label>
                                                <br><b>{{ $agente->estados->nombre }}</b>
                                            </div>
                                            <div class="form-group col-md-5">
                                                <label for="rol">Rol</label>
                                                <br><b>{{ $agente->roles->nombre }}</b>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="name">Nombres</label>
                                                <br><b>{{ $agente->nombres }}</b>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="last_name">Apellidos</label>
                                                <br><b>{{ $agente->apellidos }}</b>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="email">Email</label>
                                                <br><b>{{ $agente->email }}</b>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="domus_token">Token en Domus</label>
                                                <br><b>{{ $agente->domus_token }}</b>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="comment">Comentario</label>
                                                <br><b>{{ $agente->comentario }}</b>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="comment2">Comentario adicional</label>
                                                <br><b>{{ $agente->comentario2 }}</b>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label for="document_type">Tipo doc</label>
                                                <br><b>CC</b>
                                            </div>

                                            <div class="form-group col-md-8">
                                                <label for="document">Número</label>
                                                <br><b>{{ $agente->documento }}</b>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label for="phone">Teléfono</label>
                                                <br><b>{{ $agente->telefono }}</b>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="cellphone">Celular</label>
                                                <br><b>{{ $agente->celular }}</b>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="whatsapp">Whatsapp</label>
                                                <br><b>{{ $agente->whatsapp }}</b>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="department">Departamento</label>
                                                <br><b>{{ $agente->departamentos->nombre }}</b>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label for="order">Orden</label>
                                                <br><b>{{ $agente->orden }}</b>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label>Fecha nacimiento</label>
                                                <div>
                                                    <b>{{ \Carbon\Carbon::parse($agente->fecha_nacimiento)->diffForHumans() }}
                                                        ({{ $agente->fecha_nacimiento }})</b>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Fecha de ingreso</label>
                                                <div>
                                                    <b>{{ \Carbon\Carbon::parse($agente->fecha_ingreso)->diffForHumans() }}
                                                        ({{ $agente->fecha_ingreso }})</b>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label>Fecha último cambio plan</label><br>
                                                <b>{{ \Carbon\Carbon::parse($agente->fecha_ultimo_cambio_plan)->diffForHumans() }}
                                                    ({{ $agente->fecha_ultimo_cambio_plan }})</b>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>Plan</label>
                                                <div>
                                                    <b>{{ $agente->plan }} %</b>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>Foto (no mayor a 120kb)</label>
                                                <br><img src="{{ $agente->foto }}" alt="" width="100%">
                                            </div>
                                        </div>
                                    </div>
                                    <footer class="card-footer">
                                        <div class="row">
                                            <div class="col-md-12 text-right">
                                                <button class="btn btn-default modal-dismiss">Cerrar</button>
                                            </div>
                                        </div>
                                    </footer>
                                </section>
                            </div>
                            {{-- Final de Ver Agente --}}

                            {{-- Editar Agente --}}
                            <div id="modalEdit_{{ $agente->id }}" class="modal-block modal-block-primary mfp-hide">
                                <section class="card">
                                    <form action="{{ url("admin/agentes/" . $agente->id) }}" method="post"
                                        enctype="multipart/form-data">
                                        <header class="card-header">
                                            <h2 class="card-title">Editar agente</h2>
                                        </header>
                                        <div class="card-body">

                                            {{ csrf_field() }}

                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="code">Código interno</label>
                                                    <input type="text" class="form-control" name="code"
                                                        value="{{ $agente->codigo_interno ?? "" }}"
                                                        placeholder="Código interno" required>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="username">Nombre de usuario</label>
                                                    <input type="hidden" name="username"
                                                        value="{{ $agente->nombre_usuario ?? "" }}">
                                                    <input type="text" disabled class="form-control"
                                                        value="{{ $agente->nombre_usuario ?? "" }}"
                                                        placeholder="Usuario" required>
                                                </div>
                                                <div class="form-group col-md-6 mb-3 mb-lg-0">
                                                    <label for="password">Clave</label>
                                                    <input type="text" class="form-control"
                                                        value="{{ $agente->ingreso ?? "" }}" name="password"
                                                        placeholder="Password" required>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label for="city">Ciudad</label>
                                                    <select id="city" class="form-control" name="city">
                                                        <option value="">Ciudad</option>
                                                        @foreach($ciudades as $ciudad)
                                                        <option value="{{ $ciudad->id }}"
                                                            {{ (isset($agente->ciudad_id) && $agente->ciudad_id == $ciudad->id) ? "selected" : "" }}>
                                                            {{ $ciudad->nombre }}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-3">
                                                    <label for="estado">Estado</label>
                                                    <select id="estado" name="status" class="form-control">
                                                        <option value="">Estado</option>
                                                        <option value="1"
                                                            {{ (isset($agente->estado_id) && $agente->estado_id == 1) ? "selected" : "" }}>
                                                            Activo
                                                        </option>
                                                        <option value="2"
                                                            {{ (isset($agente->estado_id) && $agente->estado_id == 2) ? "selected" : "" }}>
                                                            Inactivo
                                                        </option>
                                                        <option value="3"
                                                            {{ (isset($agente->estado_id) && $agente->estado_id == 3) ? "selected" : "" }}>
                                                            Retirado
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-5">
                                                    <label for="rol">Rol</label>
                                                    <select id="rol" class="form-control" name="role">
                                                        <option value="">Rol</option>
                                                        @foreach($roles as $rol)
                                                        <option value="{{ $rol->id }}"
                                                            {{ $rol->id == 3 ? "selected" : "" }}
                                                            {{ (isset($agente->rol_id) && $agente->rol_id == $rol->id) ? "selected" : "" }}>
                                                            {{ $rol->nombre }}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="name">Nombres</label>
                                                    <input type="text" class="form-control"
                                                        value="{{ $agente->nombres ?? "" }}" id="name" name="name"
                                                        placeholder="Nombres" required>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="last_name">Apellidos</label>
                                                    <input type="text" class="form-control"
                                                        value="{{ $agente->apellidos ?? "" }}" name="last_name"
                                                        id="last_name" placeholder="Apellidos" required>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="email">Email</label>
                                                    <input type="text" class="form-control"
                                                        value="{{ $agente->email ?? "" }}" name="email" id="email"
                                                        placeholder="Correo" required>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="domus_token">Token en Domus</label>
                                                    <input type="text" class="form-control"
                                                        value="{{ $agente->domus_token ?? "" }}" name="domus_token"
                                                        id="domus_token" placeholder="Token de Domus">
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="comment">Comentario</label>
                                                    <textarea name="comment"
                                                        class="form-control">{{ $agente->comentario ?? "" }}</textarea>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="comment2">Comentario adicional</label>
                                                    <textarea name="comment2"
                                                        class="form-control">{{ $agente->comentario2 ?? "" }}</textarea>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label for="document_type">Tipo doc</label>
                                                    <select id="document_type" name="document_type"
                                                        class="form-control">
                                                        <option value="">Tipo de documento</option>
                                                        @foreach ($tipos_documento as $tipo)
                                                        <option value="{{ $tipo->id }}"
                                                            {{ (isset($agente->tipo_documento_id) && $agente->tipo_documento_id == $tipo->id) ? "selected" : "" }}>
                                                            {{ $tipo->nombre }}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group col-md-8">
                                                    <label for="document">Número</label>
                                                    <input type="number" class="form-control"
                                                        value="{{ $agente->documento ?? "" }}" name="document"
                                                        id="document" placeholder="31950000" required>
                                                </div>
                                            </div>


                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label for="phone">Teléfono</label>
                                                    <input type="text" class="form-control"
                                                        value="{{ $agente->telefono ?? "" }}" name="phone" id="phone"
                                                        placeholder="7153573">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="cellphone">Celular</label>
                                                    <input type="text" class="form-control"
                                                        value="{{ $agente->celular ?? "" }}" name="cellphone"
                                                        id="cellphone" placeholder="3181234567">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="whatsapp">Whatsapp</label>
                                                    <input type="text" class="form-control"
                                                        value="{{ $agente->whatsapp ?? "" }}" name="whatsapp"
                                                        id="whatsapp" placeholder="3181234567">
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="department">Departamento</label>
                                                    <select name="department" id="" class="form-control" required>
                                                        <option value="">Departamento</option>
                                                        @foreach ($departamentos as $departamento)
                                                        <option value="{{ $departamento->id }}"
                                                            {{ (isset($agente->departamento_id) && $agente->departamento_id == $departamento->id) ? "selected" : "" }}>
                                                            {{ $departamento->nombre }}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="order">Orden</label>
                                                    <input type="number" class="form-control" name="order"
                                                        value="{{ $agente->orden ?? "" }}">
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>Fecha nacimiento</label>
                                                    {{-- <div data-plugin-datepicker data-plugin-skin="primary">
                                                    </div> --}}

                                                    <input type="date" class="form-control" name="birthdate"
                                                        value="{{ explode(" ", $agente->fecha_nacimiento)[0] ?? date("Y-m-d") }}">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Fecha de ingreso</label>
                                                    <input type="date" class="form-control" name="entry_date"
                                                        value="{{ explode(" ", $agente->fecha_ingreso)[0] ?? date("Y-m-d") }}">
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label>Fecha último cambio plan</label>
                                                    <input type="date" class="form-control" name="entry_date"
                                                        value="{{ session("content")["entry_date"] ?? date("Y-m-d") }}">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>Plan</label>
                                                    <input type="number" class="form-control" name="plan"
                                                        value="{{ $agente->plan ?? "" }}">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>Foto (no mayor a 120kb)</label>
                                                    <input type="file" name="foto"
                                                        accept="image/jpeg,image/png"></input>
                                                </div>
                                            </div>
                                        </div>
                                        <footer class="card-footer">
                                            <div class="row">
                                                <div class="col-md-12 text-right">
                                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                                    <button class="btn btn-default modal-dismiss">Cancelar</button>
                                                </div>
                                            </div>
                                        </footer>
                                    </form>
                                </section>
                            </div>
                            {{-- Final de Editar Usuario --}}
                            @endforeach
                        </tbody>
                    </table>

                    <br>
                    {{$agentes->links('pagination::bootstrap-4')}}
                </div>
            </section>
        </div>
    </div>


    {{-- Crear agente --}}
    <div id="modalForm" class="modal-block modal-block-primary mfp-hide">
        <section class="card">
            <form action="{{ url("admin/agentes") }}" method="post" enctype="multipart/form-data">
                <header class="card-header">
                    <h2 class="card-title">Crear agente</h2>
                </header>
                <div class="card-body">

                    {{ csrf_field() }}

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="code">Código interno</label>
                            <input type="text" class="form-control" name="code"
                                value="{{ session("content")["code"] ?? "" }}" placeholder="Código interno" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="username">Nombre de usuario</label>
                            <input type="text" class="form-control" id="username"
                                value="{{ session("content")["username"] ?? "" }}" name="username" placeholder="Usuario"
                                required>
                        </div>
                        <div class="form-group col-md-6 mb-3 mb-lg-0">
                            <label for="password">Clave</label>
                            <input type="text" class="form-control" id="password"
                                value="{{ session("content")["password"] ?? "" }}" name="password"
                                placeholder="Password" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="city">Ciudad</label>
                            <select id="city" class="form-control" name="city">
                                <option value="">Ciudad</option>
                                @foreach($ciudades as $ciudad)
                                <option value="{{ $ciudad->id }}"
                                    {{ (isset(session("content")["city"]) && session("content")["city"] == $ciudad->id) ? "selected" : "" }}>
                                    {{ $ciudad->nombre }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="estado">Estado</label>
                            <select id="estado" name="estado" class="form-control">
                                <option value="">Estado</option>
                                @foreach($estados as $estado)
                                <option value="{{ $estado->id }}"
                                    {{ (isset(session("content")["estado"]) && session("content")["estado"] == $estado->id) ? "selected" : "" }}>
                                    {{ $estado->nombre }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-5">
                            <label for="rol">Rol</label>
                            <select id="rol" class="form-control" name="role">
                                <option value="">Rol</option>
                                @foreach($roles as $rol)
                                <option value="{{ $rol->id }}" {{ $rol->id == 3 ? "selected" : "" }}
                                    {{ (isset(session("content")["role"]) && session("content")["role"] == $rol->id) ? "selected" : "" }}>
                                    {{ $rol->nombre }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="name">Nombres</label>
                            <input type="text" class="form-control" value="{{ session("content")["name"] ?? "" }}"
                                id="name" name="name" placeholder="Nombres" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="last_name">Apellidos</label>
                            <input type="text" class="form-control" value="{{ session("content")["last_name"] ?? "" }}"
                                name="last_name" id="last_name" placeholder="Apellidos" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" value="{{ session("content")["email"] ?? "" }}"
                                name="email" id="email" placeholder="Correo" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="domus_token">Token en Domus</label>
                            <input type="text" class="form-control"
                                value="{{ session("content")["domus_token"] ?? "" }}" name="domus_token"
                                id="domus_token" placeholder="Token de Domus">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="comment">Comentario</label>
                            <textarea name="comment"
                                class="form-control">{{ session("content")["comment"] ?? "" }}</textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="comment2">Comentario adicional</label>
                            <textarea name="comment2"
                                class="form-control">{{ session("content")["comment2"] ?? "" }}</textarea>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="document_type">Tipo doc</label>
                            <select id="document_type" name="document_type" class="form-control">
                                <option value="">Tipo de documento</option>
                                @foreach ($tipos_documento as $tipo)
                                <option value="{{ $tipo->id }}"
                                    {{ (isset(session("content")["document_type"]) && session("content")["document_type"] == $tipo->id) ? "selected" : "" }}>
                                    {{ $tipo->nombre }}
                                </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-8">
                            <label for="document">Número</label>
                            <input type="number" class="form-control" value="{{ session("content")["document"] ?? "" }}"
                                name="document" id="document" placeholder="31950000" required>
                        </div>
                    </div>


                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="phone">Teléfono</label>
                            <input type="text" class="form-control" value="{{ session("content")["phone"] ?? "" }}"
                                name="phone" id="phone" placeholder="7153573">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="cellphone">Celular</label>
                            <input type="text" class="form-control" value="{{ session("content")["cellphone"] ?? "" }}"
                                name="cellphone" id="cellphone" placeholder="3181234567">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="whatsapp">Whatsapp</label>
                            <input type="text" class="form-control" value="{{ session("content")["whatsapp"] ?? "" }}"
                                name="whatsapp" id="whatsapp" placeholder="3181234567">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="department">Departamento</label>
                            <select name="department" id="" class="form-control" required>
                                <option value="">Departamento</option>
                                @foreach ($departamentos as $departamento)
                                <option value="{{ $departamento->id }}"
                                    {{ (isset(session("content")["department"]) && session("content")["department"] == $departamento->id) ? "selected" : "" }}>
                                    {{ $departamento->nombre }}
                                </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="order">Orden</label>
                            <input type="number" class="form-control" name="order"
                                value="{{ session("content")["order"] ?? "" }}">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Fecha nacimiento</label>
                            <input type="date" class="form-control" name="birthdate"
                                value="{{ session("content")["birthdate"] ?? date("Y-m-d") }}">
                        </div>

                        <div class="form-group col-md-6">
                            <label>Fecha de ingreso</label>
                            <input type="date" class="form-control" name="entry_date"
                                value="{{ session("content")["entry_date"] ?? date("Y-m-d") }}">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label>Fecha último cambio plan</label>
                            <input type="date" class="form-control" name="entry_date"
                                value="{{ session("content")["entry_date"] ?? date("Y-m-d") }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Plan</label>
                            <input type="number" class="form-control" name="plan"
                                value="{{ session("content")["plan"] ?? "" }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Foto (no mayor a 120kb)</label>
                            <input type="file" name="foto" accept="image/jpeg,image/png"></input>
                        </div>
                    </div>
                </div>
                <footer class="card-footer">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                            <button class="btn btn-default modal-dismiss">Cancelar</button>
                        </div>
                    </div>
                </footer>
            </form>
        </section>
    </div>
    {{-- Final de formulario de crear usuario --}}