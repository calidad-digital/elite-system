<section ng-controller="Ventas" role="main" class="content-body">
    <header class="page-header">
        <h2>Seguimiento de venta</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>ventas</span></li>
                <li><span>Seguimiento&nbsp;&nbsp;&nbsp;</span></li>
            </ol>
            <a class="" data-open=""></a>
        </div>
    </header>

    {{-- Mensajes --}}
    @if (session("mensaje"))
    <div class="row alert alert-success">
        <strong>Éxito!</strong> {{ session("mensaje") }}
    </div>
    @endif

    @if (session("error"))
    <div class="row alert alert-danger">
        <strong>Ha ocurrido un error!</strong>
        @foreach (session("error") as $error)
        @foreach ($error as $key => $value)
        - {{ $value }} <br>
        @endforeach
        @endforeach
    </div>
    @endif
    {{-- Final de mensajes --}}

    <div class="row">

        <div class="col-lg-12">


            <br>


            <section class="card" ng-init="getSeguimientosList({{$venta->id}})" ng-cloak>
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>
                    <h2 class="card-title">Seguimientos de la venta MLS #
                        {{$venta->inmuebles->codigoMLS}}</h2>
                </header>
                <div class="card-body">
                    <table class="table table-responsive-md table-striped mb-0">
                        <thead>
                            <tr>
                                <th>Estado</th>
                                <th>Fecha-hora</th>
                                <th>Usuario</th>
                                <th>Descripción</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody>


                            <tr ng-repeat="seg in listaSeguimientos">
                                <td>
                                    <span class="ecommerce-status on-hold">
                                        <%seg.estados.nombre%>
                                    </span>
                                </td>
                                <td><%seg.fecha_seguimiento%></td>
                                <td><%seg.encargado.nombres %> <%seg.encargado.apellidos %></td>
                                <td><%seg.comentario | limitTo:'15' %>...</td>
                                <td>
                                    <a class="modal-with-form btn-default" href="#modalVerseg_">
                                        <i class="el el-eye-open" data-toggle="tooltip" data-placement="top"
                                            data-original-title="Ver este seguimiento"></i>
                                    </a>
                                    &nbsp;
                                </td>
                            </tr>

                            <div id="modalVerseg_" class="modal-block modal-block-primary mfp-hide modal-xl">
                                <section class="card">
                                    <form action="" method="post" enctype="multipart/form-data">
                                        <header class="card-header">
                                            <h2 class="card-title">
                                                Ver seguimiento de la venta MLS código XXX
                                            </h2>
                                        </header>

                                        <div class="card-body">

                                            <div class="form-row">
                                                <div class="form-group col-md-4">
                                                    <label for="status">
                                                        <b>Estado</b>
                                                    </label>
                                                    <p></p>
                                                </div>

                                                <div class="form-group col-md-4">
                                                    <label for="broker">
                                                        <b>Encargado</b>
                                                    </label>
                                                    <p>
                                                        Camilo Romero
                                                    </p>
                                                </div>

                                                <div class="form-group col-md-12">
                                                    <label>
                                                        <b>Fecha</b>
                                                    </label>
                                                    <div>
                                                        12/09/2021
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="description">
                                                        <b>Descripción</b>
                                                    </label>
                                                    <p>Comentario del seguimietno</p>
                                                </div>
                                            </div>
                                        </div>
                                        <footer class="card-footer">
                                            <div class="row">
                                                <div class="col-md-12 text-right">
                                                    <button class="btn btn-default modal-dismiss">
                                                        Cerrar
                                                    </button>
                                                </div>
                                            </div>
                                        </footer>
                                    </form>
                                </section>
                            </div>


                        </tbody>
                    </table>


                    <br>
                    <a class="modal-with-form mb-1 mt-1 mr-1 btn btn-success" href="#modalFormseg">
                        <i class="el el-check"></i> Crear seguimiento
                    </a>

                </div>
            </section>

        </div>
    </div>

</section>


<div ng-controller="Ventas" id="modalFormseg" class="modal-block modal-block-primary mfp-hide modal-xl">
    <section class="card" ng-init="getEstadosSeguimientos();getallUsers('{{$usuario->id}}')">
        <form action="/admin/ventas/seguimiento/crear" method="post" enctype="multipart/form-data"
            ng-controller="FormSeguimiento">
            @csrf
            <header class="card-header">
                <h2 class="card-title">
                    Crear seguimiento
                </h2>
            </header>


            <div class="card-body">



                <input type="hidden" name="venta_id" value="{{$venta->id}}">

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="status">Estado</label>
                        <select id="status" class="form-control" name="estado_venta_id" ng-model="estado" required>
                            <option value="">Seleccionar estado</option>
                            <option ng-repeat="estado in listaEstadosSeguimientos" value="<%estado.id%>">
                                <%estado.nombre%>
                            </option>
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="broker">Encargado</label>
                        <input type="hidden" value="{{$venta->usuarios->id}}" name="usuario_encargado"
                            ng-model="usuario_encargado">
                        <div>
                            <input class="form-control" type="text"
                                value="{{$venta->usuarios->nombres}} {{$venta->usuarios->apellidos}}" disabled>
                        </div>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="broker" data-toggle="tooltip" data-placement="top"
                            data-original-title="Adjunte comprobantes de pago, facturas, entre otros">Documento
                            soporte</label>
                        <input type="file" class="file" name="soporte">
                    </div>

                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label>Fecha</label>
                        <div>
                            <input type="date" class="form-control" name="fecha_seguimiento" value="">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="description">Descripción</label>
                        <textarea class="form-control" rows="3" name="comentario" id="textareaAutosize"></textarea>
                    </div>
                </div>
            </div>

            <br>

            <footer class="card-footer">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-default modal-dismiss">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </div>
            </footer>
        </form>
    </section>
</div>