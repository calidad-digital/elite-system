<section role="main" class="content-body">
    <header class="page-header">
        <h2>Ventas</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>Ventas</span></li>
                <li><span>Facturar venta&nbsp;&nbsp;&nbsp;</span></li>
            </ol>
            <a class="" data-open=""></a>
        </div>
    </header>
    <div class="row" ng-controller="Ventas">

        <div class="col-lg-12">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Listado de ventas</h2>
                </header>
                <div class="card-body">
                    <form action="" method="get" ng-controller="Formulario">
                        <div class="row">
                            <div class="form-group col-md-3"
                            ng-init="usuario = '{{ isset($request['usuario']) ? $request['usuario'] : $usuario->id }}'">
                            <label for="usuario" class="sr-only">Agente</label>
                            <select name="usuario" class="form-control" ng-model="usuario" id="usuario" {{ $usuario->rol_id == 1 || $usuario->rol_id == 6 ? '' : 'disabled' }}>
                                <option value="">Agente</option>
                                @foreach ($agentes as $agt)
                                    <option value="{{ $agt->id }}" {{ $agt->id == $usuario->id ? 'selected' : '' }}>
                                        {{ $agt->nombres }} {{ $agt->apellidos }}
                                    </option>
                                @endforeach
                            </select>

                        </div>
                        <div class="col-lg-2">
                            <input class="form-control mb-3" name="mls" type="text" placeholder="# MLS" value="{{ isset($request['mls']) ? $request['mls'] : '' }}">
                        </div>
                        <div class="col-lg-6">
                            <button class="btn btn-primary">Buscar</button>
                        </div>
                    </div>
                </form>

                <!-- VENTAS en estado pendientes -->

                <table class="table table-bordered table-striped mb-0" id="datatable-default">
                    <thead>
                        <tr>
                            <th colspan="12">PENDIENTES</th>
                        </tr>
                        <tr>
                            <th>MLS</th>
                            <th>Estado</th>
                            <th>Agente</th>
                            <th>Tipo</th>
                            <th>barrio</th>
                            <th>Valor</th>
                            <th>Fecha - Hora</th>
                            <th>Vencimiento</th>
                            <th>Acción</th>
                            <th class="d-lg-none">Engine version</th>
                            <th class="d-lg-none">CSS grade</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($ventas as $venta)
                            @if ($venta->estado_venta_id == 1)
                                <tr>
                                    <td>{{ $venta->inmuebles->codigoMLS }}</td>
                                    <td>{{ $venta->inmuebles->estados->nombre }}</td>
                                    <td>{{ $venta->usuarios->nombres }} {{ $venta->usuarios->apellidos }}</td>
                                    <td>{{ $venta->inmuebles->tipos_inmueble->nombre }}</td>
                                    <td>{{ $venta->inmuebles->barrio_comun }}</td>
                                    <td ng-cloak>
                                        <% '{{ $venta->inmuebles->gestion_id == 1 ? $venta->inmuebles->canon : $venta->inmuebles->venta }}' | currency:"$ ":0 %>
                                    </td>
                                    <td>
                                        {{ \Carbon\Carbon::parse($venta->created_at)->diffForHumans() }}
                                    </td>
                                    <td>
                                        <span data-toggle="tooltip" data-placement="top"
                                        data-original-title="14/09/2021 13:08:14">
                                        en 12 horas
                                    </span>
                                </td>
                                <td>
                                    <a class="" href="/admin/ventas/editar/{{ $venta->id }}" target="_blank">
                                        <i class="el el-pencil" data-toggle="tooltip" data-placement="top" data-original-title="Editar venta"></i>
                                    </a>
                                    &nbsp;
                                    <a class="modal-with-form " href="#sellDetail" ng-click="detalleVentas({{ $venta }})">
                                        <i class="el el-eye-open" data-toggle="tooltip" data-placement="top" data-original-title="Ver venta"></i>
                                    </a>
                                    &nbsp;
                                    <a href="/admin/ventas/4/{{ $venta->id }}">
                                        <i class="el el-check" data-toggle="tooltip" data-placement="top" data-original-title="Seguimientos"></i>
                                    </a>
                                    &nbsp;
                                    <a href="/admin/ventas/2/{{ $venta->id }}">
                                        <i class="el el-usd" data-toggle="tooltip" data-placement="top" data-original-title="Factura"></i>
                                    </a>
                                    <a class="modal-with-form btn-default" href="#modalCorreos_{{ $venta->id }}">
                                        <i class="fa fa-check-circle" data-toggle="tooltip" data-placement="top" data-original-title="Ver aprobaciones"></i>
                                    </a>

                                    @if ($usuario->rol_id == 1)
                                        <a class="btn-default" href="{{ url("alt/contrato-venta/" . base64_encode($venta->id . "_" . $usuario->id)) }}" target="_blank">
                                            <i class="fa fa-book" data-toggle="tooltip" data-placement="top" data-original-title="Ver borrador de contrato"></i>
                                        </a>
                                    @endif
                                </td>
                                <td class="center d-lg-none">4</td>
                                <td class="center d-lg-none">X</td>
                            </tr>
                        @endif
                    @endforeach


                </tbody>
            </table>
            <br>

            {{$ventas->links('pagination::bootstrap-4')}}

            <!-- VENTAS en estado vendido -->
            <br>
            <table class="table table-bordered table-striped mb-0" id="datatable-default">
                <thead>
                    <tr>
                        <th colspan="12">VENDIDAS</th>

                    </tr>
                    <tr>
                        <th>MLS</th>
                        <th>Estado</th>
                        <th>Agente</th>
                        <th>Tipo</th>
                        <th>barrio</th>
                        <th>Valor</th>
                        <th>Fecha - Hora</th>
                        <th>Vencimiento</th>
                        <th>Acción</th>
                        <th class="d-lg-none">Engine version</th>
                        <th class="d-lg-none">CSS grade</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($ventas as $venta)
                        @if ($venta->estado_venta_id == 2)
                            <tr>
                                <td>{{ $venta->inmuebles->codigoMLS }}</td>
                                <td>{{ $venta->inmuebles->estados->nombre }}</td>
                                <td>{{ $venta->usuarios->nombres }} {{ $venta->usuarios->apellidos }}</td>
                                <td>{{ $venta->inmuebles->tipos_inmueble->nombre }}</td>
                                <td>{{ $venta->inmuebles->barrio_comun }}</td>
                                <td ng-cloak>
                                    <% '{{ $venta->inmuebles->gestion_id == 1 ? $venta->inmuebles->canon : $venta->inmuebles->venta }}' | currency:"$ ":0 %>
                                </td>
                                <td>
                                    {{ \Carbon\Carbon::parse($venta->created_at)->diffForHumans() }}
                                </td>
                                <td>
                                    <span data-toggle="tooltip" data-placement="top"
                                    data-original-title="14/09/2021 13:08:14">
                                    en 12 horas
                                </span>
                            </td>
                            <td>
                                <a class="" href="" target="_blank">
                                    <i class="el el-pencil" data-toggle="tooltip" data-placement="top"
                                    data-original-title="Editar venta"></i>
                                </a>
                                &nbsp;
                                <a class="modal-with-form " href="#sellDetail">
                                    <i class="el el-eye-open" data-toggle="tooltip" data-placement="top"
                                    data-original-title="Ver venta"></i>
                                </a>
                                &nbsp;
                                <a href="/admin/ventas/4">
                                    <i class="el el-check" data-toggle="tooltip" data-placement="top"
                                    data-original-title="Seguimientos"></i>
                                </a>
                            </td>
                            <td class="center d-lg-none">4</td>
                            <td class="center d-lg-none">X</td>
                        </tr>
                    @endif
                @endforeach

            </tbody>
        </table>

        <!-- VENTAS CRUZADAS, estas se toman de la misma tabla pero filtrando el campo cruzada -->
        <br>
        <table class="table table-bordered table-striped mb-0" id="datatable-default">
            <thead>
                <tr>
                    <th colspan="12">CRUZADAS</th>

                </tr>
                <tr>
                    <th>MLS</th>
                    <th>Estado</th>
                    <th>Agente</th>
                    <th>Tipo</th>
                    <th>barrio</th>
                    <th>Valor</th>
                    <th>Fecha - Hora</th>
                    <th>Vencimiento</th>
                    <th>Acción</th>
                    <th class="d-lg-none">Engine version</th>
                    <th class="d-lg-none">CSS grade</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($ventas as $venta)
                    @if ($venta->estado_venta_id == 3)
                        <tr>
                            <td>{{ $venta->inmuebles->codigoMLS }}</td>
                            <td>{{ $venta->inmuebles->estados->nombre }}</td>
                            <td>{{ $venta->usuarios->nombres }} {{ $venta->usuarios->apellidos }}</td>
                            <td>{{ $venta->inmuebles->tipos_inmueble->nombre }}</td>
                            <td>{{ $venta->inmuebles->barrio_comun }}</td>
                            <td ng-cloak>
                                <% '{{ $venta->inmuebles->gestion_id == 1 ? $venta->inmuebles->canon : $venta->inmuebles->venta }}' | currency:"$ ":0 %>
                            </td>
                            <td>
                                {{ \Carbon\Carbon::parse($venta->created_at)->diffForHumans() }}
                            </td>
                            <td>
                                <span data-toggle="tooltip" data-placement="top"
                                data-original-title="14/09/2021 13:08:14">
                                en 12 horas
                            </span>
                        </td>
                        <td>
                            <a href="" target="_blank">
                                <i class="el el-pencil" data-toggle="tooltip" data-placement="top"
                                data-original-title="Editar venta"></i>
                            </a>
                            &nbsp;
                            <a class="modal-with-form " href="#sellDetail">
                                <i class="el el-eye-open" data-toggle="tooltip" data-placement="top"
                                data-original-title="Ver venta"></i>
                            </a>
                            &nbsp;
                            <a href="/admin/ventas/4">
                                <i class="el el-check" data-toggle="tooltip" data-placement="top"
                                data-original-title="Seguimientos"></i>
                            </a>
                        </td>
                        <td class="center d-lg-none">4</td>
                        <td class="center d-lg-none">X</td>
                    </tr>
                @endif
            @endforeach

        </tbody>
    </table>
</div>
</section>
</div>

</div>

<div ng-controller="Ventas" id="sellDetail" class="modal-block modal-block-primary mfp-hide modal-xl">
    <section class="card">
        <form action="" method="post" enctype="multipart/form-data">
            <header class="card-header">
                <h2 class="card-title">
                    Vista del inmueble código <% generalfactory.ventaDetail.inmuebles.codigo %>
                </h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-4 col-xl-4 mb-4 mb-xl-0">
                        <section class="card">
                            <div class="card-body">
                                <div class="thumb-info mb-3 forcedgallery">
                                    {{-- <% generalfactory.ventaDetail.inmuebles %> --}}
                                    <img ng-if="generalfactory.ventaDetail.inmuebles.imagenes.length == 0"
                                    src="{{ asset('assets/img/flags.png') }}" class="rounded img-fluid gp"
                                    href="{{ asset('assets/img/flags.png') }}">
                                    <img ng-repeat="img in generalfactory.ventaDetail.inmuebles.imagenes"
                                    ng-if="$index < 1" src="<% img.url %>" class="rounded img-fluid gp"
                                    href="<% img.url %>">
                                    <img ng-repeat="img in generalfactory.ventaDetail.inmuebles.imagenes"
                                    ng-if="$index >= 1" src="<% img.url %>" class="rounded img-fluid gp hidden"
                                    href="<% img.url %>">


                                    <div class="thumb-info-title">
                                        <span class="thumb-info-inner">
                                            <% generalfactory.ventaDetail.inmuebles.tipos_inmueble.nombre %>
                                        </span>
                                        <span class="thumb-info-type">
                                            <% generalfactory.ventaDetail.inmuebles.gestiones.nombre %>
                                        </span>
                                    </div>
                                </div>

                                <div class="widget-toggle-expand mb-3">
                                    <div class="widget-header">
                                        <h5 class="mb-2">
                                            <% generalfactory.ventaDetail.inmuebles.barrio_comun %>
                                        </h5>
                                    </div>
                                    <div class="widget-content-expanded">
                                        <p>
                                            <% generalfactory.ventaDetail.inmuebles.ciudades.nombre %>, Colombia
                                            <br><b>Zona:</b> <% generalfactory.ventaDetail.inmuebles.zonas.nombre %>
                                            <br><b>Localidad:</b>
                                            <% generalfactory.ventaDetail.inmuebles.localidades.nombre %>
                                            <br><b>Estrato:</b>
                                            <% generalfactory.ventaDetail.inmuebles.estratos.nombre %>
                                            <br><b>Año
                                                construcción:</b><% generalfactory.ventaDetail.inmuebles.anio_construccion %>
                                                <br><b>Nivel:</b> <% generalfactory.ventaDetail.inmuebles.nivel %>
                                            </p>
                                        </div>
                                    </div>

                                    <hr class="dotted short">
                                    <h5 class="mb-2 mt-3">Especificaciones</h5>
                                    <p>
                                        <br><b>Habitaciones:</b> <% generalfactory.ventaDetail.inmuebles.habitaciones %>
                                        <br><b>Baños:</b> <% generalfactory.ventaDetail.inmuebles.banios %>
                                        <br><b>Área lote:</b> <% generalfactory.ventaDetail.inmuebles.area_lote %> m²
                                        <br><b>Área construida:</b>
                                        <% generalfactory.ventaDetail.inmuebles.area_construida %>
                                        m²
                                        <br><b>Área privada:</b> <% generalfactory.ventaDetail.inmuebles.area_privada %>
                                        m²
                                        <br><b>Frente:</b> <% generalfactory.ventaDetail.inmuebles.frente %> m²
                                        <br><b>Fondo:</b> <% generalfactory.ventaDetail.inmuebles.fondo %> m²
                                        <br><b>Parq sencillos:</b>
                                        <% generalfactory.ventaDetail.inmuebles.parqueaderos_sencillos %>
                                    </p>

                                    <hr class="dotted short">
                                    <h5 class="mb-2 mt-3">
                                        Parqueaderos <% generalfactory.ventaDetail.inmuebles.parqueaderos %>
                                    </h5>
                                    <p>
                                        <br><b>Parq cubiertos:</b>
                                        <% generalfactory.ventaDetail.inmuebles.parqueaderos_cubiertos %>
                                        <br><b>Parq descubiertos:</b>
                                        <% generalfactory.ventaDetail.inmuebles.parqueaderos_descubiertos %>
                                        <br><b>Parq dobles:</b>
                                        <% generalfactory.ventaDetail.inmuebles.parqueaderos_dobles %>
                                        <br><b>Parq sencillos:</b>
                                        <% generalfactory.ventaDetail.inmuebles.parqueaderos_sencillos %>
                                    </p>

                                </div>
                            </section>

                        </div>
                        <div class="col-lg-8 col-xl-8">

                            <div class="card card-modern">
                                <div class="card-header">
                                    <h2 class="card-title">MLS <% generalfactory.ventaDetail.inmuebles.codigoMLS %>
                                    </h2>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-auto mr-xl-5 pr-xl-5 mb-4 mb-xl-0">
                                            <ul class="list list-unstyled list-item-bottom-space-0">
                                                <li><b>Código: </b> <% generalfactory.ventaDetail.inmuebles.codigo %>
                                                </li>
                                                <li><b>Destinación: </b>
                                                    <% generalfactory.ventaDetail.inmuebles.destinos.nombre %></li>
                                                    <li><b>Canon: </b>$
                                                        <% generalfactory.ventaDetail.inmuebles.canon %>
                                                    </li>
                                                    <li><b>Admón: </b>$
                                                        <% generalfactory.ventaDetail.inmuebles.administracion %>
                                                    </li>
                                                    <li><b>Venta: </b>$
                                                        <% generalfactory.ventaDetail.inmuebles.venta %>
                                                    </li>
                                                    <li><b># Matrícula: </b>
                                                        <% generalfactory.ventaDetail.inmuebles.matricula %>
                                                    </li>
                                                    <li><b>Chip: </b> <% generalfactory.ventaDetail.inmuebles.chip %>
                                                    </li>
                                                    <li><b>Cédula catastral: </b>
                                                        <% generalfactory.ventaDetail.inmuebles.cedula_catastral %>
                                                    </li>
                                                    <li><b># Matrícula garaje: </b>
                                                        <% generalfactory.ventaDetail.matricula_garaje %>
                                                    </li>
                                                    <li><b>Chip garaje: </b> <% generalfactory.ventaDetail.chip_garaje %>
                                                    </li>
                                                    <li><b>Cédula catastral garaje: </b>
                                                        <% generalfactory.ventaDetail.cedula_catastral_garaje %>
                                                    </li>
                                                    <li><b># Matrícula depósito: </b>
                                                        <% generalfactory.ventaDetail.matricula_deposito %>
                                                    </li>
                                                    <li><b>Chip depósito: </b>
                                                        <% generalfactory.ventaDetail.chip_deposito %>
                                                    </li>
                                                    <li><b>Cédula catastral depósito: </b>
                                                        <% generalfactory.ventaDetail.cedula_catastral_deposito %>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tabs">
                                    <ul class="nav nav-tabs tabs-primary">
                                        <li class="nav-item active">
                                            <a class="nav-link" href="#selling" data-toggle="tab">Negociación</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#buyers" data-toggle="tab">Compradores</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#pays" data-toggle="tab">Pagos</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#reading" data-toggle="tab">Escrituración</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#docs" data-toggle="tab">Documentos</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="selling" class="tab-pane active">

                                            <div class="p-3">

                                                <h5 class="mb-3">Precio</h5>
                                                <p> <% generalfactory.ventaDetail.negociacion_precio | currency%>
                                                </p>
                                                <h5 class="mb-3">Pagado promesa</h5>
                                                <p> <% generalfactory.ventaDetail.negociacion_pagado_promesa | currency%>
                                                </p>
                                                <h5 class="mb-3">Saldos Pendientes</h5>
                                                <p> <% generalfactory.ventaDetail.negociacion_saldo_pendiente| currency %>
                                                </p>
                                                <h5 class="mb-3">Arras</h5>
                                                <p> <% generalfactory.ventaDetail.negociacion_arras| currency %></p>
                                                <h5 class="mb-3">Arras a favor de</h5>
                                                <p> <% generalfactory.ventaDetail.negociacion_arras_favor %></p>

                                            </div>

                                        </div>
                                        <div id="reading" class="tab-pane">

                                            <div class="p-3">

                                                <h5 class="mb-3">Fecha escritura pública</h5>
                                                <p> <% generalfactory.ventaDetail.escritura_fecha_publica %></p>
                                                <h5 class="mb-3">Notaría</h5>
                                                <p> <% generalfactory.ventaDetail.escritura_notaria %></p>
                                                <h5 class="mb-3">Fecha entrega inmueble</h5>
                                                <p> <% generalfactory.ventaDetail.escritura_fecha_entrega_inmueble %></p>
                                                <h5 class="mb-3">Predial asumido por</h5>
                                                <p> <% generalfactory.ventaDetail.escritura_predial_asumido %></p>
                                                <h5 class="mb-3">Gastos notariales asumidos por</h5>
                                                <p> <% generalfactory.ventaDetail.escritura_gasto_notarial_asumido %></p>
                                                <h5 class="mb-3">Beneficiado por instrumentos públicos</h5>
                                                <p> <% generalfactory.ventaDetail.escritura_beneficiado_instrumento_publico %>
                                                </p>
                                                <h5 class="mb-3">¿Negociación adicional?</h5>
                                                <p> <% generalfactory.ventaDetail.escritura_negociacion_adicional %></p>
                                                <h5 class="mb-3">pago comisión Elite</h5>
                                                <p> <% generalfactory.ventaDetail.escritura_pago_comision_elite %></p>
                                                <h5 class="mb-3">Observaciones</h5>
                                                <p> <% generalfactory.ventaDetail.escritura_observacion %></p>

                                            </div>

                                        </div>

                                        <div id="buyers" class="tab-pane">

                                            <div class="p-3" ng-if="generalfactory.ventaDetail.comprador_1_nombre">

                                                <h4 class="mb-3">Comprador 1</h4>
                                                <h5 class="mb-3">Nombres y apellidos</h5>
                                                <p> <% generalfactory.ventaDetail.comprador_1_nombre %></p>
                                                <h5 class="mb-3">Número de cédula</h5>
                                                <p> <% generalfactory.ventaDetail.comprador_1_cedula %></p>
                                                <h5 class="mb-3">Estado civil</h5>
                                                <p> <% generalfactory.ventaDetail.comprador_1_estado_civil %></p>
                                                <h5 class="mb-3">Dirección</h5>
                                                <p> <% generalfactory.ventaDetail.comprador_1_direccion %></p>
                                                <h5 class="mb-3">Teléfono</h5>
                                                <p> <% generalfactory.ventaDetail.comprador_1_telefono %></p>
                                                <h5 class="mb-3">Correo electrónico</h5>
                                                <p> <% generalfactory.ventaDetail.comprador_1_email %></p>
                                                <h5 class="mb-3">Cédula</h5>
                                                <a ng-if="generalfactory.ventaDetail.comprador_1_cedula_url"
                                                href="<%generalfactory.ventaDetail.comprador_1_cedula_url%>"
                                                target="_blank">
                                                Archivo</a>

                                            </div>
                                            <div class="p-3" ng-if="generalfactory.ventaDetail.comprador_2_nombre">

                                                <h4 class="mb-3">Comprador 2</h4>
                                                <h5 class="mb-3">Nombres y apellidos</h5>
                                                <p> <% generalfactory.ventaDetail.comprador_2_nombre %></p>
                                                <h5 class="mb-3">Número de cédula</h5>
                                                <p> <% generalfactory.ventaDetail.comprador_2_cedula %></p>
                                                <h5 class="mb-3">Estado civil</h5>
                                                <p> <% generalfactory.ventaDetail.comprador_2_estado_civil %></p>
                                                <h5 class="mb-3">Dirección</h5>
                                                <p> <% generalfactory.ventaDetail.comprador_2_direccion %></p>
                                                <h5 class="mb-3">Teléfono</h5>
                                                <p> <% generalfactory.ventaDetail.comprador_2_telefono %></p>
                                                <h5 class="mb-3">Correo electrónico</h5>
                                                <p> <% generalfactory.ventaDetail.comprador_2_email %></p>
                                                <h5 class="mb-3">Cédula</h5>
                                                <a ng-if="generalfactory.ventaDetail.comprador_2_cedula_url"
                                                href="<%generalfactory.ventaDetail.comprador_2_cedula_url%>"
                                                target="_blank">
                                                Archivo</a>

                                            </div>
                                            <div class="p-3" ng-if="generalfactory.ventaDetail.comprador_3_nombre">

                                                <h4 class="mb-3">Comprador 3</h4>
                                                <h5 class="mb-3">Nombres y apellidos</h5>
                                                <p> <% generalfactory.ventaDetail.comprador_3_nombre %></p>
                                                <h5 class="mb-3">Número de cédula</h5>
                                                <p> <% generalfactory.ventaDetail.comprador_3_cedula %></p>
                                                <h5 class="mb-3">Estado civil</h5>
                                                <p> <% generalfactory.ventaDetail.comprador_3_estado_civil %></p>
                                                <h5 class="mb-3">Dirección</h5>
                                                <p> <% generalfactory.ventaDetail.comprador_3_direccion %></p>
                                                <h5 class="mb-3">Teléfono</h5>
                                                <p> <% generalfactory.ventaDetail.comprador_3_telefono %></p>
                                                <h5 class="mb-3">Correo electrónico</h5>
                                                <p> <% generalfactory.ventaDetail.comprador_3_email %></p>
                                                <h5 class="mb-3">Cédula</h5>
                                                <a href="<%generalfactory.ventaDetail.comprador_3_cedula_url%>"
                                                    target="_blank">
                                                    Archivo</a>

                                                </div>
                                                <div class="p-3" ng-if="generalfactory.ventaDetail.comprador_4_nombre">

                                                    <h4 class="mb-3">Comprador 4</h4>
                                                    <h5 class="mb-3">Nombres y apellidos</h5>
                                                    <p> <% generalfactory.ventaDetail.comprador_4_nombre %></p>
                                                    <h5 class="mb-3">Número de cédula</h5>
                                                    <p> <% generalfactory.ventaDetail.comprador_4_cedula %></p>
                                                    <h5 class="mb-3">Estado civil</h5>
                                                    <p> <% generalfactory.ventaDetail.comprador_4_estado_civil %></p>
                                                    <h5 class="mb-3">Dirección</h5>
                                                    <p> <% generalfactory.ventaDetail.comprador_4_direccion %></p>
                                                    <h5 class="mb-3">Teléfono</h5>
                                                    <p> <% generalfactory.ventaDetail.comprador_4_telefono %></p>
                                                    <h5 class="mb-3">Correo electrónico</h5>
                                                    <p> <% generalfactory.ventaDetail.comprador_4_email %></p>
                                                    <h5 class="mb-3">Cédula</h5>
                                                    <a href="<%generalfactory.ventaDetail.comprador_4_cedula_url%>"
                                                        target="_blank">
                                                        Archivo</a>

                                                    </div>

                                                </div>
                                                <div id="pays" class="tab-pane">

                                                    <div class="p-3" ng-if="generalfactory.ventaDetail.pago_1_valor_pago">

                                                        <h4 class="mb-3">Pago 1</h4>
                                                        <h5 class="mb-3">Fecha promesa y pago</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_1_fecha_promesa_pago %></p>
                                                        <h5 class="mb-3">Notaría</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_1_notaria %></p>
                                                        <h5 class="mb-3">Medio de pago</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_1_medio_pago %></p>
                                                        <h5 class="mb-3">Valor del primer pago</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_1_valor_pago | currency%></p>
                                                        <h5 class="mb-3">Datos si es cheque o transferencia</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_1_cheque_transferencia %></p>
                                                        <h5 class="mb-3">Origen recursos</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_1_origen_recurso %></p>

                                                    </div>
                                                    <div class="p-3" ng-if="generalfactory.ventaDetail.pago_2_valor_pago">

                                                        <h4 class="mb-3">Pago 2</h4>
                                                        <h5 class="mb-3">Fecha promesa y pago</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_2_fecha_pago %></p>
                                                        <h5 class="mb-3">Pago 2 a favor de</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_2_pago_favor %></p>
                                                        <h5 class="mb-3">Medio de pago</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_2_medio_pago %></p>
                                                        <h5 class="mb-3">Valor del primer pago</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_2_valor_pago %></p>
                                                        <h5 class="mb-3">Datos si es cheque o transferencia</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_2_cheque_transferencia %></p>
                                                        <h5 class="mb-3">Origen recursos</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_2_origen_recurso %></p>

                                                    </div>

                                                    <div class="p-3" ng-if="generalfactory.ventaDetail.pago_3_valor_pago">

                                                        <h4 class="mb-3">Pago 3</h4>
                                                        <h5 class="mb-3">Fecha promesa y pago</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_3_fecha_pago %></p>
                                                        <h5 class="mb-3">Pago 3 a favor de</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_3_pago_favor %></p>
                                                        <h5 class="mb-3">Medio de pago</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_3_medio_pago %></p>
                                                        <h5 class="mb-3">Valor del primer pago</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_3_valor_pago %></p>
                                                        <h5 class="mb-3">Datos si es cheque o transferencia</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_3_cheque_transferencia %></p>
                                                        <h5 class="mb-3">Origen recursos</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_3_origen_recurso %></p>

                                                    </div>
                                                    <div class="p-3" ng-if="generalfactory.ventaDetail.pago_4_valor_pago">

                                                        <h4 class="mb-3">Pago 4</h4>
                                                        <h5 class="mb-3">Fecha promesa y pago</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_4_fecha_pago %></p>
                                                        <h5 class="mb-3">Pago 4 a favor de</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_4_pago_favor %></p>
                                                        <h5 class="mb-3">Medio de pago</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_4_medio_pago %></p>
                                                        <h5 class="mb-3">Valor del primer pago</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_4_valor_pago %></p>
                                                        <h5 class="mb-3">Datos si es cheque o transferencia</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_4_cheque_transferencia %></p>
                                                        <h5 class="mb-3">Origen recursos</h5>
                                                        <p> <% generalfactory.ventaDetail.pago_4_origen_recurso %></p>

                                                    </div>

                                                </div>
                                                <div id="docs" class="tab-pane">
                                                    <div class="p-3">
                                                        <h4 class="mb-3">Documentos</h4>
                                                        <i class="el el-file"></i>
                                                        <a href="/<% generalfactory.ventaDetail.clt_url %>" target="_blank"
                                                            class="<% generalfactory.ventaDetail.clt_url ? '' : 'disable-href'%>">
                                                            CLT
                                                        </a><br>
                                                        <i class="el el-file"></i>
                                                        <a href="<% generalfactory.ventaDetail.clt_parqueadero_1_url %>"
                                                            target="_blank"
                                                            class="<% generalfactory.ventaDetail.clt_parqueadero_1_url ? '' : 'disable-href'%>">
                                                            CLT parqueadero
                                                        </a><br>
                                                        <i class="el el-file"></i>
                                                        <a href="<% generalfactory.ventaDetail.clt_deposito_1_url %>"
                                                            target="_blank"
                                                            class="<% generalfactory.ventaDetail.clt_deposito_1_url ? '' : 'disable-href'%>">
                                                            CLT depósito
                                                        </a><br>
                                                        <i class="el el-file"></i>
                                                        <a href="<% generalfactory.ventaDetail.oferta_compra_url %>" target="_blank"
                                                            class="<% generalfactory.ventaDetail.oferta_compra_url ? '' : 'disable-href'%>">
                                                            Oferta de compra
                                                        </a><br>
                                                        <i class="el el-file"></i>
                                                        <a href="<% generalfactory.ventaDetail.clt_parqueadero_2_url %>"
                                                            target="_blank"
                                                            class="<% generalfactory.ventaDetail.clt_parqueadero_2_url ? '' : 'disable-href'%>">
                                                            CLT parqueadero 2
                                                        </a><br>
                                                        <i class="el el-file"></i>
                                                        <a href="<% generalfactory.ventaDetail.clt_deposito_2_url %>"
                                                            target="_blank"
                                                            class="<% generalfactory.ventaDetail.clt_deposito_2_url ? '' : 'disable-href'%>">
                                                            CLT depósito 2
                                                        </a><br>
                                                        <i class="el el-file"></i>
                                                        <a href="<% generalfactory.ventaDetail.escritura_publica_url %>"
                                                            target="_blank"
                                                            class="<% generalfactory.ventaDetail.escritura_publica_url ? '' : 'disable-href'%>">
                                                            Escritura pública
                                                        </a><br>
                                                        <i class="el el-file"></i>
                                                        <a href="<% generalfactory.ventaDetail.camara_comercio_url %>"
                                                            target="_blank"
                                                            class="<% generalfactory.ventaDetail.camara_comercio_url ? '' : 'disable-href'%>">
                                                            Cámara de comercio
                                                        </a><br>
                                                        <i class="el el-file"></i>
                                                        <a href="<% generalfactory.ventaDetail.poder_copia_diligenciada_url %>"
                                                            target="_blank"
                                                            class="<% generalfactory.ventaDetail.poder_copia_diligenciada_url ? '' : 'disable-href'%>">
                                                            Poderes en copia diligenciados
                                                        </a><br>
                                                        <i class="el el-file"></i>
                                                        <a href="<% generalfactory.ventaDetail.oferta_compra_url %>" target="_blank"
                                                            class="<% generalfactory.ventaDetail.oferta_compra_url ? '' : 'disable-href'%>">
                                                            Promesa firmada
                                                        </a><br>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <footer class="card-footer">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-default modal-dismiss">Cerrar</button>
                                    </div>
                                </div>
                            </footer>
                        </form>
                    </section>
                </div>

            </section>

            @foreach ($ventas as $venta)
                {{-- Ver Aprobaciones --}}
                <div id="modalCorreos_{{ $venta->id }}" class="modal-block modal-block-primary mfp-hide">
                    <section class="card">
                        <header class="card-header">
                            <h2 class="card-title">Aprobaciones de la venta {{ $venta->id }}</h2>
                        </header>
                        <div class="card-body">
                            <div class="row">

                                @php
                                $countAprobados = 0;
                                @endphp
                                @foreach ($venta->aprobaciones as $aprobacion)
                                    <div class="col-md-12">
                                        <div class="row">
                                            <p class="col-sm-6"> <b>Tipo: </b> {{ $aprobacion->usuario_tipo }} </p>
                                            <p class="col-sm-6"> <b>Nombre: </b> {{ $aprobacion->usuario_nombre }} </p>
                                            <p class="col-sm-6"> <b>Correo: </b> {{ $aprobacion->usuario_correo }} </p>
                                            <p class="col-sm-6"> <b>Fecha envio: </b> {{ $aprobacion->created_at }} </p>
                                            <p class="col-sm-6"> <b>Fecha última apertura: </b>
                                                {{ $aprobacion->ultima_visita }}
                                            </p>
                                            <p class="col-sm-6"> <b>Aprobado: </b>
                                                {{ $aprobacion->aprobado == 0 ? 'No' : 'Si' }}
                                            </p>
                                            <p class="col-sm-12"> <b>Comentario: </b> {{ $aprobacion->comentario }} </p>

                                            @if ($usuario->rol_id == 1)
                                                <form action="{{ url("admin/aprobar-cualquier-cosa") }}" method="post" class="col-sm-12">
                                                    <input type="hidden" name="record" value="{{ $aprobacion->id }}">
                                                    <input type="hidden" name="type" value="Venta">
                                                    {{ csrf_field() }}

                                                    @if ($aprobacion->aprobado == 0)
                                                        <button class="btn btn-success" type="submit" name="aprobar" value="1">
                                                            Aprobar
                                                        </button>
                                                    @else
                                                        <button class="btn btn-danger" type="submit" name="rechazar" value="1">
                                                            Rechazar
                                                        </button>
                                                    @endif
                                                </form>
                                            @endif
                                        </div>
                                        <hr>
                                    </div>
                                    @php
                                    if ($aprobacion->aprobado == 1) {
                                        $countAprobados++;
                                    }
                                    @endphp
                                @endforeach

                                @if (count($venta->aprobaciones) > 2 && $countAprobados == count($venta->aprobaciones))
                                    <div class="col-lg-12">
                                        <a href="{{ url("admin/ventas/contrato/{$venta->id}") }}" role="button" class="btn btn-success">
                                            Generar contrato
                                        </a>
                                    </div>
                                @endif

                            </div>
                        </div>
                        <footer class="card-footer">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button class="btn btn-default modal-dismiss">Cerrar</button>
                                </div>
                            </div>
                        </footer>
                    </section>
                </div>
                {{-- Final de ver Aprobaciones --}}
            @endforeach
