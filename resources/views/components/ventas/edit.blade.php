<section role="main" class="content-body" ng-controller="Ventas">
    <form id="formulario-crear-ventas" name="formulario-crear-ventas" action="{{ url("admin/ventas/1/{$venta->inmuebles->id}") }}" method="post" enctype="multipart/form-data">
        @csrf
        <header class="page-header">
            <h2>Ventas</h2>
            <div class="right-wrapper text-right">
                <ol class="breadcrumbs">
                    <li>
                        <a href="#">
                            <i class="bx bx-home-alt"></i>
                        </a>
                    </li>
                    <li><span>Ventas</span></li>
                    <li><span>Editar venta&nbsp;&nbsp;&nbsp;</span></li>
                </ol>
                <a class="" data-open=""></a>
            </div>
        </header>
        <div class="row">
            <div class="col-lg-12">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                        </div>

                        <h2 class="card-title">
                            Editar venta {{ $venta->id }}
                        </h2>
                    </header>
                    <div class="card-body">

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="direccion">Dirección del inmueble</label>
                                <input type="text" class="form-control" value="{{ $venta->inmuebles->direccion }}" name="direccion" disabled>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="parqueaderos_sencillos">Garajes</label>
                                <input type="text" class="form-control" name="parqueaderos_sencillos" value="{{ $venta->inmuebles->parqueaderos_sencillos }}" disabled>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="banios">Depósito</label>
                                <input type="text" class="form-control" value="{{ $venta->inmuebles->depositos }}" name="deposito" disabled>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="banios">Otro</label>
                                <input type="text" class="form-control" value="" name="otro" placeholder="2" disabled>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="matricula"># Matrícula</label>
                                <input type="text" class="form-control" value="{{ $venta->inmuebles->matricula }}" name="matricula" disabled>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="chip">Chip</label>
                                <input type="text" class="form-control" value="{{ $venta->inmuebles->chip }}" name="chip" disabled>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="cedula_catastral">Cédula catastral</label>
                                <input type="text" class="form-control" value="{{$venta->inmuebles->cedula_catastral}}" name="cedulacatastral" disabled>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="create_user_id">Agente encargado</label>
                                <select name="create_user_id" class="form-control">
                                    <option value="">Agente encargado</option>
                                    @foreach ($agentes as $agt)
                                        <option value="{{ $agt->id }}" {{ $agt->id == $venta->create_user_id ? 'selected' : '' }}>
                                            {{ $agt->nombres }} {{ $agt->apellidos }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        @for ($i = 1; $i <= 3; $i++)
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="numero_parqueadero_{{ $i }}"># PQ{{ $i }}</label>
                                    <input type="text" class="form-control" value="{{ $venta->inmuebles["numero_parqueadero_{$i}"] }}" name="numero_parqueadero_{{ $i }}" placeholder="80">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="matricula_parqueadero_{{ $i }}"># Matrícula</label>
                                    <input type="text" class="form-control" value="{{ $venta->inmuebles["matricula_parqueadero_{$i}"] }}" name="matricula_parqueadero_{{ $i }}" placeholder="60">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="chip_parqueadero_{{ $i }}"># Chip</label>
                                    <input type="text" class="form-control" value="{{ $venta->inmuebles["chip_parqueadero_{$i}"] }}" name="chip_parqueadero_{{ $i }}" placeholder="60">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="cedula_parqueadero_{{ $i }}"># Cédula Catastral</label>
                                    <input type="text" class="form-control" value="{{ $venta->inmuebles["cedula_parqueadero_{$i}"] }}" name="cedula_parqueadero_{{ $i }}" placeholder="60">
                                </div>
                            </div>
                        @endfor

                        @for ($i = 1; $i <= 2; $i++)
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="numero_deposito_{{ $i }}"># DP{{ $i }}</label>
                                    <input type="text" class="form-control" value="{{ $venta->inmuebles["numero_deposito_{$i}"] }}" name="numero_deposito_{{ $i }}" placeholder="80">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="matricula_deposito_{{ $i }}"># Matrícula</label>
                                    <input type="text" class="form-control" value="{{ $venta->inmuebles["matricula_deposito_{$i}"] }}" name="matricula_deposito_{{ $i }}" placeholder="60">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="chip_deposito_{{ $i }}"># Chip</label>
                                    <input type="text" class="form-control" value="{{ $venta->inmuebles["chip_deposito_{$i}"] }}" name="chip_deposito_{{ $i }}" placeholder="60">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="cedula_deposito_{{ $i }}"># Cédula Catastral</label>
                                    <input type="text" class="form-control" value="{{ $venta->inmuebles["cedula_deposito_{$i}"] }}" name="cedula_deposito_{{ $i }}" placeholder="60">
                                </div>
                            </div>
                        @endfor

                    </div>
                </section>
            </div>
        </div>

        <br>

        {{-- Vendedores --}}
        <input type="hidden" name="cantidad_vendedores" value="<% cantidad_vendedores %>">
        <input type="hidden" name="vendedores_removidos" value="<% vendedoresRemovidos %>">
        <section class="card col-lg-12" ng-init="initVendedores({{ $vendedores }})">
            <header class="card-header correct-margin">
                <h2 class="card-title">
                    Vendedores
                    {{-- <span ng-show="cantidad_vendedores >=2 " class="btn-add" ng-click="removeVendedor()">
                    -
                </span>
                <span class="btn-add" ng-click="addVendedor()">
                +
            </span> --}}
        </h2>
    </header>
    <div class="row" ng-repeat="cod in vendedores">
        <div class="card-body">
            <b>Vendedor <%cod%></b>
            <input type="hidden" value="<% vendedores_array[cod].id %>" name="<%cod%>_vendedor_id">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="<% cod %>_vendedor_nombre">Nombre y apellido</label>
                    <input type="text" class="form-control" ng-model="vendedores_array[cod].nombre" name="<% cod %>_vendedor_nombre" placeholder="Pedro Perez Galindo">
                </div>
                <div class="form-group col-md-6">
                    <label for="<% cod %>_vendedor_ciudad">Ciudad</label>
                    <input type="text" class="form-control" ng-model="vendedores_array[cod].ciudad" name="<% cod %>_vendedor_ciudad" placeholder="Bogotá">
                </div>
                <div class="form-group col-md-2">
                    <label for="<% cod %>_vendedor_tipo_doc">Tipo doc</label>
                    <select name="<% cod %>_vendedor_tipo_doc" class="form-control">
                        <option value="">Tipo de documento</option>
                        @foreach ($tipos_documento as $tipo)
                            <option value="{{ $tipo->id }}" ng-selected="vendedores_array[cod].tipo_documento == {{ $tipo->id }}">
                                {{ $tipo->nombre }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="<% cod %>_vendedor_cedula">Cédula</label>
                    <input type="text" class="form-control" ng-model="vendedores_array[cod].doc" name="<% cod %>_vendedor_cedula" placeholder="31950070">
                </div>
                <div class="form-group col-md-6">
                    <label for="<% cod %>_vendedor_expedicion">Expedición</label>
                    <input type="text" class="form-control" ng-model="vendedores_array[cod].expedicion" name="<% cod %>_vendedor_expedicion" placeholder="Bogotá">
                </div>
                <div class="form-group col-md-6">
                    <label for="<% cod %>_vendedor_estado_civil">Estado civil</label>
                    <input type="text" class="form-control" ng-model="vendedores_array[cod].estado_civil" name="<% cod %>_vendedor_estado_civil" placeholder="Soltero">
                </div>
                <div class="form-group col-md-6">
                    <label for="<% cod %>_vendedor_subestado_civil">Detalle estado civil</label>
                    <select name="<% cod %>_vendedor_subestado_civil" class="form-control">
                        <option value="">Detalle estado civil</option>
                        @foreach ($estados_civiles as $estado)
                            <option value="{{ $estado->id }}" ng-selected="vendedores_array[cod].subestado_civil_id == {{ $estado->id }}">
                                {{ $estado->nombre }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="<% cod %>_vendedor_telefono">Teléfono</label>
                    <input type="text" class="form-control" ng-model="vendedores_array[cod].telefono" name="<% cod %>_vendedor_telefono" placeholder="1234567890">
                </div>
                <div class="form-group col-md-6">
                    <label for="<% cod %>_vendedor_celular">Celular/Whatsapp</label>
                    <input type="text" class="form-control" ng-model="vendedores_array[cod].celular" name="<% cod %>_vendedor_celular" placeholder="1234567890">
                </div>
                <div class="form-group col-md-6">
                    <label for="<% cod %>_vendedor_email">Correo Electrónico</label>
                    <input type="text" class="form-control" ng-model="vendedores_array[cod].email" name="<% cod %>_vendedor_email" placeholder="prueba@prueba.com">
                </div>
                <div class="form-group col-md-6">
                    <label for="<% cod %>_vendedor_direccion">Dirección</label>
                    <input type="text" class="form-control" ng-model="vendedores_array[cod].direccion" name="<% cod %>_vendedor_direccion" placeholder="KR 14 I">
                </div>
                <div class="form-group col-md-12">
                    <label for="<% cod %>_vendedor_archivo_cedula">Cédula</label>
                    <input type="file" name="<% cod %>_vendedor_archivo_cedula" />
                    <small ng-if="vendedores_array[cod].url_documento && vendedores_array[cod].url_documento != ''">
                        <a href="<% vendedores_array[cod].url_documento %>" target="_blank">
                            Ver documento
                        </a>
                    </small>
                </div>
                <div class="form-group col-md-6">
                    <div class="form-check">
                        <input type="radio" class="form-check-input" id="propio" ng-model="vendedores_array[cod].apoderado" name="<% cod %>_vendedor_apoderado" value="vendedor">Actúa en nombre propio
                        <label class="form-check-label" for="propio"></label>
                    </div>
                    <div class="form-check">
                        <input type="radio" class="form-check-input" id="apoderado" ng-model="vendedores_array[cod].apoderado" name="<% cod %>_vendedor_apoderado" value="apoderado">Apoderado
                        <label class="form-check-label" for="apoderado"></label>
                    </div>
                </div>
            </div>

            {{-- Apoderado --}}
            <section class="card col-lg-12" ng_show="vendedores_array[cod].apoderado == 'apoderado'">
                <header class="card-header correct-margin">
                    <h2 class="card-title">
                        Apoderado del vendedor
                    </h2>
                </header>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="<% cod %>_vendedor_apoderado_nombre">Nombre y apellido</label>
                            <input type="text" class="form-control" ng-model="vendedores_array[cod].apoderados.nombre" name="<% cod %>_vendedor_apoderado_nombre" placeholder="Pedro Perez Galindo">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="<% cod %>_vendedor_apoderado_ciudad">Ciudad</label>
                            <input type="text" class="form-control" ng-model="vendedores_array[cod].apoderados.ciudad" name="<% cod %>_vendedor_apoderado_ciudad" placeholder="Bogotá">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="<% cod %>_vendedor_apoderado_tipo_doc">Tipo doc</label>
                            <select name="<% cod %>_vendedor_apoderado_tipo_doc" class="form-control">
                                <option value="">Tipo de documento</option>
                                @foreach ($tipos_documento as $tipo)
                                    <option value="{{ $tipo->id }}" ng-selected="vendedores_array[cod].apoderados.tipo_documento_id == {{ $tipo->id }}">
                                        {{ $tipo->nombre }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="<% cod %>_vendedor_apoderado_cedula">Cédula</label>
                            <input type="text" class="form-control" ng-model="vendedores_array[cod].apoderados.numero_documento" name="<% cod %>_vendedor_apoderado_cedula" placeholder="31950070">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="<% cod %>_vendedor_apoderado_expedicion">Expedición</label>
                            <input type="text" class="form-control" ng-model="vendedores_array[cod].apoderados.lugar_expedicion_documento" name="<% cod %>_vendedor_apoderado_expedicion" placeholder="Bogotá">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="<% cod %>_vendedor_apoderado_archivo_cedula">Cédula</label>
                            <input type="file" name="<% cod %>_vendedor_apoderado_archivo_cedula" />
                            <small ng-if="vendedores_array[cod].apoderados.url_documento != ''">
                                <a href="<% vendedores_array[cod].apoderados.url_documento %>" target="_blank">
                                    Ver documento
                                </a>
                            </small>
                        </div>
                    </div>
                </div>
            </section>
            {{-- Apoderado --}}

        </div>
    </div>
</section>
{{-- Final de Vendedores --}}

<br>

{{-- Compradores --}}
<input type="hidden" name="cantidad_compradores" value="<% cantidad_compradores %>">
<section class="card col-lg-12" ng-init="initCompradores({{ $compradores }})">
    <header class="card-header correct-margin">
        <h2 class="card-title">
            Compradores
            <span ng-show="cantidad_compradores >=2 " class="btn-add" ng-click="removeCompradores()">
                -
            </span>
            <span class="btn-add" ng-click="addCompradores()">
                +
            </span>
        </h2>
    </header>
    <div class="row" ng-repeat="cod in compradores">
        <div class="card-body">
            <b>Comprador <%cod%></b>
            <input type="hidden" value="<% compradores_array[cod].id %>" name="<%cod%>_comprador_id">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="<% cod %>_comprador_nombre">Nombre y apellido</label>
                    <input type="text" class="form-control" ng-model="compradores_array[cod].nombre" name="<% cod %>_comprador_nombre" placeholder="Pedro Perez Galindo" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="<% cod %>_comprador_ciudad">Ciudad</label>
                    <input type="text" class="form-control" ng-model="compradores_array[cod].ciudad" name="<% cod %>_comprador_ciudad" placeholder="Bogotá" required>
                </div>
                <div class="form-group col-md-2">
                    <label for="<% cod %>_comprador_tipo_doc">Tipo doc</label>
                    <select name="<% cod %>_comprador_tipo_doc" class="form-control">
                        <option value="">Tipo de documento</option>
                        @foreach ($tipos_documento as $tipo)
                            <option value="{{ $tipo->id }}" ng-selected="compradores_array[cod].tipo_documento == {{ $tipo->id }}" required>
                                {{ $tipo->nombre }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="<% cod %>_comprador_cedula">Cédula</label>
                    <input type="text" class="form-control" ng-model="compradores_array[cod].doc" name="<% cod %>_comprador_cedula" placeholder="31950070" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="<% cod %>_comprador_expedicion">Expedición</label>
                    <input type="text" class="form-control" ng-model="compradores_array[cod].expedicion" name="<% cod %>_comprador_expedicion" placeholder="Bogotá" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="<% cod %>_comprador_estado_civil">Estado civil</label>
                    <input type="text" class="form-control" ng-model="compradores_array[cod].estado_civil" name="<% cod %>_comprador_estado_civil" placeholder="Soltero">
                </div>
                <div class="form-group col-md-6">
                    <label for="<% cod %>_comprador_subestado_civil">Detalle estado civil</label>
                    <select name="<% cod %>_comprador_subestado_civil" class="form-control">
                        <option value="">Detalle estado civil</option>
                        @foreach ($estados_civiles as $estado)
                            <option value="{{ $estado->id }}" ng-selected="compradores_array[cod].subestado_civil_id == {{ $estado->id }}">
                                {{ $estado->nombre }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="<% cod %>_comprador_telefono">Teléfono</label>
                    <input type="text" class="form-control" ng-model="compradores_array[cod].telefono" name="<% cod %>_comprador_telefono" placeholder="1234567890" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="<% cod %>_comprador_celular">Celular/Whatsapp</label>
                    <input type="text" class="form-control" ng-model="compradores_array[cod].celular" name="<% cod %>_comprador_celular" placeholder="1234567890" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="<% cod %>_comprador_email">Correo Electrónico</label>
                    <input type="text" class="form-control" ng-model="compradores_array[cod].email" name="<% cod %>_comprador_email" placeholder="prueba@prueba.com" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="<% cod %>_comprador_direccion">Dirección</label>
                    <input type="text" class="form-control" ng-model="compradores_array[cod].direccion" name="<% cod %>_comprador_direccion" placeholder="KR 14 I">
                </div>
                <div class="form-group col-md-12">
                    <label for="<% cod %>_compredor_archivo_cedula">Cédula</label>
                    <input type="file" name="<% cod %>_comprador_archivo_cedula" />
                    <small ng-if="compradores_array[cod].url_documento && compradores_array[cod].url_documento != ''">
                        <a href="<% compradores_array[cod].url_documento %>" target="_blank">
                            Ver documento
                        </a>
                    </small>
                </div>
                <div class="form-group col-md-6">
                    <div class="form-check">
                        <input type="radio" class="form-check-input" id="comprador_propio" ng-model="compradores_array[cod].apoderado" name="<% cod %>_comprador_apoderado" value="vendedor">Actúa en nombre propio
                        <label class="form-check-label" for="comprador_propio"></label>
                    </div>
                    <div class="form-check">
                        <input type="radio" class="form-check-input" id="comprador_apoderado" ng-model="compradores_array[cod].apoderado" name="<% cod %>_comprador_apoderado" value="apoderado">Apoderado
                        <label class="form-check-label" for="comprador_apoderado"></label>
                    </div>
                </div>
            </div>

            {{-- Apoderado --}}
            <section class="card col-lg-12" ng_show="compradores_array[cod].apoderado == 'apoderado'">
                <header class="card-header correct-margin">
                    <h2 class="card-title">
                        Apoderado del comprador
                    </h2>
                </header>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="<% cod %>_comprador_apoderado_nombre">Nombre y apellido</label>
                            <input type="text" class="form-control" ng-model="compradores_array[cod].apoderados.nombre" name="<% cod %>_comprador_apoderado_nombre" placeholder="Pedro Perez Galindo">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="<% cod %>_comprador_apoderado_ciudad">Ciudad</label>
                            <input type="text" class="form-control" ng-model="compradores_array[cod].apoderados.ciudad" name="<% cod %>_comprador_apoderado_ciudad" placeholder="Bogotá">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="<% cod %>_comprador_apoderado_tipo_doc">Tipo doc</label>
                            <select name="<% cod %>_comprador_apoderado_tipo_doc" class="form-control">
                                <option value="">Tipo de documento</option>
                                @foreach ($tipos_documento as $tipo)
                                    <option value="{{ $tipo->id }}" ng-selected="compradores_array[cod].apoderados.tipo_documento == {{ $tipo->id }}">
                                        {{ $tipo->nombre }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="<% cod %>_comprador_apoderado_cedula">Cédula</label>
                            <input type="text" class="form-control" ng-model="compradores_array[cod].apoderados.numero_documento" name="<% cod %>_comprador_apoderado_cedula" placeholder="31950070">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="<% cod %>_comprador_apoderado_expedicion">Expedición</label>
                            <input type="text" class="form-control" ng-model="compradores_array[cod].apoderados.lugar_expedicion_documento" name="<% cod %>_comprador_apoderado_expedicion" placeholder="Bogotá">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="<% cod %>_comprador_apoderado_archivo_cedula">Cédula</label>
                            <input type="file" name="<% cod %>_comprador_apoderado_archivo_cedula" />
                            <small ng-if="compradores_array[cod].apoderados.url_documento != ''">
                                <a href="<% compradores_array[cod].apoderados.url_documento %>" target="_blank">
                                    Ver documento
                                </a>
                            </small>
                        </div>
                    </div>
                </div>
            </section>
            {{-- Apoderado --}}

        </div>
    </div>
</section>
{{-- Final de Compradores --}}

<br>
<div class="row">

    <section class="card col-lg-12">
        <header class="card-header">
            <h2 class="card-title">Negociación</h2>
        </header>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                    data-original-title="¿Cuál es el precio del inmueble?">Precio
                </label>
                <input type="text" class="form-control price_input"
                value="{{ number_format($venta->negociacion_precio) }}"
                ng-change="updateSaldoPendiente()" ng-model="negociacion_precio"
                ng-init="negociacion_precio = '{{$venta->negociacion_precio}}'" id="negociacion_precio"
                name="negociacion_precio" placeholder="$100.000.000" format="currency">
            </div>
            <div class="form-group col-md-4">
                <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                data-original-title="¿Cuál es el valor pagado a promesa?">Pagado promesa</label>
                <input type="text" class="form-control price_input"
                value="{{ number_format($venta->negociacion_pagado_promesa) }}"
                id="negociacion_pagado_promesa" ng-model="negociacion_pagado_promesa"
                ng-init="negociacion_pagado_promesa = '{{$venta->negociacion_pagado_promesa}}'"
                ng-change="updateSaldoPendiente()" name="negociacion_pagado_promesa"
                placeholder="$10.000.000" format="currency">
            </div>
            <div class="form-group col-md-4">
                <label for="habitaciones" data-toggle="tooltip" data-placement="top" data-original-title="">
                    Saldos Pendientes
                    <span ng-if="negociacion_number < 0" class="danger-text">
                        El saldo
                        no puede ser
                        negativo
                    </span>
                    <span ng-if="negociacion_number > 0" class="danger-text">
                        Hay pagos pendientes
                    </span>
                </label>
                <input type="text" class="form-control price_input <%negociacion_number < 0 ? 'danger-input' : ''%> <%negociacion_number > 0 ? 'danger-input' : ''%>"
                value="{{ $venta->negociacion_saldo_pendiente }}" ng-model="negociacion_saldo_pendiente"
                ng-init="negociacion_saldo_pendiente = '{{number_format($venta->negociacion_saldo_pendiente)}}'"
                id="negociacion_saldo_pendiente" name="negociacion_saldo_pendiente" placeholder="$0" readonly>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                data-original-title="¿Cuánto se pactó por concepto de arras?">Arras</label>
                <input type="text" class="form-control price_input"
                value="{{ number_format($venta->negociacion_arras) }}" id="negociacion_arras"
                ng-model="negociacion_arras" ng-change="updateSaldoPendiente()"
                ng-init="negociacion_arras = '{{$venta->negociacion_arras}}'" name="negociacion_arras"
                placeholder="$50.000.000" format="currency">
            </div>
            <div class="form-group col-md-4">
                <label for="habitaciones" data-toggle="tooltip" data-placement="top" data-original-title="¿A favor de quién se hace el pago de Arras? (Nombre y cédula si es diferente al
                vendedor)">Arras a favor de</label>
                <input type="text" class="form-control" value="{{ $venta->negociacion_arras_favor }}"
                id="negociacion_arras_favor" name="negociacion_arras_favor"
                placeholder="Nombre y cédula">
            </div>

            <div class="form-group col-md-4">
                <label for="paragrafo_unico">Parágrafo único</label>
                <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input" id="show_paragrafo_unico_si" value="1" name="show_paragrafo_unico" {{ $venta->mostrar_paragrafo_unico ? "checked" : "" }}>
                    <label class="custom-control-label" for="show_paragrafo_unico_si">Si</label>
                </div>
                <div class="custom-control custom-radio">
                    <input type="radio" class="custom-control-input" id="show_paragrafo_unico_no" value="0" name="show_paragrafo_unico" {{ !$venta->mostrar_paragrafo_unico ? "checked" : "" }}>
                    <label class="custom-control-label" for="show_paragrafo_unico_no">No</label>
                </div>
            </div>

        </div>

    </div>
</section>

</div>
<br>

{{-- Pagos --}}
<input type="hidden" name="cantidad_pagos" value="<% cantidad_pagos %>">
<section class="card col-lg-12" ng-init="initPagos({{ $venta }})">
    <header class="card-header correct-margin">
        <h2 class="card-title">
            Pagos
            <span ng-show="cantidad_pagos >=2 " class="btn-add" ng-click="removePago()">
                -
            </span>
            <span class="btn-add" ng-click="addPago()">
                +
            </span>
        </h2>
    </header>
    <div class="row" ng-repeat="cod in pagosTotales">
        <div class="card-body">
            <b>Pago <%cod%></b>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="pago_<% cod %>_valor_pago" data-toggle="tooltip" data-placement="top" data-original-title="">
                        Valor del pago <% cod %>
                    </label>
                    <input type="text" class="form-control price_input"
                    id="pago_<% cod %>_valor_pago"
                    name="pago_<% cod %>_valor_pago"
                    ng-model="pagos_array[cod].valor_pago"
                    ng-change="updateSaldoPendiente()"
                    placeholder="$10.000.000">
                </div>
                <div class="form-group col-md-4">
                    <label for="habitaciones" data-toggle="tooltip" data-placement="top" data-original-title="¿Se paga con recursos propios, por medio de banco o alguna otra entidad? (Mencione cual) Si es con credito indique la modalidad del credito.">
                        Origen recursos
                    </label>
                    <input type="text" class="form-control" ng-model="pagos_array[cod].origen_recurso" id="pago_<% cod %>_origen_recurso" name="pago_<% cod %>_origen_recurso" placeholder="Recursos propios">
                </div>
                <div class="form-group col-md-4">
                    <label for="pago_<% cod %>_medio_pago" data-toggle="tooltip" data-placement="top" data-original-title="¿Cuál es el medio de pago para el valor que se da en la promesa? (cheque, efectivo, transferencia, etc)">
                        Medio de pago <% cod %>
                    </label>
                    <input type="text" class="form-control" ng-model="pagos_array[cod].medio_pago" id="pago_<% cod %>_medio_pago" name="pago_<% cod %>_medio_pago" placeholder="Transferencia bancaria">
                </div>
                <div class="form-group col-md-4" ng-if="cod == 1">
                    <label for="habitaciones" data-toggle="tooltip" data-placement="top" data-original-title="¿Cuál es la Notaría en que se firma la promesa?">
                        Pago 1 en favor de
                    </label>
                    <input type="text" class="form-control" ng-model="pagos_array[cod].notaria" id="pago_<% cod %>_notaria" name="pago_<% cod %>_notaria" placeholder="Pago 1 a favor de">
                </div>
                <div class="form-group col-md-4" ng-if="cod != 1">
                    <label for="pago_<% cod %>_pago_favor" data-toggle="tooltip" data-placement="top" data-original-title="Pago a favor de">
                        Pago <% cod %> en favor de
                    </label>
                    <input type="text" class="form-control" ng-model="pagos_array[cod].pago_favor" id="pago_<% cod %>_pago_favor" name="pago_<% cod %>_pago_favor" placeholder="Pago a favor de">
                </div>
                <div class="form-group col-md-4">
                    <label for="habitaciones" data-toggle="tooltip" data-placement="top" data-original-title="Si el pago es con cheque, indique nombres y cédula de la persona a la que debe ir girado el cheque. Si el pago es por transferencia, indique número de cuenta, tipo de cuenta y entidad">
                        Datos explicativos
                    </label>
                    <input type="text" class="form-control" ng-model="pagos_array[cod].cheque_transferencia" id="pago_<% cod %>_cheque_transferencia" name="pago_<% cod %>_cheque_transferencia" placeholder="Cuenta de ahorros Banco Caja Social No. 24034003666">
                </div>
                <div class="form-group col-md-4">
                    <label for="pago_<% cod %>_fecha_pago" data-toggle="tooltip" data-placement="top" data-original-title="¿Cuál es la fecha y hora en que se firma la promesa y primer pago? D/M/A">
                        Fecha promesa y pago <% cod %>
                    </label>
                    <input type="date" class="form-control" value="<% pagos_array[cod].fecha_pago %>" id="pago_<% cod %>_fecha_pago" name="pago_<% cod %>_fecha_pago" placeholder="13/09/2021 Hora: 10:00 am">
                </div>
            </div>
        </div>
    </div>
</section>
{{-- Final de Pagos --}}
<br>
<div class="row">

    <section class="card col-lg-12">
        <header class="card-header">
            <h2 class="card-title">Escrituración</h2>
        </header>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="escritura_fecha_publica" data-toggle="tooltip" data-placement="top" data-original-title="¿Cuál es la fecha y hora en que se firma la Escritura Pública?">
                        Fecha escritura pública
                    </label>
                    <input type="date" class="form-control" value="{{ $venta->escritura_fecha_publica }}" id="escritura_fecha_publica" name="escritura_fecha_publica" placeholder="22/10/2021">
                </div>
                <div class="form-group col-md-4">
                    <label for="escritura_notaria" data-toggle="tooltip" data-placement="top" data-original-title="¿Cuál es la Notaría en que se firma la Escritura Pública?">
                        Notaría y Dirección
                    </label>
                    <input type="text" class="form-control" value="{{ $venta->escritura_notaria }}" id="escritura_notaria" name="escritura_notaria" placeholder="Notaría 62 Carrera 24 No. 53-18">
                </div>
                <div class="form-group col-md-4">
                    <label for="notaria_circulo" data-toggle="tooltip" data-placement="top" data-original-title="¿Cuál es la Notaría en que se firma la Escritura Pública?">
                        Del círculo de
                    </label>
                    <input type="text" class="form-control" value="{{ $venta->notaria_circulo }}" id="notaria_circulo" name="notaria_circulo" placeholder="Bogotá">
                </div>
                <div class="form-group col-md-4">
                    <label for="hora_escrituracion" data-toggle="tooltip" data-placement="top" data-original-title="¿Cuál es la Notaría en que se firma la Escritura Pública?">
                        Hora escrituración
                    </label>
                    <input type="text" class="form-control" value="{{ $venta->hora_escrituracion }}" id="hora_escrituracion" name="hora_escrituracion" placeholder="11:00 am">
                </div>
                <div class="form-group col-md-4">
                    <label for="notas_escrituracion" data-toggle="tooltip" data-placement="top" data-original-title="¿Cuál es la Notaría en que se firma la Escritura Pública?">
                        Notas
                    </label>
                    <input type="text" class="form-control" value="{{ $venta->notas_escrituracion }}" id="notas_escrituracion" name="notas_escrituracion" placeholder="">
                </div>
                <div class="form-group col-md-4">
                    <label for="escritura_predial_asumido" data-toggle="tooltip" data-placement="top" data-original-title="¿Quién asume el impuesto predial ?">
                        Predial asumido por
                    </label>
                    <textarea type="text" class="form-control" id="escritura_predial_asumido" name="escritura_predial_asumido" placeholder="Proporcional entre las partes">{{ $venta->escritura_predial_asumido }}</textarea>
                </div>
                <div class="form-group col-md-4">
                    <label for="escritura_gasto_notarial_asumido" data-toggle="tooltip" data-placement="top" data-original-title="¿Quién asume los gastos Notariales?">
                        Gastos notariales asumidos por
                    </label>
                    <textarea type="text" class="form-control" id="escritura_gasto_notarial_asumido" name="escritura_gasto_notarial_asumido" placeholder="50% Comprador 50% Vendedor">{{ $venta->escritura_gasto_notarial_asumido }}</textarea>
                </div>
                <div class="form-group col-md-4">
                    <label for="habitaciones" data-toggle="tooltip" data-placement="top" data-original-title="¿Quién asume beneficencia e inscripción en Registro de Instrumentos Públicos?">
                        Beneficiado por instrumentos públicos
                    </label>
                    <textarea type="text" class="form-control" id="escritura_beneficiado_instrumento_publico" name="escritura_beneficiado_instrumento_publico" placeholder="EL COMPRADOR">{{ $venta->escritura_beneficiado_instrumento_publico }}</textarea>
                </div>
                <div class="form-group col-md-4">
                    <label for="habitaciones" data-toggle="tooltip" data-placement="top" data-original-title="¿Se pactó una condición de negociación adicional? (Mencione cual)">
                        ¿Negociación adicional?
                    </label>
                    <textarea type="text" class="form-control" id="escritura_negociacion_adicional" name="escritura_negociacion_adicional" placeholder="NO">{{ $venta->escritura_negociacion_adicional }}</textarea>
                </div>
                <div class="form-group col-md-4">
                    <label for="habitaciones" data-toggle="tooltip" data-placement="top" data-original-title="¿Cómo y cuando será pagada la comisión de Elite?">
                        Pago comisión Elite
                    </label>
                    <div class="custom-control custom-radio" ng-init="show_pago_comision_elite = '{{ $venta->escritura_pago_comision_elite }}' != '' ? 1 : 0">
                        <input type="radio" class="custom-control-input" id="show_pago_comision_elite" ng-model="show_pago_comision_elite" ng-value="1">
                        <label class="custom-control-label" for="show_pago_comision_elite">Si</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" id="dont_show_pago_comision_elite" ng-model="show_pago_comision_elite" checked ng-value="0">
                        <label class="custom-control-label" for="dont_show_pago_comision_elite">No</label>
                    </div>
                    <textarea type="text" class="form-control" id="escritura_pago_comision_elite" ng-show="show_pago_comision_elite == 1" name="escritura_pago_comision_elite" placeholder="A la firma de la promesa de Compraventa"><% show_pago_comision_elite == 1 ? '{{ $venta->escritura_pago_comision_elite }}' : '' %></textarea>
                </div>
                <div class="form-group col-md-4">
                    <label for="habitaciones" data-toggle="tooltip" data-placement="top" data-original-title="Texto libre para el Agente">
                        Entrega
                    </label>
                    <textarea type="text" class="form-control" id="escritura_observacion" name="escritura_observacion" placeholder="Introduzca comentarios adicionales">{{ $venta->escritura_observacion }}</textarea>
                </div>
                <div class="form-group col-md-4">
                    <label for="fecha_firma_promesa" data-toggle="tooltip" data-placement="top" data-original-title="¿Cuál es la fecha y hora en que se firma la promesa?">
                        Fecha firma promesa
                    </label>
                    <input type="date" class="form-control" value="{{ $venta->fecha_firma_promesa != "" ? explode(" ", $venta->fecha_firma_promesa)[0] : "" }}" name="fecha_firma_promesa" placeholder="22/10/2021">
                </div>
            </div>

        </div>
    </section>

</div>
<br>

<section class="card col-lg-12" ng-if="{{ $usuario->rol_id }} == 1 || {{ $usuario->rol_id }} == 6">
    <header class="card-header correct-margin" style="background-color: #00519F;">
        <h2 class="card-title" style="color:white">
            Cláusulas del contrato
        </h2>
    </header>
    <div class="row">
        <div class="card-body">

            <div class="form-row">

                @php
                $conditionalClausula = 5;
                $contentClausulas = explode(".ñ.", $venta->clausulas);
                @endphp

                @for ($i = 1; $i <= $conditionalClausula + 1; $i++)
                    <div class="form-group col-md-4">
                        <label for="clausula_{{ $i }}">
                            @switch($i)
                                @case(1)
                                Identificación del inmueble
                                @break
                                @case(2)
                                Instrumentos públicos
                                @break
                                @case(3)
                                Folio matrícula inmobiliaria
                                @break
                                @case(4)
                                Escritura pública
                                @break
                                @case(5)
                                Propiedad horizontal
                                @break
                                @case(6)
                                Tradición
                                @break
                                @default
                                Cláusula {{ $i }}
                            @endswitch
                        </label>

                        @if ($i == $conditionalClausula)
                            <div class="custom-control custom-radio" ng-init="show_propiedad = '{{ $contentClausulas[$i] ?? "" }}' != '' ? 1 : 0">
                                <input type="radio" class="custom-control-input" id="show_propiedad" ng-model="show_propiedad" ng-value="1">
                                <label class="custom-control-label" for="show_propiedad">Si</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="dont_show_propiedad" ng-model="show_propiedad" checked ng-value="0">
                                <label class="custom-control-label" for="dont_show_propiedad">No</label>
                            </div>

                            <textarea name="clausula_{{ $i }}" class="form-control" rows="5" ng-show="show_propiedad == 1"><% show_propiedad == 1 ? '{{ $contentClausulas[$i] ?? "" }}' : '' %></textarea>
                        @else
                            <textarea name="clausula_{{ $i }}" class="form-control" rows="5">{{ $contentClausulas[$i] ?? "" }}</textarea>
                        @endif
                    </div>
                @endfor

            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="paragrafo_segundo">Parágrafo segundo para arriendo</label>
                    <div class="custom-control custom-radio" ng-init="show_paragrafo_segundo = {{ $venta->mostrar_paragrafo_segundo }} ? 1 : 0">
                        <input type="radio" class="custom-control-input" id="show_paragrafo_segundo_si" ng-model="show_paragrafo_segundo" ng-value="1" name="mostrar_paragrafo_segundo">
                        <label class="custom-control-label" for="show_paragrafo_segundo_si">Si</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" id="show_paragrafo_segundo_no" ng-model="show_paragrafo_segundo" ng-value="0" name="mostrar_paragrafo_segundo">
                        <label class="custom-control-label" for="show_paragrafo_segundo_no">No</label>
                    </div>

                    <textarea name="paragrafo_segundo_texto" class="form-control" rows="5" ng-show="show_paragrafo_segundo == 1"><% show_paragrafo_segundo == 1 ? '{{ $venta->paragrafo_segundo_texto ?? "" }}' : '' %></textarea>
                </div>

                <div class="form-group col-md-4">
                    <label for="paragrafo_segundo">Mostrar cláusula extraordinaria (Emergencia Covid)</label>
                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" id="mostrar_clausula_covid_si" name="mostrar_clausula_covid" value="1" name="mostrar_paragrafo_segundo" {{ $venta->mostrar_clausula_covid ? "checked" : "" }}>
                        <label class="custom-control-label" for="mostrar_clausula_covid_si">Si</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" id="mostrar_clausula_covid_no" name="mostrar_clausula_covid" value="0" name="mostrar_paragrafo_segundo" {{ !$venta->mostrar_clausula_covid ? "checked" : "" }}>
                        <label class="custom-control-label" for="mostrar_clausula_covid_no">No</label>
                    </div>
                </div>

                <div class="form-group col-md-4">
                    <label for="paragrafo_segundo">Tipo de firma</label>
                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" id="mostrar_firma_electronica_si" name="mostrar_firma_electronica" value="1" name="mostrar_paragrafo_segundo" {{ $venta->mostrar_firma_electronica ? "checked" : "" }}>
                        <label class="custom-control-label" for="mostrar_firma_electronica_si">Electrónica</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" id="mostrar_firma_electronica_no" name="mostrar_firma_electronica" value="0" name="mostrar_paragrafo_segundo" {{ !$venta->mostrar_firma_electronica ? "checked" : "" }}>
                        <label class="custom-control-label" for="mostrar_firma_electronica_no">Notaría</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br>

<div class="row">

    <section class="card col-lg-12">
        <header class="card-header">
            <h2 class="card-title">Documentos: Escritura Máximo 20Mb, el resto de documentos máximo 10Mb</h2>
        </header>
        <div class="card-body">
            <div class="form-row">
                <!-- linea 1 -->
                <div class="form-group col-md-4">
                    <label for="video" data-toggle="tooltip" data-placement="top" data-original-title="Certificado de tradición del inmueble menor a 30 días">
                        CLT
                    </label>
                    @if ($venta->clt_url != "")
                        <small>
                            <a href="{{ $venta->clt_url }}" target="_blank">
                                Ver: CLT
                            </a>
                        </small>
                    @endif
                    <br>
                    <input type="file" id="clt_url" name="clt_url" />
                </div>
                <div class="form-group col-md-4">
                    <label for="video" data-toggle="tooltip" data-placement="top" data-original-title="Certificado de tradición del parqueadero menor a días (si aplica)">
                        CLT parqueadero 1
                    </label>
                    @if ($venta->clt_parqueadero_1_url != "")
                        <small>
                            <a href="{{ $venta->clt_parqueadero_1_url }}" target="_blank">
                                Ver: CLT parqueadero 1
                            </a>
                        </small>
                    @endif
                    <br>
                    <input type="file" id="clt_parqueadero_1_url" name="clt_parqueadero_1_url" />
                </div>
                <div class="form-group col-md-4">
                    <label for="video" data-toggle="tooltip" data-placement="top" data-original-title="Certificado de tradición del depósito menor a 30 días (si aplica)">
                        CLT depósito 1
                    </label>
                    @if ($venta->clt_deposito_1_url != "")
                        <small>
                            <a href="{{ $venta->clt_deposito_1_url }}" target="_blank">
                                Ver: CLT depósito 1
                            </a>
                        </small>
                    @endif
                    <br>
                    <input type="file" id="clt_deposito_1_url" name="clt_deposito_1_url" />
                </div>
                <div class="form-group col-md-4">
                    <label for="video" data-toggle="tooltip" data-placement="top" data-original-title="Certificado de tradición del parqueadero 2 menor a días (si aplica)">
                        CLT
                        parqueadero 2
                    </label>
                    @if ($venta->clt_parqueadero_2_url != "")
                        <small>
                            <a href="{{ $venta->clt_parqueadero_2_url }}" target="_blank">
                                Ver: parqueadero 2
                            </a>
                        </small>
                    @endif
                    <br>
                    <input type="file" id="clt_parqueadero_2_url" name="clt_parqueadero_2_url" />
                </div>
                <div class="form-group col-md-4">
                    <label for="video" data-toggle="tooltip" data-placement="top" data-original-title="Certificado de tradición del depósito 2 menor a 30 días (si aplica)">
                        CLT depósito 2
                    </label>
                    @if ($venta->clt_deposito_2_url != "")
                        <small>
                            <a href="{{ $venta->clt_deposito_2_url }}" target="_blank">
                                Ver: CLT depósito 2
                            </a>
                        </small>
                    @endif
                    <br>
                    <input type="file" id="clt_deposito_2_url" name="clt_deposito_2_url" />
                </div>
                <!-- linea 2 -->
                <div class="form-group col-md-4">
                    <label for="video" data-toggle="tooltip" data-placement="top" data-original-title="Oferta de compra">
                        Oferta de compra
                    </label>
                    @if ($venta->oferta_compra_url != "")
                        <small>
                            <a href="{{ $venta->oferta_compra_url }}" target="_blank">
                                Ver: Oferta de compra
                            </a>
                        </small>
                    @endif
                    <br>
                    <input type="file" id="oferta_compra_url" name="oferta_compra_url" />
                </div>
                <!-- linea 3 -->
                <div class="form-group col-md-4">
                    <label for="video" data-toggle="tooltip" data-placement="top" data-original-title="Escritura Pública con la que nuestro cliente vendedor adquirio el derecho real de dominio del inmueble">
                        Escritura pública
                    </label>
                    @if ($venta->escritura_publica_url != "")
                        <small>
                            <a href="{{ $venta->escritura_publica_url }}" target="_blank">
                                Ver: Escritura pública
                            </a>
                        </small>
                    @endif
                    <br>
                    <input type="file" id="escritura_publica_url" name="escritura_publica_url" />
                </div>
                <div class="form-group col-md-4">
                    <label for="video" data-toggle="tooltip" data-placement="top" data-original-title="Promesa firmada al final del ejercicio">
                        Promesa firmada
                    </label>
                    @if ($venta->promesa_firmada_url != "")
                        <small>
                            <a href="{{ $venta->promesa_firmada_url }}" target="_blank">
                                Ver: Promesa firmada
                            </a>
                        </small>
                    @endif
                    <br>
                    <input type="file" id="promesa_firmada_url" name="promesa_firmada_url" />
                </div>
                <div class="form-group col-md-4">
                    <label for="video" data-toggle="tooltip" data-placement="top" data-original-title="Certificado de Cámara de Comercio menor a 30 días (si aplica)">
                        Cámara de comercio
                    </label>
                    @if ($venta->camara_comercio_url != "")
                        <small>
                            <a href="{{ $venta->camara_comercio_url }}" target="_blank">
                                Ver: Cámara de comercio
                            </a>
                        </small>
                    @endif
                    <br>
                    <input type="file" id="camara_comercio_url" name="camara_comercio_url" />
                </div>
                <div class="form-group col-md-4">
                    <label for="video" data-toggle="tooltip" data-placement="top" data-original-title="Poderes en copia debidamente diligenciados por las partes (si aplica)">
                        Poderes en copia diligenciados
                    </label>
                    @if ($venta->poder_copia_diligenciada_url != "")
                        <small>
                            <a href="{{ $venta->poder_copia_diligenciada_url }}" target="_blank">
                                Ver: Poderes en copia diligenciados
                            </a>
                        </small>
                    @endif
                    <br>
                    <input type="file" id="poder_copia_diligenciada_url" name="poder_copia_diligenciada_url" />
                </div>
                <!-- Comentario para abogado -->
                <div class="form-group col-md-12">
                    <label for="comment" data-toggle="tooltip" data-placement="top" data-original-title="Promesa firmada al final del ejercicio">
                        @if ($usuario["rol_id"] == 6)
                            Comentario para agente
                        @else
                            Comentario para área jurídica
                        @endif
                    </label>
                    <br>
                    <textarea name="comment_email" rows="5" cols="100" placeholder="Comentarios adicionales...">{{ $ventaCorreo->comentario ?? "" }}</textarea>
                </div>
            </div>

        </div>
        <footer class="card-footer">
            <div class="row justify-content-end">
                <div class="col-sm-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <input type="hidden" name="venta_id" value="{{$venta->id}}">
                    <input type="hidden" name="inmueble_id" value="{{$venta->inmuebles->id}}">

                    <button type="submit" class="btn btn-primary" name="revision" value="0" ng-disabled="negocionVal">
                        Guardar
                    </button>

                    @if ($usuario->rol_id == 6 && $ventaCorreo != null)
                        @php
                            $urlAprobacion = $ventaCorreo->url_aprobacion ?? url("/") . "/alt/contrato-venta/" . base64_encode($venta->id . "_" . 47);
                        @endphp
                        <a href="{{ $urlAprobacion }}" target="_blank" class="btn btn-primary">
                            Revisar contrato
                        </a>
                    @else
                        <button type="submit" class="btn btn-primary" name="revision" value="1" ng-disabled="negocionVal">
                            Enviar a Revisión
                        </button>
                    @endif

                </div>
            </div>
        </footer>
    </section>
</form>
</div>


</section>
