<section role="main" class="content-body" ng-controller="Ventas2Controller">

    <header class="page-header">
        <h2>Ventas</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>Ventas</span></li>
                <li><span>Facturar venta&nbsp;&nbsp;&nbsp;</span></li>
            </ol>
            <a class="" data-open=""></a>
        </div>
    </header>

    <form action="/admin/ventas/seguimiento/crear" method="post" enctype="multipart/form-data">
        <input type="hidden" value="{{$venta->id}}" name="venta_id">
        <input type="hidden" value="4" name="estado_venta_id">
        <input type="hidden" value="{{$venta->usuarios->id}}" name="usuario_encargado">
        <input type="hidden" value="Factura" name="comentario">
        <input type="hidden" value="commentario_email" name="Factura para la venta {{$venta->id}}">
        @csrf
        <div class="row">

            <section class="card col-lg-12">
                <header class="card-header">
                    <h2 class="card-title">Facturar venta</h2>
                </header>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="habitaciones">Nombres y apellidos propietario</label>
                            <input type="text" class="form-control"
                                value="{{$venta->inmuebles->clientes[0]->clientes->nombres}} {{$venta->inmuebles->clientes[0]->clientes->apellidos}}"
                                name="name" disabled>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="banios">Cédula</label>
                            <input type="text" class="form-control"
                                value="{{$venta->inmuebles->clientes[0]->clientes->documento}}" name="doc"
                                placeholder="2" disabled>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="banios">Celular</label>
                            <input type="text" class="form-control"
                                value="{{$venta->inmuebles->clientes[0]->clientes->celular}}" name="cel" placeholder="2"
                                disabled>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="parqueaderos">Dirección</label>
                            <input type="text" class="form-control" value="" name="matricula" placeholder="2" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="parqueaderos_cubiertos">Teléfono</label>
                            <input type="text" class="form-control"
                                value="{{$venta->inmuebles->clientes[0]->clientes->telefono}}" name="phone"
                                placeholder="1" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="parqueaderos_descubiertos">Correo electrónico</label>
                            <input type="text" class="form-control"
                                value="{{$venta->inmuebles->clientes[0]->clientes->email}}" name="mail" placeholder="1"
                                disabled>
                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="parqueaderos_cubiertos">Agente captador </label>
                            <select class="form-control" name="captador" ng-model="captador"
                                ng-init="setCaptador('{{ $venta->inmuebles->asesores->id }}', '{{$venta->inmuebles->asesores->nombres}}','{{$venta->inmuebles->asesores->apellidos}}','{{$venta->inmuebles->asesores->documento}}')"
                                ng-change="setPlanCaptador('{{ $agentes }}')">
                                <option value="">Seleccionar asesor</option>

                                @foreach ($agentes as $agente)
                                <option value="{{ $agente->id }}">
                                    {{ $agente->nombres }} {{ $agente->apellidos }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="parqueaderos_descubiertos">Plan a. captador</label>
                            <input type="text" class="form-control" value="" name="cedulacatastral" placeholder="1"
                                disabled ng-model="plan_agente_captador"
                                ng-init="plan_agente_captador = '{{ $venta->inmuebles->asesores->plan }}'">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="parqueaderos_cubiertos">Agente vendedor</label>
                            <select class="form-control" name="promotor" ng-model="promotor"
                                ng-init="promotor = '{{ $venta->inmuebles->asesores->id }}'"
                                ng-change="setPlanPromotor('{{ $agentes }}')">
                                <option value="">Seleccionar asesor</option>

                                @foreach ($agentes as $agente)
                                <option value="{{ $agente->id }}">
                                    {{ $agente->nombres }} {{ $agente->apellidos }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="parqueaderos_descubiertos">Plan a. vendedor</label>
                            <input type="text" class="form-control" value="" disabled name="cedulacatastral"
                                placeholder="1" ng-model="plan_agente_promotor"
                                ng-init="plan_agente_promotor = '{{ $venta->inmuebles->asesores->plan }}'">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="parqueaderos"># MLS</label>
                            <input type="text" class="form-control" value="" name="matricula" placeholder="2">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="parqueaderos">Valor Comisión Pactada</label>
                            <input type="text" class="form-control" value="" name="comision_pactada"
                                ng-model="comision_pactada">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="parqueaderos_descubiertos">Precio del cierre</label>
                            <input type="text" class="form-control price_input" value="" name="precio_cierre"
                                ng-model="precio_cierre" placeholder="$ 100.000.000">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="parqueaderos">Tipo de cierre</label>
                            <select class="form-control" name="promotor">
                                <option value="">Seleccionar</option>
                                <option value="">Venta</option>
                                <option value="">Corretaje</option>
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="parqueaderos">Compartido MLS, AFYDI, otra inmobiliaria</label>
                            <input type="text" class="form-control" value="" name="matricula"
                                placeholder="Escriba el nombre de la otra inmobiliaria y grupo si aplica">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="parqueaderos">Puntas</label>
                            <select class="form-control" name="caso" ng-model="caso">
                                <option value="1">A. Tengo las 2 puntas 3%</option>
                                <option value="2">B. Soy captador 1.5%</option>
                                <option value="3">C. Compartido 2 agentes Elite</option>
                            </select>
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="parqueaderos_descubiertos">Cuenta de cobro</label><br>
                            <input type="file" name="file_1" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="parqueaderos_descubiertos">Cámara de comercio</label><br>
                            <input type="file" name="file_1" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="parqueaderos_descubiertos">RUT</label><br>
                            <input type="file" name="file_1" />
                        </div>
                    </div>

                </div>
                <footer class="card-footer">
                    <div class="row justify-content-end">
                        <div class="col-sm-12">

                            <button type="submit" class="btn btn-primary" ng-click="calcular($event)">
                                Calcular
                            </button>

                        </div>
                    </div>
                </footer>
            </section>

        </div>
        <div class="row" ng-init="agentes = {{$agentes}}">

            <section class="card col-lg-12" ng-cloak>
                <header class="card-header">
                    <h2 class="card-title">Valores factura</h2>
                </header>
                <div class="card-body">

                    <div class="form-row" ng-if="caso == '1'">
                        <div class="form-group col-md-12">
                            <table width="100%" border="1">
                                <tr>
                                    <th>Concepto (Caso A)</th>
                                    <th>Complemento</th>
                                    <th>Valor</th>
                                </tr>
                                <tr>
                                    <td>COMISIÓN VENTA MLS:</td>
                                    <td>1207-0018</td>
                                    <td><% comision_venta_mls | currency:"$ ":0 %></td>
                                </tr>
                                <tr>
                                    <td>INGRESOS PARA TERCEROS:</td>
                                    <td><%agente1.nombres%> <%agente1.apellidos%> C.C
                                        <%agente1.documento%></td>
                                    <td><% ingresos_terceros | currency:"$ ":0 %></td>
                                </tr>
                                <tr>
                                    <td colspan="2">SUBTOTAL FACTURA</td>
                                    <td><% subtotal_factura | currency:"$ ":0 %></td>
                                </tr>
                                <tr>
                                    <td colspan="2">IVA OFICINA 19%</td>
                                    <td><% iva_oficina | currency:"$ ":0 %></td>
                                </tr>
                                <tr>
                                    <td colspan="2">TOTAL FACTURA</td>
                                    <td><b><% total_factura | currency:"$ ":0 %></b></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="form-row" ng-if="caso == '2'">
                        <div class="form-group col-md-12">
                            <table width="100%" border="1">
                                <tr>
                                    <th>Concepto (Caso B1 y B2)</th>
                                    <th>Complemento</th>
                                    <th>Valor</th>
                                </tr>
                                <tr>
                                    <td>COMISIÓN VENTA MLS:</td>
                                    <td>1207-0018</td>
                                    <td><% comision_venta_mls | currency:"$ ":0 %></td>
                                </tr>
                                <tr>
                                    <td>INGRESOS PARA TERCEROS:</td>
                                    <td><%agente1.nombres%> <%agente1.apellidos%> C.C
                                        <%agente1.documento%>></td>
                                    <td><% ingresos_terceros | currency:"$ ":0 %></td>
                                </tr>
                                <tr>
                                    <td colspan="2">SUBTOTAL FACTURA</td>
                                    <td><% subtotal_factura | currency:"$ ":0 %></td>
                                </tr>
                                <tr>
                                    <td colspan="2">IVA OFICINA 19%</td>
                                    <td><% iva_oficina | currency:"$ ":0 %></td>
                                </tr>
                                <tr>
                                    <td colspan="2">TOTAL FACTURA</td>
                                    <td><b><% total_factura | currency:"$ ":0 %></b></td>
                                </tr>
                            </table>
                        </div>
                    </div>


                    <div class="form-row" ng-if="caso == '3'">
                        <div class="form-group col-md-12">
                            <table width="100%" border="1">
                                <tr>
                                    <th>Concepto (Caso C)</th>
                                    <th>Complemento</th>
                                    <th>Valor</th>
                                </tr>
                                <tr>
                                    <td>COMISIÓN VENTA MLS:</td>
                                    <td>1207-0018</td>
                                    <td><% comision_venta_mls | currency:"$ ":0 %></td>
                                </tr>
                                <tr>
                                    <td>INGRESOS PARA TERCEROS:</td>
                                    <td><%agente1.nombres%> <%agente1.apellidos%> C.C
                                        <%agente1.documento%></td>
                                    <td><% ingresos_terceros | currency:"$ ":0 %></td>
                                </tr>
                                <tr>
                                    <td>INGRESOS PARA TERCEROS:</td>
                                    <td><%agente2.nombres%> <%agente2.apellidos%> C.C
                                        <%agente2.documento%></td>
                                    <td><% ingresos_terceros_2 | currency:"$ ":0 %></td>
                                </tr>
                                <tr>
                                    <td colspan="2">SUBTOTAL FACTURA</td>
                                    <td><% subtotal_factura | currency:"$ ":0 %></td>
                                </tr>
                                <tr>
                                    <td colspan="2">IVA OFICINA 19%</td>
                                    <td><% iva_oficina | currency:"$ ":0 %></td>
                                </tr>
                                <tr>
                                    <td colspan="2">TOTAL FACTURA</td>
                                    <td><% total_factura | currency:"$ ":0 %></td>
                                </tr>
                            </table>
                        </div>
                    </div>


                    <div class="form-row" ng-if="caso == '40'">
                        <div class="form-group col-md-12">
                            <table width="100%" border="1">
                                <tr>
                                    <th>Concepto (Caso D)</th>
                                    <th>Complemento</th>
                                    <th>Valor</th>
                                </tr>
                                <tr>
                                    <td>COMISIÓN VENTA MLS:</td>
                                    <td>1207-0018</td>
                                    <td><% comision_venta_mls | currency:"$ ":0 %></td>
                                </tr>
                                <tr>
                                    <td>INGRESOS PARA TERCEROS:</td>
                                    <td>SINDY TAUTIVA C.C. XXXXXXX</td>
                                    <td><% ingresos_terceros | currency:"$ ":0 %></td>
                                </tr>
                                <tr>
                                    <td colspan="2">SUBTOTAL FACTURA</td>
                                    <td><% subtotal_factura | currency:"$ ":0 %></td>
                                </tr>
                                <tr>
                                    <td colspan="2">IVA OFICINA 19%</td>
                                    <td><% iva_oficina | currency:"$ ":0 %></td>
                                </tr>
                                <tr>
                                    <td colspan="2">TOTAL FACTURA</td>
                                    <td><% total_factura | currency:"$ ":0 %></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
                <footer class="card-footer">
                    <div class="row justify-content-end">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary">
                                Guardar y enviar a contabilidad
                            </button>

                        </div>
                    </div>
                </footer>
            </section>
    </form>
    </div>

</section>