<section role="main" class="content-body" ng-controller="Ventas">
    <form id=" formulario-crear-ventas" name="formulario-crear-ventas"
        action="{{ url("admin/ventas/1/{$inmueble->id}") }}" method="post" enctype="multipart/form-data">
        @csrf
        <header class="page-header">
            <h2>Ventas</h2>
            <div class="right-wrapper text-right">
                <ol class="breadcrumbs">
                    <li>
                        <a href="#">
                            <i class="bx bx-home-alt"></i>
                        </a>
                    </li>
                    <li><span>Ventas</span></li>
                    <li><span>Promesa de compraventa&nbsp;&nbsp;&nbsp;</span></li>
                </ol>
                <a class="" data-open=""></a>
            </div>
        </header>

        <input type="hidden" name="inmueble_id" value="{{ $inmueble->id }}" id="inmueble_id">

        <div class="row">
            <section class="card col-lg-12">
                <header class="card-header">
                    <h2 class="card-title">Promesa de compraventa</h2>
                </header>
                <div class="card-body">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="habitaciones">Dirección del inmueble</label>
                            <input type="text" class="form-control" value="{{ $inmueble->direccion }}" name="direccion"
                                placeholder="Cra 22 # 13 - 23" disabled>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="banios">Garajes</label>
                            <input type="text" class="form-control" value="{{ $inmueble->parqueaderos }}" name="garaje"
                                placeholder="2" disabled>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="banios">Depósito</label>
                            <input type="text" class="form-control" value="{{ $inmueble->depositos }}" name="deposito"
                                placeholder="2" disabled>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="banios">Otro</label>
                            <input type="text" class="form-control" value="" name="otro" placeholder="2" disabled>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="parqueaderos"># Matrícula</label>
                            <input type="text" class="form-control" value="{{ $inmueble->matricula }}" name="matricula"
                                placeholder="2" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="parqueaderos_cubiertos">Chip</label>
                            <input type="text" class="form-control" value="{{ $inmueble->chip }}" name="chip"
                                placeholder="1" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="parqueaderos_descubiertos">Cédula catastral</label>
                            <input type="text" class="form-control" value="{{ $inmueble->cedula_catastral }}"
                                name="cedulacatastral" placeholder="1" disabled>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="area_lote"># Matrícula parqueadero</label>
                            <input type="text" class="form-control" value="{{ $inmueble->matricula_parqueadero_1 }}"
                                id="matricula_parqueadero" name="matricula_parqueadero" placeholder="80" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="area_cons">Chip parqueadero</label>
                            <input type="text" class="form-control" value="{{ $inmueble->chip_parqueadero_1 }}"
                                id="chip_parqueadero" name="chip_parqueadero" placeholder="60" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="area_cons">Cédula catastral parqueadero</label>
                            <input type="text" class="form-control" value="{{ $inmueble->cedula_parqueadero_1 }}"
                                id="cedula_catastral_parqueadero" name="cedula_catastral_parqueadero" placeholder="60"
                                disabled>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="area_lote"># Matrícula parqueadero 2</label>
                            <input type="text" class="form-control" value="{{ $inmueble->matricula_parqueadero_2 }}"
                                id="matricula_parqueadero" name="matricula_parqueadero" placeholder="80" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="area_cons">Chip parqueadero 2</label>
                            <input type="text" class="form-control" value="{{ $inmueble->chip_parqueadero_2 }}"
                                id="chip_parqueadero" name="chip_parqueadero" placeholder="60" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="area_cons">Cédula catastral parqueadero 2</label>
                            <input type="text" class="form-control" value="{{ $inmueble->cedula_parqueadero_2 }}"
                                id="cedula_catastral_parqueadero" name="cedula_catastral_parqueadero" placeholder="60"
                                disabled>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="area_lote"># Matrícula parqueadero 3</label>
                            <input type="text" class="form-control" value="{{ $inmueble->matricula_parqueadero_3 }}"
                                id="matricula_parqueadero" name="matricula_parqueadero" placeholder="80" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="area_cons">Chip parqueadero 3</label>
                            <input type="text" class="form-control" value="{{ $inmueble->chip_parqueadero_3 }}"
                                id="chip_parqueadero" name="chip_parqueadero" placeholder="60" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="area_cons">Cédula catastral parqueadero 3</label>
                            <input type="text" class="form-control" value="{{ $inmueble->cedula_parqueadero_3 }}"
                                id="cedula_catastral_parqueadero" name="cedula_catastral_parqueadero" placeholder="60"
                                disabled>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="area_lote"># Matrícula depósito</label>
                            <input type="text" class="form-control" value="{{ $inmueble->matricula_deposito_1 }}"
                                id="matricula_deposito" name="matricula_deposito" placeholder="80" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="area_cons">Chip depósito</label>
                            <input type="text" class="form-control" value="{{ $inmueble->chip_deposito_1 }}"
                                id="chip_deposito" name="chip_deposito" placeholder="60" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="area_cons">Cédula catastral depósito</label>
                            <input type="text" class="form-control" value="{{ $inmueble->cedula_deposito_1 }}"
                                id="cedula_catastral_deposito" name="cedula_catastral_deposito" placeholder="60"
                                disabled>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="area_lote"># Matrícula depósito 2</label>
                            <input type="text" class="form-control" value="{{ $inmueble->matricula_deposito_2 }}"
                                id="matricula_deposito" name="matricula_deposito" placeholder="80" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="area_cons">Chip depósito 2</label>
                            <input type="text" class="form-control" value="{{ $inmueble->chip_deposito_2 }}"
                                id="chip_deposito" name="chip_deposito" placeholder="60" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="area_cons">Cédula catastral depósito 2</label>
                            <input type="text" class="form-control" value="{{ $inmueble->cedula_deposito_2 }}"
                                id="cedula_catastral_deposito" name="cedula_catastral_deposito" placeholder="60"
                                disabled>
                        </div>
                    </div>

                </div>
            </section>

        </div>
        <br>
        <div class="row">

            <section class="card col-lg-12">
                <header class="card-header">
                    <h2 class="card-title">Vendedor 1</h2>
                </header>
                @foreach ($inmueble->clientes as $clientes)
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="habitaciones">Nombres y apellidos</label>

                            <input type="text" class="form-control"
                                value="{{ $clientes->clientes->nombres }} {{ $clientes->clientes->apellidos }}"
                                placeholder="Pedro Perez Galindo" disabled>

                        </div>
                        <div class="form-group col-md-3">
                            <label for="banios">Cédula</label>
                            <input type="text" class="form-control" value="{{ $clientes->clientes->documento }}"
                                placeholder="2" disabled>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="banios">Estado civil</label>
                            <input type="text" class="form-control" value="{{ $clientes->clientes->estado_civil }}"
                                placeholder="2" disabled>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="parqueaderos">Dirección</label>
                            <input type="text" class="form-control" value="{{ $clientes->clientes->direccion }}"
                                placeholder="2" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="parqueaderos_cubiertos">Teléfono</label>
                            <input type="text" class="form-control" value="{{ $clientes->clientes->telefono }}"
                                placeholder="1" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="parqueaderos_descubiertos">Correo electrónico</label>
                            <input type="email" class="form-control" value="{{ $clientes->clientes->email }}"
                                placeholder="1" disabled>
                        </div>
                    </div>
                </div>
                @endforeach
            </section>

        </div>
        <br>
        <div class="row">
            <section class="card col-lg-12">
                <header class="card-header">
                    <h2 class="card-title">Comprador 1</h2>
                </header>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="habitaciones">Nombres y apellidos</label>
                            <input type="text" class="form-control" value="{{ old("comprador_1_nombre") }}"
                                id="comprador_1_nombre" name="comprador_1_nombre" placeholder="Pedro Perez Galindo">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="banios">Cédula</label>
                            <input type="text" class="form-control" value="{{ old("comprador_1_cedula") }}"
                                id="comprador_1_cedula" name="comprador_1_cedula" placeholder="31950070">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="banios">Estado civil</label>
                            <select class="form-control" id="comprador_1_estado_civil" name="comprador_1_estado_civil">
                                <option value="">Seleccionar</option>
                                <option value="soltero"
                                    {{ old("comprador_1_estado_civil") == "soltero" ? "selected" : ""}}>
                                    Soltero</option>
                                <option value="casado"
                                    {{ old("comprador_1_estado_civil") == "casado" ? "selected" : ""}}>
                                    Casado</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="parqueaderos">Dirección</label>
                            <input type="text" class="form-control" value="{{ old("comprador_1_direccion") }}"
                                id="comprador_1_direccion" name="comprador_1_direccion" placeholder="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="parqueaderos_cubiertos">Teléfono</label>
                            <input type="text" class="form-control" value="{{ old("comprador_1_telefono") }}"
                                id="comprador_1_telefono" name="comprador_1_telefono" placeholder="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="parqueaderos_descubiertos">Correo electrónico</label>
                            <input type="email" class="form-control" value="{{ old("comprador_1_email") }}"
                                id="comprador_1_email" name="comprador_1_email" placeholder="">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="parqueaderos">Cédula</label>
                            <input type="file" name="comprador_1_cedula_url" />
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <br>

        <div class="row">

            <section class="card col-lg-12">
                <header class="card-header">
                    <h2 class="card-title">Comprador 2</h2>
                </header>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="habitaciones">Nombres y apellidos</label>
                            <input type="text" class="form-control" value="{{ old("comprador_2_nombre") }}"
                                id="comprador_2_nombre" name="comprador_2_nombre" placeholder="Pedro Perez Galindo">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="banios">Cédula</label>
                            <input type="text" class="form-control" value="{{ old("comprador_2_cedula") }}"
                                id="comprador_2_cedula" name="comprador_2_cedula" placeholder="31950070">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="banios">Estado civil</label>
                            <select class="form-control" id="comprador_2_estado_civil" name="comprador_2_estado_civil">
                                <option value="">Seleccionar</option>
                                <option value="soltero"
                                    {{ old("comprador_2_estado_civil") == "soltero" ? "selected" : ""}}>
                                    Soltero</option>
                                <option value="casado"
                                    {{ old("comprador_2_estado_civil") == "casado" ? "selected" : ""}}>
                                    Casado</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="parqueaderos">Dirección</label>
                            <input type="text" class="form-control" value="{{ old("comprador_2_direccion") }}"
                                id="comprador_2_direccion" name="comprador_2_direccion" placeholder="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="parqueaderos_cubiertos">Teléfono</label>
                            <input type="text" class="form-control" value="{{ old("comprador_2_telefono") }}"
                                id="comprador_2_telefono" name="comprador_2_telefono" placeholder="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="parqueaderos_descubiertos">Correo electrónico</label>
                            <input type="email" class="form-control" value="{{ old("comprador_2_email") }}"
                                id="comprador_2_email" name="comprador_2_email" placeholder="">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="parqueaderos">Cédula</label>
                            <input type="file" name="comprador_2_cedula_url" />
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <br>
        <div class="row">

            <section class="card col-lg-12">
                <header class="card-header">
                    <h2 class="card-title">Comprador 3</h2>
                </header>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="habitaciones">Nombres y apellidos</label>
                            <input type="text" class="form-control" value="{{ old("comprador_3_nombre") }}"
                                id="comprador_3_nombre" name="comprador_3_nombre" placeholder="Pedro Perez Galindo">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="banios">Cédula</label>
                            <input type="text" class="form-control" value="{{ old("comprador_3_cedula") }}"
                                id="comprador_3_cedula" name="comprador_3_cedula" placeholder="31950070">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="banios">Estado civil</label>
                            <select class="form-control" id="comprador_3_estado_civil" name="comprador_3_estado_civil">
                                <option value="">Seleccionar</option>
                                <option value="soltero"
                                    {{ old("comprador_3_estado_civil") == "soltero" ? "selected" : ""}}>
                                    Soltero</option>
                                <option value="casado"
                                    {{ old("comprador_3_estado_civil") == "casado" ? "selected" : ""}}>
                                    Casado</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="parqueaderos">Dirección</label>
                            <input type="text" class="form-control" value="{{ old("comprador_3_direccion") }}"
                                id="comprador_3_direccion" name="comprador_3_direccion" placeholder="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="parqueaderos_cubiertos">Teléfono</label>
                            <input type="text" class="form-control" value="{{ old("comprador_3_telefono") }}"
                                id="comprador_3_telefono" name="comprador_3_telefono" placeholder="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="parqueaderos_descubiertos">Correo electrónico</label>
                            <input type="email" class="form-control" value="{{ old("comprador_3_email") }}"
                                id="comprador_3_email" name="comprador_3_email" placeholder="">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="parqueaderos">Cédula</label>
                            <input type="file" name="comprador_3_cedula_url" />
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <br>

        <div class="row">

            <section class="card col-lg-12">
                <header class="card-header">
                    <h2 class="card-title">Comprador 4</h2>
                </header>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="habitaciones">Nombres y apellidos</label>
                            <input type="text" class="form-control" value="{{ old("comprador_4_nombre") }}"
                                id="comprador_4_nombre" name="comprador_4_nombre" placeholder="Pedro Perez Galindo">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="banios">Cédula</label>
                            <input type="text" class="form-control" value="{{ old("comprador_4_cedula") }}"
                                id="comprador_4_cedula" name="comprador_4_cedula" placeholder="31950070">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="banios">Estado civil</label>
                            <select class="form-control" id="comprador_4_estado_civil" name="comprador_4_estado_civil">
                                <option value="">Seleccionar</option>
                                <option value="soltero"
                                    {{ old("comprador_4_estado_civil") == "soltero" ? "selected" : ""}}>
                                    Soltero</option>
                                <option value="casado"
                                    {{ old("comprador_4_estado_civil") == "casado" ? "selected" : ""}}>
                                    Casado</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="parqueaderos">Dirección</label>
                            <input type="text" class="form-control" value="{{ old("comprador_4_direccion") }}"
                                id="comprador_4_direccion" name="comprador_4_direccion" placeholder="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="parqueaderos_cubiertos">Teléfono</label>
                            <input type="text" class="form-control" value="{{ old("comprador_4_telefono") }}"
                                id="comprador_4_telefono" name="comprador_4_telefono" placeholder="">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="parqueaderos_descubiertos">Correo electrónico</label>
                            <input type="email" class="form-control" value="{{ old("comprador_4_email") }}"
                                id="comprador_4_email" name="comprador_4_email" placeholder="">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="parqueaderos">Cédula</label>
                            <input type="file" name="comprador_4_cedula_url" />
                        </div>
                    </div>
                </div>
            </section>

        </div>
        <br>
        <div class="row">

            <section class="card col-lg-12">
                <header class="card-header">
                    <h2 class="card-title">Negociación</h2>
                </header>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Cuál es el precio del inmueble?">Precio</label>
                            <input type="text" class="form-control price_input" value="{{ old("negociacion_precio") }}"
                                ng-change="updateSaldoPendiente()" ng-model="negociacion_precio"
                                ng-init="negociacion_precio = '0'" id="negociacion_precio" name="negociacion_precio"
                                placeholder="$100.000.000">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Cuál es el valor pagado a promesa?">Pagado promesa</label>
                            <input type="text" class="form-control price_input" id="negociacion_pagado_promesa"
                                ng-model="negociacion_pagado_promesa" ng-init="negociacion_pagado_promesa = '0'"
                                name="negociacion_pagado_promesa" placeholder="$10.000.000">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="">Saldos Pendientes <span ng-if="negociacion_number < 0"
                                    class="danger-text">El saldo
                                    no puede ser
                                    negativo</span></label>
                            <input type="text"
                                class="form-control price_input <%negociacion_number < 0 ? 'danger-input' : ''%>"
                                ng-model="negociacion_saldo_pendiente" ng-init="negociacion_saldo_pendiente = '0'"
                                id="negociacion_saldo_pendiente" name="negociacion_saldo_pendiente" placeholder="$0"
                                readonly>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Cuánto se pactó por concepto de arras?">Arras</label>
                            <input type="text" class="form-control price_input" ng-model="negociacion_arras"
                                ng-init="negociacion_arras = '0'" id="negociacion_arras" name="negociacion_arras"
                                placeholder="$50.000.000">
                        </div>
                        <div class="form-group col-md-8">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top" data-original-title="¿A favor de quién se hace el pago de Arras? (Nombre y cédula si es diferente al
                            vendedor)">Arras a favor de</label>
                            <input type="text" class="form-control" id="negociacion_arras_favor"
                                name="negociacion_arras_favor" placeholder="Nombre y cédula">
                        </div>

                    </div>

                </div>
            </section>

        </div>
        <br>
        <div class="row">

            <section class="card col-lg-12">
                <header class="card-header">
                    <h2 class="card-title">Pago 1</h2>
                </header>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Cuál es la fecha y hora en que se firma la promesa y primer pago? D/M/A">Fecha
                                promesa y pago 1</label>
                            <input type="date" class="form-control"
                                value="{{ old("pago_1_fecha_promesa_pago") }}" id="pago_1_fecha_promesa_pago"
                                name="pago_1_fecha_promesa_pago" placeholder="13/09/2021 Hora: 10:00 am">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Cuál es la Notaría en que se firma la promesa?">Notaría</label>
                            <input type="text" class="form-control" value="{{ old("pago_1_notaria") }}"
                                id="pago_1_notaria" name="pago_1_notaria" placeholder="Décima de Cali">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Cuál es el medio de pago para el valor que se da en la promesa? (cheque, efectivo, transferencia, etc)">Medio
                                de pago 1</label>
                            <input type="text" class="form-control" value="{{ old("pago_1_medio_pago") }}"
                                id="pago_1_medio_pago" name="pago_1_medio_pago" placeholder="Transferencia bancaria">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="">Valor del primer pago</label>
                            <input type="text" class="form-control price_input" value="{{ old("pago_1_valor_pago") }}"
                                ng-model="pago_1_valor_pago" ng-change="updateSaldoPendiente()"
                                ng-init="pago_1_valor_pago = '0'" id="pago_1_valor_pago" name="pago_1_valor_pago"
                                placeholder="$10.000.000">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="Si el pago es con cheque, indique nombres y cédula de la persona a la que debe ir girado el cheque. Si el pago es por transferencia, indique número de cuenta, tipo de cuenta y entidad">Datos
                                si es cheque o transferencia</label>
                            <input type="text" class="form-control" value="{{ old("pago_1_cheque_transferencia") }}"
                                id="pago_1_cheque_transferencia" name="pago_1_cheque_transferencia"
                                placeholder="Cuenta de ahorros Banco Caja Social No. 24034003666">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Se paga con recursos propios, por medio de banco o alguna otra entidad? (Mencione cual) Si es con credito indique la modalidad del credito.">Origen
                                recursos</label>
                            <input type="text" class="form-control" value="{{ old("pago_1_origen_recurso") }}"
                                id="pago_1_origen_recurso" name="pago_1_origen_recurso" placeholder="Recursos propios">
                        </div>
                    </div>

                </div>
            </section>

        </div>
        <br>
        <div class="row">

            <section class="card col-lg-12">
                <header class="card-header">
                    <h2 class="card-title">Pago 2</h2>
                </header>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Fecha y lugar en que se hace el segundo pago? D/M/A">Fecha pago
                                2</label>
                            <input type="date" class="form-control" value="{{ old("pago_2_medio_pago") }}"
                                id="pago_2_medio_pago" name="pago_2_fecha_pago" placeholder="20/09/21">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Cuál es el medio para realizar el segundo pago? (cheque, efectivo, etc)">Medio
                                pago 2</label>
                            <input type="text" class="form-control" value="{{ old("pago_2_pago_favor") }}"
                                id="pago_2_pago_favor" name="pago_2_pago_favor" placeholder="Cheque">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿A favor de quién se hace el segundo pago? (Nombre y cédula si es diferente al comprador y vendedor)">Pago
                                2 a favor de</label>
                            <input type="text" class="form-control" value="{{ old("pago_2_pago_favor") }}"
                                id="pago_2_pago_favor" name="pago_2_pago_favor" placeholder="El vendedor">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="">Valor del segundo pago</label>
                            <input type="text" class="form-control price_input" value="{{ old("pago_2_valor_pago") }}"
                                ng-model="pago_2_valor_pago" ng-change="updateSaldoPendiente()"
                                ng-init="pago_2_valor_pago = '0'" id="pago_2_valor_pago" name="pago_2_valor_pago"
                                placeholder="$20.000.000">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="Si el pago es con cheque, indique nombres y cédula de la persona a la que debe ir girado el cheque. Si el pago es por transferencia, indique número de cuenta, tipo de cuenta y entidad">Datos
                                si es cheque o transferencia</label>
                            <input type="text" class="form-control" value="{{ old("pago_2_cheque_transferencia") }}"
                                id="pago_2_cheque_transferencia" name="pago_2_cheque_transferencia"
                                placeholder="SERGIO PULIDO LOPEZ CC. No. 19163441">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Se paga con recursos propios, por medio de banco o alguna otra entidad? (Mencione cual) Si es con credito indique la modalidad del credito.">Origen
                                recursos</label>
                            <input type="text" class="form-control" value="{{ old("pago_2_origen_recurso") }}"
                                id="pago_2_origen_recurso" name="pago_2_origen_recurso"
                                placeholder="Fondo de Pensiones PORVENIR">
                        </div>
                    </div>

                </div>
            </section>

        </div>
        <br>
        <div class="row">

            <section class="card col-lg-12">
                <header class="card-header">
                    <h2 class="card-title">Pago 3</h2>
                </header>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Fecha y lugar en que se hace el segundo pago? D/M/A">Fecha pago
                                3</label>
                            <input type="date" class="form-control" value="{{ old("pago_3_medio_pago") }}"
                                id="pago_3_medio_pago" name="pago_3_fecha_pago" placeholder="20/09/21">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Cuál es el medio para realizar el segundo pago? (cheque, efectivo, etc)">Medio
                                pago 3</label>
                            <input type="text" class="form-control" value="{{ old("pago_3_pago_favor") }}"
                                id="pago_3_pago_favor" name="pago_3_pago_favor" placeholder="Cheque">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿A favor de quién se hace el segundo pago? (Nombre y cédula si es diferente al comprador y vendedor)">Pago
                                3 a favor de</label>
                            <input type="text" class="form-control" value="{{ old("pago_3_pago_favor") }}"
                                id="pago_3_pago_favor" name="pago_3_pago_favor" placeholder="El vendedor">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="">Valor del tercer pago</label>
                            <input type="text" class="form-control price_input" value="{{ old("pago_3_valor_pago") }}"
                                ng-model="pago_3_valor_pago" ng-change="updateSaldoPendiente()"
                                ng-init="pago_3_valor_pago = '0'" id="pago_3_valor_pago" name="pago_3_valor_pago"
                                placeholder="$20.000.000">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="Si el pago es con cheque, indique nombres y cédula de la persona a la que debe ir girado el cheque. Si el pago es por transferencia, indique número de cuenta, tipo de cuenta y entidad">Datos
                                si es cheque o transferencia</label>
                            <input type="text" class="form-control" value="{{ old("pago_3_cheque_transferencia") }}"
                                id="pago_3_cheque_transferencia" name="pago_3_cheque_transferencia"
                                placeholder="SERGIO PULIDO LOPEZ CC. No. 19163441">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Se paga con recursos propios, por medio de banco o alguna otra entidad? (Mencione cual) Si es con credito indique la modalidad del credito.">Origen
                                recursos</label>
                            <input type="text" class="form-control" value="{{ old("pago_3_origen_recurso") }}"
                                id="pago_3_origen_recurso" name="pago_3_origen_recurso"
                                placeholder="Fondo de Pensiones PORVENIR">
                        </div>
                    </div>

                </div>
            </section>

        </div>
        <br>
        <div class="row">

            <section class="card col-lg-12">
                <header class="card-header">
                    <h2 class="card-title">Pago 4</h2>
                </header>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Fecha y lugar en que se hace el segundo pago? D/M/A">Fecha pago
                                4</label>
                            <input type="date" class="form-control" value="{{ old("pago_4_medio_pago") }}"
                                id="pago_4_medio_pago" name="pago_4_fecha_pago" placeholder="20/09/21">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Cuál es el medio para realizar el segundo pago? (cheque, efectivo, etc)">Medio
                                pago 4</label>
                            <input type="text" class="form-control" value="{{ old("pago_4_pago_favor") }}"
                                id="pago_4_pago_favor" name="pago_4_pago_favor" placeholder="Cheque">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿A favor de quién se hace el segundo pago? (Nombre y cédula si es diferente al comprador y vendedor)">Pago
                                4 a favor de</label>
                            <input type="text" class="form-control" value="{{ old("pago_4_pago_favor") }}"
                                id="pago_4_pago_favor" name="pago_4_pago_favor" placeholder="El vendedor">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="">Valor del cuarto pago</label>
                            <input type="text" class="form-control price_input" value="{{ old("pago_4_valor_pago") }}"
                                ng-model="pago_4_valor_pago" ng-change="updateSaldoPendiente()"
                                ng-init="pago_4_valor_pago = '0'" id="pago_4_valor_pago" name="pago_4_valor_pago"
                                placeholder="$20.000.000">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="Si el pago es con cheque, indique nombres y cédula de la persona a la que debe ir girado el cheque. Si el pago es por transferencia, indique número de cuenta, tipo de cuenta y entidad">Datos
                                si es cheque o transferencia</label>
                            <input type="text" class="form-control" value="{{ old("pago_4_cheque_transferencia") }}"
                                id="pago_4_cheque_transferencia" name="pago_4_cheque_transferencia"
                                placeholder="SERGIO PULIDO LOPEZ CC. No. 19163441">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Se paga con recursos propios, por medio de banco o alguna otra entidad? (Mencione cual) Si es con credito indique la modalidad del credito.">Origen
                                recursos</label>
                            <input type="text" class="form-control" value="{{ old("pago_4_origen_recurso") }}"
                                id="pago_4_origen_recurso" name="pago_4_origen_recurso"
                                placeholder="Fondo de Pensiones PORVENIR">
                        </div>
                    </div>

                </div>
            </section>

        </div>
        <br>
        <div class="row">

            <section class="card col-lg-12">
                <header class="card-header">
                    <h2 class="card-title">Escrituración</h2>
                </header>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Cuál es la fecha y hora en que se firma la Escritura Pública?">Fecha
                                escritura pública</label>
                            <input type="date" class="form-control"
                                value="{{ old("escritura_fecha_publica") }}" id="escritura_fecha_publica"
                                name="escritura_fecha_publica" placeholder="22/10/2021 Hora: 2:00pm">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Cuál es la Notaría en que se firma la Escritura Pública?">Notaría</label>
                            <input type="text" class="form-control" value="{{ old("escritura_notaria") }}"
                                id="escritura_notaria" name="escritura_notaria"
                                placeholder="Notaría 62 Carrera 24 No. 53-18">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Cuál es la fecha de entrega real y material del inmueble?">Fecha
                                entrega inmueble</label>
                            <input type="date" class="form-control"
                                value="{{ old("escritura_fecha_entrega_inmueble") }}"
                                id="escritura_fecha_entrega_inmueble" name="escritura_fecha_entrega_inmueble"
                                placeholder="22/10/21">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Quién asume el impuesto predial ?">Predial asumido por</label>
                            <input type="text" class="form-control" value="{{ old("escritura_predial_asumido") }}"
                                id="escritura_predial_asumido" name="escritura_predial_asumido"
                                placeholder="Proporcional entre las partes">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Quién asume los gastos Notariales?">Gastos notariales asumidos
                                por</label>
                            <input type="text" class="form-control"
                                value="{{ old("escritura_gasto_notarial_asumido") }}"
                                id="escritura_gasto_notarial_asumido" name="escritura_gasto_notarial_asumido"
                                placeholder="50% Comprador 50% Vendedor">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Quién asume beneficencia e inscripción en Registro de Instrumentos Públicos?">Beneficiado
                                por instrumentos públicos</label>
                            <input type="text" class="form-control"
                                value="{{ old("escritura_beneficiado_instrumento_publico") }}"
                                id="escritura_beneficiado_instrumento_publico"
                                name="escritura_beneficiado_instrumento_publico" placeholder="EL COMPRADOR">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Se pactó una condición de negociación adicional? (Mencione cual)">¿Negociación
                                adicional?</label>
                            <input type="text" class="form-control" value="{{ old("escritura_negociacion_adicional") }}"
                                id="escritura_negociacion_adicional" name="escritura_negociacion_adicional"
                                placeholder="NO">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="¿Cómo y cuando será pagada la comisión de Elite?">pago comisión
                                Elite</label>
                            <input type="text" class="form-control" value="{{ old("escritura_pago_comision_elite") }}"
                                id="escritura_pago_comision_elite" name="escritura_pago_comision_elite"
                                placeholder="A la firma de la promesa de Compraventa">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="Texto libre para el Agente">Observaciones</label>
                            <input type="text" class="form-control" value="{{ old("escritura_observacion") }}"
                                id="escritura_observacion" name="escritura_observacion"
                                placeholder="Introduzca comentarios adicionales">
                        </div>
                    </div>

                </div>
            </section>


        </div>
        <br>
        <div class="row">

            <section class="card col-lg-12">
                <header class="card-header">
                    <h2 class="card-title">Documentos</h2>
                </header>
                <div class="card-body">
                    <div class="form-row">
                        <!-- linea 1 -->
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Certificado de tradición del inmueble menor a 30 días">CLT</label>
                            <br>
                            <input type="file" id="clt_url" name="clt_url" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Certificado de tradición del parqueadero menor a días (si aplica)">CLT
                                parqueadero</label>
                            <br>
                            <input type="file" id="clt_parqueadero_1_url" name="clt_parqueadero_1_url" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Certificado de tradición del depósito menor a 30 días (si aplica)">CLT
                                depósito</label>
                            <br>
                            <input type="file" id="clt_deposito_1_url" name="clt_deposito_1_url" />
                        </div>
                        <!-- linea 2 -->
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Oferta de compra">Oferta de compra</label>
                            <br>
                            <input type="file" id="oferta_compra_url" name="oferta_compra_url" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Certificado de tradición del parqueadero 2 menor a días (si aplica)">CLT
                                parqueadero 2</label>
                            <br>
                            <input type="file" id="clt_parqueadero_2_url" name="clt_parqueadero_2_url" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Certificado de tradición del depósito 2 menor a 30 días (si aplica)">CLT
                                depósito 2</label>
                            <br>
                            <input type="file" id="clt_deposito_2_url" name="clt_deposito_2_url" />
                        </div>
                        <!-- linea 3 -->
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Escritura Pública con la que nuestro cliente vendedor adquirio el derecho real de dominio del inmueble">Escritura
                                pública</label>
                            <br>
                            <input type="file" id="escritura_publica_url" name="escritura_publica_url" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Certificado de Cámara de Comercio menor a 30 días (si aplica)">Cámara
                                de comercio</label>
                            <br>
                            <input type="file" id="camara_comercio_url" name="camara_comercio_url" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Poderes en copia debidamente diligenciados por las partes (si aplica)">Poderes
                                en copia diligenciados</label>
                            <br>
                            <input type="file" id="poder_copia_diligenciada_url" name="poder_copia_diligenciada_url" />
                        </div>
                        <!-- linea 4 -->
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Promesa firmada al final del ejercicio">Promesa firmada</label>
                            <br>
                            <input type="file" id="promesa_firmada_url" name="promesa_firmada_url" />
                        </div>
                    </div>

                </div>
                <footer class="card-footer">
                    <div class="row justify-content-end">
                        <div class="col-sm-12">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <button type="submit" class="btn btn-primary" name="revision" value="0"
                                ng-disabled="negocionVal">
                                Guardar promesa de compraventa
                            </button>
                            <button type="submit" class="btn btn-primary" name="revision" value="1"
                                ng-disabled="negocionVal">
                                Guardar y enviar promesa de compraventa
                            </button>
                        </div>
                    </div>
                </footer>
            </section>

        </div>
    </form>
</section>
