<!DOCTYPE html>
<html lang="es-ES">
<head>
    @php
    $height = "100%";
    $width = "100%";
    @endphp

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Contrato</title>

    <style media="screen">
    @font-face {
        font-family: "Calibri";
        font-weight: normal;
        font-style: normal;
        font-variant: normal;
        src: url(https://fonts.gstatic.com/l/font?kit=J7afnpV-BGlaFfdAhLEY6w&skey=a1029226f80653a8&v=v11) format("woff2");
    }
    @page {
        margin: 0px;
        margin-top: 7.5% !important;
    }
    body {
        margin: 0px;
        font-family: "Calibri", sans-serif;
        background-image: url('{{ asset("assets/img/marca_de_agua.png") }}');
        background-size: contain;
    }

    html {
        margin: 0px;
    }

    .pagina {
        margin-left: 7.5%;
        margin-right: 7.5%;
        margin-bottom: 7.5%;
    }

    .header {
        text-align: center;
        position: fixed;
        top: 0 !important;
        margin-top: -7.5% !important;
        left: 25%;
    }

    .page_break {
        page-break-after: always;
    }
    </style>
</head>
<body style="margin:0;">

    <div class="page_break">
        <img alt="" src="{{ asset("assets/img/logo_encabezado_2.png") }}" width="100%">
        <div id="p1_contenido" class="pagina">
            <br>
            <br>
            <br>
            <br>
            <p style="text-align: justify">
                <b>Advertencia</b>: El texto de la presente promesa de compraventa
                es un aporte <u><b>SUGERIDO</b></u> de <b>Elite Inmobiliaria</b> a las partes,
                pero en ningún momento reemplaza la voluntad de las mismas, ni compromete la
                responsabilidad de la inmobiliaria o sus agentes asociados. Los cambios que se
                le introduzcan son una facultad exclusiva de las partes. No obstante lo anterior:
            </p>
            <h3 style="text-align: center">CERSIÓRESE:</h3>
            <ol style="text-align: justify;">
                <li>
                    Que su nombre e identificación estén como aparece en el documento de identidad.
                </li>
                <li>
                    Que el inmueble descrito sí sea el que se promete vender y comprar
                    <span style="font-size: 9px;"><i>1</i></span>.
                </li>
                <li>
                    Que el precio y la forma de pago en cuanto a la cuantía de los pagos y sus
                    correspondientes fechas, son los acordados con su contraparte. De lo contrario
                    o en caso de una nueva propuesta, acuérdelo con su contraparte y reemplace el
                    texto sugerido. Esas son decisiones que solo le competen a las partes.
                </li>
                <li>
                    Que la fecha, hora y Notaría escogidas para la firma de las escrituras sean las
                    que acordó con su contraparte; de lo contrario reemplace el texto sugerido por
                    aquellas que haya acordado nuevamente con su contraparte. Esas son decisiones
                    que solo le competen a las partes.
                </li>
                <li>
                    Que la fecha y hora para la entrega y recibo real y material del inmueble sean
                    las que acordó con su contraparte; de lo contrario reemplace el texto sugerido
                    por aquellas que haya acordado nuevamente con su contraparte. Esas son decisiones
                    que solo le competen a las partes.
                </li>
                <li>
                    Que ha quedado a conformidad con la negociación frente al impuesto de valorización
                    he impacto tributario de la reciente ley de financiamiento 1943 de 2018.
                </li>
            </ol>
        </div>
        <div class="pie" style="position: absolute; bottom: 60px; width: 80%; margin: -0px 10%">
            <p>
                <i>
                    1. Aún persiste la discusión de si la promesa debe incluir los linderos o no;
                    hay tesis encontradas y sustentadas en ambos sentidos. Es claro que la escritura
                    sí debe contener los linderos.
                </i>
            </p>
        </div>
    </div>

    <div class="header">
        <p>
            Contrato de promesa de compraventa {{ $venta->inmuebles->direccion }}
        </p>
    </div>

    <div class="pagina">
        <h4 style="text-align: center;">
            CONTRATO DE PROMESA DE COMPRAVENTA SOBRE {{ $venta->inmuebles->direccion }}
        </h4>

        {{-- Texto general --}}
        <p style="text-align: justify;">
            Entre los suscritos a saber, de una parte:
            @foreach ($vendedores as $key => $vendedor)
                {{ $general->mayus($vendedor->nombre) }} mayor de edad, domiciliado(a) en la ciudad de: {{ $vendedor->ciudad }},
                e identificado(a) con la {{ $vendedor->tipos_documento->nombre }} {{ $vendedor->numero_documento }}
                de estado civil: {{ $vendedor->estado_civil ?? "" }}
                {{ $vendedor->subestadociviles?->nombre }}

                @if (isset($vendedor->apoderados))
                    representado(a) mediante poder especial por: {{ $general->mayus($vendedor->apoderados->nombre) }}, mayor de edad,
                    domiciliado(a) en la ciudad de: {{ $vendedor->apoderados->ciudad }}, e identificado con la
                    {{ $vendedor->apoderados->tipos_documento->nombre }} {{ $vendedor->apoderados->numero_documento }}
                @endif

                @if ($key == count($vendedores) - 1)

                @elseif ($key == count($vendedores) - 2)
                    y
                @elseif ($key < count($vendedores) - 2)
                    ,
                @endif
            @endforeach

            @if (count($vendedores) == 1)
                quien para efectos de este contrato se denominará
                <b>“EL(LA) PROMITENTE VENDEDOR(A)”</b>
            @else
                quienes para efectos de este contrato se denominarán
                <b>“LOS PROMITENTES VENDEDORES”</b>
            @endif

            y de otra parte

            @foreach ($compradores as $key => $comprador)
                {{ $general->mayus($comprador->nombre) }} mayor de edad, domiciliado(a) en la ciudad de: {{ $comprador->ciudad }},
                e identificado(a) con la {{ $comprador->tipos_documento->nombre }} {{ $comprador->numero_documento }}
                de estado civil: {{ $comprador->estado_civil ?? "" }}
                {{ $comprador->subestadociviles?->nombre }}

                @if (isset($comprador->apoderados))
                    representado(a) mediante poder especial por: {{ $general->mayus($comprador->apoderados->nombre) }}, mayor de edad,
                    domiciliado(a) en la ciudad de: {{ $comprador->apoderados->ciudad }}, e identificado con la
                    {{ $comprador->apoderados->tipos_documento?->nombre }} {{ $comprador->apoderados->numero_documento }}
                @endif

                @if ($key == count($compradores) - 1)

                @elseif ($key == count($compradores) - 2)
                    y
                @elseif ($key < count($compradores) - 2)
                    ,
                @endif
            @endforeach

            @if (count($compradores) == 1)
                quien para efectos de este contrato se denominará
                <b>“EL(LA) PROMITENTE COMPRADOR(A)”</b>
            @else
                quienes para efectos de este contrato se denominarán
                <b>“LOS PROMITENTES COMPRADORES”</b>
            @endif

            manifiestan que han decidido celebrar este contrato de <b>PROMESA DE COMPRAVENTA</b>
            que se regirá por las siguientes cláusulas:

            <b>
                <u>PRIMERA.- OBJETO:</u>
            </b>
            <b>EL/LA/LOS PROMITENTE(S) VENDEDOR(ES)</b>
            promete(n) transferir a título de venta real y efectiva los derechos de dominio y posesión en
            favor DE/LA/LOS PROMITENTE(S) COMPRADOR(ES) y éste(os) promete(n) a su vez comprar tales
            derechos que tiene sobre: {{ $clausulas[1] ?? "" }} registrados en la oficina de instrumentos públicos
            de {{ $clausulas[2] ?? "" }} e identificado con Folio de Matrícula número {{ $clausulas[3] ?? "" }};
            inmueble(s) que para efectos de este contrato, se denominarán <b>“El Inmueble”</b>. Inmueble que se promete vender y
            comprar junto con sus anexidades, usos, costumbres y servidumbres. Cuya descripción, cabida y linderos
            se encuentran relacionados en: {{ $clausulas[4] ?? "" }}, y cuyas copias simples, para todos los efectos,
            forman parte integral de la presente promesa y cualquiera de las partes podrá aducirla cuando lo estime
            conveniente.
            <br>
            <b>
                PARÁGRAFO PRIMERO: Cuerpo Cierto:
            </b>
            No obstante, la descripción cabida y
            linderos, el inmueble objeto de este documento que se promete vender y comprar como cuerpo cierto,
            las partes declaran conocerlos  satisfacción y renuncian a cualquier reclamación derivada de su
            ubicación, descripción, cabida y linderos.<br>
            <b>
                PARÁGRAFO SEGUNDO: Propiedad Horizontal:
            </b>
            El inmueble objeto de esta Promesa de compraventa, {{ (isset($clausulas[5]) && $clausulas[5] != "") ? "" : "NO" }} se encuentra
            sometido al Régimen de Propiedad
            Horizontal, cuyo reglamento consta en la Escritura Pública {{ (isset($clausulas[5]) && $clausulas[5] != "") ? $clausulas[5] : "NO" }}
            escrituras que, en lo
            relacionado con coeficiente de copropiedad, descripción, ubicación, cabida, linderos,
            anexidades, usos y costumbres, forma parte integral de esta promesa de compraventa y podrá
            anexarse a la misma cuando cualquiera de las partes lo estime conveniente. <br>
            <b>
                PARÁGRAFO TERCERO: Tradición:
            </b>
            El inmueble objeto de este documento fue adquirido por
            <b>
                EL/LA/LOS PROMITENTE(S) VENDEDOR(ES)
            </b>
            por {{ $clausulas[6] ?? "" }}
        </p>
        <p style="text-align: justify">
            <b>
                <u>SEGUNDA.- PRECIO, FORMA DE PAGO Y ARRAS:</u>
            </b>
            Las partes acuerdan como precio del inmueble prometido en la cláusula antecedente,
            es la suma de

            <b>
                $ {{ number_format($venta->negociacion_precio, 0, ",", ".") }}.
                {{ $general->mayus($formater->format($venta->negociacion_precio)) }}
                PESOS MONEDA CORRIENTE COLOMBIANA,
            </b>
            que EL/LA/LOS PROMITENTE(S) COMPRADOR(ES) pagará(n) a EL/LA/LOS PROMITENTES VENDEDOR(ES)
            de la siguiente manera:

            <ol>
                @for ($i = 1; $i <= 10; $i++)
                    @if (isset($venta["pago_{$i}_valor_pago"]) && $venta["pago_{$i}_valor_pago"] != 0)
                        <li>
                            La suma de
                            <b>
                                ${{ number_format($venta["pago_{$i}_valor_pago"], 0, ",", ".") }}.
                                {{ $general->mayus($formater->format($venta["pago_{$i}_valor_pago"])) }}
                                PESOS MONEDA CORRIENTE COLOMBIANA,
                            </b>
                            provenientes de: {{ $venta["pago_{$i}_origen_recurso"] }}, que serán pagados mediante
                            {{ $venta["pago_{$i}_medio_pago"] }}, en favor {{ $venta["pago_{$i}_pago_favor"] ?? $venta["pago_{$i}_notaria"] }}
                            {{ $venta["pago_{$i}_cheque_transferencia"] }}.
                        </li>
                    @endif
                @endfor
            </ol>

            <br>
            <b>
                ARRAS:
            </b>

            La suma de
            <b>
                $ {{ number_format($venta->negociacion_arras, 0, ",", ".") }}.
                {{ $general->mayus($formater->format($venta->negociacion_arras)) }}
                PESOS MONEDA CORRIENTE COLOMBIANA,
            </b>
            imputables al precio que se entregan y reciben como <b>ARRAS DE RETRACTACION</b>
            en los términos del artículo 1859 del Código Civil.
        </p>

        @if ($venta->mostrar_paragrafo_unico)
            <p style="text-align: justify">
                <strong>PARAGRAFO ÚNICO:</strong> Si la entidad bancaria donde se tramita el
                crédito Bancario no aprueba el inmueble como garantía, se hará devolución de los montos entregados
                a la fecha de la notificación del banco a <strong>EL/LA/LOS PROMITENTE(S) COMPRADOR(ES)</strong> y no habrá lugar
                a ninguna penalidad, esto a la presentación del respectivo concepto de no aprobación emitido por
                la entidad bancaria y a más tardar a los quince días hábiles después de la emisión de este.
            </p>
        @endif

        <p style="text-align: justify">
            <b>TERCERA.- ESCRITURAS:</b>
            Las partes aquí presentes, acuerdan que la firma de la Escritura Pública que perfeccione
            la presente promesa de compraventa se llevará a cabo el día {{ $general->formatFecha($venta->escritura_fecha_publica) }}
            en la {{ $venta->escritura_notaria }} {{ $venta->notaria_circulo }}
            a las {{ $venta->hora_escrituracion }};
            {{ $venta->notas_escrituracion }}. No obstante, las partes por mutuo acuerdo por escrito, podrán cambiar la fecha y hora.
        </p>

        <p style="text-align: justify">
            <b>
                <u>CUARTA.- ENTREGA:</u>
            </b>
            Las partes aquí presentes acuerdan que la entrega real y material del inmueble acá prometido será,
            {{ $venta->escritura_observacion }}.
            <b>EL/LA/LOS PROMITENTE(S) VENDEDOR(ES)</b>
            de manera expresa declara(n) que El Inmueble es de su exclusiva y plena propiedad, que en la actualidad
            lo posee de manera regular y pacífica y que a la fecha se encuentra libres de embargo, pleito pendiente,
            demandas civiles y que sobre él no recae ningún tipo de censos, anticresis, servidumbre, desmembraciones,
            condición resolutoria; y en general de toda condición limitativa o extintiva del dominio.
            <b>EL/LA/LOS PROMITENTE(S) VENDEDOR(ES)</b> tiene la obligación de salir al saneamiento ya sea por evicción
            o por vicios redhibitorios o vicios ocultos del sobre el inmueble vendido y
            <b>EL/LA/LOS PROMITENTE(S) COMPRADOR(ES)</b>  podrán ejercer acción redhibitoria en
            los términos del artículo 1914 del Código Civil Colombiano. <b>EL/LA/LOS PROMITENTE(S) VENDEDOR(ES)</b>
            hará(n) además esta entrega real a Paz y Salvo por el pago de los servicios públicos,
            cuotas de administración, cuotas ordinarias o extraordinarias, las cuales se entenderán pagadas por este,
            en la proporción que se hubiere cobrado por la administración hasta el momento de tal entrega; y a Paz y Salvo
            por todo concepto de impuestos, tasas y contribuciones de todo orden, los cuales una vez entregado el
            inmueble seguirá siendo asumidos por <b>EL/LA/LOS PROMITENTE(S) COMPRADOR(ES).</b>
            <br>

            <b>PARÁGRAFO {{ $venta->mostrar_paragrafo_segundo ? "PRIMERO" : "UNICO" }}:</b>
            Mientras existan dineros pendientes a cargo de <b>EL/LA/LOS PROMITENTE(S) COMPRADOR(ES)</b>
            y a favor de <b>EL/LA/LOS PROMITENTE(S) VENDEDOR(ES)</b> la entrega de los inmuebles será a
            <b>TÍTULO DE MERA TENENCIA.</b> (Arts 775 y 777 del Cod Civil).

            @if ($venta->mostrar_paragrafo_segundo)
                <br>
                <strong>PARAGRAFO SEGUNDO:</strong>
                {{ $venta->paragrafo_segundo_texto }}
            @endif

        </p>

        <p style="text-align: justify">
            <b>
                <u>QUINTA.- EXENCIÓN:</u>
            </b>
            Conoce <b>EL/LA/LOS PROMITENTE(S) COMPRADOR(ES)</b>
            que a partir de la fecha de suscripción de la presente Promesa de Compraventa
            <b>EL/LA/LOS PROMITENTE(S) VENDEDOR(ES)</b> no es(son) responsable(s) por alguna
            decisión administrativa municipal que eventualmente modifique la reglamentación
            relacionada con el uso y destinación de suelo, cambio en el P.O.T., aumento o
            disminución de avalúo catastral, valorización, etc.
        </p>

        <p style="text-align: justify">
            <b>
                <u>SEXTA.- IMPUESTOS:</u>
            </b>
            Las partes se reconocerán mutuamente la proporción del Impuesto Predial y valorización
            correspondiente al año en curso por la fracción de este a su cargo, desde la entrega material
            del inmueble hasta finalizar el año.
        </p>

        <p style="text-align: justify">
            <b>
                <u>SÉPTIMA.- GASTOS:</u>
            </b>
            Los gastos notariales que se originen por el otorgamiento de la Escritura Pública de compraventa,
            serán cubiertos por mitad entre las partes.  Los correspondientes a Beneficencia e inscripción
            en la Oficina de Registro de Instrumentos Públicos serán cubiertos por
            <b>EL/LA/LOS PROMITENTE(S) COMPRADOR(ES)</b> y la Retención en la Fuente será asumida por
            <b>EL/LA/LOS PROMITENTE(S) VENDEDOR(ES)</b>.
        </p>

        <p style="text-align: justify">
            <b>
                <u>OCTAVA.- JURAMENTO SOBRE EL PRECIO:</u> LAS PARTES,
            </b>
            declaran, bajo juramento, que el precio de transferencia es real y no es objeto de pactos
            privados en los que hayan convenido un valor diferente, que, además, no existen sumas que
            se hayan acordado o facturado por fuera de la presente Promesa de Compraventa. Igualmente
            se manifiesta que el precio señalado está conformado por todas las sumas pagadas para la
            adquisición del inmueble, objeto de este documento, incluidas las mejoras, construcciones e
            intermediación.
        </p>

        <p style="text-align: justify">
            <b>
                <u>NOVENA.- ORIGEN DE LOS FONDOS:</u>  EL/LA/LOS PROMITENTE(S) COMPRADOR(ES)
            </b>
            declara(n) que el origen de los recursos con los que promete adquirir los inmuebles objeto de
            este contrato proviene(n) de ocupación, oficio, profesión, actividad o negocio lícito. Así mismo,
            declara(n) que dichos recursos no provienen de ninguna actividad ilícita, de las contempladas
            en el Código Penal Colombiano, o en cualquier norma que lo modifique o adicione.
            <b>EL/LA/LOS PROMITENTE(S) VENDEDOR(ES)</b> quedará(n) eximido(s) de toda responsabilidad que se
            derive por información errónea, falsa o inexacta que <b>EL/LA/LOS PROMITENTE(S) COMPRADOR(ES)</b>
            proporcione a aquellos para la celebración de este negocio.
        </p>

        <p style="text-align: justify">
            <b>
                <u>DÉCIMA.- MÉRITO EJECUTIVO:</u>
            </b>
            Este Contrato presta mérito ejecutivo para exigir el pago y cumplimiento de todas
            las obligaciones en él establecidas, incluyendo la comisión generada por el corretaje
            y las partes se comprometen a no ceder ni parcial, ni totalmente las obligaciones contenidas
            en él, sin autorización previa y por escrito de la otra parte. Si contravinieren esta disposición,
            la cesión no tendrá efectos jurídicos y por tanto no exime de responsabilidad a quien la haya
            realizado sin autorización de la otra parte.
        </p>

        <p style="text-align: justify">
            <b>
                <u>DÉCIMOPRIMERA.- CLÁUSULA COMPROMISORIA:</u>
            </b>
            Las partes acuerdan que cualquier diferencia que llegare a surgir derivada de esta promesa se dirimirá,
            por vía de conciliación, ante un Centro de Conciliación y Arbitramento debidamente reconocido.
            Este trámite será previo a cualquier instancia judicial.
        </p>

        <p style="text-align: justify">
            <b>
                <u>DÉCIMOSEGUNDA.- FIRMA Y CONTENIDOS:</u>
            </b>
            Se entiende que, con la firma y huella de las partes, se encuentra legalizado el presente documento pudiéndose
            ejecutar en todas y cada una de sus partes.
        </p>

        <p style="text-align: justify">
            <b>
                <u>DÉCIMOTERCERA.- DIRECCIONES PARA NOTIFICACIONES:</u> LAS PARTES
            </b>
            declaran, que en la dirección y correo electrónico consignado junto a su firma recibirán notificaciones
            judiciales.
        </p>
        <p style="text-align: justify">
            <b>
                <u>DÉCIMOCUARTA.- COMISIÓN:</u>
            </b>
            Por concepto de gestión de corretaje pactada durante el proceso de captación y promoción del inmueble,
            EL/LA/LOS PROMITENTE(S) VENDEDOR(ES) pagará la comisión establecida en el contrato de comercialización
            sobre el precio final de la venta, {{ $venta->escritura_pago_comision_elite ?? "" }} a órdenes de INVERSIONES ELITE GROUP S.A.S.,
            identificada con Nit. 900.541.1115.
        </p>

        @if (isset($venta->mostrar_clausula_covid) && $venta->mostrar_clausula_covid)
            <p style="text-align: justify">
                <b>
                    <u>CLÁUSULA EXTRAORDINARIA:</u>
                </b>
                Las partes conocen y aceptan que están celebrando el presente negocio jurídico dentro del marco
                de emergencia económica, social y ecológica ocasionado por la pandemia del COVID-19. En este orden,
                conocen plenamente sus condiciones económicas actuales estando en el marco de esta emergencia; y deciden
                celebrar el presente contrato dejando claro que no alegarán como causal de incumplimiento tal situación.
                Adicionalmente; aceptan que no invocarán como causales de exoneración de responsabilidad tales circunstancias.
            </p>
        @endif

        <div class="page_break"></div>

        <p style="text-align: justify">
            Para constancia el presente contrato se firma
            {{ (isset($venta->mostrar_firma_electronica) && $venta->mostrar_firma_electronica) ? "de forma electrónica" : "" }}
            en la ciudad de Bogotá D.C., a los
            @if ($venta->fecha_firma_promesa != null)
                @php
                $fecha = \Carbon\Carbon::parse($venta->fecha_firma_promesa);
                $meses = [
                    "enero",
                    "febrero",
                    "marzo",
                    "abril",
                    "mayo",
                    "junio",
                    "julio",
                    "agosto",
                    "septiembre",
                    "octubre",
                    "noviembre",
                    "diciembre",
                ];
                @endphp
                {{ $formater->format($fecha->day) }} ({{ $fecha->day }}) días del mes de
                {{ $meses[$fecha->month - 1] }} del año {{ $formater->format($fecha->year) }} {{ $fecha->year }}
            @endif
            en dos ejemplares del mismo valor cada uno de ellos con destino a cada una de las partes.
        </p>
        <h4>
            EL/LA/LOS PROMITENTE(S) VENDEDOR(ES)
        </h4>
        <br>
        <br>
        <br>
        <br>
        <hr>
        <table style="width: 100%">
            <tbody>
                <tr>
                    @foreach ($vendedores as $key => $vendedor)
                        <td>
                            <td>
                                <b>{{ $general->mayus($vendedor->nombre) }}</b><br>
                                <b>{{ $vendedor->numero_documento }}</b><br>
                                Dirección: <b>{{ $vendedor->direccion }}</b><br>
                                Correo electrónico:  <b>{{ $vendedor->email }}</b><br>
                                Teléfono: <b>{{ $vendedor->telefono }}</b> <br>
                            </td>
                        </td>
                    @endforeach
                </tr>
            </tbody>
        </table>
        <br>
        <br>
        <h4>EL/LA/LOS PROMITENTE(S) COMPRADOR(ES)</h4>
        <br>
        <br>
        <br>
        <br>
        <hr>
        <table style="width: 100%">
            <tbody>
                <tr>
                    @foreach ($compradores as $key => $comprador)
                        <td>
                            <b>{{ $general->mayus($comprador->nombre) }}</b><br>
                            <b>{{ $comprador->numero_documento }}</b><br>
                            Dirección: <b>{{ $comprador->direccion }}</b><br>
                            Correo electrónico:  <b>{{ $comprador->email }}</b><br>
                            Teléfono: <b>{{ $comprador->telefono }}</b> <br>
                        </td>
                    @endforeach
                </tr>
            </tbody>
        </table>
    </div>
    <div class="pagina" style="position: absolute; bottom: 60px;">
        <hr>
        <p>
            <i>
                El texto de la presente promesa de compraventa es un aporte SUGERIDO de Elite
                Inmobiliaria a las partes, pero en ningún momento reemplaza la voluntad de las mismas,
                ni compromete la responsabilidad de la inmobiliaria o sus agentes asociados.
            </i>
        </p>
    </div>
</div>

</body>
</html>
