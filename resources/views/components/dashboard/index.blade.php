<section role="main" class="content-body">
	<header class="page-header">
		<h2>Elite System</h2>

		<div class="right-wrapper text-right">
			<ol class="breadcrumbs">
				<li>
					<a href="#">
						<i class="bx bx-home-alt"></i>
					</a>
				</li>
				<li><span>Documentos</span></li>
				<li><span>Consultar&nbsp;&nbsp;&nbsp;</span></li>
			</ol>

			<a class="" data-open=""></a>
		</div>
	</header>

	<!-- start: page -->
	<div class="row">
		<div class="col-lg-6 mb-3">
			<section class="card">
				<header class="card-header">
					<div class="card-actions">
						<a href="#" class="card-action card-action-toggle" data-card-toggle></a>
						<a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
					</div>

					<h2 class="card-title">Documentos internos Elite Inmobiliaria</h2>
				</header>
				<div class="card-body">
					<div id="treeBasic">
						<ul>
							<li data-jstree='{ "opened" : false }'>
								Elite
								<ul>
									<!-- <li data-jstree='{ "selected" : true }'>
									<a href="#">Empresa</a>
								</li> -->
								<li data-jstree='{ "opened" : false }'>
									Formatos
									<ul>
										<li data-jstree='{ "opened" : false }'>
											Alquileres
											<ul>
												<li data-jstree='{  "opened" : false }'>
													Bogotá

													<ul>
														<li data-jstree='{  "opened" : false }'>
															Capt Comercial Admon
															<ul>
																<!-- <li data-jstree='{ "disabled" : true }'>
																Disabled Node
															</li> -->
															<li data-jstree='{ "type" : "file" }'>
																<a href="{{ asset("docs/Captacion Comercial Plus Fisico.pdf") }}" target="_blank">
																	Captacion Comercial Plus Fisico.pdf
																</a>
															</li>
															<li data-jstree='{ "type" : "file" }'>
																<a href="{{ asset("docs/Mandato Comercial Fisico.pdf") }}" target="_blank">
																	Mandato Comercial Fisico.pdf
																</a>
															</li>
														</ul>
													</li>
													<li data-jstree='{  "opened" : false }'>
														Capt Residencial Admon
														<ul>
															<li data-jstree='{ "type" : "file" }'>
																<a href="{{ asset("docs/Captacion Residencial Plus Fisico.pdf") }}">
																	Captacion Residencial Plus Fisico.pdf
																</a>
															</li>
															<li data-jstree='{ "type" : "file" }'>
																<a href="{{ asset("docs/Captacion Residencial Simple Fisico.pdf") }}">
																	Captacion Residencial Simple Fisico.pdf
																</a>
															</li>
															<li data-jstree='{ "type" : "file" }'>
																<a href="{{ asset("docs/Mandato Residencial Fisico.pdf") }}">
																	Mandato Residencial Fisico.pdf
																</a>
															</li>
														</ul>
													</li>
												</ul>
											</li>
											<li data-jstree='{  "opened" : false}'>
												Cali
												<ul>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/Captacion Residencial Plus Fisico CLO.pdf") }}">
															Captacion Residencial Plus Fisico CLO.pdf
														</a>

													</li>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/Captacion Residencial Simple Fisico CLO.pdf") }}">
															Captacion Residencial Simple Fisico CLO.pdf
														</a>
													</li>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/Mandato Residencial Fisico CLO.pdf") }}">
															Mandato Residencial Fisico CLO.pdf
														</a>
													</li>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/Matricula Arrendador 06-21.pdf") }}">
															Matricula Arrendador 06-21.pdf
														</a>
													</li>
												</ul>
											</li>
											<li data-jstree='{  "opened" : false}'>
												El libertador
												<ul>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/01-EstudioArrendatario.mp4") }}">
															01-EstudioArrendatario.mp4
														</a>
													</li>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/02-EstudioDeudor.mp4") }}">
															02-EstudioDeudor.mp4
														</a>
													</li>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/FormJuridica.pdf") }}">
															FormJuridica.pdf
														</a>
													</li>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/FormNatural.docx") }}">
															FormNatural.docx
														</a>
													</li>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/FormNatural.pdf") }}">
															FormNatural.pdf
														</a>
													</li>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/PolizaHogar.pdf") }}">
															PolizaHogar.pdf
														</a>
													</li>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/PolizaHogar-Asistencias.pdf") }}">
															PolizaHogar-Asistencias.pdf
														</a>
													</li>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/Seguros3.docx") }}">
															Seguros3.docx
														</a>
													</li>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/Seguros3.pdf") }}">
															Seguros3.pdf
														</a>
													</li>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/SegurosPresentacion.pdf") }}">
															SegurosPresentacion.pdf
														</a>
													</li>
												</ul>
											</li>
											<li data-jstree='{  "opened" : false}'>
												Mundial seguros
												<ul>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/01.pdf") }}">
															01
														</a>
													</li>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/CLAUSULADO_SEGURO_COLECTIVO_DE_ARRENDAMIENTO.pdf") }}">
															CLAUSULADO_SEGURO_COLECTIVO_DE_ARRENDAMIENTO.pdf
														</a>
													</li>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/Condicionado Asistencias Producto Arriendos IKE 062016 (4) Revisión producto 27062016.pdf") }}">
															Condicionado Asistencias Producto Arriendos IKE 062016 (4) Revisión producto 27062016.pdf
														</a>
													</li>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/Formato Estudio Póliza Arrendamientos .pdf") }}">
															Formato Estudio Póliza Arrendamientos .pdf
														</a>
													</li>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/Persona Natural.pdf") }}">
															Persona Natural.pdf
														</a>
													</li>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/Persona-Juridica.pdf") }}">
															Persona-Juridica.pdf
														</a>
													</li>
													<li data-jstree='{ "type" : "file" }'>
														<a href="{{ asset("docs/presentacion colectiva 27032019.pdf") }}">
															presentacion colectiva 27032019.pdf
														</a>
													</li>
												</ul>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Checklist Elaboración Contrato Arrendamiento.pdf") }}">
													Checklist Elaboración Contrato Arrendamiento.pdf
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Cobro a Propietarios 8.xlsxCobro a Propietarios 8.xlsx") }}">
													Cobro a Propietarios 8.xlsx
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Cobro a Propietarios 10.xlsx") }}">
													Cobro a Propietarios 10.xlsx
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Directorio Extensiones Agentes y Oficina.xlsx") }}">
													Directorio Extensiones Agentes y Oficina.xlsx
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Información a Propietarios para Captación.pdf") }}">
													Información a Propietarios para Captación.pdf
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Inventario Digital Apertura.pdf") }}">
													Inventario Digital Apertura.pdf
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Inventario Digital Desocupación.pdf") }}">
													Inventario Digital Desocupación.pdf
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Inventario Digital Tutorial.mp4") }}">
													Inventario Digital Tutorial.mp4
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Inventario Físico Apertura.pdf") }}">
													Inventario Físico Apertura.pdf
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Inventario Fisico Captacion.pdf") }}">
													Inventario Fisico Captacion.pdf
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Lista Chequeo - Coordinadora 1.pdf") }}">
													Lista Chequeo - Coordinadora 1.pdf
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Lista Chequeo - Coordinadora 2.pdf") }}">
													Lista Chequeo - Coordinadora 2.pdf
												</a>
											</li>

										</ul>
									</li>
									<li data-jstree='{ "opened" : false }'>
										Ventas
										<ul>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Captacion Venta Plus3 Fisico.pdf") }}">
													Captacion Venta Plus3 Fisico.pdf
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Captacion Venta Plus4 Fisico.pdf") }}">
													Captacion Venta Plus4 Fisico.pdf
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Captacion Venta Plus5 Fisico.pdf") }}">
													Captacion Venta Plus5 Fisico.pdf
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Captacion Venta Simple3 Fisico.pdf") }}">
													Captacion Venta Simple3 Fisico.pdf
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Captacion Venta Simple4 Fisico.pdf") }}">
													Captacion Venta Simple4 Fisico.pdf
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Captacion Venta Simple5 Fisico.pdf") }}">
													Captacion Venta Simple5 Fisico.pdf
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Checklist Elaboracion de Promesa.xlsx") }}">
													Checklist Elaboracion de Promesa.xlsx
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Convenio Inmobiliarias BOG.docx") }}">
													Convenio Inmobiliarias BOG.docx
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Convenio Inmobiliarias CLO.docx") }}">
													Convenio Inmobiliarias CLO.docx
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Formato Oferta de Compra.pdf") }}">
													Formato Oferta de Compra.pdf
												</a>
											</li>
										</ul>
									</li>

									<li data-jstree='{ "opened" : false }'>
										Corretaje
										<ul>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Captacion Corretaje Plus Fisico.pdf") }}">
													Captacion Corretaje Plus Fisico.pdf
												</a>
											</li>

										</ul>
									</li>

									<li data-jstree='{ "opened" : false }'>
										Formatos generales
										<ul>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Cuenta de Cobro del Agente.pdf") }}">
													Cuenta de Cobro del Agente.pdf
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Ficha Tecnica para fotos.pptx") }}">
													Ficha Tecnica para fotos.pptx
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/PASO A PASO ITAU PSE.pdf") }}">
													PASO A PASO ITAU PSE.pdf
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Presentación Elite 2021.pdf") }}">
													Presentación Elite 2021.pdf
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Requerimientos Horizontal.pptx") }}">
													Requerimientos Horizontal.pptx
												</a>
											</li>
											<li data-jstree='{ "type" : "file" }'>
												<a href="{{ asset("docs/Requerimientos Vertical.pptx") }}">
													Requerimientos Vertical.pptx
												</a>
											</li>

										</ul>
									</li>

								</ul>
							</li>

						</ul>
					</li>
					<!-- <li class="colored">
					Papelera
				</li> -->

			</ul>
		</div>
	</div>
</section>
</div>

<div class="col-lg-6">
	<div class="row mb-3">
		<section class="card card-modern card-big-info">
			<div class="card-body">
				<div class="row">
					<div class="col-lg-2-5 col-xl-2-5">
						<i class="card-big-info-icon bx bx-box"></i>
						<h2 class="card-big-info-title">Enlaces frecuentes</h2>
						<p class="card-big-info-desc">Ayudas para agentes</p>
					</div>
					<div class="col-lg-3-5 col-xl-3-5">
						<div class="form-group row align-items-center">
							@foreach ($tutoriales as $tutorial)
								<a href="{{ $tutorial->url }}" target="_blank" type="button" class="mb-1 mt-1 mr-1 btn btn-lg btn-default">
									{{ $tutorial->titulo }}
								</a>
							@endforeach
							{{-- <label class="col-lg-5 col-xl-3 control-label text-lg-right mb-0">Nombre de la carpeta</label>
							<div class="col-lg-7 col-xl-6">
							<input type="text" class="form-control form-control-modern" name="productName" value="" required />
						</div> --}}
					</div>

				</div>
			</div>
		</div>
		{{-- <footer class="card-footer text-right">
		<button class="btn btn-primary">Guardar </button>
		<button type="reset" class="btn btn-default">Cancelar</button>
	</footer> --}}
</section>
</div>
</div>
</div>

<div id="modalInicio" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<p>
					De parte del equipo técnico de élite system.
				</p>

				<p>
					Actualmente se están haciendo cambios constantes en la plataforma.
					Uno de los métodos que mantienen el buen rendimiento es el cache del navegador,
					sin embargo este puede generar comportamientos no deseados.
				</p>

				<p>
					Por favor refresque el cache de su navegador en caso de encontrar algún
					comportamiento extraño o error antes de reportarlo.
				</p>

				<p>
					Pará esto solo debe hacer la combinación de teclas <strong>Ctrl + R</strong> o
					<strong>Ctrl + F5</strong>
					en windows o <strong>Cmd + R</strong> en Mac
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">
					Cerrar
				</button>
			</div>
		</div>

	</div>
</div>
