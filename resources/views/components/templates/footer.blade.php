<!-- end: page -->
</section>
</div>

</section>

<!-- Vendor -->
<script src="{{ asset("assets/vendor/jquery/jquery.js") }}"></script>
<script src="{{ asset("assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js") }}"></script>
<script src="{{ asset("assets/vendor/jquery-cookie/jquery.cookie.js") }}"></script>
<!-- borrrar style switcher
<script src="{{ asset("assets/master/style-switcher/style.switcher.js") }}"></script>
-->
<script src="{{ asset("assets/vendor/popper/umd/popper.min.js") }}"></script>
<script src="{{ asset("assets/vendor/bootstrap/js/bootstrap.js") }}"></script>
<!-- <script src="{{ asset("assets/js/bootstrap.min.js") }}"></script> -->
<script src="{{ asset("assets/vendor/common/common.js") }}"></script>
<script src="{{ asset("assets/vendor/nanoscroller/nanoscroller.js") }}"></script>
<script src="{{ asset("assets/vendor/magnific-popup/jquery.magnific-popup.js") }}"></script>
<script src="{{ asset("assets/vendor/jquery-placeholder/jquery.placeholder.js") }}"></script>

<!-- Specific Page Vendor -->
<script src="{{ asset("assets/vendor/jquery-ui/jquery-ui.js") }}"></script>
<script src="{{ asset("assets/vendor/jqueryui-touch-punch/jquery.ui.touch-punch.js") }}"></script>
<script src="{{ asset("assets/vendor/jquery-appear/jquery.appear.js") }}"></script>
<script src="{{ asset("assets/vendor/bootstrap-multiselect/js/bootstrap-multiselect.js") }}"></script>
<script src="{{ asset("assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.js") }}"></script>
<script src="{{ asset("assets/vendor/flot/jquery.flot.js") }}"></script>
<script src="{{ asset("assets/vendor/flot.tooltip/jquery.flot.tooltip.js") }}"></script>
<script src="{{ asset("assets/vendor/flot/jquery.flot.pie.js") }}"></script>
<script src="{{ asset("assets/vendor/flot/jquery.flot.categories.js") }}"></script>
<script src="{{ asset("assets/vendor/flot/jquery.flot.resize.js") }}"></script>
<script src="{{ asset("assets/vendor/jquery-sparkline/jquery.sparkline.js") }}"></script>
<script src="{{ asset("assets/vendor/raphael/raphael.js") }}"></script>
<script src="{{ asset("assets/vendor/morris/morris.js") }}"></script>
<script src="{{ asset("assets/vendor/gauge/gauge.js") }}"></script>
<script src="{{ asset("assets/vendor/snap.svg/snap.svg.js") }}"></script>
<script src="{{ asset("assets/vendor/liquid-meter/liquid.meter.js") }}"></script>
<script src="{{ asset("assets/vendor/jqvmap/jquery.vmap.js") }}"></script>
<script src="{{ asset("assets/vendor/jqvmap/data/jquery.vmap.sampledata.js") }}"></script>
<script src="{{ asset("assets/vendor/jqvmap/maps/jquery.vmap.world.js") }}"></script>
<script src="{{ asset("assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js") }}"></script>
<script src="{{ asset("assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js") }}"></script>
<script src="{{ asset("assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js") }}"></script>
<script src="{{ asset("assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js") }}"></script>
<script src="{{ asset("assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js") }}"></script>
<script src="{{ asset("assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js") }}"></script>
<!-- Specific Page Vendor -->
<script src="{{ asset("assets/vendor/jstree/jstree.js") }}"></script>
<script src="{{ asset("assets/js/jquery-sortable.js") }}"></script>

<!-- Specific Page Vendor -->
<script src="{{ asset("assets/vendor/autosize/autosize.js") }}"></script>
<script src="{{ asset("assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js") }}"></script>
<script src="{{ asset("assets/vendor/4image-upload/4image-upload.js") }}"></script>

<!--(remove-empty-lines-end)-->

<!-- Theme Base, Components and Settings -->
<script src="{{ asset("assets/js/theme.js") }}"></script>

<!-- Theme Initialization Files -->
<script src="{{ asset("assets/js/theme.init.js") }}"></script>
<!-- Analytics to Track Preview Website -->
<!-- inicio y vista de arbol -->
<script src="{{ asset("assets/js/examples/examples.dashboard.js") }}"></script>
<script src="{{ asset("assets/js/examples/examples.treeview.js") }}"></script>

<!-- Specific modal select digitar-->
<script src="{{ asset("assets/vendor/select2/js/select2.js") }}"></script>
<!-- arrastrar y soltar-->
<script src="{{ asset("assets/vendor/bootstrap-markdown/js/markdown.js") }}"></script>
{{-- Slider que si funciona --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.3.9/js/lightgallery.min.js"></script>

<script src="{{ asset("assets/vendor/pnotify/pnotify.custom.js") }}"></script>
<script src="{{ asset("assets/js/examples/examples.modals.js") }}"></script>

@if ($photoSphere == 1)
{{-- Photo Sphere --}}
<script src="https://cdn.jsdelivr.net/npm/three/build/three.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/uevent@2/browser.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/photo-sphere-viewer@4/dist/photo-sphere-viewer.min.js"></script>
@endif


<script src="{{ asset("assets/js/photoswipe/jqPhotoSwipe.js") }}"></script>
<script src="{{ asset("assets/js/photoswipe/photoswipe.min.js") }}"></script>
<script src="{{ asset("assets/js/photoswipe/photoswipe-ui-default.min.js") }}"></script>
{{-- Mapa de google --}}
<script type="text/javascript" src="{{ asset("/assets/js/markerclusterer.js") }}"></script>
<script type="text/javascript" src="{{ asset("/assets/js/markerclusterer_compiled.js") }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAWDvwjlWacYKqiIKTTYWKTwBVMy-ZkgHs"></script>
{{-- <script src="https://maps.googleapis.com/maps/api/js"></script> --}}

{{-- Angular JS --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.1/angular.min.js" type="text/javascript"></script>

@if ($script != "")
<script src="{{ asset("assets/js/components/$script.js?v=1.01") }}"></script>
@else
<script src="{{ asset("assets/js/angular.js?v=1.012") }}"></script>
<script src="https://cdn.jsdelivr.net/npm/angular-animate@1.6.1/angular-animate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/angular-touch@1.5.11/angular-touch.min.js"></script>
<script src="{{ asset("assets/js/screenfull.js") }}"></script>
<script src="{{ asset("assets/js/angular-super-gallery.js") }}"></script>
@endif

<script src="{{ asset("assets/js/custom.js?v=1.02") }}"></script>
<!-- swal -->
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

</body>

</html>
