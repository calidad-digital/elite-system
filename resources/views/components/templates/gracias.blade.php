<!DOCTYPE html>
<html class="fixed">
<head>

    <!-- Basic -->
    <meta charset="UTF-8">

    <meta name="keywords" content="HTML5 Admin Template" />
    <meta name="description" content="Porto Admin - Responsive HTML5 Template">
    <meta name="author" content="okler.net">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <title>{{ $titulo }} - {{ config("app.name") }}</title>

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ asset("assets/vendor/bootstrap/css/bootstrap.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/vendor/animate/animate.compat.css") }}" />

    <link rel="stylesheet" href="{{ asset("assets/vendor/font-awesome/css/all.min.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/vendor/boxicons/css/boxicons.min.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/vendor/magnific-popup/magnific-popup.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css") }}" />

    <!--(remove-empty-lines-end)-->

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset("assets/css/theme.css") }}" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{ asset("assets/css/custom.css") }}">

    <!-- Head Libs -->
    <script src="{{ asset("assets/vendor/modernizr/modernizr.js") }}"></script>
    <script src="{{ asset("assets/master/style-switcher/style.switcher.localstorage.js") }}"></script>

</head>
<body>
    <!-- start: page -->
    <section class="body-sign">

        <div class="center-sign">
            <a href="{{ url("/") }}" class="logo text-center">
                <img src="{{ asset("assets/img/logo.png") }}" height="54" alt="Porto Admin" />
            </a>

            <div class="panel card-sign">
                <br><br>
                <div class="card-body">
                    <h4>Gracias por sus comentarios</h4>
                    <br><br>
                </div>
            </div>

            <p class="text-center text-muted mt-3 mb-3">
                &copy; Copyright {{ date("Y") }}. All Rights Reserved.
            </p>
        </div>
    </section>
    <!-- end: page -->

    <!-- Vendor -->
    <script src="{{ asset("assets/vendor/jquery/jquery.js") }}"></script>
    <script src="{{ asset("assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js") }}"></script>
    <script src="{{ asset("assets/vendor/jquery-cookie/jquery.cookie.js") }}"></script>
    <script src="{{ asset("assets/vendor/popper/umd/popper.min.js") }}"></script>
    <script src="{{ asset("assets/vendor/bootstrap/js/bootstrap.js") }}"></script>
    <script src="{{ asset("assets/master/style-switcher/style.switcher.js") }}"></script>
    <script src="{{ asset("assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js") }}"></script>
    <script src="{{ asset("assets/vendor/common/common.js") }}"></script>
    <script src="{{ asset("assets/vendor/nanoscroller/nanoscroller.js") }}"></script>
    <script src="{{ asset("assets/vendor/magnific-popup/jquery.magnific-popup.js") }}"></script>
    <script src="{{ asset("assets/vendor/jquery-placeholder/jquery.placeholder.js") }}"></script>

    <!-- Specific Page Vendor -->


    <!--(remove-empty-lines-end)-->

    <!-- Theme Base, Components and Settings -->
    <script src="{{ asset("assets/js/theme.js") }}"></script>

    <!-- Theme Custom -->
    <script src="{{ asset("assets/js/custom.js") }}"></script>

    <!-- Theme Initialization Files -->
    <script src="{{ asset("assets/js/theme.init.js") }}"></script>
    <!-- Analytics to Track Preview Website -->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-42715764-8', 'auto');
    ga('send', 'pageview');
</script>
</body>
</html>
