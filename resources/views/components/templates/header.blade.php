<!doctype html>
<html class="fixed" lang="es" ng-app="app">

<head>
    <!-- Basic -->
    <meta charset="UTF-8">

    <title>{{ $titulo }} - {{ config("app.name") }}</title>
    <meta name="keywords" content="Elite inmobiliaria, elite system, elite" />
    <meta name="description" content="Elite System">
    <meta name="author" content="calidad.digital">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Shadows+Into+Light"
    rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ asset("assets/vendor/bootstrap/css/bootstrap.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/vendor/animate/animate.compat.css") }}">

    <link rel="stylesheet" href="{{ asset("assets/vendor/font-awesome/css/all.min.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/vendor/boxicons/css/boxicons.min.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/vendor/magnific-popup/magnific-popup.css") }}" />

    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="{{ asset("assets/vendor/jstree/themes/default/style.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css") }}" />

    <!-- select digitando CSS -->
    <link rel="stylesheet" href="{{ asset("assets/vendor/select2/css/select2.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/vendor/select2-bootstrap-theme/select2-bootstrap.min.css") }}" />

    <!-- iconos CSS -->
    <link rel="stylesheet" href="{{ asset("assets/vendor/elusive-icons/css/elusive-icons.css") }}" />

    <!-- arrastrar fotos -->
    <link rel="stylesheet" href="{{ asset("assets/vendor/dropzone/basic.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/vendor/dropzone/dropzone.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/vendor/4image-upload/4image-upload.css") }}" />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset("assets/css/theme.css") }}" />

    <!-- photoswipe -->
    <link rel="stylesheet" href="{{ asset("assets/js/photoswipe/photoswipe.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/js/photoswipe/default-skin.css") }}" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{ asset("assets/css/custom.css") }}?v=2">
    <link rel="stylesheet" href="{{ asset("assets/css/angular-super-gallery.css") }}">

    <!-- Head Libs -->
    <script src="{{ asset("assets/vendor/modernizr/modernizr.js") }}"></script>
    {{-- Slider --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.3.9/css/lightgallery.min.css">

    @if ($photoSphere == 1)
        {{-- Photo Sphere --}}
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/photo-sphere-viewer@4/dist/photo-sphere-viewer.min.css" />
    @endif

    <!-- borrrar style switcher
    <script src="{{ asset("assets/master/style-switcher/style.switcher.localstorage.js") }}"></script>
-->
</head>

{{-- Validación de permisos para crear inmueble --}}
@php($crearInmueble = App\Http\Controllers\GeneralController::getPermission($usuario, 1))

<body>
    <section class="body">

        <!-- start: header -->
        <header class="header">
            <div class="logo-container">
                <a href="{{ url("admin") }}" class="logo">
                    <img src="{{ asset("assets/img/logo.png") }}" width="75" height="35" alt="Porto Admin" />
                </a>
                <div class="d-md-none toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html"
                data-fire-event="sidebar-left-opened">
                <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
            </div>
        </div>

        <!-- start: search & user box -->
        <div class="header-right">

            <ul class="notifications">

                <li>
                    <a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
                        <i class="bx bx-video"></i>
                        <!-- <span class="badge">3</span> -->
                    </a>

                    <div class="dropdown-menu notification-menu">
                        <div class="notification-title">
                            <span class="float-right badge badge-default">{{ count($tutoriales) }}</span>
                            Videotutoriales
                        </div>

                        <div class="content">
                            <ul>
                                @foreach ($tutoriales as $tutorial)
                                    <li>
                                        <a href="{{ $tutorial->url }}" class="clearfix" target="_blank">
                                            <div class="image">
                                                <i class="fab fa-{{ $tutorial->icono }} bg-danger text-light"></i>
                                            </div>
                                            <span class="title">{{ $tutorial->titulo }}</span>
                                            <span class="message">{{ $tutorial->subtitulo }}</span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>

                            <!-- <hr />

                            <div class="text-right">
                            <a href="#" class="view-more">View All</a>
                        </div> -->
                    </div>
                </div>
            </li>
        </ul>

        <span class="separator"></span>

        <div id="userbox" class="userbox">
            <a href="#" data-toggle="dropdown">
                <figure class="profile-picture">
                    <img src="{{ $usuario->foto != "" ? $usuario->foto : "https://capital.com/files/imgs/glossary/600xx/Broker.jpg" }}"
                    alt="{{ $usuario->nombres }} {{ $usuario->apellidos }}" class="rounded-circle"
                    data-lock-picture="{{ $usuario->foto }}" />
                </figure>
                <div class="profile-info" data-lock-name="{{ $usuario->nombres }} {{ $usuario->apellidos }}"
                    data-lock-email="{{ $usuario->email }}">
                    <span class="name">
                        {{ $usuario->nombres }} {{ $usuario->apellidos }}
                    </span>
                    <span class="role">
                        {{ $usuario->roles->nombre }}
                    </span>
                </div>

                <i class="fa custom-caret"></i>
            </a>

            <div class="dropdown-menu">
                <ul class="list-unstyled mb-2">
                    <li class="divider"></li>
                    <li>
                        <a role="menuitem" tabindex="-1" href="#">
                            <i class="bx bx-user-circle"></i> Mi perfil
                        </a>
                    </li>
                    <li>
                        <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true">
                            <i class="bx bx-lock"></i> Bloquear pantalla
                        </a>
                    </li>
                    <li>
                        <a role="menuitem" tabindex="-1" href="{{ url("admin/logout") }}">
                            <i class="bx bx-power-off"></i> Salir
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end: search & user box -->
</header>
<!-- end: header -->

<div class="inner-wrapper">
    <!-- start: sidebar -->
    <aside id="sidebar-left" class="sidebar-left">

        <div class="sidebar-header">
            <div class="sidebar-title">
                Navegación
            </div>
            <div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed"
            data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">

                <ul class="nav nav-main">
                    <li class="{{ $activeDashboard ?? "" }}">
                        <a class="nav-link" href="{{ url("admin") }}">
                            <i class="bx bx-loader-circle" aria-hidden="true"></i>
                            <span>Inicio</span>
                        </a>
                    </li>

                    <li
                    class="nav-parent {{ (isset($expanded) && $expanded != "") ? "nav-expanded" : "" }} {{ $activeClientes ?? "" }} {{ $activeAgentes ?? "" }}">
                    <a class="nav-link" href="#">
                        <i class="bx bx-user" aria-hidden="true"></i>
                        <span>Personas</span>
                    </a>

                    <ul class="nav nav-children">
                        <li class="{{ $activeClientes ?? "" }}">
                            <a class="nav-link" href="{{ url("admin/clientes") }}">
                                Clientes
                            </a>
                        </li>
                        @if($usuario->rol_id == 1)
                            <li class="{{ $activeAgentes ?? "" }}">
                                <a class="nav-link" href="{{ url("admin/agentes") }}">
                                    Agentes
                                </a>
                            </li>
                        @endif
                    </ul>

                </li>

                <li
                class="nav-parent {{ (isset($expandedInmuebles) && $expandedInmuebles != "") ? "nav-expanded" : "" }} {{ $activeInmuebles ?? "" }} {{ $activeInmueblesCrear ?? "" }} {{ $activeInmueblesSeguimiento ?? "" }}">
                <a class="nav-link" href="#">
                    <i class="bx bx-home-alt" aria-hidden="true"></i>
                    <span>Inmuebles</span>
                </a>
                <ul class="nav nav-children">
                    @if ($crearInmueble == 1)
                        <li class="{{ $activeInmueblesCrear ?? "" }}">
                            <a class="nav-link" href="{{ url("admin/inmuebles/crear") }}">
                                Crear
                            </a>
                        </li>
                    @endif
                    <li class="{{ $activeInmueblesSeguimiento ?? "" }}">
                        <a class="nav-link" href="{{ url("admin/seguimientos") }}">
                            Seguimiento
                        </a>
                    </li>
                    <li class="{{ $activeInmuebles ?? "" }}">
                        <a class="nav-link" href="{{ url("admin/inmuebles") }}">
                            Mis inmuebles
                        </a>
                    </li>
                    <li class="{{ $activeVentas ?? "" }}">
                        <a class="nav-link" href="{{ url("admin/ventas/3") }}">
                            Ventas
                        </a>
                    </li>
                    <li class="{{ $activeVentas ?? "" }}">
                        <a class="nav-link" href="{{ url("admin/arriendos/lista") }}">
                            Arriendos
                        </a>
                    </li>
                    <li class="{{ $activeMLS ?? "" }}">
                        <a class="nav-link" href="{{ url("admin/mls") }}">
                            MLS
                        </a>
                    </li>

                </ul>
            </li>

            <li class="nav-parent">
                <a class="nav-link" href="#">
                    <i class="bx bx-layout" aria-hidden="true"></i>
                    <span>Documentos</span>
                </a>
                <ul class="nav nav-children">
                    <li>
                        <a class="nav-link" href="#">
                            General
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="#">
                            Alquileres
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="#">
                            Ventas
                        </a>
                    </li>
                    <li>
                        <a class="nav-link" href="#">
                            Corretaje
                        </a>
                    </li>

                </ul>
            </li>

            {{-- Tutoriales --}}
            @if ($usuario->rol_id == 1)
                <li class="nav-parent {{ (isset($expandedConfig) && $expandedConfig != "") ? "nav-expanded" : "" }} {{ $activeConfigTutorials ?? "" }}">
                    <a class="nav-link" href="#">
                        <i class="bx bx-wrench" aria-hidden="true"></i>
                        <span>Configuración general</span>
                    </a>
                    <ul class="nav nav-children">
                        <li class="{{ $activeConfigTutorials ?? "" }}">
                            <a class="nav-link" href="{{ url("admin/config/tutorials") }}">
                                Links de tutoriales
                            </a>
                        </li>
                    </ul>
                </li>
            @endif

            <li>
                <a class="nav-link" href="#">
                    <i class="bx bx-collection" aria-hidden="true"></i>
                    <span>Mailing</span>
                </a>
            </li>

            <li>
                <a class="nav-link" href="#">
                    <i class="bx bx-collection" aria-hidden="true"></i>
                    <span>Administrar usuarios</span>
                </a>
            </li>

        </ul>
    </nav>

    <hr class="separator" />

    <script>
    // Maintain Scroll Position
    if (typeof localStorage !== 'undefined') {
        if (localStorage.getItem('sidebar-left-position') !== null) {
            var initialPosition = localStorage.getItem('sidebar-left-position'),
            sidebarLeft = document.querySelector('#sidebar-left .nano-content');

            sidebarLeft.scrollTop = initialPosition;
        }
    }
    </script>


</div>

</aside>
<!-- end: sidebar -->
