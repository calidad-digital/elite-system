<section role="main" class="content-body">
    <header class="page-header">
        <h2>Inmuebles</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>MLS Inmuebles</span></li>
                <li><span>Consultar&nbsp;&nbsp;&nbsp;</span></li>
            </ol>
            <a class="" data-open=""></a>
        </div>
    </header>

    <div class="row">
        <div class="col-lg-12">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Inmuebles</h2>
                </header>
                <div class="card-body">

                    <form action="" method="get">
                        <div class="row">

                            <div class="form-group col-md-3">
                                <label for="ciudad" class="sr-only">Ciudad</label>
                                <select name="ciudad" class="form-control">
                                    <option value="">Ciudad</option>
                                    @foreach ($ciudades as $ciudad)
                                        <option value="{{ $ciudad["code"] }}"
                                        {{ isset($request["ciudad"]) && $request["ciudad"] == $ciudad["code"] ? "selected" : "" }}>
                                        {{ $ciudad["city"] }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="gestion" class="sr-only">Gestion</label>
                            <select name="gestion" class="form-control">
                                <option value="">Gestión</option>
                                @foreach ($gestiones as $gestion)
                                    <option value="{{ $gestion["code"] }}"
                                    {{ isset($request["gestion"]) && $request["gestion"] == $gestion["code"] ? "selected" : "" }}>
                                    {{ $gestion["biz"] }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-3" ng-init="">
                        <label for="tipo" class="sr-only">Tipo</label>
                        <select class="form-control" name="tipo">
                            <option value="">Tipo</option>
                            @foreach ($tipos as $tipo)
                                <option value="{{ $tipo["code"] }}"
                                {{ isset($request["tipo"]) && $request["tipo"] == $tipo["code"] ? "selected" : "" }}>
                                {{ $tipo["type"] }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="col-lg-3">
                    <input class="form-control mb-3" name="barrio" type="text" placeholder="Barrio"
                    value="{{ isset($request["barrio"]) && $request["barrio"] != "" ? $request["barrio"] : "" }}">
                </div>
                <div class="col-lg-3">
                    <input class="form-control mb-3" name="codigo" type="text" placeholder="Código"
                    value="{{ isset($request["codigo"]) && $request["codigo"] != "" ? $request["codigo"] : "" }}">
                </div>

                <div class="form-group col-md-3">
                    <label for="estado" class="sr-only">Estado</label>
                    <select class="form-control" name="estado">
                        <option value="">Estado</option>
                        @foreach ($estados["data"] as $estado)
                            <option value="{{ $estado["id"] }}"
                            {{ isset($request["estado"]) && $request["estado"] == $estado["id"] ? "selected" : "" }}>
                            {{ $estado["status"] }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-md-3">
                <label for="orden" class="sr-only">Orden</label>
                <select class="form-control" name="orden">
                    <option value="">Orden</option>
                    <option value="1"
                    {{ (isset($request["orden"]) && $request["orden"] == 1) ? "selected" : "" }}>
                    Nuevos primero</option>
                    <option value="2"
                    {{ (isset($request["orden"]) && $request["orden"] == 2) ? "selected" : "" }}>
                    Antiguos primero</option>
                    <option value="3"
                    {{ (isset($request["orden"]) && $request["orden"] == 3) ? "selected" : "" }}>
                    Codigo (MLS)</option>
                    <option value="4"
                    {{ (isset($request["orden"]) && $request["orden"] == 4) ? "selected" : "" }}>
                    Valor arriendo</option>
                    <option value="5"
                    {{ (isset($request["orden"]) && $request["orden"] == 5) ? "selected" : "" }}>
                    Valor venta</option>
                </select>
            </div>

            <div class="form-group col-md-3">
                <label for="agente" class="sr-only">Agente</label>
                <select id="agente" class="form-control" name="agente" disabled>
                    <option value="">Agente</option>
                </select>
            </div>
            <div class="col-lg-2">
                <input class="form-control mb-3 price_input" name="pricemin" type="text"
                placeholder="$ Valor desde"
                value="{{ isset($request['pricemin']) ? $request['pricemin'] : '' }}">
            </div>
            <div class="col-lg-2">
                <input class="form-control mb-3 price_input" name="pricemax" type="text"
                placeholder="$ Valor hasta"
                value="{{ isset($request['pricemax']) ? $request['pricemax'] : '' }}">
            </div>
            <div class="col-lg-2">
                <input class="form-control mb-3" name="areamin" type="text" placeholder="Área minima M²"
                value="{{ isset($request['areamin']) ? $request['areamin'] : '' }}">
            </div>
            <div class="col-lg-2">
                <input class="form-control mb-3" name="areamax" type="text" placeholder="Área maxima M²"
                value="{{ isset($request['areamax']) ? $request['areamax'] : '' }}">
            </div>
            <div class="col-lg-2">
                <input class="form-control mb-3" name="habmin" type="number" placeholder="Hab. desde"
                value="{{ isset($request['habmin']) ? $request['habmin'] : '' }}">
            </div>
            <div class="col-lg-2">
                <input class="form-control mb-3" name="habmax" type="number" placeholder="Hab. hasta"
                value="{{ isset($request['habmax']) ? $request['habmax'] : '' }}">
            </div>
            <div class="col-lg-2">
                <input class="form-control mb-3" name="banosmin" type="number" placeholder="Baños desde"
                value="{{ isset($request['banosmin']) ? $request['banosmin'] : '' }}">
            </div>
            <div class="col-lg-2">
                <input class="form-control mb-3" name="banosmax" type="number" placeholder="Baños hasta"
                value="{{ isset($request['banosmax']) ? $request['banosmax'] : '' }}">
            </div>
            <div class="col-lg-2">
                <input class="form-control mb-3" name="parqmin" type="number" placeholder="Parq. desde"
                value="{{ isset($request['parqmin']) ? $request['parqmin'] : '' }}">
            </div>
            <div class="col-lg-2">
                <input class="form-control mb-3" name="parqmax" type="number" placeholder="Parq. hasta"
                value="{{ isset($request['parqmax']) ? $request['parqmax'] : '' }}">
            </div>
            <div class="col-lg-2">
                <input class="form-control mb-3" name="antmin" type="number"
                placeholder="Antiguedad desde (años)"
                value="{{ isset($request['antmin']) ? $request['antmin'] : '' }}">
            </div>
            <div class="col-lg-2">
                <input class="form-control mb-3" name="antmax" type="number"
                placeholder="Antiguedad hasta (años)"
                value="{{ isset($request['antmax']) ? $request['antmax'] : '' }}">
            </div>

            <div class="col-lg-6">
                <button class="btn btn-primary">Buscar</button>

                <button type="button" class="btn btn-primary btn-default"
                onclick="window.open('https://med.domus.la/NDg3Ljg3','_blank')">
                Estadisticas AFYDI
            </button>
            <button type="button" class="btn btn-primary btn-default"
            onclick="window.open('https://eliteinmobiliaria.com.co/wp-content/uploads/2022/02/DIRECTORIO-AFYDI.xlsx','_blank')">
            Directorio AFYDI
        </button>
    </div>
</div>
</form>
<br>

<div class="title">
    <h6>
        Mostrando desde {{ $inmuebles["from"] }} a {{ $inmuebles["to"] }} de
        {{ $inmuebles["total"] }} inmuebles encontrados
    </h6>
</div>

<table class="table table-bordered table-striped mb-0" id="datatable-default">
    <thead>
        <tr>
            <th>Código MLS</th>
            <th>Tipo</th>
            <th>Gestion</th>
            <th>Barrio</th>
            <th>Valor</th>
            <th>Inmobiliaria</th>
            <th>Fecha de registro</th>
            <th>Acción</th>
            <th class="d-lg-none">Engine version</th>
            <th class="d-lg-none">CSS grade</th>
        </tr>
    </thead>
    <tbody>

        @foreach ($inmuebles["data"] as $inmueble)
            <tr>
                <td>{{ $inmueble["codpro"] }}</td>
                <td>{{ $inmueble["type"] }}</td>
                <td>{{ $inmueble["biz"] }}</td>
                <td>{{ $inmueble["neighborhood"] }}</td>
                <td>{{ $inmueble["price_format"] }}</td>
                <td>{{ $inmueble["real_state_name"] }}...</td>
                <td>
                    <span data-toggle="tooltip" data-placement="top" data-original-title="{{ $inmueble["registry_date"] }}">
                        {{ \Carbon\Carbon::parse($inmueble["registry_date"])->diffForHumans() }}
                    </span>
                </td>
                <th>
                    <a ng-controller="Inmuebles" class="modal-with-form btn-default" ng-click="detalle({{$inmueble['codpro']}}, {{$inmueble['idpro']}})" href="#modalForm">
                        <i class="el el-eye-open" data-toggle="tooltip" data-placement="top" data-original-title="Ver inmueble"></i>
                    </a>
                    &nbsp;
                    <a class="btn-default" href="http://ficha.domus.la/{{ $clase->replacespacios($inmueble["type"]) }}-en-{{ $clase->replacespacios($inmueble["biz"]) }}-en-{{ $clase->replacespacios($inmueble["neighborhood"]) }}/{{ base64_encode($inmueble["idpro"]) }}/{{ base64_encode($usuario->domus_id) }}/1" target="_blank">
                        <i class="el el-website" data-placement="top" data-toggle="tooltip" data-original-title="Ficha inmueble"></i>
                    </a>
                    &nbsp;
                    <a class="btn-default" href="http://ficha.domus.la/1/{{ $clase->replacespacios($inmueble["type"]) }}-en-{{ $clase->replacespacios($inmueble["biz"]) }}-en-{{ $clase->replacespacios($inmueble["neighborhood"]) }}/{{ base64_encode($inmueble["idpro"]) }}/{{ base64_encode($usuario->domus_id) }}/1" target="_blank">
                        <i class="el el-website" data-placement="top" data-toggle="tooltip" data-original-title="Ficha sin datos"></i>
                    </a>
                </th>
                <td class="center d-lg-none">4</td>
                <td class="center d-lg-none">X</td>
            </tr>
        @endforeach

    </tbody>

</table>

<br><br>
{{-- Paginador de inmuebles --}}
<ul class="pagination pagination-modern">

    @if ($inmuebles["current_page"] != 1)
        <li>
            <a href="{{ url()->full().'&page=1' }}">
                << </a>
            </li>
            <li>
                <a href="{{ url()->full().'&page='.($inmuebles["current_page"] - 1)  }}">
                    {{ $inmuebles["current_page"] - 1 }}
                </a>
            </li>
        @endif

        <li class="active">
            <a href="">{{ $inmuebles["current_page"] }}</a>
        </li>

        @if ($inmuebles["current_page"] != $inmuebles["last_page"])
            <li>
                <a href="{{url()->full().'&page='.($inmuebles["current_page"] + 1)  }}">
                    {{ $inmuebles["current_page"] + 1 }}
                </a>
            </li>
            <li>
                <a href="{{ url()->full().'&page='.($inmuebles["last_page"])  }}">
                    >>
                </a>
            </li>
        @endif

    </ul>

    {{-- Final de paginador de inmuebles --}}
</div>
</section>

</div>
</div>
{{-- Vista inmueble --}}
<div ng-controller="Inmuebles" id="modalForm" class="modal-block modal-block-primary mfp-hide modal-xl">
    <section class="card">
        <form action="" method="post" enctype="multipart/form-data">
            <header class="card-header">
                <h2 class="card-title">
                    Vista del inmueble código <% generalfactory.detalleInmueble.codpro %>
                </h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-4 col-xl-4 mb-4 mb-xl-0">
                        <section class="card">
                            <div class="card-body">




                                <div class="thumb-info mb-3 lightgallery">


                                    <img ng-if="generalfactory.detalleInmueble.images[0]"
                                    ng-src="<%generalfactory.detalleInmueble.images[0].imageurl%>"
                                    class="rounded img-fluid item">


                                    <div class="thumb-info-title">
                                        <span class="thumb-info-inner">
                                            <% generalfactory.detalleInmueble.type %>
                                        </span>
                                        <span class="thumb-info-type">
                                            <% generalfactory.detalleInmueble.biz %>
                                        </span>
                                    </div>
                                </div>

                                <div class="widget-toggle-expand mb-3">
                                    <div class="widget-header">
                                        <h5 class="mb-2">
                                            <% generalfactory.detalleInmueble.neighborhood %>
                                        </h5>
                                        <div class="widget-toggle">+</div>
                                    </div>
                                    <div class="widget-content-expanded">
                                        <p>
                                            <% generalfactory.detalleInmueble.city %>, Colombia
                                            <br><b>Zona:</b> <% generalfactory.detalleInmueble.zone %>

                                            <br><b>Estrato:</b> <% generalfactory.detalleInmueble.stratum %>
                                            <br><b>Año
                                                construcción:</b><% generalfactory.detalleInmueble.build_year %>
                                            </p>
                                        </div>
                                    </div>

                                    <hr class="dotted short">
                                    <h5 class="mb-2 mt-3">Especificaciones</h5>
                                    <p>
                                        <br><b>Habitaciones:</b> <% generalfactory.detalleInmueble.bedrooms %>
                                        <br><b>Baños:</b> <% generalfactory.detalleInmueble.bathrooms %>
                                        <br><b>Área lote:</b> <% generalfactory.detalleInmueble.area_lot %> m²
                                        <br><b>Área construida:</b> <% generalfactory.detalleInmueble.area_cons %>
                                        m²
                                        <br><b>Frente:</b> <% generalfactory.detalleInmueble.front %> m²
                                        <br><b>Fondo:</b> <% generalfactory.detalleInmueble.fondo %> m²
                                        <br><b>Parq sencillos:</b>
                                        <% generalfactory.detalleInmueble.parking %>
                                    </p>

                                    <hr class="dotted short">
                                    <h5 class="mb-2 mt-3">
                                        Parqueaderos
                                        <% generalfactory.detalleInmueble.parking +  generalfactory.detalleInmueble.parking_covered%>
                                    </h5>
                                    <p>
                                        <br><b>Parq cubiertos:</b>
                                        <% generalfactory.detalleInmueble.parking_covered %>
                                        <br><b>Parq sencillos:</b>
                                        <% generalfactory.detalleInmueble.parking %>
                                    </p>

                                </div>
                            </section>

                        </div>
                        <div class="col-lg-8 col-xl-8">

                            <div class="card card-modern">
                                <div class="card-header">
                                    <h2 class="card-title">MLS <% generalfactory.detalleInmueble.codigoMLS %></h2>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-auto mr-xl-5 pr-xl-5 mb-4 mb-xl-0">
                                            <ul class="list list-unstyled list-item-bottom-space-0">
                                                <li><b>Código: </b> <% generalfactory.detalleInmueble.codpro %></li>
                                                <li><b>Destinación: </b>
                                                    <% generalfactory.detalleInmueble.destination_name %></li>
                                                    <li><b>Canon: </b>$ <% generalfactory.detalleInmueble.rent %></li>
                                                    <li><b>Admón: </b>$ <% generalfactory.detalleInmueble.administration %>
                                                    </li>
                                                    <li><b>Venta: </b>$ <% generalfactory.detalleInmueble.saleprice %></li>
                                                </ul>
                                            </div>
                                            <div class="col-xl-auto pl-xl-5">
                                                <ul class="list list-unstyled list-item-bottom-space-0">
                                                    <li><b>Consignado: </b>
                                                        <% generalfactory.detalleInmueble.consignation_date %></li>
                                                        <li><b>Dirección: </b> <% generalfactory.detalleInmueble.address %>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tabs">
                                        <ul class="nav nav-tabs tabs-primary">
                                            <li class="nav-item active">
                                                <a class="nav-link" href="#overview" data-toggle="tab">Descripción</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#edit" data-toggle="tab">Características</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#mult" data-toggle="tab">Multimedia</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="overview" class="tab-pane active">

                                                <div class="p-3">

                                                    <h4 class="mb-3">Principal</h4>
                                                    <p> <% generalfactory.detalleInmueble.description %></p>

                                                    <h4 class="mb-3">Comentarios</h4>
                                                    <p> <% generalfactory.detalleInmueble.comment %></p>

                                                    <h4 class="mb-3">Comentarios 2</h4>
                                                    <p> <% generalfactory.detalleInmueble.comment2 %></p>
                                                </div>

                                            </div>
                                            <div id="edit" class="tab-pane">

                                                <form class="p-3">
                                                    <div class="form-row">
                                                        <div class="form-group col-md-6 sinmargenab">
                                                            <h4 class="mb-3">Interiores</h4>
                                                            <ul class="simple-todo-list mt-3">
                                                                <li class="completed"
                                                                ng-repeat="$caract in generalfactory.detalleInmueble.amenities"
                                                                ng-if="$caract.type == 1">
                                                                <% $caract.name %>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="form-group col-md-6 sinmargenab">
                                                        <h4 class="mb-3">Exteriores</h4>
                                                        <ul class="simple-todo-list mt-3">
                                                            <li class="completed"
                                                            ng-repeat="$caract in generalfactory.detalleInmueble.amenities"
                                                            ng-if="$caract.type == 2">
                                                            <% $caract.name %>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <hr class="dotted tall">
                                            <div class="form-row">
                                                <div class="form-group col-md-6 sinmargenab">
                                                    <h4 class="mb-3">del sector</h4>
                                                    <ul class="simple-todo-list mt-3">
                                                        <li class="completed"
                                                        ng-repeat="$caract in generalfactory.detalleInmueble.amenities"
                                                        ng-if="$caract.type == 3">
                                                        <% $caract.name %>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="form-group col-md-6 sinmargenab">
                                                <h4 class="mb-3">generales</h4>
                                                <ul class="simple-todo-list mt-3">
                                                    <li class="completed"
                                                    ng-repeat="$caract in generalfactory.detalleInmueble.amenities"
                                                    ng-if="$caract.type == 4">
                                                    <% $caract.name %>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                </form>

                            </div>
                            <div id="mult" class="tab-pane">

                                <div class="p-3">
                                    <h4 class="mb-3">Video</h4>
                                    <p ng-if="generalfactory.detalleInmueble.video">
                                        <iframe width="100%" height="315"
                                        src="https://www.youtube.com/embed/RdMjRv3bk7A"
                                        title="YouTube video player" frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                                    </p>
                                    <h4 class="mb-3">Fotos 360</h4>

                                    <div id="viewer"></div>
                                    <!-- ACA VAN LAS IMAGENES 360 -->
                                </div>

                            </div>

                        </div>
                    </div>


                </div>

            </div>

        </div>
        <footer class="card-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-default modal-dismiss">Cerrar</button>
                </div>
            </div>
        </footer>
    </form>
</section>
</div>
{{-- Final de Vista inmueble  --}}
