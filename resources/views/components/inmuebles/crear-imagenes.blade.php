<section role="main" class="content-body">

    <header class="page-header">
        <h2>Inmuebles</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>Inmuebles</span></li>
                <li><span>Crear imágenes&nbsp;&nbsp;&nbsp;</span></li>
            </ol>
            <a class="" data-open=""></a>
        </div>
    </header>

    {{-- Validación de permisos para crear imágenes --}}
    @php($crearImagenes = App\Http\Controllers\GeneralController::getPermission($usuario, 2))
    <button class="btn btn-primary separaraltbottom" ng-controller="FormImage" ng-click="generalfactory.previousPage()">
        <i class="fas fa-arrow-left"></i> Volver</button>
    @if ($crearImagenes == 1)
    <div class="row">
        <div class="col" ng-controller="FormImage"
            ng-init="initDropZone('{{ csrf_token() }}', '{{ url("admin/inmuebles/crear/$inmueble->id/imagenes") }}', '{{ $inmueble->id }}')">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Fotos del inmueble (máximo 30)</h2>
                </header>
                <div class="card-body">
                    <div id="image-upload-demo">
                        <div id="iu-gallery"></div>
                        <div id="iu-image-upload-zone"></div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <section class="card" ng-controller="FormImage360"
                ng-init="initDropZone('{{ csrf_token() }}', '{{ url("admin/inmuebles/crear/$inmueble->id/imagenes") }}', '{{ $inmueble->id }}')">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Fotos del inmueble 360 &deg; (máximo 30)</h2>
                </header>
                <div class="card-body">
                    <div id="image-upload-360">
                        <div id="iu-gallery-360"></div>
                        <div id="iu-image-upload-zone-360"></div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <section class="card">
                <div class="text-right pull-right">
                    <a role="button" class="btn btn-primary"
                        href="{{ url("admin/inmuebles/crear/$inmueble->id/propietarios") }}">
                        Guardar imágenes
                    </a>
                </div>
            </section>
        </div>
    </div>
    @else
    <div class="row">
        <div class="col" ng-controller="FormImage"
            ng-init="initDropZone('{{ csrf_token() }}', '{{ url("admin/inmuebles/crear/$inmueble->id/imagenes") }}', '{{ $inmueble->id }}')">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">No tiene permiso para usar este módulo</h2>
                </header>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <section class="card">
                <div class="text-right pull-right">
                    <a role="button" class="btn btn-primary"
                        href="{{ url("admin/inmuebles/crear/$inmueble->id/propietarios") }}">
                        Ir al módulo siguiente
                    </a>
                </div>
            </section>
        </div>
    </div>
    @endif

</section>