<section role="main" class="content-body" ng-controller="Propietarios">

    <header class="page-header">
        <h2>Inmuebles</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>Inmuebles</span></li>
                <li><span>Editar Propietarios&nbsp;&nbsp;&nbsp;</span></li>
                <li><span>{{ $inmueble->codigo }}&nbsp;&nbsp;&nbsp;</span></li>
            </ol>
            <a class="" data-open=""></a>
        </div>
    </header>
    <button class="btn btn-primary separaraltbottom" ng-controller="FormImage" ng-click="generalfactory.previousPage()">
        <i class="fas fa-arrow-left"></i> Volver</button>
    <div class="row">
        <div class="col-lg-12">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Propietarios del inmueble código {{ $inmueble->codigo }}</h2>
                </header>
                <div class="card-body" ng-init="agregarPropietariosYaExistentes('{{ $inmuebleClientes }}')">
                    <form id="formulario-asociar-propietario"
                        ng-submit="asociarPropietarios('{{ url("api/inmuebles/editar/$inmueble->id/propietarios") }}', '{{ $usuario["token"] }}')">
                        <table class="table table-responsive-md mb-0">
                            <thead>
                                <tr>
                                    <th>Cédula</th>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Participación %</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="(index, contacto) in propietarios" ng-if="contacto.id" ng-cloak>
                                    <input type="hidden" name="owner_<% index %>" value="<% contacto.id %>">
                                    <td><% contacto.documento %></td>
                                    <td><% contacto.nombres %></td>
                                    <td><% contacto.apellidos %></td>
                                    <td>
                                        <input type="number" step="0.01" min="0" max="100" class="form-control"
                                            placeholder="Porcentaje %"
                                            ng-value="<% contacto.inmuebles ? contacto.inmuebles[0].porcentaje : 0 %>"
                                            min="0" max="100" name="owner_percentage_<% index %>" required>
                                    </td>
                                    <td class="actions">
                                        <a href="#" class="delete-row" ng-click="eliminarPropietario(index, $event)">
                                            <i class="far fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>

                            </tbody>
                        </table>

                        <div class="col-md-12">

                            <input type="hidden" id="borrados" value="" name="borrados">
                            <br>
                            <button type="submit" class="btn btn-primary pull-right">
                                Guardar
                            </button>
                        </div>

                        <div class="row">
                            <div class="col-md-12 alert alert-info" role="alert" id="formulario-cargando">
                                Cargando...
                            </div>

                            <div class="col-md-12 alert alert-success" role="alert" id="formulario-enviado">
                                ¡Propietarios asociados con éxito!
                            </div>

                            <div class="col-md-12 alert alert-danger" role="alert" id="formulario-error"></div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>

    <div class="row">

        <div class="col-lg-6">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Propietarios existentes</h2>
                </header>
                <div class="card-body">
                    <table class="table table-responsive-md table-striped mb-0"
                        ng-init="lista('{{ $usuario["token"] }}')">

                        <thead>
                            <tr>
                                <td colspan="4">
                                    <input type="text" class="form-control" id="inputDefault"
                                        placeholder="Buscar por palabra clave" ng-model="nombre"
                                        ng-change="lista('{{ $usuario["token"] }}')">
                                </td>
                            </tr>
                            <tr>
                                <th>Cédula</th>
                                <th>Nombres</th>
                                <th>Apellidos</th>
                                <th>Agregar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="contacto in contactos.data" ng-cloak>
                                <td><% contacto.documento %></td>
                                <td><% contacto.nombres %></td>
                                <td><% contacto.apellidos %></td>
                                <td>
                                    <a href="#" ng-click="agregarPropietario(contacto, $event)">
                                        <i class="fas fa-plus"></i>
                                    </a>
                                </td>
                            </tr>

                        </tbody>

                        </tbody>
                    </table>
                    <br>
                    <ul class="pagination">
                        <li class="paginate_button page-item previous" id="datatable-default_previous"
                            ng-if="contactos.current_page > 1" ng-cloak>
                            <a href="#"
                                ng-click="lista('{{ $usuario["token"] }}', (contactos.current_page - 1), $event)"
                                aria-controls="datatable-default" data-dt-idx="0" tabindex="0" class="page-link">
                                Anterior
                            </a>
                        </li>
                        <li class="paginate_button page-item" ng-if="contactos.current_page > 1" ng-cloak>
                            <a href="#"
                                ng-click="lista('{{ $usuario["token"] }}', (contactos.current_page - 1), $event)"
                                aria-controls="datatable-default" data-dt-idx="2" tabindex="0" class="page-link">
                                <% (contactos.current_page - 1) %>
                            </a>
                        </li>
                        <li class="paginate_button page-item active" ng-cloak>
                            <a href="#" aria-controls="datatable-default" data-dt-idx="1" tabindex="0"
                                class="page-link">
                                <% contactos.current_page %>
                            </a>
                        </li>
                        <li class="paginate_button page-item" ng-if="contactos.current_page < contactos.last_page"
                            ng-cloak>
                            <a href="#"
                                ng-click="lista('{{ $usuario["token"] }}', (contactos.current_page + 1), $event)"
                                aria-controls="datatable-default" data-dt-idx="2" tabindex="0" class="page-link">
                                <% (contactos.current_page + 1) %>
                            </a>
                        </li>
                        <li class="paginate_button page-item next" id="datatable-default_next"
                            ng-if="contactos.current_page < contactos.last_page" ng-cloak>
                            <a href="#"
                                ng-click="lista('{{ $usuario["token"] }}', (contactos.current_page + 1), $event)"
                                aria-controls="datatable-default" data-dt-idx="7" tabindex="0" class="page-link">
                                Siguiente
                            </a>
                        </li>
                    </ul>
                </div>
            </section>
        </div>

        <section class="card col-md-6">
            <form ng-submit="creatPropietario('{{ url("api/clientes") }}', '{{ $usuario["token"] }}')"
                id="formulario-crear-propietario" enctype="multipart/form-data">
                <header class="card-header">
                    <h2 class="card-title">Crear propietario</h2>
                </header>
                <div class="card-body">

                    {{ csrf_field() }}

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="username">Usuario ( Cédula )</label>
                            <input type="number" class="form-control" id="username"
                                value="{{ session("content")["username"] ?? "" }}" name="username" placeholder="Usuario"
                                ng-required="rol != 13">
                        </div>
                        <div class="form-group col-md-6 mb-3 mb-lg-0">
                            <label for="password">Clave</label>
                            <input type="text" class="form-control" id="password"
                                value="{{ session("content")["password"] ?? "" }}" name="password"
                                placeholder="Password" ng-required="rol != 13">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="name">Nombres *</label>
                            <input type="text" class="form-control" value="{{ session("content")["name"] ?? "" }}"
                                id="name" name="name" placeholder="Nombres" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="last_name">Apellidos *</label>
                            <input type="text" class="form-control" value="{{ session("content")["last_name"] ?? "" }}"
                                name="last_name" id="last_name" placeholder="Apellidos" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="ciudad">Ciudad de residencia <span ng-if="rol != 13">*</span></label>
                            <select id="ciudad" class="form-control" name="ciudad" ng-model="ciudad">
                                <option value="">Ciudad </option>
                                <option value="ext">EXTRANJERO
                                </option>
                                @foreach($ciudades as $ciudad)
                                <option value="{{ $ciudad->id }}">
                                    {{ $ciudad->nombre }}
                                </option>
                                @endforeach
                            </select>
                        </div>

                        <div ng-if="ciudad == 'ext'" class="form-group col-md-6">
                            <label for="ciudad">&nbsp;</label>
                            <input type="text" class="form-control" name="ciudad_ext" placeholder="Washington" required>
                        </div>
                        <div class="form-group <%ciudad == 'ext' ? 'col-md-12' : 'col-md-6'%>">
                            <label for="name">Dirección <span ng-if="rol != 13">*</span></label>
                            <input type="text" class="form-control" id="direccion" name="direccion"
                                placeholder="Calle 123 # 45 B - 72" ng-required="rol != 13">
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="document_type">Tipo doc</label>
                            <select id="document_type" name="document_type" class="form-control">
                                <option value="">Tipo de documento</option>
                                @foreach ($tipos_documento as $tipo)
                                <option value="{{ $tipo->id }}"
                                    {{ (isset(session("content")["document_type"]) && session("content")["document_type"] == $tipo->id) ? "selected" : "" }}>
                                    {{ $tipo->nombre }}
                                </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-5">
                            <label for="document">Número <span ng-if="rol != 13">*</span></label>
                            <input type="number" class="form-control" value="{{ session("content")["document"] ?? "" }}"
                                name="document" id="document" placeholder="31950000" ng-required="rol != 13">
                        </div>
                        <div class="form-group col-md-5">
                            <label for="expedicion">Expedición </label>
                            <input type="text" class="form-control" value="{{ session("content")["expedicion"] ?? "" }}"
                                name="expedicion" id="expedicion">
                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-6">
                            <label>Estado Civil</label>
                            <select class="form-control" name="estado_civil"
                                value="{{ session("content")["estado_civil"] ?? "" }}">
                                <option value="">Seleccionar</option>
                                <option value="Casado">Casado</option>
                                <option value="Soltero">Soltero</option>
                                <option value="Juridica">Persona jurídica</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Detalle Estado Civil</label>

                            <select class="form-control" name="subestado_civil"
                                value="{{ session("content")["subestado_civil"] ?? "" }}">
                                <option value="">Seleccionar</option>
                                @foreach($estados_civiles as $ec)
                                <option value="{{$ec->id}}">{{$ec->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="phone">Teléfono</label>
                            <input type="text" class="form-control" value="{{ session("content")["phone"] ?? "" }}"
                                name="phone" id="phone" placeholder="7153573">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="cellphone">Celular/Whatsapp</label>
                            <input type="text" class="form-control" value="{{ session("content")["cellphone"] ?? "" }}"
                                name="cellphone" id="cellphone" placeholder="3181234567" ng-required="rol == 13">
                        </div>
                    </div>

                    <div class="form-row">


                        <div class="form-group col-md-6">
                            <label for="email">Correo Electrónico *</label>
                            <input type="email" class="form-control" value="{{ session("content")["email"] ?? "" }}"
                                name="email" id="email" placeholder="Correo" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="rol">Rol *</label>
                            <select id="rol" class="form-control" name="role" ng-model="rol" required>
                                <option value="">Rol</option>
                                @foreach($roles as $rol)
                                <option value="{{ $rol->id }}" {{ $rol->id == 3 ? "selected" : "" }}
                                    {{ (isset(session("content")["role"]) && session("content")["role"] == $rol->id) ? "selected" : "" }}>
                                    {{ $rol->nombre }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="estado">Estado del cliente<span ng-if="rol != 13">*</span></label>
                            <select id="estado" name="estado" class="form-control" ng-required="rol == 13">
                                <option value="">Estado</option>
                                @foreach($estados as $estado)
                                <option value="{{ $estado->id }}"
                                    {{ (isset(session("content")["estado"]) && session("content")["estado"] == $estado->id) ? "selected" : "" }}>
                                    {{ $estado->nombre }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="profession">Profesión</label>
                            <input type="text" class="form-control" value="{{ session("content")["profession"] ?? "" }}"
                                name="profession" id="profession" placeholder="Profesión">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Fecha nacimiento</label>
                            <div>
                                <input type="date" class="form-control" name="birthdate"
                                    value="{{ session("content")["birthdate"] ?? date("Y-m-d") }}">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Cédula</label> <br>
                            <input type="file" id="archivo_documento" name="archivo_documento"
                                accept="application/pdf,image/jpeg,image/png"></input>
                        </div>
                    </div>
                </div>
                <footer class="card-footer">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>

                    <div class="col-md-9">

                        <div class="col-md-12 alert alert-info" role="alert" id="formlario-cargando">
                            Cargando...
                        </div>

                        <div class="col-md-12 alert alert-success" role="alert" id="formlario-enviado">
                            ¡Propietario creado con éxito!
                        </div>

                        <div class="col-md-12 alert alert-danger" role="alert" id="formlario-error"></div>
                    </div>
                </footer>
            </form>
        </section>

</section>