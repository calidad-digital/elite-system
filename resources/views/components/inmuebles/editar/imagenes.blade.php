<section role="main" class="content-body">

    <header class="page-header">
        <h2>Inmuebles</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>Inmuebles</span></li>
                <li><span>Editar imágenes&nbsp;&nbsp;&nbsp;</span></li>
                <li><span>{{ $inmueble->codigo }}&nbsp;&nbsp;&nbsp;</span></li>
            </ol>
            <a class="" data-open=""></a>
        </div>
    </header>

    {{-- Validación de permisos para editar imágenes --}}
    @php($editarImagenes = App\Http\Controllers\GeneralController::getPermission($usuario, 3))

    @if ($editarImagenes != 1)
    <style>
    div#iu-image-upload-zone,
    div#iu-image-upload-zone-360 {
        display: none;
    }
    </style>
    @endif

    <button class="btn btn-primary separaraltbottom" ng-controller="FormImage" ng-click="generalfactory.previousPage()">
        <i class="fas fa-arrow-left"></i> Volver</button>
    <div class="row">
        <div class="col" ng-controller="FormImage"
            ng-init="initDropZone('{{ csrf_token() }}', '{{ url("admin/inmuebles/crear/$inmueble->id/imagenes") }}', '{{ $inmueble->id }}', '{{ json_encode($inmueble->imagenes) }}')">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Fotos del inmueble (máximo 30)</h2>
                </header>
                <div class="card-body">
                    <div id="image-upload-demo">
                        <div id="iu-gallery"></div>
                        <div id="iu-image-upload-zone"></div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <section class="card" ng-controller="FormImage360"
                ng-init="initDropZone('{{ csrf_token() }}', '{{ url("admin/inmuebles/crear/$inmueble->id/imagenes") }}', '{{ $inmueble->id }}', '{{ json_encode($inmueble->imagenes) }}')">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Fotos del inmueble 360 &deg; (máximo 30)</h2>
                </header>
                <div class="card-body">
                    <div id="image-upload-360">
                        <div id="iu-gallery-360"></div>
                        <div id="iu-image-upload-zone-360"></div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <section class="card">
                <div class="text-right pull-right">
                    <a role="button" class="btn btn-primary"
                        href="{{ url("admin/inmuebles/editar/$inmueble->id/propietarios") }}">
                        Guardar imágenes
                    </a>
                    @if($inmueble->estado_id ==10)
                    <!-- if($inmueble->estado_id !=5 || $usuario->rol_id == 1) -->
                    @else
                    <!-- <a role="button" class="btn btn-primary" href="{{ url("admin/inmuebles") }}">
                        Guardar imágenes
                    </a>
                    o
                    <a role="button" class="btn btn-primary" href="{{ url("alt/actualizar-inmueble/$inmueble->id") }}">
                        Guardar y actualizar en Domus
                    </a> -->
                    @endif
                </div>
            </section>
        </div>
    </div>


</section>