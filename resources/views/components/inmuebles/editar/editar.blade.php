<section role="main" class="content-body">
    <form ng-controller="Formulario" id="formulario-crear-inmueble"
    ng-submit="submitData('{{ url("admin/inmuebles/editar/{$inmueble->id}") }}')">

    {{ csrf_field() }}

    <header class="page-header">
        <h2>Inmuebles</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>Inmuebles</span></li>
                <li><span>Editar&nbsp;&nbsp;&nbsp;</span></li>
                <li><span>Inmueble {{ $inmueble->codigo }}&nbsp;&nbsp;&nbsp;</span></li>
            </ol>
            <a class="" data-open=""></a>
        </div>
    </header>

    {{-- Parte inicial --}}
    <div class="row">

        <section class="card col-lg-6">
            <header class="card-header">
                <h2 class="card-title">Editar inmueble {{ $inmueble->codigo }}</h2>
            </header>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="agente">Agente Encargado</label>
                        <select id="agente" name="agente" class="form-control"
                        ng-init="agente= '{{$inmueble->asesores->id}}'" ng-model="agente"
                        {{$usuario->rol_id == 1 ? '' : 'disabled'}}>
                        @foreach ($usuarios as $user)
                            <option value="{{ $user->id }}">
                                {{ $user->nombres }} {{$user->apellidos}}
                            </option>
                        @endforeach
                    </select>
                </div>

            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="codigo">Código</label>
                    <input type="text" class="form-control" value="{{ $inmueble->codigo }}" id="codigo"
                    name="codigo" placeholder="Código" readonly>
                </div>

                <div class="form-group col-md-6">
                    <label for="referencia">Referencia</label>
                    <input type="text" class="form-control" value="{{ $inmueble->referencia }}" id="referencia"
                    name="referencia" placeholder="Referencia">
                </div>
            </div>

            <div class="form-row">

                <div class="form-group col-md-4"
                ng-init="tipo_inmueble = '{{ $inmueble->tipo_inmueble_id }}'; mostrarCaracteristicas();">
                <label for="tipo">Tipo *</label>
                <select id="tipo" class="form-control" name="tipo" ng-model="tipo_inmueble"
                ng-change="mostrarCaracteristicas()" required>
                <option value="">Seleccione tipo</option>
                @foreach ($tipos as $tipo)
                    <option value="{{ $tipo->id }}">
                        {{ $tipo->nombre }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="form-group col-md-4" ng-init="gestion = '{{ $inmueble->gestion_id }}'">
            <label for="gestion">Gestion *</label>
            <select id="gestion" name="gestion" class="form-control" ng-model="gestion" required>
                <option value="">Seleccione gestón</option>
                @foreach ($gestiones as $gestion)
                    <option value="{{ $gestion->id }}">
                        {{ $gestion->nombre }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="form-group col-md-4">
            <label for="destino">Destinacion</label>
            <select id="destino" name="destino" class="form-control" required>
                <option value="">Seleccione destinación</option>
                @foreach ($destinos as $destino)
                    <option value="{{ $destino->id }}"
                        {{ $inmueble->destino_id == $destino->id ? "selected" : "" }}>
                        {{ $destino->nombre }}
                    </option>
                @endforeach
            </select>
        </div>

    </div>

    <div class="form-row" ng-show="gestion == '1'" ng-init="corretaje = '{{$inmueble->corretaje}}'"
        ng-cloak>


        <div class="form-group col-md-12" ng-model="corretaje">
            <label for="corretaje">Corretaje</label>
            <select id="corretaje" name="corretaje" class="form-control" ng-model="corretaje">
                <option value="0">No</option>
                <option value="1">Si</option>
            </select>
        </div>

    </div>

    <div class="form-row">
        <div class="form-group col-md-4" ng-init="arriendo = ({{ $inmueble->canon }}| currency:'':'0')">
            <label for="canon">Canon</label>
            <input type="text" class="form-control price_input" value="" ng-model="arriendo"
            ng-change="setTotal()" name="canon" placeholder="Canon"
            ng-required="gestion == 1 || gestion == 3">
        </div>
        <div class="form-group col-md-4"
        ng-init="administracion = ({{ $inmueble->administracion }}| currency:'':'0')">
        <label for="administracion">Administración con descuento</label>
        <input type="text" class="form-control price_input" value="" ng-model="administracion"
        ng-change="setTotal()" name="administracion" placeholder="Administración">
    </div>
    <div class="form-group col-md-4" ng-init="setValorInm(gestion,{{ $inmueble->venta }},{{ $inmueble->canon }},{{ $inmueble->administracion }})">
        <label for="total">Total</label>
        <input type="text" class="form-control" value="" ng-model="total" name="total" placeholder="total" readonly>
    </div>
</div>

<div class="form-row">
    <div class="form-group col-md-6" ng-init="venta = ({{ $inmueble->venta }}| currency:'':'0')">
        <label for="venta">Venta</label>
        <input type="text" class="form-control price_input" value="" ng-model="venta"
        ng-change="setTotal()" name="venta" placeholder="Venta"
        ng-required="gestion == 2 || gestion == 3">
    </div>

    <div class="form-group col-md-6">
        <label>Fecha consignación</label>
        <div>
            {{-- <div data-plugin-datepicker data-plugin-skin="primary">
        </div> --}}
        <input type="date" class="form-control" name="fecha_consignacion"
        value="{{ $inmueble->fecha_consignacion ?? date("Y-m-d") }}">
    </div>

</div>
</div>
</div>
</section>

<section class="card col-lg-6">
    <header class="card-header">
        <h2 class="card-title">Ubicación</h2>
    </header>
    <div class="card-body">

        <div class="form-row">

            <div class="form-group col-md-6"
            ng-init="departamento = '{{ $inmueble->estado_ubicacion_id }}'; ciudades();">
            <label for="departamento">Departamento</label>
            <select id="departamentos" class="form-control" name="departamento" ng-model="departamento"
            ng-change="ciudades()" required>
            <option value="">Seleccione departamento</option>
            @foreach ($departamentos as $departamento)
                <option value="{{ $departamento->id }}">
                    {{ $departamento->nombre }}
                </option>
            @endforeach
        </select>
    </div>

    <div class="form-group col-md-6" ng-init="ciudad = {{ $inmueble->ciudad_id }}; localidades();">
        <label for="ciudad">Ciudad</label>
        <select name="ciudad" class="form-control" ng-model="ciudad" ng-change="localidades()"
        id="ciudad" required>
        <option value="">Seleccione una ciudad</option>

        <option ng-repeat="city in cities" ng-value="city.id" ng-bind="city.nombre"></option>
    </select>
</div>

</div>

<div class="form-row">

    <div class="form-group col-md-4" ng-init="zona = '{{ $inmueble->zona_id }}'">
        <label for="zona">Zona</label>
        <select class="form-control" name="zona" ng-model="zona" required>
            <option value="">Seleccione zona</option>
            @foreach ($zonas as $zona)
                <option value="{{ $zona->id }}">
                    {{ $zona->nombre }}
                </option>
            @endforeach
        </select>
    </div>

    <div class="form-group col-md-4"
    ng-init="localidad = {{ $inmueble->localidad_id }}; barrios();">
    <label for="localidad">Localidad</label>
    <select name="localidad" class="form-control" ng-model="localidad" ng-change="barrios()"
    required>
    <option value="">Seleccione localidad</option>

    <option ng-repeat="localidad in localities" ng-value="localidad.id"
    ng-bind="localidad.nombre"></option>
</select>
</div>

<div class="form-group col-md-4">
    <label for="estrato">Estrato</label>
    <select name="estrato" class="form-control" required>

        <option value="">Seleccione estrato</option>
        @foreach ($estratos as $estrato)
            <option value="{{ $estrato->id }}"
                {{ $inmueble->estrato_id == $estrato->id ? "selected" : "" }}>
                {{ $estrato->nombre }}
            </option>
        @endforeach
    </select>
</div>

</div>

<div class="form-row">
    <div class="form-group col-md-6" ng-init="barrio_id = {{ $inmueble->barrio_id }}">
        <label for="barrio_id">Barrio</label>
        <select class="form-control" name="barrio_id" ng-model="barrio_id" id="barrio_id"
        ng-change="selectBarrio()" required>
        <option value="">Seleccione barrio</option>

        <option ng-repeat="barrio in neighborhoods" ng-value="barrio.id"
        ng-bind="barrio.nombre"></option>
    </select>
</div>
<div class="form-group col-md-6" ng-init="barrioName = '{{ $inmueble->barrio_comun }}'">
    <label for="barrio_comun">Nombre común barrio</label>
    <input type="text" class="form-control" value="" ng-value="barrioName" id="barrio"
    name="barrio_comun" placeholder="nombre común" required>
</div>
</div>
</div>
</section>
</div>
{{-- Final de ubicación --}}
<br>
<div class="row">
    <section class="card col-lg-12">
        <header class="card-header">
            <h2 class="card-title">Dirección</h2>
        </header>
        <div class="card-body" ng-init="isCompleteAddress = {{ $inmueble->direcciones == null ? "true" : "false" }}">
            <input ng-init="latitude = '{{ $inmueble->latitud }}'" type="hidden" name="latitude" ng-value="latitude">
            <input ng-init="longitude = '{{ $inmueble->longitud }}'" type="hidden" name="longitude" ng-value="longitude">

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="isCompleteAddress">
                        <input type="checkbox" name="isCompleteAddress" ng-model="isCompleteAddress">
                        Dirección personalizada
                    </label>
                </div>
            </div>

            <div class="form-row" ng-if="isCompleteAddress" ng-init="direccion = '{{ $inmueble->direccion }}'">
                <div class="form-group col-md-12" ng-init="armarDireccion()">
                    <input type="text" name="complete_address" ng-change="armarDireccion()" id="direccion_completa" ng-model="direccion" class="form-control" placeholder="Dirección completa" value="{{ $inmueble->direccion }}">
                </div>
            </div>

            <div class="form-row" ng-if="!isCompleteAddress">

                <div class="form-group col-md-2">
                    <label for="dir1">Cll/Cra</label>
                    <select class="form-control" name="dir1" id="dir1">
                        @foreach ($dir1 as $dir)
                            <option value="{{ $dir->id }}"
                                {{ $inmueble->direcciones != null && $inmueble->direcciones->c1_id == $dir->id ? "selected" : "" }}>
                                {{ $dir->nombre }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-md-1" ng-init="dir2 = '{{ $inmueble->direcciones->c2 ?? "" }}'">
                    <label for="dir2">Nombre</label>
                    <input type="text" class="form-control" value="" ng-change="armarDireccion()" ng-model="dir2" name="dir2" id="dir2" placeholder="5">
                </div>

                <div class="form-group col-md-1">
                    <label for="dir3">Letra</label>
                    <select class="form-control" name="dir3" id="dir3">
                        @foreach ($dir2 as $dir)
                            <option value="{{ $dir->id }}"
                                {{ $inmueble->direcciones != null && $inmueble->direcciones->c3_id == $dir->id ? "selected" : "" }}>
                                {{ $dir->nombre }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-md-1">
                    <label for="dir4">BIS</label>
                    <select class="form-control" name="dir4" id="dir4">
                        @foreach ($dir3 as $dir)
                            <option value="{{ $dir->id }}"
                                {{ $inmueble->direcciones != null && $inmueble->direcciones->c4_id == $dir->id ? "selected" : "" }}>
                                {{ $dir->nombre }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-md-1">
                    <label for="dir5">Letra</label>
                    <select class="form-control" name="dir5" id="dir5">
                        @foreach ($dir2 as $dir)
                            <option value="{{ $dir->id }}"
                                {{ $inmueble->direcciones != null && $inmueble->direcciones->c5_id == $dir->id ? "selected" : "" }}>
                                {{ $dir->nombre }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-md-1">
                    <label for="dir6">Zona</label>
                    <select class="form-control" name="dir6" id="dir6">
                        @foreach ($dir4 as $dir)
                            <option value="{{ $dir->id }}"
                                {{ $inmueble->direcciones != null && $inmueble->direcciones->c6_id == $dir->id ? "selected" : "" }}>
                                {{ $dir->nombre }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-md-2" ng-init="dir7 = {{ $inmueble->direcciones->c7 ?? "" }}">
                    <label for="dir7">#</label>
                    <input type="number" class="form-control" value="" ng-change="armarDireccion()" ng-model="dir7" name="dir7" id="dir7" placeholder="24">
                </div>

                <div class="form-group col-md-1">
                    <label for="dir8">Letra</label>
                    <select class="form-control" name="dir8" id="dir8">
                        @foreach ($dir2 as $dir)
                            <option value="{{ $dir->id }}"
                                {{ $inmueble->direcciones != null && $inmueble->direcciones->c8_id == $dir->id ? "selected" : "" }}>
                                {{ $dir->nombre }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-1" ng-init="dir9 = {{ $inmueble->direcciones->c9 ?? "" }}">
                    <label for="dir9">Número</label>
                    <input type="number" class="form-control" ng-change="armarDireccion()" ng-model="dir9" value="" name="dir9" id="dir9" placeholder="60">
                </div>

                <div class="form-group col-md-1">
                    <label for="dir10">Zona</label>
                    <select class="form-control" name="dir10" id="dir10">
                        @foreach ($dir4 as $dir)
                            <option value="{{ $dir->id }}"
                                {{ $inmueble->direcciones != null && $inmueble->direcciones->c10_id == $dir->id ? "selected" : "" }}>
                                {{ $dir->nombre }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-row" ng-if="!isCompleteAddress">
                <div class="form-group col-md-4" ng-init="complemento = '{{ $inmueble->direcciones->complemento ?? "" }}'">
                    <label for="complemento">Nombre del conjunto</label>
                    <input type="text" class="form-control" value="" ng-change="armarDireccion()" ng-model="complemento" name="complemento" placeholder="">
                </div>

                <div class="form-group col-md-4" ng-init="complemento2 = '{{ $inmueble->direcciones->complemento2 ?? "" }}'">
                    <label for="complemento2">CASA, APTO, BODEGA, OFICINA</label>
                    <input type="text" class="form-control" value="" ng-change="armarDireccion()" ng-model="complemento2" name="complemento2" placeholder="">
                </div>
                <div class="form-group col-md-4" ng-init="complemento3 = '{{ $inmueble->direcciones->complemento3 ?? "" }}'">
                    <label for="complemento3">BLOQUE, LOTE, MANZANA. TORRE</label>
                    <input type="text" class="form-control" value="" ng-change="armarDireccion()" ng-model="complemento3" name="complemento3" placeholder="">
                </div>

                <div class="form-group col-md-6" ng-init="direccion = '{{ $inmueble->direccion }}'; esperarDireccion();">
                    <label>Dirección (Resultado final)</label>
                    <input type="text" class="form-control" ng-model="direccion" name="direccion" id="direccion" placeholder="Dirección" readonly required>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-9">
                    <label for="buscardireccion">Buscar ubicación en el mapa</label>
                    <input type="text" class="form-control" value="" name="buscardireccion" placeholder="Ciudad, Barrio, dirección" ng-model="campodireccion">
                </div>

                <br>

                <div class="form-group col-md-3">
                    <label>Presionar para actualizar</label> <br>
                    <a href="#" id="searchMap" role="button" class="btn btn-primary" ng-click="buscardireccion($event)">
                        Buscar
                    </a>
                </div>
            </div>

            <div class="form-row">
                <div id="mapa"></div>
            </div>
        </div>
    </section>
</div>
<br>
<div class="row">
    <section class="card col-lg-6">
        <header class="card-header">
            <h2 class="card-title">Comodidades</h2>
        </header>
        <div class="card-body">

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="habitaciones">Habitaciones</label>
                    <input type="text" class="form-control" value="{{ $inmueble->habitaciones }}"
                    name="habitaciones" placeholder="3">
                </div>
                <div class="form-group col-md-6">
                    <label for="banios">Baños</label>
                    <input type="text" class="form-control" value="{{ $inmueble->banios }}" name="banios"
                    placeholder="2">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="parqueaderos">Parqueaderos totales</label>
                    <input type="text" class="form-control" value="{{ $inmueble->parqueaderos }}"
                    name="parqueaderos" placeholder="2">
                </div>
                <div class="form-group col-md-6">
                    <label for="parqueaderos_cubiertos">Parqueaderos cubiertos</label>
                    <input type="text" class="form-control" value="{{ $inmueble->parqueaderos_cubiertos }}"
                    name="parqueaderos_cubiertos" placeholder="1">
                </div>
                <div class="form-group col-md-4">
                    <label for="parqueaderos_descubiertos">Parqueaderos descubiertos</label>
                    <input type="text" class="form-control" value="{{ $inmueble->parqueaderos_descubiertos }}"
                    name="parqueaderos_descubiertos" placeholder="1">
                </div>
                <div class="form-group col-md-4">
                    <label for="parqueaderos_dobles">Parqueaderos dobles</label>
                    <input type="text" class="form-control" value="{{ $inmueble->parqueaderos_dobles }}"
                    name="parqueaderos_dobles" placeholder="1">
                </div>
                <div class="form-group col-md-4">
                    <label for="parqueaderos_sencillos">Parqueaderos sencillos</label>
                    <input type="text" class="form-control" value="{{ $inmueble->parqueaderos_sencillos }}"
                    name="parqueaderos_sencillos" placeholder="1">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="area_lote">Área lote</label>
                    <input type="text" class="form-control" value="{{ $inmueble->area_lote }}" name="area_lote"
                    placeholder="80">
                </div>
                <div class="form-group col-md-6">
                    <label for="area_cons">Área construida</label>
                    <input type="text" class="form-control" value="{{ $inmueble->area_construida }}"
                    name="area_cons" placeholder="60">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="area_privada">Área privada</label>
                    <input type="text" class="form-control" value="{{ $inmueble->area_privada }}"
                    name="area_privada" placeholder="55">
                </div>
                <div class="form-group col-md-4">
                    <label for="frente">Frente</label>
                    <input type="text" class="form-control" value="{{ $inmueble->frente }}" name="frente"
                    placeholder="7">
                </div>
                <div class="form-group col-md-4">
                    <label for="fondo">Fondo</label>
                    <input type="text" class="form-control" value="{{ $inmueble->fondo }}" name="fondo"
                    placeholder="8">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="construccion">Año contrucción</label>
                    <input type="text" class="form-control" value="{{ $inmueble->anio_construccion }}"
                    name="construccion" placeholder="2005">
                </div>
                <div class="form-group col-md-4">
                    <label for="nivel">Nivel</label>
                    <input type="text" class="form-control" value="{{ $inmueble->nivel }}" name="nivel"
                    placeholder="3">
                </div>
                <div class="form-group col-md-4">
                    <label for="nivel">Depositos</label>
                    <input type="text" class="form-control" value="{{ $inmueble->depositos }}" name="depositos"
                    placeholder="3">
                </div>
            </div>

        </div>
    </section>

    <section class="card col-lg-6">
        <header class="card-header">
            <h2 class="card-title">Adicionales</h2>
        </header>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="video">Video</label>
                    <input type="text" class="form-control" value="{{ $inmueble->video }}" name="video"
                    placeholder="Link de youtube">
                </div>
                <div class="form-group col-md-4"
                ng-init="sucursal = '{{ $inmueble->oficina_id }}'; asesores();">
                <label for="sucursal">Sucursal</label>
                <select name="sucursal" class="form-control" ng-model="sucursal" ng-change="asesores()">
                    <option value="">Seleccione sucursal</option>

                    @foreach ($sucursales as $sucursal)
                        <option value="{{ $sucursal->id }}">
                            {{ $sucursal->nombre }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="tour_virtual">Tour virtual</label>
                <input type="text" class="form-control" value="{{ $inmueble->tour_virtual }}"
                name="tour_virtual" placeholder="Link tour">
            </div>
        </div>

        <div class="form-row">

            <div class="form-group col-md-4">

                <div class="checkbox-custom chekbox-primary">
                    <input id="aviso" value="1" type="checkbox" name="aviso"
                    {{ $inmueble->aviso_ventana ? "checked" : "" }} />
                    <label for="aviso">Aviso ventana</label>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="checkbox-custom chekbox-primary">
                    <input id="destacado" type="checkbox" name="destacado"
                    {{ $inmueble->destacado ? "checked" : "" }} />
                    <label for="destacado">Destacado</label>
                </div>
            </div>
            <div class="form-group col-md-4">
                <div class="checkbox-custom chekbox-primary">
                    <input id="exclusivo" type="checkbox" name="exclusivo"
                    {{ $inmueble->exclusivo ? "checked" : "" }} />
                    <label for="exclusivo">Exclusivo</label>
                </div>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="remodelacion">Año de remodelación, si lo esta</label>
                <input type="text" class="form-control" value="{{ $inmueble->anio_remodelacion }}"
                name="remodelacion" placeholder="2000">
            </div>
            <div class="form-group col-md-6">
                <label for="valor_renta">Valor de renta, si genera</label>
                <input type="text" class="form-control price_input"
                ng-init="valor_renta = ({{ $inmueble->valor_renta }}| currency:'':'0')"
                ng-model="valor_renta" name="valor_renta" placeholder="$2.900.000">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="comision">Comisión (%)</label>
                <input type="number" class="form-control" value="{{ $inmueble->comision }}" name="comision"
                placeholder="3">
            </div>
            <div class="form-group col-md-6">
                <label for="matricula">Número matrícula</label>
                <input type="text" class="form-control" value="{{ $inmueble->matricula }}" name="matricula"
                placeholder="#">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="chip">Chip</label>
                <input type="text" class="form-control" name="chip" value="{{ $inmueble->chip }}"
                placeholder="AAA12345678">
            </div>
            <div class="form-group col-md-6">
                <label for="cedula_catastral">Cédula catastral</label>
                <input type="text" class="form-control" name="cedula_catastral"
                value="{{ $inmueble->cedula_catastral }}" placeholder="AAA12345678">
            </div>
        </div>

        <!-- <div class="form-row">
        <div class="form-group col-md-12">
        <label for="promotor">Asesor</label>
        <select class="form-control" name="promotor">
        <option value="">Seleccionar asesor</option>

        <option ng-repeat="asesor in agents"
        ng-selected="asesor.id == {{ $inmueble->asesor_id }}" ng-value="asesor.id"
        ng-bind="asesor.nombres + ' ' + asesor.apellidos"></option>
    </select>
</div>

</div> -->
</div>
</section>

</div>
<br>
<div class="row">
    <section class="card col-lg-12">
        <header class="card-header">
            <h2 class="card-title">Información parqueaderos y depositos</h2>
        </header>
        <div class="card-body">

            <div class="form-row">
                <b class="col-md-12">Parqueadero 1</b>
                <div class="form-group col-md-3">
                    <label for="parq1_numero"># PQ1</label>
                    <input type="text" class="form-control" value="{{ $inmueble->numero_parqueadero_1 }}"
                    name="parq1_numero" placeholder="#">
                </div>
                <div class="form-group col-md-3">
                    <label for="parq1_matricula"># Matrícula</label>
                    <input type="text" class="form-control" value="{{ $inmueble->matricula_parqueadero_1 }}"
                    name="parq1_matricula" placeholder="#">
                </div>
                <div class="form-group col-md-3">
                    <label for="parq1_chip"># Chip</label>
                    <input type="text" class="form-control" value="{{ $inmueble->chip_parqueadero_1 }}"
                    name="parq1_chip" placeholder="AAA12335">
                </div>
                <div class="form-group col-md-3">
                    <label for="parq1_cedula"># Cédula catastral</label>
                    <input type="text" class="form-control" value="{{ $inmueble->cedula_parqueadero_1 }}"
                    name="parq1_cedula" placeholder="AAA12335">
                </div>
            </div>
            <div class="form-row">
                <b class="col-md-12">Parqueadero 2</b>
                <div class="form-group col-md-3">
                    <label for="parq2_numero"># PQ2</label>
                    <input type="text" class="form-control" value="{{ $inmueble->numero_parqueadero_2 }}"
                    name="parq2_numero" placeholder="#">
                </div>
                <div class="form-group col-md-3">
                    <label for="parq2_matricula"># Matrícula</label>
                    <input type="text" class="form-control" value="{{ $inmueble->matricula_parqueadero_2 }}"
                    name="parq2_matricula" placeholder="#">
                </div>
                <div class="form-group col-md-3">
                    <label for="parq2_chip"># Chip</label>
                    <input type="text" class="form-control" value="{{ $inmueble->chip_parqueadero_2 }}"
                    name="parq2_chip" placeholder="AAA12345">
                </div>
                <div class="form-group col-md-3">
                    <label for="parq2_cedula"># Cédula catastral</label>
                    <input type="text" class="form-control" value="{{ $inmueble->cedula_parqueadero_2 }}"
                    name="parq2_cedula" placeholder="AAA12345">
                </div>
            </div>
            <div class="form-row">
                <b class="col-md-12">Parqueadero 3</b>
                <div class="form-group col-md-3">
                    <label for="parq3_numero"># PQ3</label>
                    <input type="text" class="form-control" value="{{ $inmueble->numero_parqueadero_3 }}"
                    name="parq3_numero" placeholder="#">
                </div>
                <div class="form-group col-md-3">
                    <label for="parq3_matricula"># Matrícula</label>
                    <input type="text" class="form-control" value="{{ $inmueble->matricula_parqueadero_3 }}"
                    name="parq3_matricula" placeholder="#">
                </div>
                <div class="form-group col-md-3">
                    <label for="parq3_chip"># Chip</label>
                    <input type="text" class="form-control" value="{{ $inmueble->chip_parqueadero_3 }}"
                    name="parq3_chip" placeholder="AAA12345">
                </div>
                <div class="form-group col-md-3">
                    <label for="parq3_cedula"># Cédula catastral</label>
                    <input type="text" class="form-control" value="{{ $inmueble->cedula_parqueadero_3 }}"
                    name="parq3_cedula" placeholder="AAA12345">
                </div>
            </div>
            <div class="form-row">
                <b class="col-md-12">Deposito 1</b>
                <div class="form-group col-md-3">
                    <label for="dpto1_numero"># DP1</label>
                    <input type="text" class="form-control" value="{{ $inmueble->numero_deposito_1 }}"
                    name="dpto1_numero" placeholder="#">
                </div>
                <div class="form-group col-md-3">
                    <label for="dpto1_matricula"># Matrícula</label>
                    <input type="text" class="form-control" value="{{ $inmueble->matricula_deposito_1 }}"
                    name="dpto1_matricula" placeholder="#">
                </div>
                <div class="form-group col-md-3">
                    <label for="dpto1_chip"># Chip</label>
                    <input type="text" class="form-control" value="{{ $inmueble->chip_deposito_1 }}"
                    name="dpto1_chip" placeholder="AAA12345">
                </div>
                <div class="form-group col-md-3">
                    <label for="dpto1_cedula"># Cédula catastral</label>
                    <input type="text" class="form-control" value="{{ $inmueble->cedula_deposito_1 }}"
                    name="dpto1_cedula" placeholder="AAA12345">
                </div>
            </div>
            <div class="form-row">
                <b class="col-md-12">Deposito 2</b>
                <div class="form-group col-md-3">
                    <label for="dpto2_numero"># DP2</label>
                    <input type="text" class="form-control" value="{{ $inmueble->numero_deposito_2 }}"
                    name="dpto2_numero" placeholder="#">
                </div>
                <div class="form-group col-md-3">
                    <label for="dpto2_matricula"># Matrícula</label>
                    <input type="text" class="form-control" value="{{ $inmueble->matricula_deposito_2 }}"
                    name="dpto2_matricula" placeholder="#">
                </div>
                <div class="form-group col-md-3">
                    <label for="dpto2_chip"># Chip</label>
                    <input type="text" class="form-control" value="{{ $inmueble->chip_deposito_2 }}"
                    name="dpto2_chip" placeholder="AAA12345">
                </div>
                <div class="form-group col-md-3">
                    <label for="dpto2_cedula"># Cédula catastral</label>
                    <input type="text" class="form-control" value="{{ $inmueble->cedula_deposito_2 }}"
                    name="dpto2_cedula" placeholder="AAA12345">
                </div>
            </div>

        </div>
    </section>
</div>
<br>
<div class="row">
    <section class="card col-lg-12">
        <header class="card-header">
            <h2 class="card-title">Descripciones</h2>
        </header>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="descripcion">Descripción <%descripcion.length%>/1000</label>
                    <textarea name="descripcion" rows="5" class="form-control" maxlength="1000"
                    ng-init="descripcion = '{{ $inmueble->descripcion }}'" ng-model="descripcion"
                    placeholder="Descripción principal" required></textarea>
                </div>
            </div>
        </div>
    </section>
</div>
<br>

<div class="row" ng-init="setAlreatySelectedAmenities('{{ json_encode($caracteristicas) }}')">
    <section class="card col-lg-12">
        <header class="card-header">
            <h2 class="card-title">Características interiores</h2>
        </header>
        <div class="card-body">
            <div class="form-row">

                <div class="form-group col-md-3 sinmargenab" ng-repeat="caract in amenities"
                ng-if="caract.tipo_caracteristica_id == 1">
                <label class="btn caract col mr-2">
                    <input type="checkbox" name="caract_<% caract.id %>"
                    ng-checked="caracteriticasSeleccionadas.includes(caract.id)"><% caract.nombre %>
                </label>

            </div>

        </div>

    </div>
</section>
</div>
<br>
<div class="row">
    <section class="card col-lg-12">
        <header class="card-header">
            <h2 class="card-title">Características exteriores</h2>
        </header>
        <div class="card-body">

            <div class="form-row">

                <div class="form-group col-md-3 sinmargenab" ng-repeat="caract in amenities"
                ng-if="caract.tipo_caracteristica_id == 2">
                <label class="btn caract col mr-2">
                    <input type="checkbox" name="caract_<% caract.id %>"
                    ng-checked="caracteriticasSeleccionadas.includes(caract.id)"><% caract.nombre %>
                </label>
            </div>

        </div>

    </div>
</section>
</div>
<br>
<div class="row">
    <section class="card col-lg-12">
        <header class="card-header">
            <h2 class="card-title">Características del sector</h2>
        </header>
        <div class="card-body">

            <div class="form-row">

                <div class="form-group col-md-3 sinmargenab" ng-repeat="caract in amenities"
                ng-if="caract.tipo_caracteristica_id == 3">
                <label class="btn caract col mr-2">
                    <input type="checkbox" name="caract_<% caract.id %>"
                    ng-checked="caracteriticasSeleccionadas.includes(caract.id)"><% caract.nombre %>
                </label>
            </div>

        </div>

    </div>
</section>
</div>
<br>
<div class="row">
    <section class="card col-lg-12">
        <header class="card-header">
            <h2 class="card-title">Características generales</h2>
        </header>
        <div class="card-body">

            <div class="form-row">

                <div class="form-group col-md-3 sinmargenab" ng-repeat="caract in amenities"
                ng-if="caract.tipo_caracteristica_id == 4">
                <label class="btn caract col mr-2">
                    <input type="checkbox" name="caract_<% caract.id %>"
                    ng-checked="caracteriticasSeleccionadas.includes(caract.id)"><% caract.nombre %>
                </label>
            </div>

        </div>

    </div>
</section>
</div>

<footer class="card-footer">
    <div class="row">

        <div class="col-md-6">

            <div class="col-md-12 alert alert-info" role="alert" id="formlario-cargando">
                Cargando...
            </div>

            <div class="col-md-12 alert alert-success" role="alert" id="formlario-enviado">
                ¡El inmueble fue actualizado con éxito!
            </div>

            <div class="col-md-12 alert alert-danger" role="alert" id="formlario-error"></div>
        </div>


        <div class="col-md-6 text-right pull-right">
            <button type="submit" class="btn btn-primary">
                Guardar inmueble
            </button>
            <!--<button class="btn btn-default modal-dismiss">Cancelar</button>-->
        </div>
    </div>
</footer>
</section>
</div>
</form>
</section>
