<section role="main" class="content-body">

    <header class="page-header">
        <h2>Inmuebles</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>Inmuebles</span></li>
                <li><span>Editar documentos&nbsp;&nbsp;&nbsp;</span></li>
                <li><span>{{ $inmueble->codigo }}&nbsp;&nbsp;&nbsp;</span></li>
            </ol>
            <a class="" data-open=""></a>
        </div>
    </header>
    <button class="btn btn-primary separaraltbottom" ng-controller="FormImage" ng-click="generalfactory.previousPage()">
        <i class="fas fa-arrow-left"></i> Volver</button>
    <form action="{{ url("admin/inmuebles/editar/$inmueble->id/documentos") }}" method="post"
        enctype="multipart/form-data" ng-controller="FormDocumentos">
        {{ csrf_field() }}
        <div class="row">
            <div class="col">
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                        </div>

                        <h2 class="card-title">Documentos adjuntos a este inmueble</h2>
                    </header>
                    <div class="card-body">

                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2">
                                CLT
                            </label>
                            <div class="col-lg-6">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-append">
                                        <div class="uneditable-input">
                                            <i class="fas fa-file fileupload-exists"></i>
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-exists">Cambiar</span>
                                            <span class="fileupload-new">Seleccionar archivo</span>
                                            <input type="file" name="file_1"
                                                ng-required=" file_5 == '' &&  file_1 != 'lleno'" ng-model="file_1"
                                                select-ng-files />
                                        </span>
                                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">
                                            Eliminar
                                        </a>
                                    </div>

                                    <small>
                                        @foreach ($inmueble->documentos as $documentos)
                                        @if ($documentos->tipo_documento_id == 1)
                                        <a href="{{ $documentos->url }}" target="_blank" ng-init="file_1 = 'lleno'">
                                            {{ $documentos->comentario }} <br>
                                        </a>
                                        @endif
                                        @endforeach
                                    </small>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row hidden">
                            <label class="col-lg-3 control-label text-lg-right pt-2">
                                Cédula Propietario
                            </label>
                            <div class="col-lg-6">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-append">
                                        <div class="uneditable-input">
                                            <i class="fas fa-file fileupload-exists"></i>
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-exists">Cambiar</span>
                                            <span class="fileupload-new">Seleccionar archivo</span>
                                            <input type="file" name="file_2" ng-required="false" ng-model="file_2"
                                                select-ng-files />
                                        </span>
                                        <a href="#" class="btn btn-default fileupload-exists"
                                            data-dismiss="fileupload">Borrar</a>
                                    </div>
                                    <small>
                                        @foreach ($inmueble->documentos as $documentos)
                                        @if ($documentos->tipo_documento_id == 2)
                                        <a href="{{ $documentos->url }}" target="_blank" ng-init="file_2 = 'lleno'">
                                            {{ $documentos->comentario }} <br>
                                        </a>
                                        @endif
                                        @endforeach
                                    </small>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2">
                                Contrato firmado
                            </label>
                            <div class="col-lg-6">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-append">
                                        <div class="uneditable-input">
                                            <i class="fas fa-file fileupload-exists"></i>
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-exists">Cambiar</span>
                                            <span class="fileupload-new">Seleccionar archivo</span>
                                            <input type="file" name="file_3"
                                                ng-required="file_5 == '' &&  file_3 != 'lleno'" ng-model="file_3"
                                                select-ng-files />
                                        </span>
                                        <a href="#" class="btn btn-default fileupload-exists"
                                            data-dismiss="fileupload">Borrar</a>
                                    </div>
                                    <small>
                                        @foreach ($inmueble->documentos as $documentos)
                                        @if ($documentos->tipo_documento_id == 3)
                                        <a href="{{ $documentos->url }}" target="_blank" ng-init="file_3 = 'lleno'">
                                            {{ $documentos->comentario }} <br>
                                        </a>
                                        @endif
                                        @endforeach
                                    </small>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2">
                                Cuenta de cobro administración
                            </label>
                            <div class="col-lg-6">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-append">
                                        <div class="uneditable-input">
                                            <i class="fas fa-file fileupload-exists"></i>
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-exists">Cambiar</span>
                                            <span class="fileupload-new">Seleccionar archivo</span>
                                            <input type="file" name="file_4" ng-model="file_4" select-ng-files />
                                        </span>
                                        <a href="#" class="btn btn-default fileupload-exists"
                                            data-dismiss="fileupload">Borrar</a>
                                    </div>
                                    <small>
                                        @foreach ($inmueble->documentos as $documentos)
                                        @if ($documentos->tipo_documento_id == 4)
                                        <a href="{{ $documentos->url }}" target="_blank" ng-init="file_4 = 'lleno'">
                                            {{ $documentos->comentario }} <br>
                                        </a>
                                        @endif
                                        @endforeach
                                    </small>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2">
                                Docusign
                            </label>
                            <div class="col-lg-6">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-append">
                                        <div class="uneditable-input">
                                            <i class="fas fa-file fileupload-exists"></i>
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-exists">Cambiar </span>
                                            <span class="fileupload-new">Seleccionar archivo</span>
                                            <input type="file" name="file_5"
                                                ng-required="(file_1 == '' && file_2 == '') &&  file_5 != 'lleno'"
                                                ng-model="file_5" select-ng-files />
                                        </span>
                                        <a href="#" class="btn btn-default fileupload-exists"
                                            data-dismiss="fileupload">Borrar</a>
                                    </div>
                                    @if(isset($docs[5]))
                                    <small>
                                        <a href="{{ $docs[5] }}" target="_blank" ng-init="file_5 = 'lleno'">
                                            Docusign <br>
                                        </a>
                                    </small>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <h4 class="col-sm-12">Documentos adicionales (opcionales)</h4>
                            @foreach($tipos_doc as $doc)
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="col-lg-12 control-label pt-2">
                                        {{$doc->nombre}}
                                    </label>
                                    <div class="col-lg-12">
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="input-append">
                                                <div class="uneditable-input">
                                                    <i class="fas fa-file fileupload-exists"></i>
                                                    <span class="fileupload-preview"></span>
                                                </div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileupload-exists">Cambiar</span>
                                                    <span class="fileupload-new">Seleccionar archivo</span>
                                                    <input type="file" name="file_{{$doc->id}}"
                                                        ng-model="file_{{$doc->id}}" select-ng-files />
                                                </span>
                                                <a href="#" class="btn btn-default fileupload-exists"
                                                    data-dismiss="fileupload">Borrar</a>
                                            </div>
                                            @if(isset($docs[$doc->id]))
                                            <small>
                                                <a href="{{ $docs[$doc->id] }}" target="_blank"
                                                    ng-init="file_5 = 'lleno'">
                                                    {{$doc->nombre}} <br>
                                                </a>
                                            </small>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <div class="form-group col-md-12">
                                <label for="comentarios_documentos">Comentarios adicionales documentos</label>
                                <textarea name="comentarios_documentos" rows="5" class="form-control"
                                    placeholder="Comentarios adicionales">{{$inmueble->comentarios_documentos}}</textarea>
                            </div>
                        </div>
                        <footer class="card-footer">
                            <div class="row justify-content-end">
                                <div class="col-sm-12">
                                    <div class="icons_docs col-sm-5">
                                        <a href="https://wetransfer.com/" target="_blank" data-toggle="tooltip"
                                            data-placement="top"
                                            data-original-title="Enviar archivos pesados por correo">
                                            <img src="{{ asset("assets/img/we-trasnfer.png") }}" alt="">
                                        </a>
                                    </div>
                                    <div class="icons_docs col-sm-5">
                                        <a href="https://www.ilovepdf.com/es" target="_blank" data-toggle="tooltip"
                                            data-placement="top"
                                            data-original-title="Herramienta para pdfs como, unir, dividir, comprimir, etc...">
                                            <img src="{{ asset("assets/img/ilovepdf.svg") }}" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <button type="submit" class="btn btn-primary">
                                        Guardar
                                    </button>
                                    @if($inmueble->estado_id==1|| $inmueble->estado_id == 3|| $inmueble->estado_id ==4)
                                    o
                                    <button type="submit" class="btn btn-primary" name="revision" value="1">
                                        Enviar a revisión
                                    </button>
                                    @endif
                                    @if($inmueble->publicado == 1)
                                    o
                                    <button type="submit" class="btn btn-primary" name="revision" value="2">
                                        Actualizar en Domus
                                    </button>
                                    @endif
                                </div>
                            </div>
                        </footer>
                    </div>
                </section>
            </div>
        </div>
    </form>

</section>