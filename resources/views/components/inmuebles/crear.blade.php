<section role="main" class="content-body">
    <form ng-controller="Formulario" id="formulario-crear-inmueble" ng-init="valRequired = false"
    ng-submit="valRequired = true;submitData('{{ url("admin/inmuebles/crear") }}')" ng-cloak>

    {{ csrf_field() }}

    <header class="page-header">
        <h2>Inmuebles</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>Inmuebles</span></li>
                <li><span>Crear&nbsp;&nbsp;&nbsp;</span></li>
            </ol>
            <a class="" data-open=""></a>
        </div>
    </header>

    {{-- Validación de permisos para crear inmueble --}}
    @php($crearInmueble = App\Http\Controllers\GeneralController::getPermission($usuario, 1))

    @if ($crearInmueble == 1)

        {{-- Parte inicial --}}
        <div class="row">

            <section class="card col-lg-6">
                <header class="card-header">
                    <h2 class="card-title">Crear inmueble</h2>
                </header>
                <div class="card-body">

                    <div class="form-row">


                        <div class="form-group col-md-12">
                            <label for="referencia">MLS</label>
                            <input type="text" class="form-control" value="" id="referencia" name="referencia"
                            placeholder="Se genera al aprobarse" readonly>
                        </div>
                    </div>

                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="tipo">Tipo * <small ng-if="valRequired && tipo_inmueble == ''"
                                ng-class="{'danger-text':(valRequired && tipo_inmueble == '')}">obligatorio</small></label>
                                <select id="tipo" ng-class="{'danger-input':(valRequired && tipo_inmueble == '')}"
                                class="form-control" name="tipo" ng-model="tipo_inmueble"
                                ng-change="mostrarCaracteristicas()" required>
                                <option value="">Seleccione tipo</option>
                                @foreach ($tipos as $tipo)
                                    <option value="{{ $tipo->id }}">
                                        {{ $tipo->nombre }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="gestion">Gestion * <small ng-if="valRequired && gestion == ''"
                                ng-class="{'danger-text':(valRequired && gestion == '')}">obligatorio</small></label>
                                <select id="gestion" name="gestion"
                                ng-class="{'danger-input':(valRequired && gestion == '')}" class="form-control"
                                ng-model="gestion" required>
                                <option value="">Seleccione gestión</option>
                                @foreach ($gestiones as $gestion)
                                    <option value="{{ $gestion->id }}">
                                        {{ $gestion->nombre }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="destino">Destinacion <small ng-if="valRequired && !destino"
                                ng-class="{'danger-text':(valRequired && !destino)}">obligatorio</small></label>
                                <select id="destino" name="destino" ng-class="{'danger-input':(valRequired && !destino)}"
                                class="form-control" ng-model="destino" required>
                                <option value="">Seleccione destinación</option>
                                @foreach ($destinos as $destino)
                                    <option value="{{ $destino->id }}">
                                        {{ $destino->nombre }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="form-row" ng-show="gestion == '1'" ng-init="corretaje = '0'" ng-cloak>


                        <div class="form-group col-md-12" ng-model="corretaje">
                            <label for="corretaje">Corretaje</label>
                            <select id="corretaje" name="corretaje" class="form-control" ng-model="corretaje">
                                <option value="0">No</option>
                                <option value="1">Si</option>
                            </select>
                        </div>


                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="canon">Canon</label>
                            <input type="text" class="form-control price_input" value="" ng-model="arriendo"
                            ng-change="setTotal()" name="canon" placeholder="Canon"
                            ng-required="gestion == 1 || gestion == 3">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="administracion">Administración con descuento</label>
                            <input type="text" class="form-control price_input" value="" ng-model="administracion"
                            ng-change="setTotal()" name="administracion" placeholder="Administración">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="total">Total</label>
                            <input type="text" class="form-control" value="" ng-model="total" name="total"
                            placeholder="total" readonly>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="venta">Venta</label>
                            <input type="text" class="form-control price_input" value="" ng-model="venta"
                            ng-change="setTotal()" name="venta" placeholder="Venta"
                            ng-required="gestion == 2 || gestion == 3">
                        </div>

                        <div class="form-group col-md-6">
                            <label>Fecha consignación</label>
                            <div>
                                {{-- <div data-plugin-datepicker data-plugin-skin="primary">
                            </div> --}}
                            <input type="date" class="form-control" name="fecha_consignacion"
                            value="{{ date("Y-m-d") }}">
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section class="card col-lg-6">
            <header class="card-header">
                <h2 class="card-title">Ubicación</h2>
            </header>
            <div class="card-body">

                <div class="form-row">

                    <div class="form-group col-md-6">
                        <label for="departamento">Departamento <small ng-if="valRequired && departamento == ''"
                            ng-class="{'danger-text':(valRequired && departamento == '')}">obligatorio</small></label>
                            <select id="departamentos" ng-class="{'danger-input':(valRequired && departamento == '')}"
                            class="form-control" name="departamento" ng-model="departamento" ng-change="ciudades()"
                            required>
                            <option value="">Seleccione departamento</option>
                            @foreach ($departamentos as $departamento)
                                <option value="{{ $departamento->id }}">
                                    {{ $departamento->nombre }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="ciudad">Ciudad <small ng-if="valRequired && ciudad == ''"
                            ng-class="{'danger-text':(valRequired && ciudad == '')}">obligatorio</small></label>
                            <select name="ciudad" ng-class="{'danger-input':(valRequired && ciudad == '')}"
                            class="form-control" ng-model="ciudad" ng-change="localidades()" id="ciudad" required>
                            <option value="">Seleccione una ciudad</option>

                            <option ng-repeat="ciudad in cities" ng-value="ciudad.id" ng-bind="ciudad.nombre">
                            </option>
                        </select>
                    </div>

                </div>

                <div class="form-row">

                    <div class="form-group col-md-4">
                        <label for="zona">Zona <small ng-if="valRequired && zona == ''"
                            ng-class="{'danger-text':(valRequired && zona == '')}">obligatorio</small></label>
                            <select ng-class="{'danger-input':(valRequired && zona == '')}" class="form-control"
                            name="zona" ng-model="zona" required>
                            <option value="">Seleccione zona</option>
                            @foreach ($zonas as $zona)
                                <option value="{{ $zona->id }}">
                                    {{ $zona->nombre }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="localidad">Localidad <small ng-if="valRequired && localidad == ''"
                            ng-class="{'danger-text':(valRequired && localidad == '')}">obligatorio</small></label>
                            <select name="localidad" ng-class="{'danger-input':(valRequired && localidad == '')}"
                            class="form-control" ng-model="localidad" ng-change="barrios()" required>
                            <option value="">Seleccione localidad</option>

                            <option ng-repeat="localidad in localities" ng-value="localidad.id"
                            ng-bind="localidad.nombre"></option>
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="estrato">Estrato <small ng-if="valRequired && !estrato"
                            ng-class="{'danger-text':(valRequired && !estrato)}">obligatorio</small></label>
                            <select name="estrato" ng-class="{'danger-input':(valRequired && !estrato)}"
                            class="form-control" ng-model="estrato" required>

                            <option value="">Seleccione estrato</option>
                            @foreach ($estratos as $estrato)
                                <option value="{{ $estrato->id }}">
                                    {{ $estrato->nombre }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="barrio_id">Barrio <small ng-if="valRequired && !barrio_id"
                            ng-class="{'danger-text':(valRequired && !barrio_id)}">obligatorio</small></label>
                            <select ng-class="{'danger-input':(valRequired && !barrio_id)}" class="form-control"
                            name="barrio_id" ng-model="barrio_id" id="barrio_id" ng-change="selectBarrio()"
                            required>
                            <option value="">Seleccione barrio</option>

                            <option ng-repeat="barrio in neighborhoods" ng-value="barrio.id"
                            ng-bind="barrio.nombre"></option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="barrio_comun">Nombre común barrio </label>
                        <input type="text" class="form-control" value="" ng-value="barrioName" id="barrio"
                        name="barrio_comun" placeholder="nombre común" required>
                    </div>
                </div>
            </div>
        </section>
    </div>
    {{-- Final de ubicación --}}
    <br>
    <div class="row">
        <section class="card col-lg-12">
            <header class="card-header">
                <h2 class="card-title">Dirección</h2>
            </header>
            <div class="card-body">

                <input type="hidden" name="latitude" id="latitude" ng-value="latitude">
                <input type="hidden" name="longitude" id="longitude" ng-value="longitude">

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="isCompleteAddress">
                            <input type="checkbox" name="isCompleteAddress" ng-model="isCompleteAddress" ng-checked="isCompleteAddress">
                            Dirección personalizada
                        </label>
                    </div>
                </div>

                <div class="form-row" ng-show="isCompleteAddress">
                    <div class="form-group col-md-12">
                        <input type="text" name="complete_address" ng-model="direccion" class="form-control" placeholder="Dirección completa">
                    </div>
                </div>

                <div class="form-row" ng-show="!isCompleteAddress">

                    <div class="form-group col-md-2">
                        <label for="dir1">Cll/Cra</label>
                        <select class="form-control" name="dir1" id="dir1">
                            @foreach ($dir1 as $dir)
                                <option value="{{ $dir->id }}">
                                    {{ $dir->nombre }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-1">
                        <label ng-class="{'danger-text':(valRequired && dir2 == '')}" for="dir2">Nombre</label>

                        <input type="text" ng-class="{'danger-input':(valRequired && dir2 == '')}"
                        class="form-control" value="" ng-change="armarDireccion()" ng-model="dir2" name="dir2"
                        placeholder="5">
                    </div>

                    <div class="form-group col-md-1">
                        <label for="dir3">Letra</label>
                        <select class="form-control" name="dir3" id="dir3">
                            @foreach ($dir2 as $dir)
                                <option value="{{ $dir->id }}">
                                    {{ $dir->nombre }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-1">
                        <label for="dir4">BIS</label>
                        <select class="form-control" name="dir4" id="dir4">
                            @foreach ($dir3 as $dir)
                                <option value="{{ $dir->id }}">
                                    {{ $dir->nombre }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-1">
                        <label for="dir5">Letra</label>
                        <select class="form-control" name="dir5" id="dir5">
                            @foreach ($dir2 as $dir)
                                <option value="{{ $dir->id }}">
                                    {{ $dir->nombre }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-1">
                        <label for="dir6">Zona</label>
                        <select class="form-control" name="dir6" id="dir6">
                            @foreach ($dir4 as $dir)
                                <option value="{{ $dir->id }}">
                                    {{ $dir->nombre }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-2">
                        <label ng-class="{'danger-text':(valRequired && dir7 == '')}" for="dir7">#</label>

                        <input type="number" ng-class="{'danger-input':(valRequired && dir7 == '')}"
                        class="form-control" value="" ng-change="armarDireccion()" ng-model="dir7" name="dir7"
                        placeholder="24">
                    </div>
                    <div class="form-group col-md-1">
                        <label for="dir8">Letra</label>
                        <select class="form-control" name="dir8" id="dir8">
                            @foreach ($dir2 as $dir)
                                <option value="{{ $dir->id }}">
                                    {{ $dir->nombre }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-1">
                        <label ng-class="{'danger-text':(valRequired && dir9 == '')}" for="dir9">Número</label>

                        <input type="number" ng-class="{'danger-input':(valRequired && dir9 == '')}"
                        class="form-control" ng-change="armarDireccion()" ng-model="dir9" name="dir9"
                        placeholder="60">
                    </div>

                    <div class="form-group col-md-1">
                        <label for="dir10">Zona</label>
                        <select class="form-control" name="dir10" id="dir10">
                            @foreach ($dir4 as $dir)
                                <option value="{{ $dir->id }}">
                                    {{ $dir->nombre }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-row" ng-show="!isCompleteAddress">
                    <div class="form-group col-md-4">
                        <label for="complemento">Nombre del conjunto</label>
                        <input type="text" class="form-control" value="" ng-change="armarDireccion()"
                        ng-model="complemento" name="complemento" placeholder="">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="complemento2">CASA, APTO, BODEGA, OFICINA</label>
                        <input type="text" class="form-control" value="" ng-change="armarDireccion()"
                        ng-model="complemento2" name="complemento2" placeholder="">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="complemento3">BLOQUE, LOTE, MANZANA. TORRE</label>
                        <input type="text" class="form-control" value="" ng-change="armarDireccion()"
                        ng-model="complemento3" name="complemento3" placeholder="">
                    </div>

                    <div class="form-group col-md-6">
                        <label>Dirección (Resultado final)</label>
                        <input type="text" class="form-control" ng-model="direccion" name="direccion"
                        placeholder="Dirección" readonly required>
                    </div>

                    <div class="form-group col-md-6">
                        <label>Dirección (Resultado final)</label>
                        <input type="text" class="form-control" ng-model="direccion" name="direccion"
                        placeholder="Dirección" readonly required>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-9">
                        <label for="buscardireccion">Buscar ubicación en el mapa</label>
                        <input type="text" class="form-control" value="" name="buscardireccion"
                        placeholder="Ciudad, Barrio, dirección" ng-model="campodireccion">
                    </div>
                    <div class="form-group col-md-3">
                        <label>Presionar para actualizar</label>
                        <a href="#" role="button" class="btn btn-primary"
                        ng-click="buscardireccion($event);buscardireccion($event)">Buscar</a>
                    </div>
                </div>

                <div class="form-row">
                    <div id="mapa"></div>
                </div>
            </div>
        </section>
    </div>
    <br>
    <div class="row">
        <section class="card col-lg-6">
            <header class="card-header">
                <h2 class="card-title">Comodidades</h2>
            </header>
            <div class="card-body">

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="habitaciones">Habitaciones</label>
                        <input type="text" class="form-control" value="" name="habitaciones" placeholder="3">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="banios">Baños</label>
                        <input type="text" class="form-control" value="" name="banios" placeholder="2">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="parqueaderos">Parqueaderos totales</label>
                        <input type="text" class="form-control" value="" name="parqueaderos" placeholder="2">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="parqueaderos_cubiertos">Parqueaderos cubiertos</label>
                        <input type="text" class="form-control" value="" name="parqueaderos_cubiertos"
                        placeholder="1">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="parqueaderos_descubiertos">Parqueaderos descubiertos</label>
                        <input type="text" class="form-control" value="" name="parqueaderos_descubiertos"
                        placeholder="1">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="parqueaderos_dobles">Parqueaderos dobles</label>
                        <input type="text" class="form-control" value="" name="parqueaderos_dobles" placeholder="1">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="parqueaderos_sencillos">Parqueaderos sencillos</label>
                        <input type="text" class="form-control" value="" name="parqueaderos_sencillos"
                        placeholder="1">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="area_lote">Área lote m²</label>
                        <input type="number" step="0.01" class="form-control" value="" name="area_lote"
                        placeholder="80">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="area_cons">Área construida m²</label>
                        <input type="number" step="0.01" class="form-control" value="" name="area_cons"
                        placeholder="60">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="area_privada">Área privada m²</label>
                        <input type="number" step="0.01" class="form-control" value="" name="area_privada"
                        placeholder="55">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="frente">Frente</label>
                        <input type="text" class="form-control" value="" name="frente" placeholder="7">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="fondo">Fondo</label>
                        <input type="text" class="form-control" value="" name="fondo" placeholder="8">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="construccion">Año contrucción</label>
                        <input type="text" class="form-control" value="" name="construccion" placeholder="2005">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="nivel">Nivel</label>
                        <input type="text" class="form-control" value="" name="nivel" placeholder="3">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="nivel">Depositos</label>
                        <input type="text" class="form-control" value="" name="depositos" placeholder="3">
                    </div>
                </div>

            </div>
        </section>

        <section class="card col-lg-6">
            <header class="card-header">
                <h2 class="card-title">Adicionales</h2>
            </header>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="video">Video</label>
                        <input type="text" class="form-control" value="" name="video" placeholder="Link de youtube">
                    </div>
                    <div class="form-group col-md-4" ng-init="sucursal = '{{ $usuario->oficina_id }}'; asesores()">
                        <label for="sucursal">Sucursal</label>
                        <select name="sucursal" class="form-control" ng-model="sucursal" ng-change="asesores()"
                        disabled>
                        <option value="">Seleccione sucursal</option>

                        @foreach ($sucursales as $sucursal)
                            <option value="{{ $sucursal->id }}">
                                {{ $sucursal->nombre }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="tour_virtual">Tour virtual</label>
                    <input type="text" class="form-control" value="" name="tour_virtual"
                    placeholder="Link tour">
                </div>
            </div>

            <div class="form-row">

                <div class="form-group col-md-4">

                    <div class="checkbox-custom chekbox-primary">
                        <input id="aviso" value="1" type="checkbox" name="aviso" />
                        <label for="aviso">Aviso ventana</label>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <div class="checkbox-custom chekbox-primary">
                        <input id="destacado" type="checkbox" name="destacado" />
                        <label for="destacado">Destacado</label>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <div class="checkbox-custom chekbox-primary">
                        <input id="exclusivo" type="checkbox" name="exclusivo" />
                        <label for="exclusivo">Exclusivo</label>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="remodelacion">Año de remodelación, si lo esta</label>
                    <input type="text" class="form-control" value="" name="remodelacion" placeholder="2000">
                </div>
                <div class="form-group col-md-6">
                    <label for="valor_renta">Valor de renta, si genera</label>
                    <input type="text" class="form-control price_input" value="" name="valor_renta"
                    placeholder="$2.900.000">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="comision">Comisión (%)</label>
                    <input type="number" class="form-control" value="" name="comision" placeholder="3">
                </div>
                <div class="form-group col-md-6">
                    <label for="matricula">Número matrícula</label>
                    <input type="text" class="form-control" value="" name="matricula" placeholder="#">
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="chip">Chip</label>
                    <input type="text" class="form-control" value="" name="chip" placeholder="AAA12345678">
                </div>
                <div class="form-group col-md-6">
                    <label for="cedula_catastral">Cédula catastral</label>
                    <input type="text" class="form-control" value="" name="cedula_catastral"
                    placeholder="AAA12345678">
                </div>
            </div>


        </div>
    </section>

</div>
<br>
<div class="row">
    <section class="card col-lg-12">
        <header class="card-header">
            <h2 class="card-title">Información parqueaderos y depositos</h2>
        </header>
        <div class="card-body">

            <div class="form-row">
                <b class="col-md-12">Parqueadero 1</b>
                <div class="form-group col-md-3">
                    <label for="parq1_numero"># PQ1</label>
                    <input type="text" class="form-control" name="parq1_numero" placeholder="#">
                </div>
                <div class="form-group col-md-3">
                    <label for="parq1_matricula">Número matrícula</label>
                    <input type="text" class="form-control" value="" name="parq1_matricula" placeholder="#">
                </div>
                <div class="form-group col-md-3">
                    <label for="parq1_chip">Chip</label>
                    <input type="text" class="form-control" value="" name="parq1_chip" placeholder="AAA12345">
                </div>
                <div class="form-group col-md-3">
                    <label for="parq1_cedula">Cédula catastral</label>
                    <input type="text" class="form-control" value="" name="parq1_cedula" placeholder="AAA12345">
                </div>
            </div>
            <div class="form-row">
                <b class="col-md-12">Parqueadero 2</b>
                <div class="form-group col-md-3">
                    <label for="parq2_numero"># PQ2</label>
                    <input type="text" class="form-control" name="parq2_numero" placeholder="#">
                </div>
                <div class="form-group col-md-3">
                    <label for="parq2_matricula">Número matrícula</label>
                    <input type="text" class="form-control" value="" name="parq2_matricula" placeholder="#">
                </div>
                <div class="form-group col-md-3">
                    <label for="parq2_chip">Chip</label>
                    <input type="text" class="form-control" value="" name="parq2_chip" placeholder="AAA12345">
                </div>
                <div class="form-group col-md-3">
                    <label for="parq2_cedula">Cédula catastral</label>
                    <input type="text" class="form-control" value="" name="parq2_cedula" placeholder="AAA12345">
                </div>
            </div>
            <div class="form-row">
                <b class="col-md-12">Parqueadero 3</b>
                <div class="form-group col-md-3">
                    <label for="parq3_numero"># PQ3</label>
                    <input type="text" class="form-control" name="parq3_numero" placeholder="#">
                </div>
                <div class="form-group col-md-3">
                    <label for="parq3_matricula">Número matrícula</label>
                    <input type="text" class="form-control" value="" name="parq3_matricula" placeholder="#">
                </div>
                <div class="form-group col-md-3">
                    <label for="parq3_chip">Chip</label>
                    <input type="text" class="form-control" value="" name="parq3_chip" placeholder="AAA12345">
                </div>
                <div class="form-group col-md-3">
                    <label for="parq3_cedula">Cédula catastral</label>
                    <input type="text" class="form-control" value="" name="parq3_cedula" placeholder="AAA12345">
                </div>
            </div>
            <div class="form-row">
                <b class="col-md-12">Deposito 1</b>
                <div class="form-group col-md-3">
                    <label for="dpto1_numero"># DP1</label>
                    <input type="text" class="form-control" name="dpto1_numero" placeholder="#">
                </div>
                <div class="form-group col-md-3">
                    <label for="dpto1_matricula">Número matrícula</label>
                    <input type="text" class="form-control" value="" name="dpto1_matricula" placeholder="#">
                </div>
                <div class="form-group col-md-3">
                    <label for="dpto1_chip">Chip</label>
                    <input type="text" class="form-control" value="" name="dpto1_chip" placeholder="AAA12345">
                </div>
                <div class="form-group col-md-3">
                    <label for="dpto1_cedula">Cédula catastral</label>
                    <input type="text" class="form-control" value="" name="dpto1_cedula" placeholder="AAA12345">
                </div>
            </div>
            <div class="form-row">
                <b class="col-md-12">Deposito 2</b>
                <div class="form-group col-md-3">
                    <label for="dpto2_numero"># DP2</label>
                    <input type="text" class="form-control" name="dpto2_numero" placeholder="#">
                </div>
                <div class="form-group col-md-3">
                    <label for="dpto2_matricula">Número matrícula</label>
                    <input type="text" class="form-control" value="" name="dpto2_matricula" placeholder="#">
                </div>
                <div class="form-group col-md-3">
                    <label for="dpto2_chip">Chip</label>
                    <input type="text" class="form-control" value="" name="dpto2_chip" placeholder="AAA12345">
                </div>
                <div class="form-group col-md-3">
                    <label for="dpto2_cedula">Cédula catastral</label>
                    <input type="text" class="form-control" value="" name="dpto2_cedula" placeholder="AAA12345">
                </div>
            </div>

        </div>
    </section>
</div>
<br>
<div class="row">
    <section class="card col-lg-12">
        <header class="card-header">
            <h2 class="card-title">Descripciones</h2>
        </header>
        <div class="card-body">
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="descripcion">Descripción <%descripcion.length%>/1000<small
                        ng-if="valRequired && !descripcion"
                        ng-class="{'danger-text':(valRequired && !descripcion)}">
                        obligatorio</small>
                    </label>
                    <textarea name="descripcion" rows="5" maxlength="1000"
                    ng-class="{'danger-input':(valRequired && !descripcion)}" class="form-control"
                    ng-model="descripcion" placeholder="Descripción principal" required></textarea>
                </div>
            </div>
        </section>
    </div>
    <br>
    <div class="row">
        <section class="card col-lg-12">
            <header class="card-header">
                <h2 class="card-title">Características interiores</h2>
            </header>
            <div class="card-body">

                <div class="form-row">

                    <div class="form-group col-md-4 sinmargenab" ng-repeat="caract in amenities"
                    ng-if="caract.tipo_caracteristica_id == 1">
                    <label class="btn caract col mr-2">
                        <input type="checkbox" name="caract_<% caract.id %>"
                        ng-checked="caracteriticasSeleccionadas.includes(caract.id)"><% caract.nombre %>
                    </label>
                </div>

            </div>

        </div>
    </section>
</div>
<br>
<div class="row">
    <section class="card col-lg-12">
        <header class="card-header">
            <h2 class="card-title">Características exteriores</h2>
        </header>
        <div class="card-body">

            <div class="form-row">

                <div class="form-group col-md-4 sinmargenab" ng-repeat="caract in amenities"
                ng-if="caract.tipo_caracteristica_id == 2">
                <label class="btn caract col mr-2">
                    <input type="checkbox" name="caract_<% caract.id %>"
                    ng-checked="caracteriticasSeleccionadas.includes(caract.id)"><% caract.nombre %>
                </label>
            </div>

        </div>

    </div>
</section>
</div>
<br>
<div class="row">
    <section class="card col-lg-12">
        <header class="card-header">
            <h2 class="card-title">Características del sector</h2>
        </header>
        <div class="card-body">

            <div class="form-row">

                <div class="form-group col-md-4 sinmargenab" ng-repeat="caract in amenities"
                ng-if="caract.tipo_caracteristica_id == 3">
                <label class="btn caract col mr-2">
                    <input type="checkbox" name="caract_<% caract.id %>"
                    ng-checked="caracteriticasSeleccionadas.includes(caract.id)"><% caract.nombre %>
                </label>
            </div>

        </div>

    </div>
</section>
</div>
<br>
<div class="row">
    <section class="card col-lg-12">
        <header class="card-header">
            <h2 class="card-title">Características generales</h2>
        </header>
        <div class="card-body">

            <div class="form-row">

                <div class="form-group col-md-4 sinmargenab" ng-repeat="caract in amenities"
                ng-if="caract.tipo_caracteristica_id == 4">
                <label class="btn caract col mr-2">
                    <input type="checkbox" name="caract_<% caract.id %>"
                    ng-checked="caracteriticasSeleccionadas.includes(caract.id)"><% caract.nombre %>
                </label>
            </div>

        </div>

    </div>
</section>
</div>

<footer class="card-footer">
    <div class="row">

        <div class="col-md-6">

            <div class="col-md-12 alert alert-info" role="alert" id="formlario-cargando">
                Cargando...
            </div>

            <div class="col-md-12 alert alert-success" role="alert" id="formlario-enviado">
                ¡El inmueble fue creado con éxito!
            </div>

            <div class="col-md-12 alert alert-danger" role="alert" id="formlario-error"></div>
        </div>


        <div class="col-md-6 text-right pull-right">
            <button type="submit" class="btn btn-primary" ng-click="valRequired = true;">
                Continuar creando inmueble
            </button>

            <!--<button class="btn btn-default modal-dismiss">Cancelar</button>-->
        </div>
    </div>
</footer>

@else

    <div class="row">

        <section class="card col-lg-6">
            <header class="card-header">
                <h2 class="card-title">No tiene permiso para crear inmuebles</h2>
            </header>
        </section>
    </div>

@endif

</section>
</div>
</form>
</section>
