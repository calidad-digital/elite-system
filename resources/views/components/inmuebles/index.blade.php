<section role="main" class="content-body">
    <header class="page-header">
        <h2>Inmuebles</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>Inmuebles</span></li>
                <li><span>Consultar&nbsp;&nbsp;&nbsp;</span></li>
            </ol>
            <a class="" data-open=""></a>
        </div>


    </header>
    {{-- Validación de permisos para editar imágenes --}}

    @php
    $verInmuebles = App\Http\Controllers\GeneralController::getPermission($agente, 4);
    @endphp

    {{-- Mensajes --}}
    @if (session('mensaje'))
        <div class="row alert alert-success">
            <strong>Éxito!</strong> {{ session('mensaje') }}
        </div>
    @endif


    <div class="row" ng-controller="Inmuebles">
        <div class="col-lg-12">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Inmuebles</h2>
                </header>
                <div class="card-body">

                    <form action="" method="get" ng-controller="Formulario">
                        <div class="row">
                            <div class="form-group col-md-3"
                            ng-init="departamento = '{{ isset($request['departamento']) ? $request['departamento'] : '' }}'; ciudades();">
                            <label for="departamento" class="sr-only">Departamento</label>
                            <select id="departamentos" class="form-control" name="departamento"
                            ng-model="departamento" ng-change="ciudades()">
                            <option value="">Departamento</option>
                            @foreach ($departamentos as $departamento)
                                <option value="{{ $departamento->id }}">
                                    {{ $departamento->nombre }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-3"
                    ng-init="ciudad = {{ isset($request['ciudad']) ? $request['ciudad'] : '""' }}">
                    <label for="ciudad" class="sr-only">Ciudad</label>
                    <select name="ciudad" class="form-control" ng-model="ciudad" id="ciudad">
                        <option value="">Ciudad</option>

                        <option ng-repeat="ciudad in cities" ng-value="ciudad.id" ng-bind="ciudad.nombre">
                        </option>
                    </select>
                </div>

                <div class="form-group col-md-3"
                ng-init="gestion = '{{ isset($request['gestion']) ? $request['gestion'] : '' }}'">
                <label for="gestion" class="sr-only">Gestion</label>
                <select id="gestion" name="gestion" class="form-control" ng-model="gestion">
                    <option value="">Gestión</option>
                    @foreach ($gestiones as $gestion)
                        <option value="{{ $gestion->id }}">
                            {{ $gestion->nombre }}
                        </option>
                    @endforeach
                </select>
            </div>


            <div class="form-group col-md-3"
            ng-init="tipo_inmueble = '{{ isset($request['tipo']) ? $request['tipo'] : '' }}'">
            <label for="tipo" class="sr-only">Tipo</label>
            <select id="tipo" class="form-control" name="tipo" ng-model="tipo_inmueble">
                <option value="">Tipo</option>
                @foreach ($tipos as $tipo)
                    <option value="{{ $tipo->id }}">
                        {{ $tipo->nombre }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="col-lg-3">
            <input class="form-control mb-3" name="barrio" type="text" placeholder="Barrio"
            value="{{ isset($request['barrio']) ? $request['barrio'] : '' }}">
        </div>
        <div class="col-lg-3">
            <input class="form-control mb-3" name="codigo" type="text" placeholder="Código"
            value="{{ isset($request['codigo']) ? $request['codigo'] : '' }}">
        </div>

        <div class="form-group col-md-3"
        ng-init="estado_inmueble = '{{ isset($request['estado']) ? $request['estado'] : '' }}'">
        <label for="estado" class="sr-only">Estado</label>
        <select id="estado" class="form-control" name="estado" ng-model="estado_inmueble">
            <option value="0">Todos los estados</option>
            @foreach ($estados as $estado)
                <option value="{{ $estado->id }}">
                    {{ $estado->nombre }}
                </option>
            @endforeach
        </select>
    </div>

    <div class="form-group col-md-3"
    ng-init="sub_estado_inmueble = '{{ isset($request['subestado']) ? $request['subestado'] : '' }}'">
    <label for="subestado" class="sr-only">Subestado</label>
    <select id="subestado" class="form-control" name="subestado"
    ng-model="sub_estado_inmueble">
    <option value="">Subestado</option>
    @foreach ($subestados as $subestado)
        <option value="{{ $subestado->id }}">
            {{ $subestado->nombre }}
        </option>
    @endforeach
</select>
</div>

<div class="col-lg-3">
    <input class="form-control mb-3" name="mls" type="text" placeholder="# MLS"
    value="{{ isset($request['mls']) ? $request['mls'] : '' }}">
</div>

<div class="form-group col-md-3"
ng-init="orden_inmuebles = '{{ isset($request['orden']) ? $request['orden'] : '' }}'">
<label for="orden" class="sr-only">Orden</label>
<select id="orden" class="form-control" name="orden" ng-model="orden_inmuebles">
    <option value="">Orden</option>
    <option value="1">Nuevos primero</option>
    <option value="2">Antiguos primero</option>
    <option value="3">Seguimiento reciente</option>
    <option value="4">Seguimiento antiguo</option>
    <option value="5">Codigo</option>
    <option value="6">MLS</option>
    <option value="7">Valor arriendo</option>
    <option value="8">Valor venta</option>
</select>
</div>
<div class="form-group col-md-3"
ng-init="agente_inmuebles = '{{ isset($request['agente']) ? $request['agente'] :
    (($agente->rol_id ==1 || $agente->rol_id ==5|| $agente->rol_id ==6)?'0':  $agente->id) }}'">
    <label for="agente" class="sr-only">Agente</label>
    <select id="agente" class="form-control" name="agente" ng_model="agente_inmuebles"
    @if($verInmuebles !=1) disabled @endif>
        <option value="0">Agente</option>
        @foreach ($usuarios as $usuario)
            <option value="{{ $usuario->id }}">
                {{ $usuario->nombres }} {{ $usuario->apellidos }}
            </option>
        @endforeach
    </select>
</div>
<div class="col-lg-6">
    <button class="btn btn-primary">Buscar</button>

    <button type="button" class="btn btn-primary btn-default" data-toggle="modal"
    data-target="#mapab">
    Buscar en mapa
</button>
<a href="/admin/inmuebles" class="btn btn-primary btn-default">Limpiar</a>
</div>
</div>
</form>

Mostrando {{$inmuebles->firstItem()}} a {{$inmuebles->lastItem()}} de {{$inmuebles->total()}} inmuebles
<br>

<div class="form-row">
    <div class="card-body">
        <div class="col-md-12 alert alert-info hidden" role="alert" id="seguimiento-cargando">
            Cargando...
        </div>

        <div class="col-md-12 alert alert-success hidden" role="alert" id="seguimiento-exito">
            Inmueble actualizado exitosamente
        </div>
    </div>
</div>

<table class="table table-bordered table-striped mb-0" id="datatable-default">
    <thead>
        <tr>
            <th>Código</th>
            <th>MLS</th>
            <th>Tipo</th>
            <th>Gestion</th>
            <th>Barrio</th>
            <th>Valor</th>
            <th>Agente</th>
            <th>Estado</th>
            <th>Acción</th>
            <th class="d-lg-none">Engine version</th>
            <th class="d-lg-none">CSS grade</th>
        </tr>
    </thead>
    <tbody>

        @foreach ($inmuebles as $inmueble)
            <tr>
                <td>{{ $inmueble->codigo }}</td>
                <td>{{ $inmueble->codigoMLS }}</td>
                <td>{{ $inmueble->tipos_inmueble->nombre }}</td>
                <td>{{ $inmueble->corretaje == 1 ? 'Corretaje' : $inmueble->gestiones->nombre }}</td>
                <td>{{ $inmueble->barrio_comun }}</td>
                <td ng-cloak>
                    <% '{{ $inmueble->gestion_id == 1 ? $inmueble->canon : $inmueble->venta }}' | currency:"$ ":0 %>
                </td>
                <td>{{ $inmueble->asesores->nombres }}
                    {{ $general->getInitials($inmueble->asesores->apellidos) }}</td>
                    <td>
                        <span data-toggle="tooltip" data-placement="top"
                        data-original-title="Últ seg: {{ \Carbon\Carbon::parse($inmueble->updated_at)->diffForHumans() }} ({{ $inmueble->updated_at }})">
                        {{ $inmueble->estados->nombre }}
                    </span>
                </td>
                <td>
                    <a class="btn-default" href="{{ url("admin/inmuebles/editar/{$inmueble->id}") }}" target="_blank">
                        <i class="el el-pencil" data-toggle="tooltip" data-placement="top" data-original-title="Editar inmueble"></i>
                    </a>
                    &nbsp;
                    <a class="modal-with-form btn-default" ng-click="generalfactory.openModal({{ $inmueble }})" href="#modalForm">
                        <i class="el el-eye-open" data-toggle="tooltip" data-placement="top" data-original-title="Ver inmueble"></i>
                    </a>
                    &nbsp;

                    @if ($agente->rol_id == 2)
                        <a class="btn-default" href="#" onclick="openFollowingsSubmenu(event, 'submenu_for_{{ $inmueble->codigo }}')">
                            <i class="el el-check" data-toggle="tooltip" data-placement="top" data-original-title="Seguimientos"></i>
                        </a>

                        <div class="followings-submenu hidden" id="submenu_for_{{ $inmueble->codigo }}" ng-controller="HacerSeguimientos">
                            <ul>
                                @if ($inmueble->estado_id != 9)
                                    <li>
                                        <a href="#" ng-click="openModal({{ $inmueble->id }}, 8, 10, '¿Desea retirar el inmueble?', {{ $agente }})">
                                            Retirado
                                        </a>
                                    </li>
                                    <li>
                                        <a href="" ng-click="openModal({{ $inmueble->id }}, 9, 11, '¿Desea separar el inmueble?', {{ $agente }})">
                                            Separado
                                        </a>
                                    </li>
                                @elseif ($inmueble->estado_id == 9)
                                    <li>
                                        <a href="" ng-click="openModal({{ $inmueble->id }}, 5, 7, '¿Desea hacer disponible el inmueble?', {{ $agente }})">
                                            Disponible
                                        </a>
                                    </li>
                                    <li>
                                        <a href="" ng-click="openModal({{ $inmueble->id }}, 8, 10, '¿Desea retirar el inmueble?', {{ $agente }})">
                                            Retirado
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </div>

                        @if( $inmueble->estado_id == 9)
                            <a href="{{ url("/admin/ventas/1/{$inmueble->id}") }}">
                                <i class="el el-shopping-cart" data-toggle="tooltip" data-placement="top" data-original-title="Vender inmueble"></i>
                            </a>
                        @endif
                        &nbsp;
                        @if (($inmueble->gestion_id == 1 || $inmueble->gestion_id == 3) && $inmueble->corretaje == 0 && $inmueble->estado_id == 9)
                            <a href="{{ url("/admin/arriendos/1/{$inmueble->id}") }}">
                                <i class="el el-usd" data-toggle="tooltip" data-placement="top" data-original-title="Arrendar inmueble"></i>
                            </a>
                        @endif
                    @else
                        <a class="btn-default" href="{{ url("admin/seguimientos?code={$inmueble->codigo}") }}">
                            <i class="el el-check" data-toggle="tooltip" data-placement="top" data-original-title="Seguimientos"></i>
                        </a>

                        @if ($inmueble->estado_id == 5 || $inmueble->estado_id == 9)
                            <a href="{{ url("/admin/ventas/1/{$inmueble->id}") }}">
                                <i class="el el-shopping-cart" data-toggle="tooltip" data-placement="top" data-original-title="Vender inmueble"></i>
                            </a>
                        @endif
                        &nbsp;
                        @if (($inmueble->gestion_id == 1 || $inmueble->gestion_id == 3) && $inmueble->corretaje == 0 && ($inmueble->estado_id == 5 || $inmueble->estado_id == 9))
                            <a href="{{ url("/admin/arriendos/1/{$inmueble->id}") }}">
                                <i class="el el-usd" data-toggle="tooltip" data-placement="top" data-original-title="Arrendar inmueble"></i>
                            </a>
                        @endif

                    @endif
                    &nbsp;

                    @if ($inmueble->estado_id == 2 && $inmueble->publicado != 1 && ($agente->rol_id == 1 || $agente->rol_id == 5))
                        <a class="modal-with-form btn-default" href="#publicarInmueble_{{ $inmueble->id }}">
                            <i class="el el-arrow-up" data-toggle="tooltip" data-placement="top"
                            data-original-title="Publicar a Domus"></i>
                        </a>
                    @endif

                    @if ($inmueble->publicado == 1 && isset($inmueble->codigoMLS))
                        <!-- <a class="modal-with-form btn-default"
                        ng-click="generalfactory.modalPortales({{ $inmueble->codigo }}, {{ $inmueble->codigoMLS }})"
                        href="#portalInmueble">
                        <i class="el el-cloud-alt" data-toggle="tooltip" data-placement="top"
                        data-original-title="Actualizar en portales"></i>
                    </a> -->
                @endif
            </td>
            <td class="center d-lg-none">4</td>
            <td class="center d-lg-none">X</td>
        </tr>
    @endforeach

</tbody>

</table>

<br><br>

{{-- Paginador de inmuebles --}}
{{$inmuebles->links('pagination::bootstrap-4')}}

{{-- Final de paginador de inmuebles --}}
</div>
</section>

<br><br>
{{-- <div class="row">
<div class="col-md-12">
<form action="">
<button class="btn btn-success">
<i class="fa fa-download"></i>
Importar inmuebles desde Domus
</button>
</form>
</div>
</div> --}}

</div>
</div>

{{-- Vista inmueble --}}
{{-- @foreach ($inmuebles as $inmueble) --}}
<div ng-controller="Inmuebles" id="modalForm" class="modal-block modal-block-primary mfp-hide modal-xl">
    <section class="card">
        <form action="" method="post" enctype="multipart/form-data">
            <header class="card-header">
                <h2 class="card-title">
                    Vista del inmueble código <% generalfactory.inmuebleActual.codigo %>
                </h2>
            </header>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-4 col-xl-4 mb-4 mb-xl-0">
                        <section class="card">
                            <div class="card-body">
                                <div class="thumb-info mb-3 forcedgallery">

                                    {{-- @if (isset($inmueble->imagenes[0])) --}}
                                    <img ng-if="generalfactory.inmuebleActual.imagenes.length == 0"
                                    src="{{ asset('assets/img/flags.png') }}" class="rounded img-fluid gp"
                                    href="{{ asset('assets/img/flags.png') }}">
                                    <img ng-repeat="img in generalfactory.inmuebleActual.imagenes"
                                    ng-if="$index < 1 && img.is_360 ==0" src="<% img.url %>"
                                    class="rounded img-fluid gp" href="<% img.url %>">
                                    <img ng-repeat="img in generalfactory.inmuebleActual.imagenes"
                                    ng-if="$index >= 1 && img.is_360 ==0" src="<% img.url %>"
                                    class="rounded img-fluid gp hidden" href="<% img.url %>">
                                    {{-- @else
                                    <img src="{{ asset('assets/img/flags.png') }}"
                                    class="rounded img-fluid item">
                                @endif --}}

                                {{-- @foreach ($inmueble->imagenes as $key => $imagen)
                                @if ($key > 0 && !$imagen->is_360)
                                <div class="hidden">
                                <img src="{{ $imagen->url }}" class="item"
                                data-thumb="{{ $imagen->url }}"
                                data-src="{{ $imagen->url }}">
                            </div>
                        @endif
                    @endforeach --}}




                    <div class="thumb-info-title">
                        <span class="thumb-info-inner">
                            <% generalfactory.inmuebleActual.tipos_inmueble.nombre %>
                        </span>
                        <span class="thumb-info-type">
                            <% generalfactory.inmuebleActual.corretaje == 1 ? 'Corretaje' : generalfactory.inmuebleActual.gestiones.nombre %>
                        </span>
                    </div>
                </div>

                <div class="widget-toggle-expand mb-3">
                    <div class="widget-header">
                        <h5 class="mb-2">
                            <% generalfactory.inmuebleActual.barrio_comun %>
                        </h5>
                        <div class="widget-toggle">+</div>
                    </div>
                    <div class="widget-content-expanded">
                        <p>
                            <% generalfactory.inmuebleActual.ciudades.nombre %>, Colombia
                            <br><b>Zona:</b> <% generalfactory.inmuebleActual.zonas.nombre %>
                            <br><b>Localidad:</b>
                            <% generalfactory.inmuebleActual.localidades.nombre %>
                            <br><b>Estrato:</b>
                            <% generalfactory.inmuebleActual.estratos.nombre %>
                            <br><b>Año
                                construcción:</b><% generalfactory.inmuebleActual.anio_construccion %>
                                <br><b>Nivel:</b> <% generalfactory.inmuebleActual.nivel %>
                                <br><b>Chip:</b> <% generalfactory.inmuebleActual.chip %>
                                <br><b>Cedula C.:</b>
                                <% generalfactory.inmuebleActual.cedula_catastral %>
                            </p>
                        </div>
                    </div>

                    <hr class="dotted short">
                    <h5 class="mb-2 mt-3">Especificaciones</h5>
                    <p>
                        <br><b>Habitaciones:</b> <% generalfactory.inmuebleActual.habitaciones %>
                        <br><b>Baños:</b> <% generalfactory.inmuebleActual.banios %>
                        <br><b>Área lote:</b> <% generalfactory.inmuebleActual.area_lote %> m²
                        <br><b>Área construida:</b>
                        <% generalfactory.inmuebleActual.area_construida %>
                        m²
                        <br><b>Área privada:</b> <% generalfactory.inmuebleActual.area_privada %> m²
                        <br><b>Frente:</b> <% generalfactory.inmuebleActual.frente %> m²
                        <br><b>Fondo:</b> <% generalfactory.inmuebleActual.fondo %> m²
                        <br><b>Parq sencillos:</b>
                        <% generalfactory.inmuebleActual.parqueaderos_sencillos %>
                    </p>

                    <hr class="dotted short">
                    <h5 class="mb-2 mt-3">
                        Parqueaderos <% generalfactory.inmuebleActual.parqueaderos %>
                    </h5>
                    <p>
                        <br><b>Parq cubiertos:</b>
                        <% generalfactory.inmuebleActual.parqueaderos_cubiertos %>
                        <br><b>Parq descubiertos:</b>
                        <% generalfactory.inmuebleActual.parqueaderos_descubiertos %>
                        <br><b>Parq dobles:</b>
                        <% generalfactory.inmuebleActual.parqueaderos_dobles %>
                        <br><b>Parq sencillos:</b>
                        <% generalfactory.inmuebleActual.parqueaderos_sencillos %>
                    </p>

                    <p>
                        <br><b>Parq 1 Matricula:</b>
                        <% generalfactory.inmuebleActual.matricula_parqueadero_1 %>
                        <br><b>Parq 1 Chip:</b>
                        <% generalfactory.inmuebleActual.chip_parqueadero_1 %>
                        <br><b>Parq 1 Cedula:</b>
                        <% generalfactory.inmuebleActual.cedula_parqueadero_1 %>
                        <br><b>Parq 2 Matricula:</b>
                        <% generalfactory.inmuebleActual.matricula_parqueadero_2 %>
                        <br><b>Parq 2 Chip:</b>
                        <% generalfactory.inmuebleActual.chip_parqueadero_2 %>
                        <br><b>Parq 2 Cedula:</b>
                        <% generalfactory.inmuebleActual.cedula_parqueadero_2 %>
                        <br><b>Parq 3 Matricula:</b>
                        <% generalfactory.inmuebleActual.matricula_parqueadero_3 %>
                        <br><b>Parq 3 Chip:</b>
                        <% generalfactory.inmuebleActual.chip_parqueadero_3 %>
                        <br><b>Parq 3 Cedula:</b>
                        <% generalfactory.inmuebleActual.cedula_parqueadero_3 %>

                    </p>
                    <hr class="dotted short">
                    <h5 class="mb-2 mt-3">
                        Depositos
                    </h5>

                    <p>
                        <br><b>Deposito 1 Matricula:</b>
                        <% generalfactory.inmuebleActual.matricula_deposito_1 %>
                        <br><b>Deposito 1 Chip:</b>
                        <% generalfactory.inmuebleActual.chip_deposito_1 %>
                        <br><b>Deposito 1 Cedula:</b>
                        <% generalfactory.inmuebleActual.cedula_deposito_1 %>
                        <br><b>Deposito 2 Matricula:</b>
                        <% generalfactory.inmuebleActual.matricula_deposito_2 %>
                        <br><b>Deposito 2 Chip:</b>
                        <% generalfactory.inmuebleActual.chip_deposito_2 %>
                        <br><b>Deposito 2 Cedula:</b>
                        <% generalfactory.inmuebleActual.cedula_deposito_2 %>

                    </p>


                </div>
            </section>

        </div>
        <div class="col-lg-8 col-xl-8">

            <div class="card card-modern">
                <div class="card-header">
                    <h2 class="card-title">MLS <% generalfactory.inmuebleActual.codigoMLS %>
                    </h2>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-auto mr-xl-5 pr-xl-5 mb-4 mb-xl-0">
                            <ul class="list list-unstyled list-item-bottom-space-0">
                                <li><b>Código: </b> <% generalfactory.inmuebleActual.codigo %></li>
                                <li><b>Destinación: </b>
                                    <% generalfactory.inmuebleActual.destinos.nombre %></li>
                                    <li><b>Canon: </b>$ <% generalfactory.inmuebleActual.canon %></li>
                                    <li><b>Admón: </b>$
                                        <% generalfactory.inmuebleActual.administracion %>
                                    </li>
                                    <li><b>Venta: </b>$ <% generalfactory.inmuebleActual.venta %></li>
                                </ul>
                            </div>
                            <div class="col-xl-auto pl-xl-5">
                                <ul class="list list-unstyled list-item-bottom-space-0">
                                    <li><b>Consignado: </b>
                                        <% generalfactory.inmuebleActual.fecha_consignacion %></li>
                                        <li><b>Dirección: </b> <% generalfactory.inmuebleActual.direccion %>
                                        </li>
                                        <li><b>Complemento 1: </b>
                                            <% generalfactory.inmuebleActual.direcciones.complemento %></li>
                                            <li><b>Complemento 2: </b>
                                                <% generalfactory.inmuebleActual.direcciones.complemento2 %>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tabs">
                            <ul class="nav nav-tabs tabs-primary tabs-property-detail">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#overview" data-toggle="tab">Descripción</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#edit" data-toggle="tab">Características</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#mult" data-toggle="tab">Multimedia</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#docs" data-toggle="tab">Documentos</a>
                                </li>
                                <li ng-if="generalfactory.inmuebleActual.estado_id == 5" class="nav-item">
                                    <a class="nav-link" href="#portal" data-toggle="tab">Portales</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="portal" class="tab-pane">
                                    <div class="p-3" ng-init="generalfactory.currentUserId = {{$agente->id}}">
                                        <span ng-repeat="$portal in generalfactory.inmuebleActual.portales" ng-if="$portal.nombre_portal != 'Olx' && $portal.nombre_portal != 'Icasas'">
                                            <i class="el el-file"></i>
                                            <% $portal.nombre_portal %> - <% $portal.codigo_portal %>
                                            <a href="<% $portal.url_portal %><%($portal.nombre_portal == 'Elite' || $portal.nombre_portal == 'Elite sin datos')?('/'+generalfactory.currentUserId):''%>" target="_blank">
                                                enlace
                                            </a>
                                            <br>
                                        </span>

                                    </div>

                                </div>
                                <div id="overview" class="tab-pane active">

                                    <div class="p-3">

                                        <h4 class="mb-3">Principal</h4>
                                        <p> <% generalfactory.inmuebleActual.descripcion %></p>

                                        <h4 class="mb-3">para metrocuadrado</h4>
                                        <p> <% generalfactory.inmuebleActual.descripcion %></p>

                                        <h4 class="mb-3">MLS</h4>
                                        <p> <% generalfactory.inmuebleActual.descripcion %></p>

                                        <h4 class="mb-3">Restricciones</h4>
                                        <p> <% generalfactory.inmuebleActual.restricciones %></p>

                                        <h4 class="mb-3">Comentarios</h4>
                                        <p> <% generalfactory.inmuebleActual.comentario %></p>

                                        <h4 class="mb-3">Comentarios 2</h4>
                                        <p> <% generalfactory.inmuebleActual.comentario2 %></p>
                                    </div>

                                </div>
                                <div id="edit" class="tab-pane">

                                    <form class="p-3">
                                        <div class="form-row">
                                            <div class="form-group col-md-6 sinmargenab">
                                                <h4 class="mb-3">Interiores</h4>
                                                <ul class="simple-todo-list mt-3">
                                                    <li class="completed"
                                                    ng-repeat="$caract in generalfactory.inmuebleActual.caracteristicas"
                                                    ng-if="$caract.caracteristicas.tipo_caracteristica_id == 1">
                                                    <% $caract.caracteristicas.nombre %>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="form-group col-md-6 sinmargenab">
                                            <h4 class="mb-3">Exteriores</h4>
                                            <ul class="simple-todo-list mt-3">
                                                <li class="completed"
                                                ng-repeat="$caract in generalfactory.inmuebleActual.caracteristicas"
                                                ng-if="$caract.caracteristicas.tipo_caracteristica_id == 2">
                                                <% $caract.caracteristicas.nombre %>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <hr class="dotted tall">
                                <div class="form-row">
                                    <div class="form-group col-md-6 sinmargenab">
                                        <h4 class="mb-3">del sector</h4>
                                        <ul class="simple-todo-list mt-3">
                                            <li class="completed"
                                            ng-repeat="$caract in generalfactory.inmuebleActual.caracteristicas"
                                            ng-if="$caract.caracteristicas.tipo_caracteristica_id == 3">
                                            <% $caract.caracteristicas.nombre %>
                                        </li>
                                    </ul>
                                </div>
                                <div class="form-group col-md-6 sinmargenab">
                                    <h4 class="mb-3">generales</h4>
                                    <ul class="simple-todo-list mt-3">
                                        <li class="completed"
                                        ng-repeat="$caract in generalfactory.inmuebleActual.caracteristicas"
                                        ng-if="$caract.caracteristicas.tipo_caracteristica_id == 4">
                                        <% $caract.caracteristicas.nombre %>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </form>

                </div>
                <div id="mult" class="tab-pane">

                    <div class="p-3">
                        <h4 class="mb-3">Video</h4>
                        <p ng-if="generalfactory.inmuebleActual.video">
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/RdMjRv3bk7A"
                            title="YouTube video player" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                        </p>
                        <h4 class="mb-3">Fotos 360</h4>

                        <div id="viewer"></div>

                        <!-- php
                        $imagenes360 = [];
                        $i = 0;

                        foreach ($inmueble->imagenes as $imagen) {
                        if ($imagen->is_360) {
                        array_push($imagenes360, $imagen);
                    }
                }

                $count = count($imagenes360);
                endphp

                <input type="hidden" id="imagesLenght" value=" $count }}">

                foreach ($imagenes360 as $imagen)
                <input type="hidden" id="imagen_360_ $i }}" value=" $imagen->url }}">
                php($i++)
                endforeach -->
            </div>

        </div>
        <div id="docs" class="tab-pane">
            <div class="p-3">
                <h4 class="mb-3">Documentos</h4>
                <b>Comentarios adicionales: </b><br>
                <% generalfactory.inmuebleActual.comentarios_documentos%> <br>
                <span ng-repeat="$documento in generalfactory.inmuebleActual.documentos">
                    <i class="el el-file"></i>
                    <a href="<% $documento.url %>" target="_blank">
                        <% $documento.tipos.nombre %>
                    </a> <br>
                </span>
                <br>

            </div>
        </div>
    </div>
</div>

<h3>Propietarios:</h3>
<!-- foreach de propietarios -->


<p ng-repeat="$clientes in generalfactory.inmuebleActual.clientes">
    <br><% $clientes.clientes.nombres%>
    <% $clientes.clientes.apellidos %>
    <br><b><% $clientes.clientes.tipos_documento.nombre%>:</b>
    <% $clientes.clientes.documento%>
    <br><b>Tel:</b> <% $clientes.clientes.telefono%>
    <br><b>Cel:</b> <% $clientes.clientes.celular%>
    <br><b>Whatsapp: </b><% $clientes.clientes.whatsapp%>
    <br><b>Email:</b> <% $clientes.clientes.email%>
    <br><b>Profesión: </b><% $clientes.clientes.profesion%>
    <br><b>F nacimiento:</b> <% $clientes.clientes.fecha_nacimiento%>
    <br><b>Porcentaje:</b> <% $clientes.porcentaje%> %
    <br>
    <span ng-if="$clientes.clientes.url_documento">
        <i class="el el-file"></i>
        <a href="<% $clientes.clientes.url_documento%>" target="_blank">
            Cédula del propietario
        </a>
    </span>
</p>

</div>

</div>

</div>
<footer class="card-footer">
    <div class="row">
        <div class="col-md-12 text-right">
            <button class="btn btn-default modal-dismiss">Cerrar</button>
        </div>
    </div>
</footer>
</form>
</section>
</div>
{{-- @endforeach --}}
{{-- Final de Vista inmueble --}}

<!-- modal mapa -->
<div class="modal fade" id="mapab" tabindex="-1" role="dialog" aria-labelledby="mapab" aria-hidden="true"
ng-controller="MapaLista" ng-init="lista('{{ json_encode($request) }}', '{{ $agente->token }}')">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
                Resultados de búsqueda en mapa
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div id="mapaLista"></div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
</div>
</div>

@foreach ($inmuebles as $inmueble)
    <!-- modal publicar Domus -->
    <div id="publicarInmueble_{{ $inmueble->id }}" class="modal-block modal-block-primary mfp-hide modal-xl">
        <section class="card">
            <form action="" method="post" enctype="multipart/form-data">
                <header class="card-header">
                    <h2 class="card-title">
                        Publicar inmueble a Domus
                    </h2>
                </header>
                <div class="card-body" ng-controller="PublicarInmuebleController">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="codigo">
                                Publicar inmueble código {{ $inmueble->codigo }} a Domus
                            </label>
                        </div>
                        <div class="form-group col-md-6">

                            @if ($inmueble->estado_id == 2 && $inmueble->publicado != 1 && ($agente->rol_id == 1 ||
                            $agente->rol_id == 5))
                            <button type="button" class="btn btn-primary btn-default"
                            ng-click="publicar('{{ url('alt/api-publish/' . $inmueble->id) }}', '{{ $agente->token }}')">
                            Publicar
                        </button>
                    @endif

                </div>
            </div>
            <div class="form-row">
                <div class="card-body">

                    <div class="row">
                        <div class="alert alert-success hidden" id="inmueble-publicado">
                            Inmueble publicado correctamente.
                        </div>
                    </div>

                    <div class="row">
                        <div class="alert alert-info hidden">
                            Publicado sin número de habitaciones
                        </div>
                    </div>

                    <div class="row">
                        <div class="alert alert-warning hidden">
                            Publicado sin número de habitaciones
                        </div>
                    </div>

                    <div class="row">
                        <div class="alert alert-danger hidden" id="publicado-error">
                            No se pudo publicar
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <footer class="card-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-default modal-dismiss">Cerrar</button>
                </div>
            </div>
        </footer>
    </form>
</section>

</div>
@endforeach

<!-- Modal publicar en portales -->
<div ng-controller="Inmuebles" id="portalInmueble" class="modal-block modal-block-primary mfp-hide modal-xl">
    <section class="card">
        <form action="" method="post" enctype="multipart/form-data">
            <header class="card-header">
                <h2 class="card-title">
                    Actualizar portales
                </h2>
            </header>
            <div class="card-body">

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="codigo">
                            Actualizar inmueble código <%generalfactory.inmueblePortales.codigo%> a portales
                        </label>
                    </div>
                    <div class="form-group col-md-6">


                        <button type="button" class="btn btn-primary btn-default"
                        ng-click="portales(generalfactory.inmueblePortales.id,{{ $agente }})">
                        Publicar
                    </button>


                </div>
            </div>
            <div class="form-row">
                <div class="card-body">
                    <div class="col-md-12 alert alert-info" role="alert" id="portales-cargando">
                        Cargando...
                    </div>

                    <div class="col-md-12 alert alert-success" role="alert" id="portales-exito">
                        Portales actualizados exitosamente
                    </div>
                </div>
            </div>
        </div>
        <footer class="card-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-default modal-dismiss">Cerrar</button>
                </div>
            </div>
        </footer>
    </form>
</section>

</div>
