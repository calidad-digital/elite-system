<section role="main" class="content-body">
    <header class="page-header">
        <h2>Seguimiento de inmuebles</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>Inmuebles</span></li>
                <li><span>Seguimiento&nbsp;&nbsp;&nbsp;</span></li>
            </ol>
            <a class="" data-open=""></a>
        </div>
    </header>

    {{-- Mensajes --}}
    @if (session("mensaje"))
        <div class="row alert alert-success">
            <strong>Éxito!</strong> {{ session("mensaje") }}
        </div>
    @endif

    @if (session("error"))
        <div class="row alert alert-danger">
            <strong>Ha ocurrido un error!</strong>
        </div>
    @endif
    {{-- Final de mensajes --}}

    <div class="row">

        <div class="col-lg-12">

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>
                    <h2 class="card-title">Buscar inmueble</h2>
                </header>

                <div class="card-body">
                    <form class="row" action="" method="get">
                        <div class="col-lg-3">
                            <input class="form-control mb-3" name="code" type="number" placeholder="Código de inmueble"
                            value="{{ isset($request["code"]) ? $request["code"] : '' }}">
                        </div>
                        <div class="col-lg-3">
                            <button type="submit" class="btn btn-primary">Buscar </button>
                        </div>
                    </form>
                </div>
            </section>

            <br>

            @if (isset($request["code"]) && $request["code"] != "")
                <section class="card">
                    <header class="card-header">
                        <div class="card-actions">
                            <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                            <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                        </div>
                        <h2 class="card-title">Seguimientos del inmueble {{ $inmueble->codigo ?? $request["code"] }}</h2>
                    </header>
                    <div class="card-body">
                        <table class="table table-responsive-md table-striped mb-0">
                            <thead>
                                <tr>
                                    <th>Estado</th>
                                    <th>Subestado</th>
                                    <th>Fecha-hora</th>
                                    <th>Usuario</th>
                                    <th>Descripción</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($seguimientos as $seguimiento)
                                    <tr>
                                        <td>
                                            <span class="ecommerce-status {{ $seguimiento->estados->clase }}">
                                                {{ $seguimiento->estados->nombre }}
                                            </span>
                                        </td>
                                        <td>{{ $seguimiento->subestados->nombre }}</td>
                                        <td>{{ $seguimiento->fecha_registro }}</td>
                                        <td>{{ $seguimiento->agentes->nombres }} {{ $seguimiento->agentes->apellidos }}</td>
                                        <td>{{ substr($seguimiento->comentario, 0, 50) }}...</td>
                                        <td>
                                            <a class="modal-with-form btn-default" href="#modalVerseg_{{ $seguimiento->id }}">
                                                <i class="el el-eye-open" data-toggle="tooltip" data-placement="top"
                                                data-original-title="Ver este seguimiento"></i>
                                            </a>
                                            &nbsp;
                                        </td>
                                    </tr>

                                    <div id="modalVerseg_{{ $seguimiento->id }}"
                                        class="modal-block modal-block-primary mfp-hide modal-xl">
                                        <section class="card">
                                            <form action="{{ url("admin/seguimientos") }}" method="post"
                                            enctype="multipart/form-data">
                                            <header class="card-header">
                                                <h2 class="card-title">
                                                    Ver seguimiento del inmueble código {{ $inmueble->codigo }} del
                                                    {{ $seguimiento->created_at }}
                                                </h2>
                                            </header>

                                            <div class="card-body">

                                                <div class="form-row">
                                                    <div class="form-group col-md-4">
                                                        <label for="status">
                                                            <b>Estado </b>
                                                        </label>
                                                        <p>{{ $seguimiento->estados->nombre }}</p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="status">
                                                            <b>Subestado</b>
                                                        </label>
                                                        <p>{{ $seguimiento->subestados->nombre }}</p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="broker">
                                                            <b>Encargado</b>
                                                        </label>
                                                        <p>
                                                            {{ $seguimiento->agentes->nombres }}
                                                            {{ $seguimiento->agentes->apellidos }}
                                                        </p>
                                                    </div>

                                                    <div class="form-group col-md-12">
                                                        <label>
                                                            <b>Fecha</b>
                                                        </label>
                                                        <div>
                                                            {{ $seguimiento->created_at }}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label for="description">
                                                            <b>Descripción</b>
                                                        </label>
                                                        <p>{{ $seguimiento->comentario }}</p>
                                                    </div>
                                                </div>
                                                @if(!empty($seguimiento->alertas))
                                                    <div>
                                                        <hr>
                                                        <div class="form-row">
                                                            <div class="form-group col-md-12">
                                                                <label for="status">
                                                                    <b>Fecha de la alerta</b>
                                                                </label>
                                                                <p>{{ $seguimiento->alertas->fecha_asignada ??''}}</p>
                                                            </div>
                                                            <div class="form-group col-md-12">
                                                                <label for="status">
                                                                    <b>Descripción</b>
                                                                </label>
                                                                <p>{{ $seguimiento->alertas->comentario ??''}}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                            <footer class="card-footer">
                                                <div class="row">
                                                    <div class="col-md-12 text-right">
                                                        <button class="btn btn-default modal-dismiss">
                                                            Cerrar
                                                        </button>
                                                    </div>
                                                </div>
                                            </footer>
                                        </form>
                                    </section>
                                </div>
                            @endforeach

                        </tbody>
                    </table>

                    @if ($inmueble != null && ($usuario->rol_id == 1 || $usuario->rol_id == 5 || $usuario->rol_id == 6
                    || $usuario->rol_id == 2))
                    <br>
                    <a class="modal-with-form mb-1 mt-1 mr-1 btn btn-success" href="#modalFormseg">
                        <i class="el el-check"></i> Crear seguimiento
                    </a>
                @endif
            </div>
        </section>
    @endif
</div>
</div>

</section>

@if ($inmueble != null)
    <div id="modalFormseg" class="modal-block modal-block-primary mfp-hide modal-xl">
        <section class="card">
            <form action="{{ url("admin/seguimientos") }}" method="post" enctype="multipart/form-data"
            ng-controller="FormSeguimiento">
            <header class="card-header">
                <h2 class="card-title">
                    Crear seguimiento de aprobación para el inmueble código {{ $inmueble->codigo }}
                </h2>
            </header>


            <div class="card-body">

                {{ csrf_field() }}

                <input type="hidden" name="property" value="{{ $inmueble->id }}">
                <input type="hidden" name="module" value="1">

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="status">Estado</label>
                        <select id="status" class="form-control" name="status" ng-model="estado"
                        ng-change="subestadosList()" required>
                        <option value="">Seleccionar estado</option>
                        @if($usuario->rol_id == 2)
                            <option value="8">
                                Retirado
                            </option>
                            <option value="9">
                                Separado
                            </option>
                        @else
                            @foreach ($estados as $estado)
                                <option value="{{ $estado->id }}">
                                    {{ $estado->nombre }}
                                </option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="status">Subestado</label>
                    <select id="substatus" class="form-control" name="substatus" required>
                        <option value="">Seleccionar subestado</option>
                        <option ng-repeat="subestado in subestados" ng-value="subestado.id"
                        ng-bind="subestado.nombre"></option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="broker">Encargado</label>
                    <select id="broker" class="form-control" name="broker" disabled>
                        @foreach ($agentes as $agente)
                            <option value="{{ $agente->id }}" {{ $agente->id == $usuario->id ? "selected" : "" }}>
                                {{ $agente->nombres }} {{ $agente->apellidos }}
                            </option>
                        @endforeach
                    </select>
                </div>

            </div>

            <div class="form-row">
                <div class="form-group col-md-12">
                    <label>Fecha</label>
                    <div>
                        <input type="datetime-local" class="form-control" name="date" value="{{ date("Y-m-d") }}">
                    </div>
                </div>
            </div>
            <div class="form-row" ng-controller="FormSeguimiento">
                <div class="form-group col-md-12">
                    <label for="description">Descripción <%description.length%>/500</label>
                    <textarea class="form-control" rows="3" ng-model="description" name="description"
                    id="textareaAutosize" maxlength="500"></textarea>
                </div>
            </div>
        </div>

        <br>

        {{-- Alerta de seguimiento --}}
        <div class="card-body">
            <div class="form-row">
                <h3>¿Crear alerta? &nbsp;<input type="checkbox" name="has_alert" ng-model="has_alert">
                </h3>
            </div>
            <div class="form-row" ng-show="has_alert == 1">
                <div class="form-group col-md-6">
                    <label>Fecha Alerta</label>
                    <div>
                        <input type="date" class="form-control" name="date_alert" value="{{ date("Y-m-d") }}">
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label>Hora alerta</label>
                    <div>
                        <select id="hour_alert" class="form-control" name="hour_alert">
                            <option value="8:00">8:00 am</option>
                            <option value="9:00">9:00 am</option>
                            <option value="10:00">10:00 am</option>
                            <option value="11:00">11:00 am</option>
                            <option value="12:00">12:00 pm</option>
                            <option value="13:00">1:00 pm</option>
                            <option value="14:00">2:00 pm</option>
                            <option value="15:00">3:00 pm</option>
                            <option value="16:00">4:00 pm</option>
                            <option value="17:00">5:00 pm</option>
                            <option value="18:00">6:00 pm</option>
                            <option value="19:00">7:00 pm</option>
                            <option value="20:00">8:00 pm</option>
                            <option value="21:00">9:00 pm</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-row" ng-show="has_alert == 1">
                <div class="form-group col-md-12">
                    <label for="description_alert">Descripción alerta</label>
                    <textarea class="form-control" rows="3" name="description_alert"
                    id="textareaAutosizea"></textarea>
                </div>
            </div>
        </div>

        <br>

        <footer class="card-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-default modal-dismiss">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </footer>
    </form>
</section>
</div>
@endif
