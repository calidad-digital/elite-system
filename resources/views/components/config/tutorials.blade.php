<section role="main" class="content-body">
    <header class="page-header">
        <h2>Tutoriales</h2>

        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>Configuración general</span></li>
                <li><span>Tutoriales&nbsp;&nbsp;&nbsp;</span></li>
            </ol>

            <a class="" data-open=""></a>
        </div>
    </header>

    {{-- Mensajes --}}
    @if (session("mensaje"))
        <div class="row alert alert-success">
            <strong>Éxito!</strong> {{ session("mensaje") }}
        </div>
    @endif

    @if (session("error"))
        <div class="row alert alert-danger">
            <strong>Ha ocurrido un error!</strong>
        </div>
    @endif
    {{-- Final de mensajes --}}

    <div class="row">
        <div class="col-md-4">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Crear registro</h2>
                </header>

                {{-- Crear tutorial --}}
                <div class="card-body">
                    <form action="" method="post">
                        {{ csrf_field(); }}
                        <input type="hidden" name="opperation" value="create">

                        <div class="form-group">
                            <label for="title">Título</label>
                            <input type="text" name="title" class="form-control" placeholder="Título del video" required>
                        </div>

                        <div class="form-group">
                            <label for="subtitle">Subtítulo</label>
                            <input type="text" name="subtitle" class="form-control" placeholder="Subtítulo del video" required>
                        </div>

                        <div class="form-group">
                            <label for="url">Link del vídeo</label>
                            <input type="text" name="url" class="form-control" placeholder="https://youtube.com/..." required>
                        </div>

                        <div class="form-group">
                            <label for="platform">Plataforma</label>
                            <select class="form-control" name="platform">
                                <option value="1">Youtube</option>
                                <option value="2">Vimeo</option>
                                <option value="3">DailyMotion</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary pull-right">Guardar</button>
                        </div>
                    </form>
                </div>
                {{-- Final de crear tutorial --}}
            </section>
        </div>

        <div class="col-md-8">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Lista de tutoriales</h2>
                </header>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <th>Título</th>
                            <th>Subtítulo</th>
                            <th>Enlace</th>
                            <th><i class="bx bx-pencil"></i></th>
                            <th><i class="bx bx-trash"></i></th>
                        </thead>
                        <tbody>
                            @foreach ($tutoriales as $tutorial)
                                <tr>
                                    <td>{{ $tutorial->titulo }}</td>
                                    <td>{{ $tutorial->subtitulo }}</td>
                                    <td>{{ $tutorial->url }}</td>
                                    <td>
                                        <a role="button" class="btn btn-warning modal-with-form" href="#modalEdit_{{ $tutorial->id }}">
                                            <i class="bx bx-pencil"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <form action="" method="post">
                                            {{ csrf_field(); }}
                                            <input type="hidden" name="opperation" value="delete">
                                            <button class="btn btn-danger" name="delete" value="{{ $tutorial->id }}">
                                                <i class="bx bx-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>

</section>


@foreach ($tutoriales as $tutorial)
    <div id="modalEdit_{{ $tutorial->id }}" class="modal-block modal-block-primary mfp-hide">
        <section class="card">
            <header class="card-header">
                <h2 class="card-title">Editar tutorial {{ $tutorial->id }}</h2>
            </header>

            <form action="" method="post">
                {{ csrf_field(); }}
                <input type="hidden" name="opperation" value="update">
                <input type="hidden" name="registry" value="{{ $tutorial->id }}">

                <div class="card-body">
                    <div class="form-group">
                        <label for="title">Título</label>
                        <input type="text" name="title" class="form-control" placeholder="Título del video" required value="{{ $tutorial->titulo }}">
                    </div>

                    <div class="form-group">
                        <label for="subtitle">Subtítulo</label>
                        <input type="text" name="subtitle" class="form-control" placeholder="Subtítulo del video" required value="{{ $tutorial->subtitulo }}">
                    </div>

                    <div class="form-group">
                        <label for="url">Link del vídeo</label>
                        <input type="text" name="url" class="form-control" placeholder="https://youtube.com/..." required value="{{ $tutorial->url }}">
                    </div>

                    <div class="form-group">
                        <label for="platform">Plataforma</label>
                        <select class="form-control" name="platform">
                            <option value="1" {{ $tutorial->icono == "youtube" ? "selected" : "" }}>Youtube</option>
                            <option value="2" {{ $tutorial->icono == "vimeo" ? "selected" : "" }}>Vimeo</option>
                            <option value="3" {{ $tutorial->icono == "dailymotion" ? "selected" : "" }}>DailyMotion</option>
                        </select>
                    </div>
                </div>

                <footer class="card-footer">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                            <button class="btn btn-default modal-dismiss">Cancelar</button>
                        </div>
                    </div>
                </footer>
            </form>

        </section>
    </div>
@endforeach
