<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta charset="utf-8">
    <title>Contrato</title>

    <style media="screen">
    @font-face {
        font-family: "Calibri";
        font-weight: normal;
        font-style: normal;
        font-variant: normal;
        src: url(https://fonts.gstatic.com/l/font?kit=J7afnpV-BGlaFfdAhLEY6w&skey=a1029226f80653a8&v=v11) format("woff2");
    }
    @page {
        margin: 0px;
    }
    body {
        margin: 0px;
        font-family: "Calibri", sans-serif;
        font-size: 12px;
    }

    html {
        margin: 0px;
    }

    .pagina {
        margin-left: 7.5%;
        margin-right: 7.5%;
    }

    .table-column-left {
        width: 50%;
        max-width: 50%;
        padding-right: 15px;
        border-collapse: collapse;
    }

    .table-column-right {
        width: 50%;
        max-width: 50%;
        padding-left: 15px;
        border-collapse: collapse;
    }

    .logo-encabezado {
        width: 100%;
        height: auto;
    }

    .terminos-contrato {
        margin-top: -20px;
        width: 100%;
        font-size: 11.5px;
    }

    .terminos-contrato p {
        text-align: justify;
        overflow: hidden;
        text-justify: inter-word;
        text-align-last:center;
        display: flex;
        justify-content: space-around;
    }

    /* .terminos-contrato p:after {
    content: "";
    width: 100%;
    display: inline-block;
    } */

    .custom-table {
        width: 100%;
        border-style: solid;
        border-width: 1px;
        border-collapse: collapse;
        font-size: 13.5px;
    }

    .yellow {
        background-color: #fdfe74;
        border-style: solid;
        border-width: 1px;
    }

    .table-row-title {
        border-style: solid;
        border-right: none;
        border-width: 1px;
        padding-left: 10px;
    }

    .custom-table tr, .custom-table td, .custom-table th {
        border: none;
    }

    .tr-solid {
        border-bottom-style: solid !important;
        border-bottom-width: 1px !important;
    }

    .header-title {
        position: absolute;
        top: 2%;
        left: 30%;
        right: 10%;
        font-size: 23px;
    }

    .header b {
        position: absolute;
        right: 10%;
        top: 8%;
        left: 30%;
    }

    .pie {
        position: absolute;
        bottom: 0%;
        display: flex;
    }

    .logo-pie {
        position: relative;
        width: 100%;
        height: auto;
        z-index: -1;
    }

    .footer-page {
        position: absolute;
        right: 40%;
        left: 40%;
        z-index: 1;
        font-size: 15px;
        text-align: center;
        top: 0.5%;
    }

    .hidden {
        color: white;
    }
    </style>
</head>
<body style="margin:0">
    @php($height = "100%")
    @php($width = "100%")
    <div id="p1" style="height: {{ $height }}; width: {{ $width }}; position: relative">
        <div class="header">
            <img alt="" class="logo-encabezado" src="{{ asset("assets/img/logo_encabezado_nuevo.png") }}">
            <h1 class="header-title">CONTRATO DE ARRENDAMIENTO DE INMUEBLE DE VIVIENDA URBANA.</h1>
            <b>Revisado: ABRIL 2022</b>
        </div>
        <div id="p1_contenido" class="pagina">
            <h2 style="text-align: center">CONTRATO No. {{ $arriendo->numero_contrato }}</h2>
            <table class="custom-table">
                <thead class="yellow">
                    <tr>
                        <th colspan="4" style="border-style:solid; border-width:1px;">1. PARTES DEL CONTRATO</th>
                    </tr>
                </thead>
                <tbody class="custom-table-body">
                    <tr>
                        <td rowspan="5" class="table-row-title" style="border-bottom-style: solid; border-bottom-width: 1px;">
                            <b>1.1 ARRENDADOR</b>
                        </td>
                        <td colspan="3"><b>INVERSIONES ELITE GROUP SAS - NIT 900.541.111-5</b></td>
                    </tr>
                    <tr>
                        <td colspan="3"><b>DIRECCIÓN JUDICIAL:</b> KR 58 128B 34  -  601-744.82.05 - 322-743.98.50</td>
                    </tr>
                    <tr>
                        <td colspan="3"><b>REP. LEGAL:</b> FABIO RUBIO LOZANO <b>C.C:</b> 16.277870 Palmira, Valle</td>
                    </tr>
                    <tr>
                        <td colspan="3"><b>Matrícula Arrendador Bogotá:</b> 20130149 - Secretaría Distrital de Hábitat.</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="border-bottom: solid; border-width: 1px;">
                            <b>Matrícula Arrendador Cali:</b> 006-2021 - Secretaría de Seguridad y Justicia.
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="7" class="table-row-title"><b>1.2 ARRENDATARIO</b></td>
                        <td>NOMBRE:</td>
                        <td colspan="2"><b>Nombre_Arrendatario_Var</b></td>
                    </tr>
                    <tr>
                        <td>CC:</td>
                        <td colspan="2"><b>CCArrendatario_Var</b></td>
                    </tr>
                    <tr>
                        <td>DIRECCIÓN:</td>
                        <td colspan="2"><b>DireccionArrendatario_Var</b></td>
                    </tr>
                    <tr>
                        <td>COPROPIEDAD</td>
                        <td colspan="2"><b>CopropiedadArrendatario_Var</b></td>
                    </tr>
                    <tr>
                        <td>TELÉFONOS:</td>
                        <td colspan="2"><b>TelefonosArrendatario_Var</b></td>
                    </tr>
                    <tr>
                        <td>DIR. COMERCIAL:</td>
                        <td colspan="2"><b>DirArrendatario_Var</b></td>
                    </tr>
                    <tr style="border-bottom: solid; border-width: 1px;">
                        <td>EMAIL:</td>
                        <td colspan="2"><b>EmailArrendatario_Var</b></td>
                    </tr>
                    <tr>
                        <td rowspan="7" class="table-row-title"><b>1.3 DEUDOR SOLIDARIO 1</b></td>
                        <td>NOMBRE:</td>
                        <td colspan="2"><b>Nombre_Deudor1_Var</b></td>
                    </tr>
                    <tr>
                        <td>CC:</td>
                        <td colspan="2"><b>CCDeudor1_Var</b></td>
                    </tr>
                    <tr>
                        <td>DIRECCIÓN:</td>
                        <td colspan="2"><b>DireccionDeudor1_Var</b></td>
                    </tr>
                    <tr>
                        <td>COPROPIEDAD</td>
                        <td colspan="2"><b>CopropiedadDeudor1_Var</b></td>
                    </tr>
                    <tr>
                        <td>TELÉFONOS:</td>
                        <td colspan="2"><b>TelefonosDeudor1_Var</b></td>
                    </tr>
                    <tr>
                        <td>DIR. COMERCIAL:</td>
                        <td colspan="2"><b>DirDeudor1_Var</b></td>
                    </tr>
                    <tr style="border-bottom: solid; border-width: 1px;">
                        <td>EMAIL:</td>
                        <td colspan="2"><b>EmailDeudor1_Var</b></td>
                    </tr>
                    <tr>
                        <td rowspan="7" class="table-row-title"><b>1.4 DEUDOR SOLIDARIO 2</b></td>
                        <td>NOMBRE:</td>
                        <td colspan="2"><b>Nombre_Deudor2_Var</b></td>
                    </tr>
                    <tr>
                        <td>CC:</td>
                        <td colspan="2"><b>CCDeudor2_Var</b></td>
                    </tr>
                    <tr>
                        <td>DIRECCIÓN:</td>
                        <td colspan="2"><b>DireccionDeudor2_Var</b></td>
                    </tr>
                    <tr>
                        <td>COPROPIEDAD</td>
                        <td colspan="2"><b>CopropiedadDeudor2_Var</b></td>
                    </tr>
                    <tr>
                        <td>TELÉFONOS:</td>
                        <td colspan="2"><b>TelefonosDeudor2_Var</b></td>
                    </tr>
                    <tr>
                        <td>DIR. COMERCIAL:</td>
                        <td colspan="2"><b>DirDeudor2_Var</b></td>
                    </tr>
                    <tr style="border-bottom: solid; border-width: 1px;">
                        <td>EMAIL:</td>
                        <td colspan="2"><b>EmailDeudor2_Var</b></td>
                    </tr>
                    <tr>
                        <td rowspan="7" class="table-row-title"><b>1.5 DEUDOR SOLIDARIO 3</b></td>
                        <td>NOMBRE:</td>
                        <td colspan="2"><b>Nombre_Deudor3_Var</b></td>
                    </tr>
                    <tr>
                        <td>CC:</td>
                        <td colspan="2"><b>CCDeudor3_Var</b></td>
                    </tr>
                    <tr>
                        <td>DIRECCIÓN:</td>
                        <td colspan="2"><b>DireccionDeudor3_Var</b></td>
                    </tr>
                    <tr>
                        <td>COPROPIEDAD</td>
                        <td colspan="2"><b>CopropiedadDeudor3_Var</b></td>
                    </tr>
                    <tr>
                        <td>TELÉFONOS:</td>
                        <td colspan="2"><b>TelefonosDeudor3_Var</b></td>
                    </tr>
                    <tr>
                        <td>DIR. COMERCIAL:</td>
                        <td colspan="2"><b>DirDeudor3_Var</b></td>
                    </tr>
                    <tr style="border-bottom: solid; border-width: 1px;">
                        <td>EMAIL:</td>
                        <td colspan="2"><b>EmailDeudor3_Var</b></td>
                    </tr>
                    <tr>
                        <td rowspan="6" class="table-row-title">
                            <b>1.6 Personas autorizadas<br>para ocupar el<br>inmueble y/o recibirlo</b>
                        </td>
                        <td colspan="2">1. <b>NombreAutorizado1_Var</b></td>
                        <td>C.C. <b>CcAutorizado1_Var</b></td>
                    </tr>
                    <tr>
                        <td colspan="2">2. <b>NombreAutorizado2_Var</b></td>
                        <td>C.C. <b>CcAutorizado2_Var</b></td>
                    </tr>
                    <tr>
                        <td colspan="2">3. <b>NombreAutorizado3_Var</b></td>
                        <td>C.C. <b>CcAutorizado3_Var</b></td>
                    </tr>
                    <tr>
                        <td colspan="2">4. <b>NombreAutorizado4_Var</b></td>
                        <td>C.C. <b>CcAutorizado4_Var</b></td>
                    </tr>
                    <tr>
                        <td colspan="2">5. <b>NombreAutorizado5_Var</b></td>
                        <td>C.C. <b>CcAutorizado5_Var</b></td>
                    </tr>
                    <tr>
                        <td colspan="2">6. <b>NombreAutorizado6_Var</b></td>
                        <td>C.C. <b>CcAutorizado6_Var</b></td>
                    </tr>
                </tbody>
            </table>
            <table class="custom-table">
                <thead class="yellow">
                    <tr>
                        <th colspan="4" class="table-row-title">
                            2. EXTREMOS DEL CONTRATO
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="width: 40%" class="table-row-title">
                            <b>2.1 INICIO DEL CONTRATO</b>
                        </td>
                        <td colspan="3"><b>fecha_inicio_contrato</b></td>
                    </tr>
                    <tr>
                        <td style="width: 40%" class="table-row-title">
                            <b>2.2 ATERMINACIÓN DEL CONTRATO</b>
                        </td>
                        <td colspan="3"><b>fecha_final_entrega</b></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="pie">
            <div class="footer-page">
                Página <strong>1</strong> de <strong>8</strong>
            </div>
            <img alt="" src="{{ asset("assets/img/pie_nuevo.png") }}" class="logo-pie">
        </div>
    </div>

    <div id="p2" style="height: {{ $height }}; width: {{ $width }}; position: relative">
        <div class="header">
            <img alt="" class="logo-encabezado" src="{{ asset("assets/img/logo_encabezado_nuevo.png") }}">
            <h1 class="header-title">CONTRATO DE ARRENDAMIENTO DE INMUEBLE DE VIVIENDA URBANA.</h1>
            <b>Revisado: ABRIL 2022</b>
        </div>
        <div id="p2_contenido" class="pagina">
            <br>
            <table class="custom-table" style="table-layout: fixed;">
                <thead class="yellow">
                    <tr>
                        <th colspan="5">
                            3. IDENTIFICACIÓN DEL INMUEBLE
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="width: 30%" rowspan="14" class="table-row-title">
                            <b>3.1 IDENTIFICACIÓN</b>
                        </td>
                        <td colspan="2"><b>Direccion1_Var</b></td>
                        <td colspan="2"><b>Ciudad_Var</b></td>
                    </tr>
                    <tr class="tr-solid">
                        <td colspan="2"><b>Direccion2_Var Direccion3_Var</b></td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>MAT. INMOB. AP. No:</b></td>
                        <td colspan="2">MatriculaVarAp1</td>
                    </tr>
                    <tr class="tr-solid">
                        <td colspan="2"><b>CHIP:</b> {{ $arriendo->inmueble->chip != "" ? $arriendo->inmueble->chip : "N/A" }}</td>
                        <td colspan="2"><b>CED. CAT:</b> CedulaCatastral1</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>MAT. INMOB. GJ. No:</b></td>
                        <td colspan="2">matricula_parqueadero_1</td>
                    </tr>
                    <tr class="tr-solid">
                        <td colspan="2"><b>CHIP:</b> chip_parqueadero_1</td>
                        <td colspan="2"><b>CED. CAT:</b> cedula_parqueadero_1</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>MAT. INMOB. GJ. No:</b></td>
                        <td colspan="2">matricula_parqueadero_2</td>
                    </tr>
                    <tr class="tr-solid">
                        <td colspan="2"><b>CHIP:</b> chip_parqueadero_2</td>
                        <td colspan="2"><b>CED. CAT:</b> cedula_parqueadero_2</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>MAT. INMOB. GJ. No:</b></td>
                        <td colspan="2">matricula_parqueadero_3</td>
                    </tr>
                    <tr class="tr-solid">
                        <td colspan="2"><b>CHIP:</b> chip_parqueadero_3</td>
                        <td colspan="2"><b>CED. CAT:</b> cedula_parqueadero_3</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>MAT. INMOB. DP. No:</b></td>
                        <td colspan="2">matricula_deposito_1</td>
                    </tr>
                    <tr class="tr-solid">
                        <td colspan="2"><b>CHIP:</b> chip_deposito_1</td>
                        <td colspan="2"><b>CED. CAT:</b> cedula_deposito_1</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>MAT. INMOB. DP. No:</b></td>
                        <td colspan="2">matricula_deposito_2</td>
                    </tr>
                    <tr class="tr-solid">
                        <td colspan="2"><b>CHIP:</b> chip_deposito_2</td>
                        <td colspan="2"><b>CED. CAT:</b> cedula_deposito_2</td>
                    </tr>
                </tbody>
            </table>
            <table class="custom-table">
                <thead class="yellow">
                    <tr>
                        <th colspan="3">4. OBJETO Y CONDICIONES DEL CONTRATO</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="width: 30%;" colspan="1" class="table-row-title">
                            <b>4.1 FECHA DE ENTREGA</b>
                        </td>
                        <td colspan="2" style="text-align: left;">fecha_entrega - {{ $arriendo->texto_fecha_entrega }}</td>
                    </tr>
                </tbody>
            </table>
            <table class="custom-table">
                <thead class="yellow">
                    <tr>
                        <th colspan="4">5. PRECIO Y FORMA DE PAGO</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="tr-solid">
                        <td colspan="1" style="width: 30%;" class="table-row-title">
                            <b>5.1 VALOR MES CANON</b>
                        </td>
                        <td colspan="3">
                            <b>PrecioCanonNumeros_Var</b> (PrecioCanonLetras_Var PESOS M/TE) <br><br>
                        </td>
                    </tr>
                    <tr class="tr-solid">
                        <td rowspan="2" colspan="1" style="width: 30%;" class="table-row-title">
                            <b>
                                5.2 VALOR MENSUAL CUOTA ADMIN.
                            </b>
                        </td>
                        <td colspan="3">
                            <b>PrecioAdmonNumeros_Var</b>
                            (PrecioAdmonLetras_Var PESOS M/TE) <br>
                            Pagaderos del 1 al 10 de cada mes<br>
                        </td>
                    </tr>
                    <tr class="tr-solid">
                        <td colspan="3">
                            <b>PrecioAdmonCompletoNumeros_Var</b>
                            (PrecioAdmonCompletoLetras_Var PESOS M/TE) <br>
                            Pagaderos del 11 al 30 de cada mes<br>
                        </td>
                    </tr>
                    <tr class="tr-solid">
                        <td colspan="1" style="width: 30%;" class="table-row-title">
                            <b>5.3 VALOR CALDERA</b>
                        </td>
                        <td colspan="3">
                            <b>PrecioCalderaNumeros_Var</b> (PrecioCalderaLetras_Var PESOS M/TE) <br><br>
                        </td>
                    </tr>
                    <tr class="tr-solid">
                        <td colspan="1" style="width: 30%;" class="table-row-title">
                            <b>5.4 VALOR PARQUEADERO</b>
                        </td>
                        <td colspan="3">
                            <b>PrecioParqueaderoNumeros_Var</b> (PrecioParqueaderoLetras_Var PESOS M/TE) <br><br>
                        </td>
                    </tr>
                    <tr class="tr-solid">
                        <td colspan="1" style="width: 30%;" class="table-row-title">
                            <b>5.5 VALOR LAVANDERÍA</b>
                        </td>
                        <td colspan="3">
                            <b>PrecioLavanderiaNumeros_Var</b> (PrecioLavanderiaLetras_Var PESOS M/TE) <br><br>
                        </td>
                    </tr>
                    <tr class="tr-solid">
                        <td class="table-row-title" colspan="1" style="width: 30%;">
                            <b>NOTAS:</b>
                        </td>
                        <td colspan="3">
                            notas_precio
                        </td>
                    </tr>
                </tbody>
            </table>

            <table class="custom-table">
                <thead class="yellow">
                    <tr>
                        <th colspan="4">6. SERVICIOS PÚBLICOS</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="tr-solid">
                        <td colspan="1" style="width: 30%;" class="table-row-title">
                            <b>6.1 ACUEDUCTO, ALCANTARILLADO Y RECOLECCIÓN DE BASURAS</b>
                        </td>
                        <td colspan="3">
                            Cuenta Contrato: <b>contrato_acueducto</b> <br>
                            notas_servicios
                        </td>
                    </tr>
                    <tr class="tr-solid">
                        <td colspan="1" style="width: 30%;" class="table-row-title">
                            <b>6.2 ENERGÍA ELÉCTRICA</b>
                        </td>
                        <td colspan="3">
                            Cuenta Contrato: <b>contrato_energia</b> <br>
                            notas_servicio_energia
                        </td>
                    </tr>
                    <tr class="tr-solid">
                        <td colspan="1" style="width: 30%;" class="table-row-title">
                            <b>6.3 GAS NATURAL</b>
                        </td>
                        <td colspan="3">
                            Cuenta Contrato: <b>contrato_gas</b> <br>
                            notas_servicio_gas
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="custom-table">
                <thead class="yellow">
                    <tr>
                        <th colspan="4">
                            7. COSAS Y USOS CONEXOS
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="1" style="width: 30%;" class="table-row-title">
                            <b>7.1 PARQUEADERO(S)</b>
                        </td>
                        <td colspan="3" class="table-row-title">
                            <b>
                                @php($parqueaderos = "")
                                @for ($i = 1; $i <= 3; $i++)
                                    @if ($arriendo["inmueble"]["numero_parqueadero_{$i}"] != "" || $arriendo["inmueble"]["numero_parqueadero_{$i}"] != 0)
                                        @php($parqueaderos .= $arriendo["inmueble"]["numero_parqueadero_{$i}"] . ", ")
                                    @endif
                                @endfor

                                {{ rtrim($parqueaderos, ", ") }}
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1" style="width: 30%;" class="table-row-title">
                            <b>7.2 DEPÓSITO(S)</b>
                        </td>
                        <td colspan="3" class="table-row-title">
                            <b>
                                @php($depositos = "")
                                @for ($i = 1; $i <= 3; $i++)
                                    @if ($arriendo["inmueble"]["numero_deposito_{$i}"] != "" || $arriendo["inmueble"]["numero_deposito_{$i}"] != 0)
                                        @php($depositos .= $arriendo["inmueble"]["numero_deposito_{$i}"] . ", ")
                                    @endif
                                @endfor

                                {{ rtrim($depositos, ", ") }}
                            </b>
                        </td>
                    </tr>
                </tbody>
            </table>
            <p><b>CONVENCIÓN:</b> LA INFORMACIÓN REFERIDA ANTERIORMENTE SE CITARÁ COMO <b>"Numeral (el número que corresponda) del encabezado".</b></p>
            <p>
                Agente: {{ $arriendo->inmueble->asesores->nombres }} {{ $arriendo->inmueble->asesores->apellidos }} - MLS: {{ $arriendo->inmueble->codigoMLS }}
            </p>
        </div>
        <div class="pie">
            <div class="footer-page">
                Página <strong>2</strong> de <strong>8</strong>
            </div>
            <img alt="" src="{{ asset("assets/img/pie_nuevo.png") }}" class="logo-pie">
        </div>
    </div>
    <div id="p3" style="height: {{ $height }}; width: {{ $width }}; position: relative">
        <div class="header">
            <img alt="" class="logo-encabezado" src="{{ asset("assets/img/logo_encabezado_nuevo.png") }}">
            <h1 class="header-title">CONTRATO DE ARRENDAMIENTO DE INMUEBLE DE VIVIENDA URBANA.</h1>
            <b>Revisado: ABRIL 2022</b>
        </div>
        <div id="p3_contenido" class="pagina">
            <h2 style="text-align:center"><u>CONDICIONES GENERALES DEL CONTRATO DE ARRENDAMIENTO</u></h2>
            <br>
            <div class="terminos-contrato">
                <p>
                    <b><u>PRIMERA.- OBJETO Y DESTINACIÓN:</u></b> Mediante el presente contrato EL ARRENDADOR concede
                    AL ARRENDATARIO el goce del inmueble que más adelante se identifica por su dirección y
                    linderos, obligándose éste a pagar a aquel una renta de arrendamiento y a destinarlo
                    para uso de <b>VIVIENDA URBANA</b>, y ser ocupado por las personas descritas en el
                    numeral 1.6 del encabezado.
                </p>
                <p>
                    <b><u>SEGUNDA.- LEGISLACIÓN:</u></b> El presente contrato se regirá en todas sus partes por las
                    cláusulas aquí consignadas, así como por los términos de la <b>Ley 820 de 2003</b> y demás
                    normas concordantes vigentes.  Así mismo EL ARRENDADOR, está sometido al control de la
                    Subsecretaría de Inspección, Vigilancia y Control de Vivienda de la Secretaría Distrital de
                    Hábitat, establecido en el artículo 28 de la Ley 820 de 2003.  Rev. 05-15.
                </p>
                <p>
                    <b><u>TERCERA.- IDENTIFICACIÓN DEL INMUEBLE:</u></b> El presente contrato recae sobre el inmueble
                    ubicado en la dirección relacionada en el numeral <b>3.1</b> del encabezado.
                </p>
                <p>
                    <b><u>CUARTA.- PRECIO Y FORMA DE PAGO:</u></b> El valor del Canon mensual de la renta del inmueble
                    objeto de este contrato, es la suma descrita en el numeral <b>5.1</b>. del encabezado, que
                    EL ARRENDATARIO se obliga a pagar AL ARRENDADOR en su totalidad, anticipadamente dentro de
                    los <b>CINCO (5)</b> primeros días de cada periodo mensual, a su orden o a quien éste autorice
                    o delegue previamente y por escrito para recibir dicha renta.  <b>PARÁGRAFO PRIMERO:</b>
                    La mera tolerancia DEL ARRENDADOR en aceptar el pago del precio del arrendamiento con
                    posterioridad a su vencimiento, no se entenderá como ánimo de novación o de modificación
                    del término establecido para el pago en este contrato. <b>PARÁGRAFO SEGUNDO:</b> En caso de mora
                    o retardo en el pago del precio del arrendamiento, de acuerdo con lo previsto en la presente
                    cláusula, EL ARRENDADOR podrá dar por terminado unilateralmente con justa causa el presente
                    contrato y exigir la entrega inmediata del inmueble, para lo cual EL ARRENDATARIO
                    renuncia expresamente a los requerimientos privados y judiciales previstos en la ley:
                    (artículos 1594 y 2007 del Código Civil). <b>PARÁGRAFO TERCERO:</b> EL ARRENDATARIO y
                    LOS DEUDORES SOLIDARIOS declaran desde ahora que con ocasión del retardo y consecuente
                    incumplimiento en el pago del contrato de arrendamiento descrito en los numerales <b>5.1 y 5.2</b>,
                    pagarán proporcionalmente a título de <strong>Claúsula Penal</strong> las siguientes sumas: El cuatro por
                    ciento <b>(4,0%)</b> del día seis (6) hasta el día diez (10) de cada mes, la suma de seis
                    por ciento <b>(6,0%)</b> desde el día once (11) hasta el día quince (15) de cada mes y la suma de nueve por ciento
                    <b>(9,0%)</b>
                    desde el día dieciséis (16) hasta el día veinte (20) de cada mes.
                    Del día veintiuno (21) en adelante,
                    EL ARRENDADOR realizará el reporte de tal retardo a la aseguradora y en este orden, no recibirá dineros
                    directamente ya que deberán ser pagados a esta última.
                </p>
                <p>
                    <b><u>QUINTA.- CUOTAS DE ADMINISTRACIÓN:</u></b> EL ARRENDATARIO se obliga a pagar por concepto de
                    cuota ordinaria mensual de las áreas comunes del Edificio o Conjunto donde se encuentra
                    ubicado el inmueble la suma descrita en el numeral <b>5.2</b> del
                    encabezado, anticipadamente
                    dentro de los <b>CINCO (5)</b> primeros días de cada periodo mensual, así como todos y cada uno
                    de los reajustes e Incrementos a su valor, que posteriormente sean aprobados por la Asamblea
                    de Copropietarios, de acuerdo con el reglamento de propiedad horizontal que rige en el mismo.
                    <b>PARÁGRAFO PRIMERO:</b> El valor correspondiente a la cuota de administración será pagado por
                    EL ARRENDATARIO directamente a EL ARRENDADOR, <b>junto con el valor del canon mensual de arrendamiento</b>
                    y cualquier pago que éste haga directamente a la Administración del conjunto residencial en donde se
                    encuentra ubicado el inmueble arrendado se tendrá por no hecho, salvo autorización expresa DEL ARRENDADOR.
                    <b>PARÁGRAFO SEGUNDO:</b> Por la celebración del presente contrato, EL ARRENDATARIO puede acceder al
                    descuento por pronto pago de la cuota mensual de administración en caso de que este exista,
                    en caso de no realizar el pago dentro de las fechas establecidas en el numeral <b>5.2</b>, deberá asumir el
                    pago de la cuota plena indicada en este mismo numeral.  <b>PARÁGRAFO TERCERO: EL ARRENDATARIO</b> se compromete
                    a cumplir y respetar cabalmente todas y cada una de las normas establecidas por el reglamento de
                    propiedad horizontal, para tal fin y en observancia de lo establecido en el Artículo 8 numeral 4 de
                    la Ley 820 de 2003, EL ARRENDATARIO Y LOS DEUDORES SOLIDARIOS, conocen y aceptan que a su disposición se encuentra el Reglamento de Propiedad
                    Horizontal y el Manual de Convivencia en la respectiva oficina de Administración.
                    <b>PARÁGRAFO CUARTO:</b> el inmueble cuenta adicionalmente con los servicios
                    relacionados en los numerales <b>5.3, 5.4 y 5.5</b> del encabezado.
                </p>
                <p>
                    <b><u>SEXTA.- LUGAR PARA EL PAGO:</u></b> EL ARRENDATARIO pagará el precio del arrendamiento y
                    la administración a favor de EL ARRENDADOR en la ciudad de BOGOTÁ, a través del Botón <b>PAGOS PSE</b>
                    que se encuentra en nuestra Página web <b>eliteinmobiliaria.com.co</b>, el cual expresamente declara conocer,
                    o mediante la factura con código de barras que podrán pagar directamente en la entidad bancaria Av. Villas.
                    <b>PARÁGRAFO PRIMERO:</b> El ARRENDADOR aplicará los pagos imputando primero a los conceptos que tengan pendientes EL
                    ARRENDATARIO y/o DEUDORES SOLIDARIOS diferentes al capital y por &uacute;ltimo lugar al capital,
                    de acuerdo con lo preceptuado en el art&iacute;culo 1653 del C&oacute;digo Civil.
                </p>
                <p>
                    <u><strong>S&Eacute;PTIMA.- SERVICIOS P&Uacute;BLICOS:</strong></u> A partir del momento en que
                    el inmueble arrendado sea entregado AL ARRENDATARIO y hasta la fecha de su desocupaci&oacute;n y
                    entrega AL ARRENDADOR, ser&aacute;n a cargo de EL ARRENDATARIO el pago de los servicios p&uacute;blicos
                    relacionados en el numeral <strong>6.1, 6.2 Y 6.3</strong> del encabezado, de acuerdo con la respectiva
                    facturaci&oacute;n. EL ARRENDADOR se reserva el derecho de solicitar mensualmente AL ARRENDATARIO los
                    recibos con la constancia de pago de estos. <strong>PAR&Aacute;GRAFO PRIMERO:</strong> Las reclamaciones que
                    tengan que ver con la &oacute;ptima prestaci&oacute;n o facturaci&oacute;n de los servicios
                    p&uacute;blicos anotados, ser&aacute;n tramitadas directamente por EL ARRENDATARIO ante las respectivas empresas
                    prestadoras del servicio. Cualquier otro servicio adicional o suntuario al que pretenda acceder EL ARRENDATARIO,
                    deber&aacute; ser previamente autorizado por EL ARRENDADOR.
                </p>
            </div>
        </div>
        <div class="pie">
            <div class="footer-page">
                Página <strong>3</strong> de <strong>8</strong>
            </div>
            <img alt="" src="{{ asset("assets/img/pie_nuevo.png") }}" class="logo-pie">
        </div>
    </div>
    <div id="p4" style="height: {{ $height }}; width: {{ $width }}; position: relative">
        <div class="header">
            <img alt="" class="logo-encabezado" src="{{ asset("assets/img/logo_encabezado_nuevo.png") }}">
            <h1 class="header-title">CONTRATO DE ARRENDAMIENTO DE INMUEBLE DE VIVIENDA URBANA.</h1>
            <b>Revisado: ABRIL 2022</b>
        </div>
        <div id="p4_contenido" class="pagina">
            <br>
            <div class="terminos-contrato">
                <p>
                    <strong>PAR&Aacute;GRAFO SEGUNDO:</strong> Si EL
                    ARRENDATARIO no paga oportunamente los servicios p&uacute;blicos antes se&ntilde;alados, este hecho se tendr&aacute;
                    como incumplimiento del contrato, pudiendo EL ARRENDADOR darlo por terminado unilateralmente sin necesidad de los
                    requerimientos privados y judiciales previstos en la Ley (art&iacute;culos 1594 y 2007 del C&oacute;digo Civil).
                    <strong>PAR&Aacute;GRAFO TERCERO:</strong> En cualquier evento de mora o retardo en el cumplimiento de las
                    obligaciones a cargo de EL ARRENDATARIO, EL ARRENDADOR queda facultado para exigir de aquel el pago de los
                    honorarios de abogado y dem&aacute;s gastos de cobranza judicial y/o extrajudicial, previa presentaci&oacute;n
                    de facturas de pago generadas. Igualmente, s&iacute; como consecuencia del no pago oportuno de los servicios
                    p&uacute;blicos las empresas respectivas los suspenden, retiran el contador o l&iacute;nea telef&oacute;nica o
                    lo que corresponda, ser&aacute;n a cargo de EL ARRENDATARIO el pago de los intereses de mora, sanciones, y los
                    gastos que demande su reconexi&oacute;n. <strong>PAR&Aacute;GRAFO CUARTO:</strong> En caso de acordarse la
                    prestaci&oacute;n de garant&iacute;as o fianzas por parte de EL ARRENDATARIO, a favor de las entidades prestadoras
                    de los servicios p&uacute;blicos antes indicadas, con el fin de garantizar a cada una de ellas el pago de las
                    facturas correspondientes, conforme al art&iacute;culo 15 de la Ley 820 del 2003 y su decreto 3130 del mismo
                    a&ntilde;o, con el lleno de los requisitos exigidos para tal efecto, documento &eacute;ste que en todo caso y
                    para todos los efectos ser&aacute; parte integrante del presente contrato de arrendamiento.
                </p>
                <p>
                    <u><strong>OCTAVA.- VIGENCIA:</strong></u> El t&eacute;rmino de duraci&oacute;n de este contrato es de DOCE (12)
                    meses, contados a partir de la fecha indicada en el numeral <strong>2.1</strong> del encabezado.
                </p>
                <p>
                    <u><strong>NOVENA.- PR&Oacute;RROGAS:</strong></u> Si a la fecha de vencimiento del t&eacute;rmino inicial o
                    de cualquiera de sus prórrogas, ninguna de las partes ha dado aviso a la otra con una antelaci&oacute;n no
                    menor a tres (3) meses a la fecha de vencimiento, de su intenci&oacute;n de darlo por terminado, el presente
                    contrato de arrendamiento se entender&aacute; prorrogado en iguales condiciones y por el mismo t&eacute;rmino
                    indicado en la cl&aacute;usula anterior, siempre y cuando cada una de las partes haya cumplido con las
                    obligaciones a su cargo, y EL ARRENDATARIO se avenga a los reajustes debidamente pactados en el presente
                    contrato.
                </p>
                <p>
                    <u><strong>D&Eacute;CIMA.- REAJUSTE DEL CANON DE ARRENDAMIENTO:</strong></u> Vencido el t&eacute;rmino inicial
                    de este contrato y as&iacute; sucesivamente cada doce (12) meses de ejecuci&oacute;n del mismo, en sus
                    pr&oacute;rrogas t&aacute;citas o expresas, el precio mensual del canon de arrendamiento se reajustar&aacute;
                    en forma autom&aacute;tica en un porcentaje igual al ciento por ciento (100%) del incremento que haya
                    tenido el &Iacute;ndice de Precios al Consumidor; IPC, en el a&ntilde;o calendario inmediatamente anterior
                    a aquel en que deba efectuarse el respectivo reajuste del canon de arrendamiento. Incremento que ser&aacute;
                    comunicado AL ARRENDATARIO, en su monto y fecha de vigencia mediante el env&iacute;o de correo electr&oacute;nico
                    o a la direcci&oacute;n donde reciban notificaciones.
                </p>
                <p>
                    <u><strong>D&Eacute;CIMO PRIMERA.- COSAS O USOS CONEXOS:</strong></u> Además del inmueble identificado y descrito
                    anteriormente tendrá EL ARRENDATARIO derecho de goce sobre las cosas y usos relacionados
                    en los numerales <strong>7.1, 7.2</strong> del encabezado.
                </p>
                <p>
                    <u><strong>D&Eacute;CIMO SEGUNDA.- INVENTARIO DE SERVICIOS, COSAS, USOS, ANEXOS O ADICIONALES:&nbsp;</strong></u>
                    EL ARRENDADOR entregar&aacute; el inmueble en su totalidad con el inventario que se anexar&aacute; al
                    presente contrato. Esto seg&uacute;n el Literal e) Art. 3&deg; de la Ley 820/03 y Num. 2) Art. 8&deg; Dec.
                    051/04. <strong>PAR&Aacute;GRAFO &Uacute;NICO:</strong> No obstante, el &aacute;rea, dimensiones, dependencias,
                    y los linderos citados, el presente arrendamiento se hace como cuerpo cierto. El arrendatario conoce el
                    inmueble desde la promoci&oacute;n, conoce el estado y las condiciones del entorno.
                </p>
                <p>
                    <u><strong>D&Eacute;CIMO TERCERA.- OBLIGACIONES DEL ARRENDATARIO:</strong></u> 1. Pagar el precio del canon de
                    arrendamiento dentro del plazo y en el lugar estipulado en el presente contrato.
                    2. Cuidar el inmueble y las cosas recibidas en arrendamiento. En caso de daños o deterioros distintos
                    a los derivados del uso normal o de la acción del tiempo, que fueren imputables al mal uso del inmueble,
                    o a su propia culpa, EL ARRENDATARIO deberá efectuar oportunamente y por su cuenta las reparaciones o
                    sustituciones necesarias. 3. Permitir el ingreso de contratistas para las reparaciones.
                    4. Pagar oportunamente los servicios y costos por usos conexos y/o adicionales, así como las expensas comunes
                    en los casos en que haya lugar, de conformidad con lo aquí establecido. 5. Cumplir las normas consagradas en los
                    reglamentos de propiedad horizontal y las que expida el gobierno en protección de los derechos de todos los vecinos.
                    6. Informar oportunamente los incrementos en el Valor de la cuota de Administración y/o fijación de cuotas extraordinarias.
                    7. Reportar mensualmente las cuentas de cobro enviadas por la Administración. 8. Informar oportunamente cualquier citación
                    que haga la Administración para reuniones ordinarias o extraordinarias de la Asamblea General de Copropietarios, así
                    mismo cualquier documentación que llegue a nombre del propietario. Si el Arrendatario no informare oportunamente
                    cualquiera de los hechos o circunstancias antes mencionadas, se hará(n) acreedor(es) a las sanciones, multas e
                    intereses que imponga la administración por el no pago oportuno de los reajustes de las cuotas o inasistencias
                    a la Asamblea Ordinaria o Extraordinaria.
                </p>
                <p>
                    <u><strong>D&Eacute;CIMO CUARTA.- OBLIGACIONES DEL ARRENDADOR:</strong></u> 1. Entregar AL ARRENDATARIO en la fecha convenida o en el momento de la celebraci&oacute;n del contrato, el inmueble dado en arrendamiento 2. Entregar con el inmueble los servicios, cosas o usos conexos para el fin convenido. 3. Expedir comprobante escrito, donde conste fecha, cuant&iacute;a y per&iacute;odo al cual corresponde el pago del canon de arrendamiento. Art. 11 Ley 820/03 y Num. 4) Art. 8&deg; Dec. 051/04. 4. Diligenciar en el menor tiempo posible, las reparaciones y da&ntilde;os del inmueble. 5. Entregar copia del Contrato de Arrendamiento firmado en su totalidad. 6. Explicar por completo el sentido de firmar un Contrato de Arrendamiento, para aclarar las dudas que surjan del mismo. 7. Elaborar la carta de presentaci&oacute;n del Arrendatario ante la Administraci&oacute;n. <strong>PAR&Aacute;GRAFO PRIMERO:</strong> El ARRENDADOR estar&aacute; sometido al control de la Subsecretaria de Inspecci&oacute;n, Vigilancia y Control de Vivienda de la Secretaria Distrital del H&aacute;bitat. Art. 28 ley 820/03.
                </p>
                <p>
                    <u><strong>D&Eacute;CIMO QUINTA.- DEUDORES SOLIDARIOS:</strong></u> Son deudores solidarios del presente contrato: los indicados en los numerales <strong>1.3, 1.4 y 1.5</strong> del encabezado. los suscritos identificados anteriormente, se declaran deudores DEL ARRENDADOR en forma solidaria e indivisible junto con EL ARRENDATARIO indicado al inicio de este documento, de todas las cargas y obligaciones contenidas en el presente contrato, tanto durante el
                    t&eacute;rmino inicialmente pactado como durante sus pr&oacute;rrogas o renovaciones expresas o t&aacute;citas y <span class="hidden">hasta</span>
                </p>
            </div>
        </div>
        <div class="pie">
            <div class="footer-page">
                Página <strong>4</strong> de <strong>8</strong>
            </div>
            <img alt="" src="{{ asset("assets/img/pie_nuevo.png") }}" class="logo-pie">
        </div>
    </div>
    <div id="p5" style="height: {{ $height }}; width: {{ $width }}; position: relative">
        <div class="header">
            <img alt="" class="logo-encabezado" src="{{ asset("assets/img/logo_encabezado_nuevo.png") }}">
            <h1 class="header-title">CONTRATO DE ARRENDAMIENTO DE INMUEBLE DE VIVIENDA URBANA.</h1>
            <b>Revisado: ABRIL 2022</b>
        </div>
        <div id="p5_contenido" class="pagina">
            <br>
            <div class="terminos-contrato">
                <p>
                    hasta la restituci&oacute;n material del inmueble AL
                    ARRENDADOR. Declaran entonces que responder&aacute;n por el cumplimiento y pago por concepto de c&aacute;nones de arrendamientos, servicios p&uacute;blicos, indemnizaciones, da&ntilde;os en el inmueble, cuotas de administraci&oacute;n, cl&aacute;usulas penales, gastos de cobranza, costas procesales y cualquier otra derivada del presente contrato, las cuales podr&aacute;n ser exigidas por EL ARRENDADOR a cualquiera de los obligados por la v&iacute;a ejecutiva; sin que por raz&oacute;n de esta solidaridad asuman el car&aacute;cter de fiadores ni ARRENDATARIOS del inmueble objeto del presente contrato; pues tal calidad la asume exclusivamente EL ARRENDATARIO y sus respectivos causa habitantes.
                </p>
                <p>
                    <u><strong>D&Eacute;CIMO SEXTA.- PREAVISOS PARA LA ENTREGA:</strong></u> Las partes se obligan,
                    en caso de terminación del contrato, a dar el correspondiente preaviso para la entrega a través
                    del servicio postal autorizado o correo electrónico, certificando su recibido por parte de la Coordinadora
                    del inmueble, con tres (3) meses de anticipación a la finalización del plazo original o de su prórroga;
                    subsistiendo durante dichas prórrogas todas las garantías compromisos y estipulaciones de este contrato.
                </p>
                <p>
                    <u><strong>D&Eacute;CIMO S&Eacute;PTIMA.- RECIBO Y ESTADO:</strong></u> El ARRENDATARIO declara que ha recibido el inmueble objeto de este contrato en buen estado de servicio y presentaci&oacute;n, conforme al inventario que suscribe por separado y que se considera incorporado a este documento; que se obliga a cuidarlo, conservarlo y mantenerlo y, que en el mismo estado, este o sus deudores solidarios lo restituir&aacute;n Al ARRENDADOR, a la terminaci&oacute;n del contrato de arrendamiento o cuando &eacute;ste haya de cesar por alguna de las causales previstas en la ley, salvo el deterioro proveniente del tiempo y uso leg&iacute;timo. Los da&ntilde;os al inmueble derivados del mal trato o descuido por parte del ARRENDATARIO, durante su tenencia, ser&aacute;n de su cargo y EL ARRENDADOR estar&aacute; facultado para hacerlos por su cuenta y posteriormente reclamar su valor AL ARRENDATARIO y/o DEUDORES SOLIDARIOS.
                </p>
                <p>
                    <u><strong>D&Eacute;CIMO OCTAVA.- REPARACIONES Y MANTENIMIENTO:</strong></u> Las reparaciones locativas que tienen como fin,
                    mantener el inmueble en el estado en que se recibió, correrán a cargo DEL ARRENDATARIO, en virtud de lo señalado el artículo
                    9 numeral segundo de la ley 820 de 2003, en concordancia con los artículos 1985, 1987, 1998, 1989, 2028, 2029 y 2030 del
                    Código Civil, Decreto 1077 del 2015, Art. 2.2.6.1.1.10 Reparaciones locativas, y demás normas concordantes;
                    <strong>PARÁGRAFO PRIMERO:</strong> Corresponde AL ARRENDATARIO, efectuar periódicamente el mantenimiento preventivo
                    de los gasodomésticos, electrodomésticos, plomería y pisos, entre otros, que se encuentren en el inmueble.
                    <strong>PARÁGRAFO SEGUNDO:</strong> Corresponden AL ARRENDADOR las reparaciones necesarias no locativas, que son aquellas
                    sin las cuales no se podría continuar usando el inmueble, por lo tanto, son trabajos orientados a la corrección de daños
                    graves, los cuales por su magnitud ponen en peligro y riesgo la estabilidad del bien como unidad física. En caso de que EL
                    ARRENDADOR no hiciere las reparaciones necesarias no locativas, tan pronto como le sea posible, este está obligado a reembolsar
                    a las que hiciere EL ARRENDATARIO, siempre que este último no las haya hecho necesarias por su culpa, y que haya dado
                    noticia AL ARRENDADOR tan pronto como tuviera conocimiento de tales, para que las hiciese por su cuenta.
                    <strong>PARÁGRAFO TERCERO: MEJORAS:</strong> No podrá EL ARRENDATARIO efectuar mejoras de ninguna especie en el
                    inmueble sin permiso escrito DEL ARRENDADOR; por tanto, todas las variaciones y reformas efectuadas por EL ARRENDATARIO
                    al inmueble, serán por cuenta de éste y de cualquier forma aquellas accederán al inmueble, sin lugar a indemnización
                    para quien las efectuó.
                </p>
                <p>
                    <u><strong>D&Eacute;CIMO NOVENA.- SUBARRIENDO Y CESI&Oacute;N:&nbsp;</strong></u>En ningún caso El ARRENDATARIO está facultado para ceder,
                    subarrendar, ni transferir la tenencia del inmueble dado en arrendamiento. <strong>PARÁGRAFO PRIMERO.</strong> En caso de
                    contravención de esta disposición, El ARRENDADOR podrá dar por terminado unilateralmente el contrato de arrendamiento y
                    exigir la entrega inmediata del inmueble arrendado, configurándose así una justa causa para su terminación unilateral.
                    <strong>PARÁGRAFO SEGUNDO:</strong> El ARRENDATARIO, aceptará cualquier cesión que haga El ARRENDADOR del presente contrato,
                    pero la misma no producirá efectos sino hasta cuando se haya notificado AL ARRENDATARIO y a sus DEUDORES SOLIDARIOS, mediante
                    comunicación enviada por correo certificado o correo electrónico. La notificación se entenderá surtida desde la fecha de
                    envío de la citada comunicación; y el cesionario del contrato debe cumplir con lo estipulado en el artículo 28 de la Ley
                    820 del 2003.
                </p>
                <p>
                    <u><strong>VIG&Eacute;SIMA.- CLAUSULA PENAL:</strong></u> LA PARTE incumplida deber&aacute; PAGAR a LA PARTE cumplida a t&iacute;tulo de cl&aacute;usula penal, una suma equivalente a tres c&aacute;nones de arrendamiento, pactados dentro del presente contrato.
                </p>
                <p>
                    <u><strong>VIG&Eacute;SIMA PRIMERA.- INDEMNIZACI&Oacute;N:</strong></u> En caso que El ARRENDATARIO promueva
                    unilateralmente y en forma anticipada la entrega del inmueble dado en arrendamiento, antes del vencimiento
                    del periodo inicial o de sus prorrogas, y sin justa causa para ello, deberá pagar a favor del ARRENDADOR,
                    a título de indemnización el valor equivalente al precio de tres (3) cánones de arriendo, vigente a la fecha
                    en que se presente tal hecho, y con el lleno de los requisitos, de conformidad con el Articulo 24, numeral 4 y
                    Artículo 25 de la Ley 820 de 2003.  Caso contrario si El ARRENDADOR incumpliere esta misma cláusula, deberá pagar
                    a favor DEL ARRENDATARIO la indemnización aquí establecida.
                </p>
                <p>
                    <u><strong>VIG&Eacute;SIMA SEGUNDA.- REQUERIMIENTOS:</strong></u> Las partes renuncian al derecho de retenci&oacute;n que a cualquier t&iacute;tulo les confiera la ley sobre el inmueble materia de este contrato.
                </p>
                <p>
                    <u><strong>VIG&Eacute;SIMA TERCERA.- EXENCI&Oacute;N DE RESPONSABILIDAD:</strong></u> EL ARRENDADOR no asume responsabilidad alguna por los daños o perjuicios que EL ARRENDATARIO pueda sufrir por causas atribuibles a terceros o a otros arrendatarios del mismo inmueble, o la culpa leve DEL ARRENDADOR o de otros arrendatarios o de sus empleados o dependientes, ni por robos, hurtos, plagas repentinas, ni por siniestros causados por incendio, inundación o terrorismo; Serán además de cargo DEL ARRENDATARIO las medidas, la dirección, prevención y el manejo tomado para la seguridad del bien inmueble y sus personales.
                </p>
                <p>
                    <u><strong>VIG&Eacute;SIMA CUARTA.- MANEJO DE DATOS:</strong></u> El ARRENDATARIO y LOS DEUDORES SOLIDARIOS, autorizan de forma expresa AL ARRENDADOR, aseguradora, cesionarios, subrogatarios, y/o de terceros definidos por esta, a realizar el tratamiento de los datos personales que ha suministrado en virtud del presente contrato,
                    para almacenar sus informaci&oacute;n en bases de datos, <span class="hidden">plataformas</span>
                </p>
            </div>
        </div>
        <div class="pie">
            <div class="footer-page">
                Página <strong>5</strong> de <strong>8</strong>
            </div>
            <img alt="" src="{{ asset("assets/img/pie_nuevo.png") }}" class="logo-pie">
        </div>
    </div>
    <div id="p6" style="height: {{ $height }}; width: {{ $width }}; position: relative">
        <div class="header">
            <img alt="" class="logo-encabezado" src="{{ asset("assets/img/logo_encabezado_nuevo.png") }}">
            <h1 class="header-title">CONTRATO DE ARRENDAMIENTO DE INMUEBLE DE VIVIENDA URBANA.</h1>
            <b>Revisado: ABRIL 2022</b>
        </div>
        <div id="p6_contenido" class="pagina">
            <br><br>
            <div class="terminos-contrato">
                <p>
                    plataformas de software interno, soportar procesos de auditor&iacute;a interna y externa, notificar cualquier cambio que se presente en los servicios y para ejecutar cualquier clase de relaci&oacute;n contractual de conformidad con lo establecido en las pol&iacute;ticas de tratamiento de datos personales DEL ARRENDADOR disponibles para consulta en <strong>eliteinmobiliaria.com.co</strong>. As&iacute; mismo autoriza compartir sus datos con funcionarios, terceros vinculados con el desarrollo del presente contrato, seg&uacute;n lo dispuesto y requerido en la Ley 1581 de 2012 y Decreto 1377 de 2013, raz&oacute;n por la cual dicha informaci&oacute;n podr&aacute; ser objeto de disposici&oacute;n, uso, circulaci&oacute;n, actualizaci&oacute;n, procesamiento, almacenamiento, recolecci&oacute;n, exclusi&oacute;n, intercambio, y compilaci&oacute;n para el tratamiento de la informaci&oacute;n o datos personales. De igual manera declara que le informaron de manera clara y comprensible que puede ejercitar derechos de acceso, correcci&oacute;n, supresi&oacute;n, revocaci&oacute;n o reclamo por infracci&oacute;n sobre datos, mediante escrito dirigido AL ARRENDADOR al correo electr&oacute;nico <strong>juridico1@eliteinmobiliaria.com.co</strong> o a la direcci&oacute;n incluida en este contrato.
                </p>
                <p>
                    <u><strong>VIG&Eacute;SIMA QUINTA.- EXIGIBILIDAD Y M&Eacute;RITO EJECUTIVO:</strong></u> Las obligaciones de pagar sumas en dinero a cargo de cualquiera de las partes, ser&aacute;n exigibles ejecutivamente con base en el presente contrato de arrendamiento y de conformidad con lo dispuesto en el C&oacute;digo Civil y en el C&oacute;digo General del Proceso. Respecto de las deudas a cargo del ARRENDATARIO por concepto de servicios p&uacute;blicos domiciliarios o expensas comunes dejadas de pagar, El ARRENDADOR podr&aacute; repetir lo pagado contra El ARRENDATARIO y sus DEUDORES SOLIDARIOS, por la v&iacute;a ejecutiva mediante la presentaci&oacute;n de las facturas, comprobantes o recibos de las correspondientes empresas debidamente pagados y la manifestaci&oacute;n que haga el demandante bajo la gravedad del juramento de que dichas facturas fueron pagadas por &eacute;l, la cual se entender&aacute; prestada con la presentaci&oacute;n de la demanda correspondiente. El ARRENDADOR podr&aacute; dar por terminado unilateralmente con justa causa el presente contrato y exigir la entrega inmediata del inmueble, para lo cual El ARRENDATARIO en los art&iacute;culos 1594 y 2007 del C&oacute;digo Civil.
                </p>
                <p>
                    <u><strong>VIG&Eacute;SIMA SEXTA.- AUTORIZACI&Oacute;N:&nbsp;</strong></u>El ARRENDATARIO y sus DEUDORES SOLIDARIOS autorizan expresamente AL ARRENDADOR, y a su eventual cesionario o subrogatorio para que con los fines de administraci&oacute;n de riesgos, estad&iacute;sticos y de informaci&oacute;n entre compa&ntilde;&iacute;as, entre las entidades de control y supervisi&oacute;n y las autoridades competentes, consulte, almacene, administre, transfiera y reporte a las centrales de informaci&oacute;n o bases de datos que considere necesario, o a cualquier otra entidad autorizada, la informaci&oacute;n relacionada o derivada del presente contrato de arrendamiento. La consecuencia de esta autorizaci&oacute;n ser&aacute; la inclusi&oacute;n de los datos DEL ARRENDATARIO y sus DEUDORES SOLIDARIOS en las mencionadas bases de datos y por tanto las entidades del sector financiero o de cualquier otro sector usuarias de dichas centrales conocer&aacute;n su comportamiento presente y pasado relacionado con las obligaciones financieras o cualquier otro dato personal o econ&oacute;mico que se estime pertinente.
                </p>
                <p>
                    <u><strong>VIG&Eacute;SIMA S&Eacute;PTIMA.- RESTITUCION DEL INMUEBLE:</strong></u> Terminado el presente contrato, El ARRENDATARIO y/o DEUDORES SOLIDARIOS, deber&aacute;n entregar el precitado inmueble Al ARRENDADOR en forma personal o a quien &eacute;ste autorice para recibirlo, conforme al inventario inicial, con el mismo color de pintura con que fue entregado, oblig&aacute;ndose a presentar los recibos de servicios p&uacute;blicos debidamente pagados, junto con el respectivo Paz y
                    Salvo expedido por administraci&oacute;n. Ser&aacute; v&aacute;lida y se entender&aacute; como entrega formal y material del inmueble arrendado la que se realice por instancias judiciales o extrajudiciales como son la conciliaci&oacute;n, ante un centro debidamente acreditado, seg&uacute;n la Ley 640 de 2001, con el fin de realizar el cobro de lo debido y no generar prorroga en los contratos dando aplicaci&oacute;n a los art&iacute;culos 35 y 36 de la ley 820 del 2003, y dem&aacute;s del C&oacute;digo General del Proceso. <strong>PAR&Aacute;GRAFO PRIMERO:</strong> De acuerdo con lo anterior, EL ARRENDADOR NO estar&aacute; obligado a recibir el inmueble en condiciones diferentes a aquellas existentes al momento de la entrega del mismo, como tampoco lo har&aacute; en caso de no entreg&aacute;rsele los recibos citados en la forma se&ntilde;alada. <strong>PAR&Aacute;GRAFO SEGUNDO:</strong> En caso de abandono del inmueble, EL ARRENDATARIO faculta expresamente a cualquiera de sus deudores solidarios para que junto con El ARRENDADOR o quien lo represente, acceda al inmueble objeto del presente contrato y reciba la tenencia del mismo, con el diligenciamiento de ACTA privada, judicial o extrajudicial de ENTREGA, suscrita por aquellos, con anotaci&oacute;n clara del estado en que se encuentre, los faltantes al inventario y los conceptos adeudados que quedaren pendientes como consecuencia del abandono e incumplimiento. EL ARRENDATARIO faculta expresamente AL ARRENDADOR para entrar en el inmueble y recuperar su tenencia, con el solo requisito de la presencia de dos testigos, en procura de evitar el deterioro o el desmantelamiento de tal inmueble siempre que por cualquier circunstancia el mismo permanezca abandonado o deshabitado por el t&eacute;rmino de un mes o m&aacute;s y que la exposici&oacute;n al riesgo sea tal, que amenace la integridad f&iacute;sica del bien o la seguridad del vecindario.
                </p>
                <p>
                    <u><strong>VIG&Eacute;SIMA OCTAVA.- LINDEROS Y ESPACIOS EN BLANCO:</strong></u> Los linderos que identifican el inmueble objeto de arrendamiento, se encuentran como anexos al presente contrato, tanto generales como espec&iacute;ficos, los cuales hacen parte integral y fundamental de este; y El ARRENDATARIO faculta expresamente Al ARRENDADOR para llenar en este documento los espacios en blanco, para aclarar o adicionar lo atinente a los linderos del inmueble arrendado, empleando si es necesario, una p&aacute;gina adicional, que formar&aacute; parte integral del presente contrato.
                </p>
                <p>
                    <u><strong>VIG&Eacute;SIMA NOVENA.- TERMINACI&Oacute;N UNILATERAL DEL CONTRATO:</strong></u> <u><strong>POR PARTE DEL ARRENDADOR:</strong></u> El arrendador podr&aacute; dar por terminado unilateralmente el contrato de arrendamiento en los siguientes eventos: 1. La no cancelaci&oacute;n por parte del arrendatario de las rentas y reajustes dentro del t&eacute;rmino estipulado en el contrato. 2. La no cancelaci&oacute;n de los servicios p&uacute;blicos, que cause la desconexi&oacute;n o p&eacute;rdida del servicio, o el pago de las expensas comunes cuando su pago estuviere a cargo del arrendatario. 3. El subarriendo total o parcial del inmueble, la cesi&oacute;n del contrato o del goce del inmueble o el cambio de destinaci&oacute;n del mismo por parte del arrendatario, sin expresa
                    autorizaci&oacute;n del arrendador. 4. La incursi&oacute;n reiterada del arrendatario en procederes que afecten la tranquilidad ciudadana de los vecinos, o la destinaci&oacute;n del inmueble para actos delictivos o que impliquen contravenci&oacute;n, debidamente comprobados ante la autoridad policiva.
                    5. La realizaci&oacute;n de mejoras, cambios o ampliaciones del inmueble, sin expresa autorizaci&oacute;n del arrendador o la destrucci&oacute;n total o parcial del inmueble o &aacute;rea arrendada por parte del arrendatario.
                    6. La violaci&oacute;n por el arrendatario a las normas del respectivo reglamento de propiedad horizontal cuando se trate de viviendas sometidas a ese r&eacute;gimen.
                    7. El arrendador podr&aacute; dar por terminado unilateralmente el contrato de arrendamiento durante <span class="hidden">las pr&oacute;rrogas,</span>
                </p>
            </div>
        </div>
        <div class="pie">
            <div class="footer-page">
                Página <strong>6</strong> de <strong>8</strong>
            </div>
            <img alt="" src="{{ asset("assets/img/pie_nuevo.png") }}" class="logo-pie">
        </div>
    </div>

    <div id="p7" style="height: {{ $height }}; width: {{ $width }}; position: relative">
        <div class="header">
            <img alt="" class="logo-encabezado" src="{{ asset("assets/img/logo_encabezado_nuevo.png") }}">
            <h1 class="header-title">CONTRATO DE ARRENDAMIENTO DE INMUEBLE DE VIVIENDA URBANA.</h1>
            <b>Revisado: ABRIL 2022</b>
        </div>
        <div id="p7_contenido" class="pagina">
            <br><br>
            <div class="terminos-contrato">
                <p>
                    las pr&oacute;rrogas, previo aviso escrito dirigido al arrendatario a trav&eacute;s del servicio postal autorizado, con una antelaci&oacute;n no menor de tres (3) meses y el pago de una indemnizaci&oacute;n equivalente al precio de tres (3) meses de arrendamiento. Cumplidas estas condiciones el arrendatario estar&aacute; obligado a restituir el inmueble. 8. El arrendador podr&aacute; dar por terminado unilateralmente el contrato de arrendamiento a la fecha de vencimiento del t&eacute;rmino inicial o de sus pr&oacute;rrogas invocando cualquiera de las siguientes causales especiales de restituci&oacute;n, previo aviso escrito al arrendatario a trav&eacute;s del servicio postal autorizado con una antelaci&oacute;n no menor a tres (3) meses a la referida fecha de vencimiento: a) Cuando el propietario o poseedor del inmueble necesitare ocuparlo para su propia habitaci&oacute;n, por un t&eacute;rmino no menor de un (1) a&ntilde;o;<u><strong>&nbsp;</strong></u>b) Cuando el inmueble haya de demolerse para efectuar una nueva construcci&oacute;n, o cuando se requiere desocuparlo con el fin de ejecutar obras independientes para su reparaci&oacute;n; c) Cuando haya de entregarse en cumplimiento de las obligaciones originadas en un contrato de compraventa; d) La plena voluntad de dar por terminado el contrato, siempre y cuando, el contrato de arrendamiento cumpliere como m&iacute;nimo cuatro (4) a&ntilde;os de ejecuci&oacute;n. El arrendador deber&aacute; indemnizar al arrendatario con una suma equivalente al precio de uno punto cinco (1.5) meses de arrendamiento. 9. En los dem&aacute;s eventos de incumplimiento contemplados en este contrato y en la ley civil.&nbsp;Y deber&aacute; cumplir con los siguientes requisitos: <strong>a)</strong> Comunicar a trav&eacute;s del servicio postal autorizado al arrendatario o a su representante legal, con la antelaci&oacute;n all&iacute; prevista, indicando la fecha para la terminaci&oacute;n del contrato y, manifestando que se pagar&aacute; la indemnizaci&oacute;n de ley (cuando esta proceda) ; <strong>b)</strong> Consignar a favor del arrendatario y a &oacute;rdenes de la autoridad competente, la indemnizaci&oacute;n de que trata el art&iacute;culo anterior de la presente ley, dentro de los tres (3) meses anteriores a la fecha se&ntilde;alada para la terminaci&oacute;n unilateral del contrato. La consignaci&oacute;n se efectuar&aacute; en las entidades autorizadas por el Gobierno Nacional para tal efecto y la autoridad competente allegar&aacute; copia del t&iacute;tulo respectivo al arrendatario o le enviar&aacute; comunicaci&oacute;n en que se haga constar tal circunstancia, inmediatamente tenga conocimiento de esta. El valor de la indemnizaci&oacute;n se har&aacute; con base en la renta vigente a la fecha del preaviso; <strong>c)</strong> Al momento de efectuar la consignaci&oacute;n se dejar&aacute; constancia en los respectivos t&iacute;tulos de las causas de la misma como tambi&eacute;n el nombre y direcci&oacute;n precisa Del ARRENDATARIO o su representante; <strong>d)</strong> Si el arrendatario cumple con la obligaci&oacute;n de entregar el inmueble en la fecha se&ntilde;alada, recibir&aacute; el pago de la indemnizaci&oacute;n, de conformidad con la autorizaci&oacute;n que expida la autoridad competente. <strong>PAR&Aacute;GRAFO PRIMERO:</strong> En caso de que El ARRENDATARIO no entregue el inmueble, El ARRENDADOR tendr&aacute; derecho a que se le devuelva la indemnizaci&oacute;n consignada, sin perjuicio de que pueda iniciar el correspondiente proceso de restituci&oacute;n del inmueble. <strong>PAR&Aacute;GRAFO SEGUNDO:</strong> Si El ARRENDADOR con la aceptaci&oacute;n del arrendatario desiste de dar por terminado el contrato de arrendamiento, podr&aacute; solicitar a la autoridad competente, la autorizaci&oacute;n para la devoluci&oacute;n de la suma consignada. <u><strong>POR PARTE DEL ARRENDATARIO:</strong></u> El ARRENDATARIO podr&aacute; dar por terminado unilateralmente el contrato de arrendamiento en los siguientes eventos: 1. La suspensi&oacute;n de la prestaci&oacute;n de los servicios p&uacute;blicos al inmueble, por acci&oacute;n premeditada del arrendador o porque incurra en mora en pagos que estuvieren a su cargo. En estos casos el arrendatario podr&aacute; optar por asumir el costo del restablecimiento del servicio y descontarlo de los pagos que le corresponda hacer como arrendatario. 2. La incursi&oacute;n reiterada del arrendador en procederes que afecten gravemente el disfrute cabal por el arrendatario del inmueble arrendado, debidamente comprobada ante la autoridad policiva. 3. El desconocimiento por parte del arrendador de derechos reconocidos al arrendatario por la Ley o contractualmente. 4. El arrendatario podr&aacute; dar por terminado unilateralmente el contrato de arrendamiento a la fecha de vencimiento del t&eacute;rmino inicial o de sus pr&oacute;rrogas, siempre y cuando d&eacute; previo aviso escrito al arrendador a trav&eacute;s del servicio postal autorizado, con una antelación no menor de tres (3) meses a la referida
                    fecha de vencimiento. En este caso el arrendatario no estará obligado a invocar causal
                    alguna diferente a la de su plena voluntad, ni deberá indemnizar al arrendador. 5.
                    El arrendatario podrá dar por terminado unilateralmente el contrato de arrendamiento
                    dentro del término inicial o durante sus prórrogas, previo aviso escrito dirigido al
                    arrendador a través del servicio postal autorizado, con una antelación no menor de tres
                    (3) meses y el pago de una indemnización equivalente al precio de tres (3) meses de
                    arrendamiento, en este evento, deberá cumplir con los siguientes requisitos: <b>a)</b> Comunicar a
                    través del servicio postal autorizado Al ARRENDADOR o a su representante legal, con la antelación
                    allí prevista, indicando la fecha para la terminación del contrato y, manifestando que se pagará la
                    indemnización de ley. <b>b)</b> Consignar a favor del arrendador y a órdenes de la autoridad competente,
                    la indemnización de que trata el artículo anterior de la presente ley, dentro de los tres (3) meses
                    anteriores a la fecha señalada para la terminación unilateral del contrato. La consignación se efectuará
                    en las entidades autorizadas por el Gobierno Nacional para tal efecto y la autoridad competente allegará
                    copia del título respectivo Al ARRENDADOR o le enviará comunicación en que se haga constar tal circunstancia,
                    inmediatamente tenga conocimiento de esta. El valor de la indemnización se hará con base en la
                    renta vigente a la fecha del preaviso; <b>c)</b> Al momento de efectuar la consignación se dejará
                    constancia en los respectivos títulos de las causas de esta como también el
                    nombre y dirección precisa DEL ARRENDATARIO o su representante; <b>d)</b> Si EL ARRENDADOR cumple con la obligación de recibir el inmueble
                    en la fecha señalada, recibirá el pago de la indemnización, de conformidad con la autorización que expida la
                    autoridad competente.  <b>PARÁGRAFO PRIMERO:</b> En caso de que el arrendador no reciba el inmueble, el arrendatario
                    tendrá derecho a que se le devuelva la indemnización consignada, sin perjuicio de que pueda realizar la
                    entrega provisional del inmueble de conformidad con lo previsto en el artículo anterior. <b>PARÁGRAFO SEGUNDO:</b>
                    Si El ARRENDATARIO con la aceptación DEL ARRENDADOR desiste de dar por terminado el contrato de arrendamiento,
                    podrá solicitar a la autoridad competente, la autorización para la devolución de la suma consignada. 6.
                    En los demás eventos de incumplimiento contemplados en este contrato y en la ley civil.
                </p>

                <p>
                    <b><u>TRIGÉSIMA.- CLÁUSULA ESPECIAL:</u></b> Conocen EL ARRENDATARIO y LOS DEUDORES SOLIDARIOS,
                    que el presente contrato ha sido pactado dentro del marco de Emergencia   Económica social y  Ecológica,
                    a nivel nacional   con ocasión de la pandemia del  COVID-19. En este orden, comprenden y aceptan que
                    no podrán sustraerse de cumplir con las obligaciones aquí pactadas citando la situación mencionada como
                    causal de exoneración de responsabilidades, de terminación de contrato o de exoneración del pago de las
                    sanciones aquí establecidas, pues declaran que se encuentran actualmente incluso en el marco señalado,
                    en las condiciones de cumplir a cabalidad con estas.
                </p>
            </div>
        </div>
        <div class="pie">
            <div class="footer-page">
                Página <strong>7</strong> de <strong>8</strong>
            </div>
            <img alt="" src="{{ asset("assets/img/pie_nuevo.png") }}" class="logo-pie">
        </div>
    </div>
    <div id="p8" style="height: {{ $height }}; width: {{ $width }}; position: relative">
        <div class="header">
            <img alt="" class="logo-encabezado" src="{{ asset("assets/img/logo_encabezado_nuevo.png") }}">
            <h1 class="header-title">CONTRATO DE ARRENDAMIENTO DE INMUEBLE DE VIVIENDA URBANA.</h1>
            <b>Revisado: ABRIL 2022</b>
        </div>
        <div id="p8_contenido" class="pagina">
            <p style=" text-align: justify">
                Para constancia se firma de forma electrónica, por las partes, ante dos (2) testigos, el día <i>fecha_firma_contrato</i>; declaramos que hemos recibido copia íntegra del presente contrato de arrendamiento al correo registrado por las partes. Para efectos de recibir notificaciones judiciales y extrajudiciales, las partes en cumplimiento del Art. 12 de la Ley 820 de 2003, a continuación, y al suscribir este contrato proceden a indicar sus respectivas direcciones.
            </p>
            <table style="width: 100%;border-style: solid; border-width: 2px;border-collapse: collapse;">
                <tbody>
                    <tr>
                        <td colspan="2" class="table-row-title"><br><br><br><br><br></td>
                        <td colspan="2" class="table-row-title"><br><br><br><br><br></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border-right: solid; border-width:1px; "><b><u>EL ARRENDADOR</u></b><br><br></td>
                        <td colspan="2" style=""><b><u>EL ARRENDATARIO</u></b><br><br></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border-right: solid; border-width:1px;"><b>INVERSIONES ÉLITE GROUP SAS</b></td>
                        <td colspan="2"><b>Nombre_Arrendatario_Var</b></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border-right: solid; border-width:1px;">900.541.111-5</td>
                        <td><b>C.C/NIT</b> CCArrendatario_Var</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border-right: solid; border-width:1px;"><b>FABIO RUBIO LOZAN</b>O - 16.277.870 de Palmira</td>
                        <td><b>Email</b> EmailArrendatario_Var</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border-right: solid; border-width:1px;">frubio@eliteinmobiliaria.com.co<br><br></td>
                        <td colspan="2"><br><br></td>
                    </tr>
                </tbody>
            </table>
            <br>

            <table style="table-layout: fixed; width: 100%; border-style: solid; border-width: 2px; border-collapse: collapse;">
                <tbody>
                    <tr>
                        <td class="table-row-title"><br><br><br><br><br></td>
                        <td class="table-row-title"><br><br><br><br><br></td>
                        <td class="table-row-title"><br><br><br><br><br></td>
                    </tr>
                    <tr>
                        <td style="border-right: solid; border-width:1px;"><b><u>CODEUDOR</u></b><br><br></td>
                        <td style="border-right: solid; border-width:1px;"><b><u>CODEUDOR</u></b><br><br></td>
                        <td style="border-right: solid; border-width:1px;"><b><u>CODEUDOR</u></b><br><br></td>
                    </tr>
                    <tr>
                        <td style="border-right: solid; border-width:1px;"><b>Nombre_Deudor1_Var</b></td>
                        <td style="border-right: solid; border-width:1px;"><b>Nombre_Deudor2_Var</b></td>
                        <td style="border-right: solid; border-width:1px;"><b>Nombre_Deudor3_Var</b></td>
                    </tr>
                    <tr>
                        <td style="border-right: solid; border-width:1px;">C.C/NIT: CCDeudor1_Var</td>
                        <td style="border-right: solid; border-width:1px;">C.C/NIT: CCDeudor2_Var</td>
                        <td style="border-right: solid; border-width:1px;">C.C/NIT: CCDeudor3_Var</td>
                    </tr>
                    <tr>
                        <td style="border-right: solid; border-width:1px;">EMAIL:<br><br> EmailDeudor1_Var<br><br></td>
                        <td style="border-right: solid; border-width:1px;">EMAIL:<br><br> EmailDeudor2_Var<br><br></td>
                        <td style="border-right: solid; border-width:1px;">EMAIL:<br><br> EmailDeudor3_Var<br><br></td>
                    </tr>
                </tbody>
            </table>

            <br>
            <table style="width: 100%;border-style: solid; border-width: 2px;border-collapse: collapse;">
                <tbody>
                    <tr>
                        <td class="table-row-title"><br><br><br><br><br></td>
                        <td class="table-row-title"><br><br><br><br><br></td>
                    </tr>
                    <tr>
                        <td style="border-right: solid; border-width:1px;"><b><u>TESTIGO</u></b><br><br></td>
                        <td style=""><b><u>TESTIGO</u></b><br><br></td>
                    </tr>
                    <tr>
                        <td style="border-right: solid; border-width:1px;">
                            <b>{{ $arriendo->coordinador->nombres }} {{ $arriendo->coordinador->apellidos }} - {{ $arriendo->coordinador->departamento }}</b>
                        </td>
                        <td><b>Jorge Andrés Chois Muriel - Depto. Jurídico</b></td>
                    </tr>
                    <tr>
                        <td style="border-right: solid; border-width:1px;">KR 58 128B 34 LO 1, Las Villas - Bogotá, Cundinamarca.</td>
                        <td>KR 58 128B 34 LO 1, Las Villas - Bogotá, Cundinamarca.</td>
                    </tr>
                    <tr>
                        <td style="border-right: solid; border-width:1px;"><b>{{ $arriendo->coordinador->telefono }}</b></td>
                        <td>7448205</td>
                    </tr>
                    <tr>
                        <td style="border-right: solid; border-width:1px;">{{ $arriendo->coordinador->email }}<br><br></td>
                        <td>juridico1@eliteinmobiliaria.com.co<br><br></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="pie">
            <div class="footer-page">
                Página <strong>8</strong> de <strong>8</strong>
            </div>
            <img alt="" src="{{ asset("assets/img/pie_nuevo.png") }}" class="logo-pie">
        </div>
    </div>
</body>
</html>
