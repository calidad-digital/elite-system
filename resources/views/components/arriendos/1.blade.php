<section role="main" class="content-body" ng-controller="Arriendos" ng-cloak>
    <form id=" formulario-crear-arriendo" name="formulario-crear-arriendo" action="{{ url("admin/arriendos/crear") }}"
        method="post" enctype="multipart/form-data">
        @csrf
        <header class="page-header">
            <h2>Arriendo</h2>
            <div class="right-wrapper text-right">
                <ol class="breadcrumbs">
                    <li>
                        <a href="#">
                            <i class="bx bx-home-alt"></i>
                        </a>
                    </li>
                    <li><span>Arriendos</span></li>
                    <li><span>Nuevo arriendo&nbsp;&nbsp;&nbsp;</span></li>
                </ol>
                <a class="" data-open=""></a>
            </div>
        </header>

        <input type="hidden" name="inmueble_id" value="{{ $inmueble->id }}" id="inmueble_id">

        <div class="row">
            <section class="card col-lg-12">
                <header class="card-header">
                    <h2 class="card-title">FORMULARIO PARA ELABORACIÓN CONTRATO DE ARRENDAMIENTO DE VIVIENDA URBANA Y/O
                        LOCAL COMERCIAL</h2>
                </header>
                <div class="card-body">

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="agente_captador">Agente captador</label>
                            <input type="text" class="form-control"
                                value="{{$inmueble->asesores->nombres}} {{ $inmueble->asesores->apellidos}}" disabled>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="agente_captador">Agente colocador/empresa</label>
                            <input type="text" class="form-control"
                                value="{{$inmueble->asesores->nombres}} {{$inmueble->asesores->apellidos}}" disabled>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="banios">Código MLS</label>
                            <input type="text" class="form-control" value="{{$inmueble->codigoMLS}}" disabled>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="banios">Destinación</label>
                            <input type="text" class="form-control" value="{{$inmueble->destinos->nombre}}" disabled>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="coordinadora">Coordinadora</label>
                            <select class="form-control" name="coordinadora">
                                <option value="">Seleccione</option>
                                @foreach($coordinadoras as $coor)
                                <option value="{{$coor->id}}">{{$coor->nombres}} {{$coor->apellidos}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="parqueaderos_cubiertos"># solicitud aseguradora</label>
                            <input type="text" class="form-control" name="solicitud_aseguradora" placeholder="1234568">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="parqueaderos_descubiertos">Nombre del propietario</label>
                            <input type="text" class="form-control"
                                value="{{$inmueble->clientes[0]->clientes->nombres??''}} {{$inmueble->clientes[0]->clientes->apellidos??''}}"
                                disabled>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="parqueaderos_descubiertos">Cédula del propietario</label>
                            <input type="text" class="form-control"
                                value="{{$inmueble->clientes[0]->clientes->documento??'N/A'}}" disabled>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-8">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="">Dirección</label>
                                    <input type="text" class="form-control" value="{{ $inmueble->direccion }}" disabled>
                                </div>
                                <div class="col-md-3">
                                    <label for="">Nombre del conjunto</label>
                                    <input type="text" class="form-control" value="{{ $inmueble->direcciones->complemento ?? "" }}" disabled>
                                </div>
                                <div class="col-md-3">
                                    <label for="">CASA, APTO, BODEGA, OFICINA</label>
                                    <input type="text" class="form-control" value="{{ $inmueble->direcciones->complemento2 ?? "" }}" disabled>
                                </div>
                                <div class="col-md-3">
                                    <label for="">BLOQUE, LOTE, MANZANA. TORRE</label>
                                    <input type="text" class="form-control" value="{{ $inmueble->direcciones->complemento3 ?? "" }}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="area_cons">Valor total alquiler</label>
                            <input type="text" class="form-control price_input"
                                ng-init="total_alquiler = ({{$inmueble->canon  + $inmueble->administracion}}| currency:'':'0')"
                                ng-model="total_alquiler" name="valor_total" readonly>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="area_cons">Canon</label>
                            <input type="text" class="form-control price_input"
                                ng-init="canon = ({{$inmueble->canon}} | currency:'':'0')" ng-change="cambiarArriendo()"
                                ng-model="canon" name="canon">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="area_cons">Admon con descuento</label>
                            <input type="text" class="form-control price_input"
                                ng-init="admon = ({{$inmueble->administracion}}| currency:'':'0')"
                                ng-change="cambiarArriendo()" ng-model="admon" name="admon">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="">Valor admon sin descuento</label>
                            <input type="text" class="form-control price_input" name="admon_no_desc">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="area_cons">Valor caldera</label>
                            <input type="text" class="form-control price_input" name="caldera">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="area_cons">Valor parqueadero</label>
                            <input type="text" class="form-control price_input" name="parqueadero">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="area_cons">Valor lavandería</label>
                            <input type="text" class="form-control price_input" name="lavanderia">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <b> Usos conexos</b>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="habitaciones" data-toggle="tooltip" data-placement="top"
                                data-original-title="Recuerde que el contrato inicia el 1 de cada mes">Fecha
                                de entrega inmueble</label>
                            <input type="date" class="form-control" name="fecha_entrega"
                                placeholder="13/09/2021 Hora: 10:00 am">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="fecha_firma_contrato" data-toggle="tooltip" data-placement="top" data-original-title="Recuerde que el contrato inicia el 1 de cada mes">
                                Fecha de firma del contrato
                            </label>
                            <input type="date" class="form-control" name="fecha_firma_contrato"
                                placeholder="13/09/2021 Hora: 10:00 am">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for=""># contrato acueducto</label>
                            <input type="text" class="form-control" name="contrato_acueducto">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="area_cons"># contrato energía</label>
                            <input type="text" class="form-control" name="contrato_energia">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="area_cons"># contrato gas</label>
                            <input type="text" class="form-control" name="contrato_gas">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="">Nombre conjunto</label>
                            <input type="text" class="form-control" name="nombre_conjunto" value="{{$inmueble->direcciones->complemento ?? ""}}">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="create_user_id">Agente encargado</label>
                            <select name="create_user_id" class="form-control">
                                <option value="">Agente encargado</option>
                                @foreach ($agentes as $agt)
                                    <option value="{{ $agt->id }}" {{ $agt->id == $usuario["id"] ? "selected" : "" }}>
                                        {{ $agt->nombres }} {{ $agt->apellidos }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="matricula"># Matrícula</label>
                            <input type="text" class="form-control" value="{{ $inmueble->matricula }}" name="matricula" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="chip">Chip</label>
                            <input type="text" class="form-control" value="{{ $inmueble->chip }}" name="chip" disabled>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="cedula_catastral">Cédula catastral</label>
                            <input type="text" class="form-control" value="{{$inmueble->cedula_catastral}}" name="cedulacatastral" disabled>
                        </div>
                    </div>

                    @for ($i = 1; $i <= 3; $i++)
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="numero_parqueadero_{{ $i }}"># PQ{{ $i }}</label>
                                <input type="text" class="form-control" value="{{ $inmueble["numero_parqueadero_{$i}"] }}" name="numero_parqueadero_{{ $i }}" placeholder="80">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="matricula_parqueadero_{{ $i }}"># Matrícula</label>
                                <input type="text" class="form-control" value="{{ $inmueble["matricula_parqueadero_{$i}"] }}" name="matricula_parqueadero_{{ $i }}" placeholder="60">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="chip_parqueadero_{{ $i }}"># Chip</label>
                                <input type="text" class="form-control" value="{{ $inmueble["chip_parqueadero_{$i}"] }}" name="chip_parqueadero_{{ $i }}" placeholder="60">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="cedula_parqueadero_{{ $i }}"># Cédula Catastral</label>
                                <input type="text" class="form-control" value="{{ $inmueble["cedula_parqueadero_{$i}"] }}" name="cedula_parqueadero_{{ $i }}" placeholder="60">
                            </div>
                        </div>
                    @endfor

                    @for ($i = 1; $i <= 2; $i++)
                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="numero_deposito_{{ $i }}"># DP{{ $i }}</label>
                                <input type="text" class="form-control" value="{{ $inmueble["numero_deposito_{$i}"] }}" name="numero_deposito_{{ $i }}" placeholder="80">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="matricula_deposito_{{ $i }}"># Matrícula</label>
                                <input type="text" class="form-control" value="{{ $inmueble["matricula_deposito_{$i}"] }}" name="matricula_deposito_{{ $i }}" placeholder="60">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="chip_deposito_{{ $i }}"># Chip</label>
                                <input type="text" class="form-control" value="{{ $inmueble["chip_deposito_{$i}"] }}" name="chip_deposito_{{ $i }}" placeholder="60">
                            </div>
                            <div class="form-group col-md-3">
                                <label for="cedula_deposito_{{ $i }}"># Cédula Catastral</label>
                                <input type="text" class="form-control" value="{{ $inmueble["cedula_deposito_{$i}"] }}" name="cedula_deposito_{{ $i }}" placeholder="60">
                            </div>
                        </div>
                    @endfor

                </div>
            </section>

        </div>
        <br>
        <div class="row">

            <section class="card col-lg-12">
                <header class="card-header">
                    <h2 class="card-title">Seguros</h2>
                </header>

                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            El libertador
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="habitaciones">Amparo Básico: Mensual - 2,57% del Canon y Admón</label>
                            <select class="form-control" name="amparo_basico" required>
                                <option value="">Seleccione</option>
                                <option value="1">Si</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones">Amparo Hogar: Mensual - $20.000 o $25.000 del Canon</label>
                            <select class="form-control" name="amparo_hogar">
                                <option value="">Seleccione</option>
                                <option value="1">Si</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones">Amparo Integral: 1 vez - $123.100 X Millón Asegurado</label>
                            <select class="form-control" name="amparo_integral">
                                <option value="">Seleccione</option>
                                <option value="1">Si</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="habitaciones">Notas</label>
                            <input type="text" class="form-control" name="notas_seguros">
                        </div>
                    </div>

                </div>
            </section>

        </div>
        <br>
        <div class="row">
            <section class="card col-lg-12">
                <header class="card-header">
                    <h2 class="card-title">Arrendatario</h2>
                </header>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="habitaciones">Nombre</label>
                            <input type="text" class="form-control" name="arrendatario_nombre" placeholder="Pedro">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones">Apellido</label>
                            <input type="text" class="form-control" name="arrendatario_apellido"
                                placeholder="Perez Galindo">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="banios">Cédula</label>
                            <input type="text" class="form-control" name="arrendatario_cedula" placeholder="31950070">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="banios">Dirección trabajo</label>
                            <input type="text" class="form-control" name="arrendatario_direccion">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="habitaciones">Email</label>
                            <input type="text" class="form-control" name="arrendatario_email" placeholder="@">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="banios">Celular</label>
                            <input type="text" class="form-control" name="arrendatario_celular"
                                placeholder="3101230001">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="parqueaderos">Cédula </label>
                            <input type="file" name="arrendatario_cedula_url" />
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <br>
        <input type="hidden" name="cantidad_codeudores" value="<%cantidad_codeudores%>">
        <section class="card col-lg-12">
            <header class="card-header correct-margin">
                <h2 class="card-title">Codeudores <span ng-show="cantidad_codeudores>=2" class="btn-add"
                        ng-click="removeCodeudor()">-</span> <span class="btn-add" ng-click="addCodeudor()">+</span>
                </h2>
            </header>
            <div class="row" ng-repeat="cod in codeudores">
                <div class="card-body">
                    <b>Codeudor <%cod%></b>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="habitaciones">Nombres y apellidos</label>
                            <input type="text" class="form-control" name="<%cod%>_codeudor_nombre"
                                placeholder="Pedro Perez Galindo">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="banios">Cédula</label>
                            <input type="text" class="form-control" name="<%cod%>_codeudor_cedula"
                                placeholder="31950070">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="banios">Dirección trabajo</label>
                            <input type="text" class="form-control" name="<%cod%>_codeudor_direccion">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="habitaciones">Email</label>
                            <input type="text" class="form-control" name="<%cod%>_codeudor_email" placeholder="@">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="banios">Celular</label>
                            <input type="text" class="form-control" name="<%cod%>_codeudor_celular"
                                placeholder="3101230001">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="parqueaderos">Cédula </label>
                            <input type="file" name="<%cod%>_codeudor_cedula_url" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <br>
        <input type="hidden" name="cantidad_ocupantes" value="<%cantidad_ocupantes%>">
        <div class="row">
            <section class="card col-lg-12">
                <header class="card-header">
                    <h2 class="card-title">Información de personas a ocupar el inmueble <span
                            ng-show="cantidad_ocupantes>=2" class="btn-add" ng-click="removeOcupante()">-</span> <span
                            class="btn-add" ng-click="addOcupante()">+</span></h2>
                </header>
                <div class="card-body" ng-repeat="ocu in ocupantes">
                    <div class="form-row">
                        <div class="form-group col-md-8">
                            <label for="habitaciones">Nombres y apellidos</label>
                            <input type="text" class="form-control" name="<%ocu%>_ocupante_nombre"
                                placeholder="Pedro Perez Galindo">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="banios">Cédula</label>
                            <input type="text" class="form-control" name="<%ocu%>_ocupante_documento"
                                placeholder="31950070">
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <br>

        <div class="row">

            <section class="card col-lg-12">
                <header class="card-header">
                    <h2 class="card-title">Documentos</h2>
                </header>
                <div class="card-body">
                    <div class="form-row">
                        <!-- linea 1 -->
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top" data-original-title="Contrato de captación, firmado con el propietario del inmueble o su apoderado: En caso de haber
un apoderado se debe remitir un ejemplar original del poder amplio y suficiente o de las escritura
pública si es poder general.">Contrato de captación</label>
                            <br>
                            <input type="file" name="contrato_captacion_url" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Certificado de Libertad y Tradición del(los) inmuebles objeto de arrendamiento, menor a 30 días">CLT</label>
                            <br>
                            <input type="file" id="clt_parqueadero_1_url" name="clt_url" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Certificado de tradición del parqueadero menor a días (si aplica)">CLT
                                parqueadero</label>
                            <br>
                            <input type="file" id="clt_parqueadero_1_url" name="clt_parqueadero_url" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Certificado de tradición del depósito menor a 30 días (si aplica)">CLT
                                depósito</label>
                            <br>
                            <input type="file" id="clt_deposito_1_url" name="clt_deposito_url" />
                        </div>
                        <!-- linea 2 -->

                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Certificado de tradición del parqueadero 2 menor a días (si aplica)">CLT
                                parqueadero 2</label>
                            <br>
                            <input type="file" id="clt_parqueadero_2_url" name="clt_parqueadero2_url" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Certificado de tradición del depósito 2 menor a 30 días (si aplica)">CLT
                                depósito 2</label>
                            <br>
                            <input type="file" id="clt_deposito_2_url" name="clt_deposito2_url" />
                        </div>
                        <!-- linea 3 -->
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Escritura Pública con la que nuestro cliente vendedor adquirio el derecho real de dominio del inmueble">Entrega
                                de Pre-Inventario</label>
                            <br>
                            <input type="file" id="entrega_inventario_url" name="entrega_inventario_url" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Mandato de administración del inmueble: firmado en su totalidad y todas las páginas, por los propietarios o por sus apoderados.">Mandato
                                de administración del inmueble</label>
                            <br>
                            <input type="file" id="mandanto_admon_url" name="mandanto_admon_url" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Recibo del Predial del año en curso, del inmueble a arrendar (no se requiere el soporte de pago)">Recibo
                                del Predial del año en curso</label>
                            <br>
                            <input type="file" id="recibo_predial_url" name="recibo_predial_url" />
                        </div>
                        <!-- linea 4 -->
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Facturas de servicios públicos más recientes pagadas">Facturas de
                                servicios públicos</label>
                            <br>
                            <input type="file" id="servicios_publicos_url" name="servicios_publicos_url" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Cuenta de cobro de la administración del edificio pagada en su último mes.">Cuenta
                                de cobro de administración</label>
                            <br>
                            <input type="file" id="cuenta_cobro_admon_url" name="cuenta_cobro_admon_url" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Escritura pública de los linderos del inmueble a arrendar, debe contener portada y firmas.">Escritura
                                pública de los linderos del inmueble</label>
                            <br>
                            <input type="file" id="escritura_linderos_url" name="escritura_linderos_url" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Aprobación de la aseguradora">Aprobación de la aseguradora</label>
                            <br>
                            <input type="file" id="aprobacion_aseguradora_url" name="aprobacion_aseguradora_url" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Entrega de inventario para amoblados. Si aplica.">Entrega de
                                inventario para amoblados</label>
                            <br>
                            <input type="file" id="entrega_amoblados_url" name="entrega_amoblados_url" />
                        </div>

                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Certificado de Cámara de Comercio, con una vigencia no mayor a 30 días, Cédula de Ciudadanía del del Representante Legal y RUT. Si aplica.">Certificado
                                de Cámara de Comercio</label>
                            <br>
                            <input type="file" id="camara_comercio_url" name="camara_comercio_url" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Leasing habitacional: Certificado de subarriendos emitido por la entidad financiera respectiva.">Leasing
                                habitacional</label>
                            <br>
                            <input type="file" id="leasing_url" name="leasing_url" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Escritura pública de constitución de usufructo. Si aplica.">Escritura
                                pública de constitución de usufructo</label>
                            <br>
                            <input type="file" id="usufructo_url" name="usufructo_url" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="RUT: Sólo si es responsable de IVA.">RUT</label>
                            <br>
                            <input type="file" id="rut_url" name="rut_url" />
                        </div>
                        <div class="form-group col-md-12">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="RUT: Sólo si es responsable de IVA."><strong>Para el arrendamiento
                                    de locales comerciales, se debe aportar, además:</strong></label>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top"
                                data-original-title="Uso permitido del suelo">Uso permitido del suelo</label>
                            <br>
                            <input type="text" class="form-control" name="uso_suelo_permitido">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top" data-original-title="">Valor
                                del Incremento anual del canon:</label>
                            <br>
                            <input type="text" class="form-control" name="incremento_anual">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="video" data-toggle="tooltip" data-placement="top" data-original-title="">¿Qué
                                uso del suelo se le dará al inmueble?</label>
                            <br>
                            <input type="text" class="form-control" name="uso_suelo_inmueble">
                        </div>
                    </div>

                </div>


                <footer class="card-footer">
                    <div class="row justify-content-end">
                        <div class="col-sm-12">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <button type="submit" class="btn btn-primary" name="revision" value="0">
                                Guardar formulario de arrendamiento
                            </button>
                            <button type="submit" class="btn btn-primary" name="revision" value="1">
                                Guardar formulario y enviar a revisión
                            </button>
                        </div>
                    </div>
                </footer>
            </section>



        </div>
    </form>
</section>
