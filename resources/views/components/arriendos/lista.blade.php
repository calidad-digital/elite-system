<section role="main" class="content-body">
    <header class="page-header">
        <h2>Arriendos</h2>
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li>
                    <span>
                        Arriendos
                    </span>
                </li>
            </ol>
            <a class="" data-open=""></a>
        </div>
    </header>
    <div class="row" ng-controller="Ventas">

        <div class="col-lg-12">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Listado de arriendos</h2>
                </header>
                <div class="card-body">
                    <form action="" method="get" ng-controller="Formulario">
                        <div class="row">
                            <div class="form-group col-md-3" ng-init="usuario = '{{ $request["usuario"] ?? "" }}'">
                                <label for="usuario" class="sr-only">Agente</label>
                                <select name="usuario" class="form-control" ng-model="usuario" id="usuario" {{ ($usuario->rol_id == 1 || $usuario->rol_id == 6 || $usuario->rol_id == 10) ? '' : 'disabled' }}>
                                    <option value="">Agente</option>
                                    @foreach ($agentes as $agt)
                                        <option value="{{$agt->id}}" {{ isset($request["usuario"]) && $request["usuario"] == $agt->id ? "selected" : "" }}>
                                            {{ $agt->nombres }} {{ $agt->apellidos }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <button class="btn btn-primary">Buscar</button>
                            </div>
                        </div>
                    </form>

                    <!-- VENTAS en estado pendientes -->

                    <table class="table table-bordered table-striped mb-0" id="datatable-default"
                    ng-controller="Arriendos">
                    <thead>
                        <tr>
                            <th>MLS</th>
                            <th>Tipo</th>
                            <th>barrio</th>
                            <th>Agente</th>
                            <th>Arrendatario</th>
                            <th>Valor</th>
                            <th>Creado</th>
                            <th>Acción</th>
                            <th class="d-lg-none">Engine version</th>
                            <th class="d-lg-none">CSS grade</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($arriendos as $arriendo)
                            <tr class="{{ $arriendo->active == 0 ? 'danger-tr' : ''}}">
                                <td>{{ $arriendo->inmueble->codigoMLS }} </td>
                                <td>{{ $arriendo->inmueble->tipos_inmueble->nombre }}</td>
                                <td>{{ $arriendo->inmueble->barrio_comun }}</td>
                                <td>{{ $arriendo->creado->nombres }} {{ $arriendo->creado->apellidos }}</td>
                                <td>{{ $arriendo->arrendatario->nombres }} {{ $arriendo->arrendatario->apellidos }}</td>
                                <td><% "{{ $arriendo->valor_total }}" |currency:"$ ":0 %></td>
                                <td>
                                    {{ \Carbon\Carbon::parse($arriendo->created_at)->diffForHumans() }}
                                </td>
                                <td>
                                    <a class="" href="/admin/arriendos/editar/{{ $arriendo->id }}" target="_blank">
                                        <i class="el el-pencil" data-toggle="tooltip" data-placement="top"
                                        data-original-title="Editar arriendo"></i>
                                    </a>
                                    &nbsp;
                                    <a class="modal-with-form btn-default"
                                    ng-click="modalCorreos({{ $arriendo->correos }})" href="#modalCorreos">
                                    <i class="el el-eye-open" data-toggle="tooltip" data-placement="top"
                                    data-original-title="Ver aprobaciones"></i>
                                </a>
                                <!-- &nbsp;
                                <a class="modal-with-form " href="#sellDetail" ng-click="">
                                <i class="el el-eye-open" data-toggle="tooltip" data-placement="top"
                                data-original-title="Ver arriendo"></i>
                            </a>
                            &nbsp;
                            <a href="">
                            <i class="el el-check" data-toggle="tooltip" data-placement="top"
                            data-original-title="Seguimientos"></i>
                        </a> -->
                    </td>
                    <td class="center d-lg-none">4</td>
                    <td class="center d-lg-none">X</td>
                </tr>
            @endforeach


        </tbody>
    </table>
    <br>
    {{$arriendos->links('pagination::bootstrap-4')}}

</div>
</section>
</div>

</div>

<div ng-controller="Arriendos" id="modalCorreos" class="modal-block modal-block-primary mfp-hide modal-xl">
    <section class="card">

        <header class="card-header">
            <h2 class="card-title">
                Vista aprobaciones
            </h2>
        </header>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 mb-xl-0">
                    <section class="card">
                        <div class="card-body">

                            <div class="row">
                                <div class="col-lg-12" ng-repeat="correos in generalfactory.correosArriendo">
                                    <div class="row">
                                        <p class="col-sm-6"> <b>Tipo: </b> <%correos.usuario_tipo%> </p>
                                        <p class="col-sm-6"> <b>Nombre: </b> <%correos.usuario_nombre%> </p>
                                        <p class="col-sm-6"> <b>Correo: </b> <%correos.usuario_correo%> </p>
                                        <p class="col-sm-6"> <b>Fecha envio: </b> <%correos.created_at%> </p>
                                        <p class="col-sm-6"> <b>Fecha última apertura: </b>
                                            <%correos.ultima_visita%>
                                        </p>
                                        <p class="col-sm-6"> <b>Aprobado: </b>
                                            <% correos.aprobado == 0 ? 'No' : 'Si' %>
                                        </p>
                                        <p class="col-sm-12"> <b>Comentario: </b> <%correos.commentario%> </p>

                                        @if ($usuario->rol_id == 1)
                                            <form action="{{ url("admin/aprobar-cualquier-cosa") }}" method="post" class="col-sm-12">
                                                <input type="hidden" name="record" value="<% correos.id %>">
                                                <input type="hidden" name="type" value="Arriendo">
                                                {{ csrf_field() }}

                                                <button class="btn btn-success" type="submit" name="aprobar" value="1" ng-if="correos.aprobado == 0">
                                                    Aprobar
                                                </button>
                                                <button class="btn btn-danger" type="submit" name="rechazar" value="1" ng-if="correos.aprobado == 1">
                                                    Rechazar
                                                </button>
                                            </form>
                                        @endif
                                    </div>
                                    <hr>
                                </div>

                                <div class="col-lg-12" ng-show="generalfactory.correosArriendo.length != 0 && generalfactory.correosArriendo.length == generalfactory.aprobacionesObtenidas.length">
                                    <form action="<% '/admin/arriendos/generar-contrato/' + generalfactory.correosArriendo[0].arriendo_id %>" method="post">
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-success">
                                            Generar contrato
                                        </button>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </section>

                </div>
            </div>
        </div>
        <footer class="card-footer">
            <div class="row">
                <div class="col-md-12 text-right">
                    <button class="btn btn-default modal-dismiss">Cerrar</button>
                </div>
            </div>
        </footer>

    </section>
</div>
