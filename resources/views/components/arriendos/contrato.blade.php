<!doctype html>
<html class="fixed" lang="es" ng-app="app">

<head>
    <!-- Basic -->
    <meta charset="UTF-8">

    <title>Contrato arrendamiento</title>
    <meta name="keywords" content="Elite inmobiliaria, elite system, elite" />
    <meta name="description" content="Elite System">
    <meta name="author" content="calidad.digital">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Shadows+Into+Light"
    rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ asset("assets/vendor/bootstrap/css/bootstrap.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/vendor/animate/animate.compat.css") }}">

    <link rel="stylesheet" href="{{ asset("assets/vendor/font-awesome/css/all.min.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/vendor/boxicons/css/boxicons.min.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/vendor/magnific-popup/magnific-popup.css") }}" />

    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="{{ asset("assets/vendor/jstree/themes/default/style.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css") }}" />

    <!-- select digitando CSS -->
    <link rel="stylesheet" href="{{ asset("assets/vendor/select2/css/select2.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/vendor/select2-bootstrap-theme/select2-bootstrap.min.css") }}" />

    <!-- iconos CSS -->
    <link rel="stylesheet" href="{{ asset("assets/vendor/elusive-icons/css/elusive-icons.css") }}" />

    <!-- arrastrar fotos -->
    <link rel="stylesheet" href="{{ asset("assets/vendor/dropzone/basic.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/vendor/dropzone/dropzone.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/vendor/4image-upload/4image-upload.css") }}" />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset("assets/css/theme.css") }}" />

    <!-- photoswipe -->
    <link rel="stylesheet" href="{{ asset("assets/js/photoswipe/photoswipe.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/js/photoswipe/default-skin.css") }}" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{ asset("assets/css/custom.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/css/angular-super-gallery.css") }}">

    <!-- Head Libs -->
    <script src="{{ asset("assets/vendor/modernizr/modernizr.js") }}"></script>


    <!-- borrrar style switcher
    <script src="{{ asset("assets/master/style-switcher/style.switcher.localstorage.js") }}"></script>
-->
<script>
function validateForm() {
    document.getElementById("submit").click();
}
</script>
</head>



<body>
    <section class="body">

        <!-- start: header -->
        <header class="header">
            <div class="logo-container">
                <a href="{{ url("admin") }}" class="logo">
                    <img src="{{ asset("assets/img/logo.png") }}" width="75" height="35" alt="Porto Admin" />
                </a>
                <div class="d-md-none toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html"
                data-fire-event="sidebar-left-opened">
                <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
            </div>
        </div>

        <!-- start: search & user box -->
        <div class="header-right">
            <ul class="notifications">
            </ul>
            <span class="separator"></span>
        </div>
        <!-- end: search & user box -->
    </header>
    <!-- end: header -->

    <div class="inner-wrapper">

        <section role="main">

            <header class="page-header" style="left:0">
                <h2>Información contrato arrendamiento</h2>

            </header>

            <div class="row">
                <section class="padding-big card col-lg-12">
                    <header class="card-header correct-margin">
                        <h2 class="card-title">Información Importante
                        </h2>
                    </header>
                    <div class="row">
                        <div class="card-body">
                            <div class="form-row">
                                <p> Muchas gracias por iniciar el proceso de Arrendamiento con nosotros. Nuestro
                                    propósito es siempre mantenerlos informados, por esta razón, presentamos este
                                    formulario para su revisión. <br> <br>
                                    A continuación, encontrarán toda la información relacionada en el Contrato de
                                    Arrendamiento, próximo a firmar, por favor revisar todo cuidadosamente. Este
                                    correo
                                    llega a todos los involucrados en el proceso. </p><br> <br>
                                    <span class="col-lg-12">
                                        <p> Al final de esta página podrán encontrar dos botones: <br>
                                            <b>Aprobar:</b> si todo está bien de tu parte. <br>
                                            <b>Corregir:</b> por si debemos hacer algún cambio, el cual se puede
                                            diligenciar
                                            y
                                            aclarar,
                                            en el cuadro de texto denominado <b>Comentarios</b>. Una vez corregido,
                                            enviaremos
                                            la
                                            información nuevamente para revisión, solo a la persona que solicita la
                                            corrección.
                                        </p>

                                    </span>

                                    <p> Una vez revisado por todos los involucrados, el departamento jurídico enviará a
                                        sus
                                        correos electrónicos el <b> Contrato de Arrendamiento final</b>, con todo el
                                        contenido
                                        legal, para sus firmas. Cualquier inquietud no dude en comunicarse con su Agente
                                        Inmobiliario.</p>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="section-container row">
                            <div class="col-lg-12 row">
                                <!-- datos generales -->
                                <section class=" padding-small card col-lg-6 col-sm-12">
                                    <header class="card-header correct-margin">
                                        <h2 class="card-title">Datos generales inmueble
                                        </h2>
                                    </header>
                                    <div class="row">
                                        <div class="card-body">
                                            <div class="form-row">
                                                <div class="form-row col-md-8">
                                                    <b class="col-md-12">Agente encargado </b>
                                                    <p class="col-md-12"> {{$arriendo->creado->nombres}}
                                                        {{$arriendo->creado->apellidos}}</p>
                                                    </div>
                                                    <div class="form-row col-md-4">
                                                        <b class="col-md-12">Código MLS</b> <br>
                                                        <p class="col-md-12">{{$arriendo->inmueble->codigoMLS}}</p>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-row col-md-8">
                                                        <b class="col-md-12">Dirección</b> <br>
                                                        <p class="col-md-12">{{$arriendo->inmueble->direccion}}</p>
                                                    </div>
                                                    <div class="form-row col-md-4">
                                                        <b class="col-md-12">Destinación</b> <br>
                                                        <p class="col-md-12">{{$arriendo->inmueble->destinos->nombre}}</p>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-row col-md-4">
                                                        <b class="col-md-12">Canon</b> <br>
                                                        <p class="col-md-12">{{$arriendo->canon}}</p>
                                                    </div>
                                                    <div class="form-row col-md-4">
                                                        <b class="col-md-12">Admon</b> <br>
                                                        <p class="col-md-12">{{$arriendo->admon}}</p>
                                                    </div>
                                                    <div class="form-row col-md-4">
                                                        <b class="col-md-12">Total alquiler</b> <br>
                                                        <p class="col-md-12">{{$arriendo->valor_total}}</p>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-row col-md-4">
                                                        <b class="col-md-12">Caldera</b> <br>
                                                        <p class="col-md-12"> {{$arriendo->caldera}}</p>
                                                    </div>
                                                    <div class="form-row col-md-4">
                                                        <b class="col-md-12">Parqueadero</b> <br>
                                                        <p class="col-md-12">{{$arriendo->parqueadero}}</p>
                                                    </div>
                                                    <div class="form-row col-md-4">
                                                        <b class="col-md-12">Lavandería</b> <br>
                                                        <p class="col-md-12">{{$arriendo->lavanderia}}</p>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <b class="col-md-12">Usos conexos</b>
                                                    <div class="form-group col-md-2">
                                                        <b class="col-md-12">PQ 1</b> <br>
                                                        <p class="col-md-12">{{$arriendo->parqueadero1}}</p>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <b class="col-md-12">PQ 2</b> <br>
                                                        <p class="col-md-12">{{$arriendo->parqueadero2}}</p>
                                                    </div>
                                                    <div class="form-group col-md-2">
                                                        <b class="col-md-12">PQ 3</b> <br>
                                                        <p class="col-md-12"> {{$arriendo->parqueadero3}}</p>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <b class="col-md-12">DP 1</b> <br>
                                                        <p class="col-md-12"> {{$arriendo->deposito}}</p>
                                                    </div>
                                                    <div class="form-group col-md-3">
                                                        <b class="col-md-12">DP 2</b> <br>
                                                        <p class="col-md-12">{{$arriendo->deposito2}}</p>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <b class="col-md-12">Contratos</b>
                                                    <div class="form-group col-md-4">
                                                        <b class="col-md-12">Acueducto</b> <br>
                                                        <p class="col-md-12">{{$arriendo->contrato_acueducto}}</p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <b class="col-md-12">Energía</b> <br>
                                                        <p class="col-md-12"> {{$arriendo->contrato_energia}}</p>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <b class="col-md-12">Gas</b> <br>
                                                        <p class="col-md-12">{{$arriendo->contrato_gas}}</p>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <b class="col-md-12">Nombre Conjunto</b> <br>
                                                        <p class="col-md-12">{{$arriendo->nombre_conjunto}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <!-- arrendatario -->
                                    <section class=" padding-small card col-lg-6 col-sm-12">
                                        <header class="card-header correct-margin">
                                            <h2 class="card-title">Información arrendatario
                                            </h2>
                                        </header>
                                        <div class="row">
                                            <div class="card-body">
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <b class="col-md-12">Nombre</b> <br>
                                                        <p class="col-md-12">{{$arriendo->arrendatario->nombres}}</p>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <b class="col-md-12">Apellido</b> <br>
                                                        <p class="col-md-12"> {{$arriendo->arrendatario->apellidos}}</p>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <b class="col-md-12">Cédula</b> <br>
                                                        <p class="col-md-12">{{$arriendo->arrendatario->documento}}</p>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <b class="col-md-12">Dirección trabajo</b> <br>
                                                        <p class="col-md-12">{{$arriendo->arrendatario->direccion_comercial}}</p>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <b class="col-md-12">Email</b> <br>
                                                        <p class="col-md-12">{{$arriendo->arrendatario->email}}</p>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <b class="col-md-12">Celular</b> <br>
                                                        <p class="col-md-12">{{$arriendo->arrendatario->celular}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>

                                </div>
                                <div class="col-lg-12 row">
                                    <!-- codeudores -->
                                    <section class=" padding-small card col-lg-6 col-sm-12">
                                        <header class="card-header correct-margin">
                                            <h2 class="card-title">Información codeudores
                                            </h2>
                                        </header>
                                        <div class="row">
                                            <div class="card-body">
                                                @foreach($arriendo->codeudores as $code)

                                                    <div class="form-row">
                                                        <div class="form-group col-md-6">
                                                            <b class="col-md-12">Nombre</b> <br>
                                                            <p class="col-md-12">{{$code->nombre}}</p>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <b class="col-md-12">Cedula</b> <br>
                                                            <p class="col-md-12">{{$code->cedula}}</p>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <b class="col-md-12">Email</b> <br>
                                                            <p class="col-md-12">{{$code->email}}</p>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <b class="col-md-12">Celular</b> <br>
                                                            <p class="col-md-12">{{$code->celular}}</p>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <b class="col-md-12">Dirección</b> <br>
                                                            <p class="col-md-12">{{$code->direccion_comercial}}</p>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <hr>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </section>
                                    <!-- ocupantes -->
                                    <section class=" padding-small card col-lg-6 col-sm-12">
                                        <header class="card-header correct-margin">
                                            <h2 class="card-title">Información ocupantes
                                            </h2>
                                        </header>
                                        <div class="row">
                                            <div class="card-body">
                                                @foreach($arriendo->ocupantes as $ocu)
                                                    <div class="form-row">
                                                        <div class="form-group col-md-6">
                                                            <b class="col-md-12">Nombre</b> <br>
                                                            <p class="col-md-12">{{$ocu->nombre}}</p>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <b class="col-md-12">Cedula</b> <br>
                                                            <p class="col-md-12">{{$ocu->cedula}}</p>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <hr>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </section>
                                </div>
                                <div class="col-lg-12 row">
                                    <!-- aspectos juridicos -->
                                    <section class=" padding-small card col-lg-6 col-sm-12">
                                        <header class="card-header correct-margin">
                                            <h2 class="card-title">Aspectos juridicos
                                            </h2>
                                        </header>
                                        <div class="row">
                                            <div class="card-body">
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <b class="col-md-12">Número de contrato</b> <br>
                                                        <p class="col-md-12">{{$arriendo->numero_contrato}}</p>
                                                    </div>
                                                    <hr>
                                                </div>
                                                <div class="form-row">
                                                    <b class="col-md-12">Contrato</b>
                                                    <div class="form-group col-md-6">
                                                        <b class="col-md-12">Inicio</b> <br>
                                                        <p class="col-md-12">{{$arriendo->fecha_inicio_contrato}}</p>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <b class="col-md-12">Final</b> <br>
                                                        <p class="col-md-12">{{$arriendo->fecha_final_entrega}}</p>
                                                    </div>

                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <b class="col-md-12">Notas precio y formas de pago</b> <br>
                                                        <p class="col-md-12">{{$arriendo->notas_precio}}</p>
                                                    </div>
                                                    <hr>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <b class="col-md-12">Notas servicios publicos</b> <br>
                                                        <p class="col-md-12">{{$arriendo->notas_servicios}}</p>
                                                    </div>
                                                    <hr>
                                                </div>
                                            </div>
                                        </div>
                                    </section>

                                    <!-- opinion -->
                                    <section class="padding-small card col-lg-6 col-sm-12">
                                        <header class="card-header correct-margin">
                                            <h2 class="card-title">Opinión
                                            </h2>
                                        </header>
                                        <div class="row">
                                            <div class="card-body">
                                                <form action="/alt/update-contract/{{ $persona->id }}" method="post" id="update">
                                                    {{ csrf_field() }}
                                                    <div class="form-row">
                                                        <div class="form-group col-md-6">
                                                            <b class="col-md-12">Comentarios</b> <br>
                                                            <textarea rows="5" class="form-control" name="comentarios" required>{{ $persona->commentario }}</textarea>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col py-3">
                                                                        <div class="btn-group-toggle row" data-toggle="buttons">
                                                                            <label class="btn btn-outline-success col mr-2">
                                                                                {{-- <input type="radio" name="like" value="1" {{ $persona->aprobado == 1 ? 'checked': '' }} required onchange="return validateForm();"> --}}
                                                                                <input type="radio" name="like" value="1" required onchange="return validateForm();" onclick="return validateForm();">
                                                                                <i class="fa fa-fw fa-thumbs-up"></i>Aprobar
                                                                            </label>
                                                                            <label class="btn btn-outline-danger col ml-2">
                                                                                {{-- <input type="radio" name="like" value="0" {{ ($persona->aprobado == 0 && $persona->commentario != '') ? 'checked': '' }} required onchange="return validateForm();"> --}}
                                                                                <input type="radio" name="like" value="0" required onchange="return validateForm();" onclick="return validateForm();">
                                                                                <i class="fa fa-fw fa-thumbs-down"></i>Corregir
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-row hidden">
                                                        <div class="form-group col-md-6">
                                                            <button type="submit" class="btn btn-primary" name="revision" id="submit" value="0">
                                                            Enviar comentario
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            @if( $persona->usuario_tipo != 'codeudor' && $persona->usuario_tipo != 'arrendatario')
                                <div class="col-lg-12 row">
                                    <!-- documentos -->
                                    <section class=" padding-small card col-lg-6 col-sm-12">
                                        <header class="card-header correct-margin">
                                            <h2 class="card-title">Documentos
                                            </h2>
                                        </header>
                                        <div class="row">
                                            <div class="card-body">
                                                <div class="form-row">
                                                    @if($arriendo->contrato_captacion_url)
                                                        <div class="form-group col-md-12">
                                                            <a href="{{$arriendo->contrato_captacion_url}}" target="_block"><b
                                                                class="col-md-12">Contrato de captación</b></a>
                                                            </div>
                                                        @endif
                                                        @if($arriendo->clt_url)
                                                            <div class="form-group col-md-12">
                                                                <a href="{{$arriendo->clt_url}}" target="_block"><b
                                                                    class="col-md-12">Clt</b></a>
                                                                </div>
                                                            @endif
                                                            @if($arriendo->clt_parqueadero_url)
                                                                <div class="form-group col-md-12">
                                                                    <a href="{{$arriendo->clt_parqueadero_url}}" target="_block"><b
                                                                        class="col-md-12">Clt Parqueadero</b></a>
                                                                    </div>
                                                                @endif
                                                                @if($arriendo->clt_parqueadero2_url)
                                                                    <div class="form-group col-md-12">
                                                                        <a href="{{$arriendo->clt_parqueadero2_url}}" target="_block"><b
                                                                            class="col-md-12">Clt Parqueadero 2</b></a>
                                                                        </div>
                                                                    @endif
                                                                    @if($arriendo->clt_deposito_url)
                                                                        <div class="form-group col-md-12">
                                                                            <a href="{{$arriendo->clt_deposito_url}}" target="_block"><b
                                                                                class="col-md-12">Clt Deposito</b></a>
                                                                            </div>
                                                                        @endif
                                                                        @if($arriendo->clt_deposito2_url)
                                                                            <div class="form-group col-md-12">
                                                                                <a href="{{$arriendo->clt_deposito2_url}}" target="_block"><b
                                                                                    class="col-md-12">Clt deposito 2</b></a>
                                                                                </div>
                                                                            @endif
                                                                            @if($arriendo->entrega_inventario_url)
                                                                                <div class="form-group col-md-12">
                                                                                    <a href="{{$arriendo->entrega_inventario_url}}" target="_block"><b
                                                                                        class="col-md-12">Entrega de Pre-Inventario</b></a>
                                                                                    </div>
                                                                                @endif
                                                                                @if($arriendo->mandanto_admon_url)
                                                                                    <div class="form-group col-md-12">
                                                                                        <a href="{{$arriendo->mandanto_admon_url}}" target="_block"><b
                                                                                            class="col-md-12">Mandato de administración del inmueble</b></a>
                                                                                        </div>
                                                                                    @endif
                                                                                    @if($arriendo->recibo_predial_url)
                                                                                        <div class="form-group col-md-12">
                                                                                            <a href="{{$arriendo->recibo_predial_url}}" target="_block"><b
                                                                                                class="col-md-12">Recibo del Predial del año en curso</b></a>
                                                                                            </div>
                                                                                        @endif
                                                                                        @if($arriendo->servicios_publicos_url)
                                                                                            <div class="form-group col-md-12">
                                                                                                <a href="{{$arriendo->servicios_publicos_url}}" target="_block"><b
                                                                                                    class="col-md-12">Facturas de servicios públicos</b></a>
                                                                                                </div>
                                                                                            @endif
                                                                                            @if($arriendo->cuenta_cobro_admon_url)
                                                                                                <div class="form-group col-md-12">
                                                                                                    <a href="{{$arriendo->cuenta_cobro_admon_url}}" target="_block"><b
                                                                                                        class="col-md-12">Cuenta de cobro de administración</b></a>
                                                                                                    </div>
                                                                                                @endif
                                                                                                @if($arriendo->escritura_linderos_url)
                                                                                                    <div class="form-group col-md-12">
                                                                                                        <a href="{{$arriendo->escritura_linderos_url}}" target="_block"><b
                                                                                                            class="col-md-12">Escritura pública de los linderos del
                                                                                                            inmueble</b></a>
                                                                                                        </div>
                                                                                                    @endif
                                                                                                    @if($arriendo->aprobacion_aseguradora_url)
                                                                                                        <div class="form-group col-md-12">
                                                                                                            <a href="{{$arriendo->aprobacion_aseguradora_url}}" target="_block"><b
                                                                                                                class="col-md-12">Aprobación de la aseguradora</b></a>
                                                                                                            </div>
                                                                                                        @endif
                                                                                                        @if($arriendo->entrega_amoblados_url)
                                                                                                            <div class="form-group col-md-12">
                                                                                                                <a href="{{$arriendo->entrega_amoblados_url}}" target="_block"><b
                                                                                                                    class="col-md-12">Entrega de inventario para amoblados
                                                                                                                </b></a>
                                                                                                            </div>
                                                                                                        @endif
                                                                                                        @if($arriendo->camara_comercio_url)
                                                                                                            <div class="form-group col-md-12">
                                                                                                                <a href="{{$arriendo->camara_comercio_url}}" target="_block"><b
                                                                                                                    class="col-md-12">Certificado de Cámara de Comercio</b></a>
                                                                                                                </div>
                                                                                                            @endif
                                                                                                            @if($arriendo->leasing_url)
                                                                                                                <div class="form-group col-md-12">
                                                                                                                    <a href="{{$arriendo->leasing_url}}" target="_block"><b
                                                                                                                        class="col-md-12">Leasing habitacional</b></a>
                                                                                                                    </div>
                                                                                                                @endif
                                                                                                                @if($arriendo->usufructo_url)
                                                                                                                    <div class="form-group col-md-12">
                                                                                                                        <a href="{{$arriendo->usufructo_url}}" target="_block"><b
                                                                                                                            class="col-md-12">Escritura pública de constitución de
                                                                                                                            usufructo</b></a>
                                                                                                                        </div>
                                                                                                                    @endif
                                                                                                                    @if($arriendo->rut_url)
                                                                                                                        <div class="form-group col-md-12">
                                                                                                                            <a href="{{$arriendo->rut_url}}" target="_block"><b
                                                                                                                                class="col-md-12">RUT</b></a>
                                                                                                                            </div>
                                                                                                                        @endif
                                                                                                                        <hr>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </section>
                                                                                                        <!-- informacion seguro -->
                                                                                                        <section class=" padding-small card col-lg-6 col-sm-12">
                                                                                                            <header class="card-header correct-margin">
                                                                                                                <h2 class="card-title">Información Seguro
                                                                                                                </h2>
                                                                                                            </header>
                                                                                                            <div class="row">
                                                                                                                <div class="card-body">
                                                                                                                    <div class="form-row">
                                                                                                                        <b class="col-md-12">Contratos</b>
                                                                                                                        <div class="form-group col-md-12">
                                                                                                                            <b class="col-md-12">Amparo Básico: Mensual - 2,57% del Canon y
                                                                                                                                Admón</b> <br>
                                                                                                                                <p class="col-md-12">{{$arriendo->amparo_basico == 1 ? 'Si' : 'No'}}</p>
                                                                                                                            </div>
                                                                                                                            <div class="form-group col-md-12">
                                                                                                                                <b class="col-md-12">Amparo Hogar: Mensual - $20.000 o $25.000 del
                                                                                                                                    Canon</b> <br>
                                                                                                                                    <p class="col-md-12"> {{$arriendo->amparo_hogar== 1 ? 'Si' : 'No'}}</p>
                                                                                                                                </div>
                                                                                                                                <div class="form-group col-md-12">
                                                                                                                                    <b class="col-md-12">Amparo Integral: 1 vez - $123.100 X Millón
                                                                                                                                        Asegurado</b> <br>
                                                                                                                                        <p class="col-md-12">{{$arriendo->amparo_integral == 1 ? 'Si' : 'No'}}
                                                                                                                                        </p>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <div class="form-row">
                                                                                                                                    <div class="form-group col-md-12">
                                                                                                                                        <b class="col-md-12">Notas</b> <br>
                                                                                                                                        <p class="col-md-12">{{$arriendo->notas_seguros}}</p>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </section>

                                                                                                                </div>
                                                                                                            @endif
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </section>
