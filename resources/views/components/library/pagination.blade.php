<ul class="pagination pagination-modern">

    @if ($inmuebles["current_page"] != 1)
        <li>
            <a href="{{ url("admin/inmuebles?page=1") }}">
                <<
            </a>
        </li>
        <li>
            <a href="{{ url("admin/inmuebles?page=" . ($inmuebles["current_page"] - 1)) }}">
                {{ $inmuebles["current_page"] - 1 }}
            </a>
        </li>
    @endif

    <li class="active">
        <a href="">{{ $inmuebles["current_page"] }}</a>
    </li>

    @if ($inmuebles["current_page"] != $inmuebles["last_page"])
        <li>
            <a href="{{ url("admin/inmuebles?page=" . ($inmuebles["current_page"] + 1)) }}">
                {{ $inmuebles["current_page"] + 1 }}
            </a>
        </li>
        <li>
            <a href="{{ url("admin/inmuebles?page=" . $inmuebles["last_page"]) }}">
                >>
            </a>
        </li>
    @endif

</ul>


{{-- Ventas 2 --}}

<div class="form-group col-md-6">
    <label for="parqueaderos_descubiertos">Compartido Elite</label>
    <select  class="form-control" name="promotor">
        <option value="">James</option>
        <option value="">Fabio</option>
    </select>
</div>
