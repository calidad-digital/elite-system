<section role="main" class="content-body">
    <header class="page-header">
        <h2>Clientes</h2>

        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="#">
                        <i class="bx bx-home-alt"></i>
                    </a>
                </li>
                <li><span>Clientes</span></li>
                <li><span>Lista&nbsp;&nbsp;&nbsp;</span></li>
            </ol>

            <a class="" data-open=""></a>
        </div>
    </header>

    {{-- Mensajes --}}
    @if (session("mensaje"))
        <div class="row alert alert-success">
            <strong>Éxito!</strong> {{ session("mensaje") }}
        </div>
    @endif

    @if (session("error"))
        <div class="row alert alert-danger">
            <strong>Ha ocurrido un error!</strong>


        </div>

    @endif
    {{-- Final de mensajes --}}

    <!-- start: page -->
    <div class="row">
        <a class="modal-with-form btn btn-default" href="#modalForm">Crear cliente</a>
    </div>

    <div class="row">

        <div class="col">
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                        <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                    </div>

                    <h2 class="card-title">Clientes</h2>
                </header>
                <div class="card-body">
                    <form action="" method="get" ng-controller="Formulario">
                        <div class="row">

                            <div class="form-group col-md-3">
                                <label for="departamento" class="sr-only">Tipo</label>
                                <select class="form-control" name="rol_id">
                                    <option value="">Tipo</option>
                                    @foreach ($roles as $rol)
                                        <option value="{{ $rol->id }}"
                                            {{ isset($request["rol_id"]) && $request["rol_id"] == $rol->id ? "selected" : "" }}>
                                            {{ $rol->nombre }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="departamento" class="sr-only">Estado</label>
                                <select class="form-control" name="status">
                                    <option value="">Estado</option>
                                    <option value="1"
                                    {{ isset($request["rol_id"]) && $request["rol_id"] == $rol->id ? "selected" : "" }}>
                                    Activo
                                </option>
                                <option value="2"
                                {{ isset($request["rol_id"]) && $request["rol_id"] == $rol->id ? "selected" : "" }}>
                                Inactivo
                            </option>
                        </select>
                    </div>

                    <div class="col-lg-3">
                        <input class="form-control mb-3" name="name" type="text"
                        placeholder="Nombre y/o apellido"
                        value="{{ isset($request["name"]) ? $request["name"] : "" }}">
                    </div>

                    <div class="col-lg-3">
                        <input class="form-control mb-3" name="document" type="text" placeholder="Documento"
                        value="{{ isset($request["document"]) ? $request["document"] : "" }}">
                    </div>

                    <div class="col-lg-3">
                        <input class="form-control mb-3" name="email" type="text" placeholder="E-mail"
                        value="{{ isset($request["email"]) ? $request["email"] : "" }}">
                    </div>

                    <div class="col-lg-3">
                        <input class="form-control mb-3" name="property" type="text" placeholder="Inmueble"
                        value="{{ isset($request["property"]) ? $request["property"] : "" }}">
                    </div>
                    <div class="col-lg-6">
                        <button class="btn btn-primary">Buscar</button>
                    </div>
                </div>
            </form>
            <table class="table table-bordered table-striped mb-0" id="datatable-default">
                <thead>
                    <tr>
                        <th>Tipo</th>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>E-mail</th>
                        <th>Inmueble</th>
                        <th>Acción</th>
                        <th class="d-lg-none">Engine version</th>
                        <th class="d-lg-none">CSS grade</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($clientes as $cliente)
                        <tr>
                            <td>{{ $cliente->roles->nombre }}</td>
                            <td>{{ $cliente->nombres }}</td>
                            <td>{{ $cliente->apellidos }}</td>
                            <td>{{ $cliente->email }}</td>
                            <td>--</td>
                            <td>
                                <a class="modal-with-form btn-default" href="#modalEdit_{{ $cliente->id }}">
                                    <i class="el el-pencil"></i>
                                </a>
                                &nbsp;
                                <a class="modal-with-form btn-default" href="#modalView_{{ $cliente->id }}">
                                    <i class="el el-eye-open"></i>
                                </a>
                            </td>
                            <td class="center d-lg-none">4</td>
                            <td class="center d-lg-none">X</td>
                        </tr>

                        {{-- Ver Cliente --}}
                        <div id="modalView_{{ $cliente->id }}" class="modal-block modal-block-primary mfp-hide">
                            <section class="card">
                                <header class="card-header">
                                    <h2 class="card-title">Vista rápida cliente</h2>
                                </header>
                                <div class="card-body">

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="username">Usuario ( Cédula )</label>
                                            <br><b>{{ $cliente->username }}</b>
                                        </div>
                                        <div class="form-group col-md-6 mb-3 mb-lg-0">
                                            <label for="password">Clave</label>
                                            <br><b>{{ $cliente->password }}</b>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="ciudad">Ciudad de residencia</label>
                                            <br><b>{{ $cliente->ciudad }}</b>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="estado">Estado</label>
                                            <br><b>{{ $cliente->estados->nombre }}</b>
                                        </div>
                                        <div class="form-group col-md-5">
                                            <label for="rol">Rol</label>
                                            <br><b>{{ $cliente->roles->nombre }}</b>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="name">Nombres</label>
                                            <br><b>{{ $cliente->nombres }}</b>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="last_name">Apellidos</label>
                                            <br><b>{{ $cliente->apellidos }}</b>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="email">Email</label>
                                            <br><b>{{ $cliente->email }}</b>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="document_type">Tipo doc</label>
                                            <br><b>CC</b>
                                        </div>

                                        <div class="form-group col-md-8">
                                            <label for="document">Número</label>
                                            <br><b>{{ $cliente->documento }}</b>
                                        </div>
                                    </div>

                                    <hr>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="phone">Teléfono</label>
                                            <br><b>{{ $cliente->telefono }}</b>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="cellphone">Celular</label>
                                            <br><b>{{ $cliente->celular }}</b>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="whatsapp">Whatsapp1</label>
                                            <br><b>{{ $cliente->whatsapp }}</b>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label>Fecha nacimiento</label>
                                            <div>
                                                <br><b>{{ \Carbon\Carbon::parse($cliente->fecha_nacimiento)->diffForHumans() }}
                                                    ({{ $cliente->fecha_nacimiento }})</b>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Estado civil</label><br>
                                                {{ $cliente->estado_civil }}
                                            </div>
                                            <div class="form-group col-md-6 hidden">
                                                <label>Foto</label>
                                                <br><img src="{{ $cliente->foto }}" width="100%" alt="">
                                            </div>
                                        </div>

                                        <?php if ($cliente->url_documento != "" && $cliente->url_documento != null): ?>
                                            <div class="form-group col-md-6">
                                                <label>Cédula</label>
                                                <br>
                                                @if (preg_match("/\.(gif|png|jpg|jpeg)$/", $cliente->url_documento))
                                                    <img src="{{ $cliente->url_documento }}" width="100%" alt="">
                                                @endif
                                                <a href="{{ $cliente->url_documento }}" target="_blank">
                                                    Ver cédula
                                                </a>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <footer class="card-footer">
                                        <div class="row">
                                            <div class="col-md-12 text-right">
                                                <button class="btn btn-default modal-dismiss">Cerrar</button>
                                            </div>
                                        </div>
                                    </footer>
                                </section>
                            </div>
                            {{-- Final de ver Cliente --}}

                            {{-- Editar Cliente --}}
                            <div ng-controller="Formulario" id="modalEdit_{{ $cliente->id }}"
                                class="modal-block modal-block-primary mfp-hide">
                                <section class="card">
                                    <form action="{{ url('admin/clientes/' . $cliente->id) }}" method="post"
                                        enctype="multipart/form-data">
                                        <header class="card-header">
                                            <h2 class="card-title">Editar cliente</h2>
                                        </header>
                                        <div class="card-body">

                                            {{ csrf_field() }}

                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="username">Usuario ( Cédula )</label>
                                                    <input type="number" class="form-control" value="{{ $cliente->username }}" name="username" placeholder="Usuario" ng-required="rol != 13">
                                                </div>
                                                <div class="form-group col-md-6 mb-3 mb-lg-0">
                                                    <label for="password">Clave</label>
                                                    <input type="text" class="form-control" value="{{ $cliente->password ?? "" }}" name="password" placeholder="Password" ng-required="rol != 13">
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="name">Nombres</label>
                                                    <input type="text" class="form-control" value="{{ $cliente->nombres ?? "" }}" name="name" placeholder="Nombres" required>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="last_name">Apellidos</label>
                                                    <input type="text" class="form-control" value="{{ $cliente->apellidos ?? "" }}" name="last_name" placeholder="Apellidos" required>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-6" ng-init="ciudad = '{{ $cliente->ciudad_ext == 1 ? $ext : $cliente->ciudad_id }}'">
                                                    <label for="ciudad">
                                                        Ciudad de residencia
                                                    </label>
                                                    <select id="ciudad" class="form-control" name="ciudad" ng-model="ciudad">
                                                        <option value="">Ciudad </option>
                                                        <option value="ext">EXTRANJERO
                                                        </option>
                                                        @foreach($ciudades as $ciudad)
                                                            <option value="{{ $ciudad->id }}">
                                                                {{ $ciudad->nombre }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div ng-if="ciudad == 'ext'" class="form-group col-md-6">
                                                    <label for="ciudad_ext">&nbsp;</label>
                                                    <input type="text" class="form-control" name="ciudad_ext" placeholder="Washington" value="{{$cliente->ciudad}}" required>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="direccion">Dirección</label>
                                                    <input type="text" class="form-control" value="{{ $cliente->direccion ?? '' }}" name="direccion" placeholder="Calle 25 # 42 C -75" ng-required="rol != 13">
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-2">
                                                    <label for="document_type">Tipo doc</label>
                                                    <select name="document_type" class="form-control">
                                                        <option value="">Tipo de documento</option>
                                                        @foreach ($tipos_documento as $tipo)
                                                            <option value="{{ $tipo->id }}" {{ (isset($cliente->tipo_documento_id) && $cliente->tipo_documento_id == $tipo->id) ? "selected" : "" }}>
                                                                {{ $tipo->nombre }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group col-md-5">
                                                    <label for="document">Número</label>
                                                    <input type="number" class="form-control" value="{{ $cliente->documento ?? "" }}" name="document" placeholder="31950000" ng-required="rol != 13">
                                                </div>

                                                <div class="form-group col-md-5">
                                                    <label for="expedicion">Expedición </label>
                                                    <input type="text" class="form-control" value="{{ $cliente->expedicion ?? "" }}" name="expedicion">
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>Estado civil</label>
                                                    <select class="form-control" name="estado_civil" ng-required="rol != 13">
                                                        <option value="">Seleccionar</option>
                                                        <option value="Casado" {{ $cliente->estado_civil == "Casado" ? "selected" : "" }}>
                                                            Casado
                                                        </option>
                                                        <option value="Soltero" {{ $cliente->estado_civil == "Soltero" ? "selected" : "" }}>
                                                            Soltero
                                                        </option>
                                                        <option value="Juridica" {{ $cliente->estado_civil == "Juridica" ? "selected" : "" }}>
                                                            Persona Jurídica
                                                        </option>
                                                    </select>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label>Detalle Estado Civil</label>
                                                    <select class="form-control" name="subestado_civil">
                                                        <option value="">Seleccionar</option>
                                                        @foreach($estados_civiles as $ec)
                                                            <option value="{{ $ec->id }}" {{ $cliente->subestado_civil_id == $ec->id ? "selected" : "" }}>
                                                                {{ $ec->nombre }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="phone">Teléfono</label>
                                                    <input type="text" class="form-control" value="{{ $cliente->telefono ?? "" }}" name="phone" placeholder="7153573">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="cellphone">Celular / Whatsapp</label>
                                                    <input type="text" class="form-control" value="{{ $cliente->celular ?? "" }}" name="cellphone" placeholder="3181234567" ng-required="rol == 13">
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="email">Correo electrónico</label>
                                                    <input type="email" class="form-control" value="{{ $cliente->email ?? "" }}" name="email" placeholder="Correo" required>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="rol">Rol</label>
                                                    <select id="rol" class="form-control" name="role" ng-model="rol" ng-init="rol = '{{ $cliente->rol_id}}'">
                                                        <option value="">Rol</option>
                                                        @foreach($roles as $rol)
                                                            <option value="{{ $rol->id }}">
                                                                {{ $rol->nombre }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="estado">Estado</label>
                                                    <select id="estado" name="status" class="form-control">
                                                        <option value="">Estado</option>
                                                        <option value="1" {{ (isset($cliente->estado_id) && $cliente->estado_id == 1) ? "selected" : "" }}>
                                                            activo
                                                        </option>
                                                        <option value="2" {{ (isset($cliente->estado_id) && $cliente->estado_id == 2) ? "selected" : "" }}>
                                                            inactivo
                                                        </option>
                                                    </select>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="profession">Profesión</label>
                                                    <input type="text" class="form-control" value="{{ $cliente->profesion ?? "" }}" name="profession" placeholder="Profesión" ng-required="rol != 13">
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label>Fecha nacimiento</label>
                                                    <div>
                                                        <input type="date" class="form-control" name="birthdate"
                                                        value="{{ $cliente->fecha_nacimiento }}">
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label>Cédula</label> <br>
                                                    <input type="file" name="archivo_documento" accept="application/pdf,image/jpeg,image/png"></input>
                                                    <br>
                                                    @if(isset($cliente->url_documento))
                                                        <small>
                                                            <a href="{{ $cliente->url_documento }}" target="_blank">
                                                                Documento
                                                                <br>
                                                            </a>
                                                        </small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="form-group col-md-6 hidden">
                                                    <label>Foto (no mayor a 120kb)</label> <br>
                                                    <input type="file" name="foto"
                                                    accept="image/jpeg,image/png"></input>
                                                </div>
                                            </div>
                                        </div>
                                        <footer class="card-footer">
                                            <div class="row">
                                                <div class="col-md-12 text-right">
                                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                                    <button class="btn btn-default modal-dismiss">Cancelar</button>
                                                </div>
                                            </div>
                                        </footer>
                                    </form>
                                </section>
                            </div>

                            {{-- Final Editar Cliente --}}
                        @endforeach
                    </tbody>
                </table>
                <br>
                {{$clientes->links('pagination::bootstrap-4')}}
            </div>
        </section>

    </div>
</div>

{{-- Crear usuario --}}
<div id="modalForm" class="modal-block modal-block-primary mfp-hide" ng-controller="Formulario">
    <section class="card">
        <form action="{{ url("admin/clientes") }}" method="post" enctype="multipart/form-data">
            <header class="card-header">
                <h2 class="card-title">Crear cliente</h2>
            </header>
            <div class="card-body">

                {{ csrf_field() }}

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="username">Usuario ( Cédula )</label>
                        <input type="number" class="form-control" id="username"
                        value="{{ session("content")["username"] ?? "" }}" name="username" placeholder="Usuario"
                        ng-required="rol != 13">
                    </div>
                    <div class="form-group col-md-6 mb-3 mb-lg-0">
                        <label for="password">Clave</label>
                        <input type="text" class="form-control" id="password"
                        value="{{ session("content")["password"] ?? "" }}" name="password"
                        placeholder="Password" ng-required="rol != 13">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="name">Nombres *</label>
                        <input type="text" class="form-control" value="{{ session("content")["name"] ?? "" }}"
                        id="name" name="name" placeholder="Nombres" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="last_name">Apellidos *</label>
                        <input type="text" class="form-control" value="{{ session("content")["last_name"] ?? "" }}"
                        name="last_name" id="last_name" placeholder="Apellidos" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="ciudad">Ciudad de residencia <span ng-if="rol != 13">*</span></label>
                        <select id="ciudad" class="form-control" name="ciudad" ng-model="ciudad">
                            <option value="">Ciudad </option>
                            <option value="ext">EXTRANJERO
                            </option>
                            @foreach($ciudades as $ciudad)
                                <option value="{{ $ciudad->id }}">
                                    {{ $ciudad->nombre }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div ng-if="ciudad == 'ext'" class="form-group col-md-6">
                        <label for="ciudad">&nbsp;</label>
                        <input type="text" class="form-control" name="ciudad_ext" placeholder="Washington" required>
                    </div>
                    <div class="form-group <%ciudad == 'ext' ? 'col-md-12' : 'col-md-6'%>">
                        <label for="name">Dirección <span ng-if="rol != 13">*</span></label>
                        <input type="text" class="form-control" id="direccion" name="direccion"
                        placeholder="Calle 123 # 45 B - 72" ng-required="rol != 13">
                    </div>

                </div>

                <div class="form-row">
                    <div class="form-group col-md-2">
                        <label for="document_type">Tipo doc</label>
                        <select id="document_type" name="document_type" class="form-control">
                            <option value="">Tipo de documento</option>
                            @foreach ($tipos_documento as $tipo)
                                <option value="{{ $tipo->id }}"
                                    {{ (isset(session("content")["document_type"]) && session("content")["document_type"] == $tipo->id) ? "selected" : "" }}>
                                    {{ $tipo->nombre }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-5">
                        <label for="document">Número <span ng-if="rol != 13">*</span></label>
                        <input type="number" class="form-control" value="{{ session("content")["document"] ?? "" }}"
                        name="document" id="document" placeholder="31950000" ng-required="rol != 13">
                    </div>
                    <div class="form-group col-md-5">
                        <label for="expedicion">Expedición </label>
                        <input type="text" class="form-control" value="{{ session("content")["expedicion"] ?? "" }}"
                        name="expedicion" id="expedicion">
                    </div>
                </div>

                <div class="form-row">

                    <div class="form-group col-md-6">
                        <label>Estado Civil</label>
                        <select class="form-control" name="estado_civil"
                        value="{{ session("content")["estado_civil"] ?? "" }}">
                        <option value="">Seleccionar</option>
                        <option value="Casado">Casado</option>
                        <option value="Soltero">Soltero</option>
                        <option value="Juridica">Persona jurídica</option>
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label>Detalle Estado Civil</label>

                    <select class="form-control" name="subestado_civil"
                    value="{{ session("content")["subestado_civil"] ?? "" }}">
                    <option value="">Seleccionar</option>
                    @foreach($estados_civiles as $ec)
                        <option value="{{$ec->id}}">{{$ec->nombre}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="phone">Teléfono</label>
                <input type="text" class="form-control" value="{{ session("content")["phone"] ?? "" }}"
                name="phone" id="phone" placeholder="7153573">
            </div>
            <div class="form-group col-md-6">
                <label for="cellphone">Celular/Whatsapp</label>
                <input type="text" class="form-control" value="{{ session("content")["cellphone"] ?? "" }}"
                name="cellphone" id="cellphone" placeholder="3181234567" ng-required="rol == 13">
            </div>
        </div>

        <div class="form-row">


            <div class="form-group col-md-6">
                <label for="email">Correo Electrónico *</label>
                <input type="email" class="form-control" value="{{ session("content")["email"] ?? "" }}"
                name="email" id="email" placeholder="Correo" required>
            </div>
            <div class="form-group col-md-6">
                <label for="rol">Rol *</label>
                <select id="rol" class="form-control" name="role" ng-model="rol" required>
                    <option value="">Rol</option>
                    @foreach($roles as $rol)
                        <option value="{{ $rol->id }}" {{ $rol->id == 3 ? "selected" : "" }}
                            {{ (isset(session("content")["role"]) && session("content")["role"] == $rol->id) ? "selected" : "" }}>
                            {{ $rol->nombre }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="estado">Estado del cliente<span ng-if="rol != 13">*</span></label>
                <select id="estado" name="estado" class="form-control" ng-required="rol == 13">
                    <option value="">Estado</option>
                    @foreach($estados as $estado)
                        <option value="{{ $estado->id }}"
                            {{ (isset(session("content")["estado"]) && session("content")["estado"] == $estado->id) ? "selected" : "" }}>
                            {{ $estado->nombre }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="profession">Profesión</label>
                <input type="text" class="form-control" value="{{ session("content")["profession"] ?? "" }}"
                name="profession" id="profession" placeholder="Profesión">
            </div>
        </div>







        <div class="form-row">
            <div class="form-group col-md-6">
                <label>Fecha nacimiento</label>
                <div>
                    <input type="date" class="form-control" name="birthdate"
                    value="{{ session("content")["birthdate"] ?? date("Y-m-d") }}">
                </div>
            </div>
            <div class="form-group col-md-6">
                <label>Cédula</label> <br>
                <input type="file" name="archivo_documento"
                accept="application/pdf,image/jpeg,image/png"></input>
            </div>
        </div>
    </div>
    <footer class="card-footer">
        <div class="row">
            <div class="col-md-12 text-right">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <button class="btn btn-default modal-dismiss">Cancelar</button>
            </div>
        </div>
    </footer>
</form>
</section>
</div>
{{-- Final de formulario de crear usuario --}}
