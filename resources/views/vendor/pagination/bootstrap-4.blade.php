@if ($paginator->hasPages())
@php
$curPage = url()->full();
$filters = strpos($curPage,"?");
if($filters === false){
$curPage = $curPage.'?';
}


//$search = strpos($curPage,"page=");
//if($search > 0){
//$curPage = substr($curPage,0,$search);
//}
@endphp
<nav>
    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())

        @else
        @if($paginator->currentPage() - 2 > 1)
        <li class="page-item">
            <a class="page-link" href="{{ $curPage.'&page=1' }}" rel="prev"
                aria-label="@lang('pagination.previous')">&lsaquo;&lsaquo;</a>
        </li>
        @endif
        <li class="page-item">
            <a class="page-link" href="{{ $curPage.'&page='.($paginator->currentPage() -1) }}" rel="prev"
                aria-label="@lang('pagination.previous')">&lsaquo;</a>
        </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)


        {{-- Array Of Links --}}
        @if (is_array($element))
        @foreach ($element as $page => $url)
        @if ($page != $paginator->currentPage() && ($page < $paginator->currentPage()) && (($page ==
            $paginator->currentPage() -1) || ($page ==
            $paginator->currentPage() -2)))
            <li class="page-item"><a class="page-link" href="{{ $curPage.'&page='.$page }}">{{ $page }}</a></li>
            @endif
            @if ($page == $paginator->currentPage())
            <li class="page-item active" aria-current="page"><span class="page-link">{{ $page }}</span></li>
            @endif
            @if ($page != $paginator->currentPage() && ($page > $paginator->currentPage()) && (($page ==
            $paginator->currentPage() +1) || ($page ==
            $paginator->currentPage() +2)))
            <li class="page-item"><a class="page-link" href="{{ $curPage.'&page='.$page }}">{{ $page }}</a></li>
            @endif
            @endforeach
            @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
            <li class="page-item">
                <a class="page-link" href="{{ $curPage.'&page='.($paginator->currentPage() +1) }}" rel="next"
                    aria-label="@lang('pagination.next')">&rsaquo;</a>
            </li>
            @if($paginator->currentPage() + 2 < $paginator->lastPage())
                <li class="page-item">
                    <a class="page-link" href="{{ $curPage.'&page='.$paginator->lastPage() }}" rel="prev"
                        aria-label="@lang('pagination.previous')">&rsaquo;&rsaquo;</a>
                </li>
                @endif
                @else
                @endif
    </ul>
</nav>
@endif