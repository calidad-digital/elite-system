<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Inmueble;
use App\Models\InmueblePortal;
use App\Http\Controllers\GeneralController;
use Carbon\Carbon;
class testCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elite:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando con codigo variable para ejecutar procesos masivos de cualquir indole.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        dump('Comenzando proceso de actualizacion de inmuebles.');
        
        $endpoint = "http://api.domus.la/";

        $inmuebles = Inmueble::where('estado_id',5)->get();
        dump('cantidad de inm disponibles: '.count($inmuebles));
        $cont = 1;
        foreach( $inmuebles  as $inmueble){
            
            if(isset($inmueble->codigoMLS)){
                dump('Actualizando el inmueble: '.$inmueble['id']);
                
                $url = "pro-detail/{$inmueble->codigoMLS}";
                $response = GeneralController::getRequest("{$endpoint}{$url}", [
                    "Authorization" => 'APP_USR_74e5d19f88cc25a46c2fa04c6e2ce80d',
                ]);
                if(isset($response['data'][0]['idpro'])){

                    $id= $response['data'][0]['idpro'];
                    $responseB2b = GeneralController::getRequest("https://b2b.domus.la/b2b/detail-id/".$id, [
                        "Authorization" => 'APP_USR_74e5d19f88cc25a46c2fa04c6e2ce80d',
                    ]);
                    
                    foreach($responseB2b['data'][0]['portals'] as $portal){
                        $nombre = $portal['name_portal'];
                        $estado = $portal['status'];
                        if($nombre != ''){
        
                            $cod = $portal['cod_portal']??'';
                            $valDatePub = substr($portal['sincronization_date'], 0,4);
                            // dd($valDatePub);
                            $actualizacion = $portal['last_update'] == '0000-00-00 00:00:00' ? Carbon::now() : new Carbon($portal['last_update']);
                            $publicacion = $portal['sincronization_date'] == '0000-00-00 00:00:00' ? Carbon::now() : new Carbon($portal['sincronization_date']);
                            switch ($portal['id_portal']) {
                                case '1':
                                    $url = 'https://www.fincaraiz.com.co/detail.aspx?a='.$cod;
                                    break;
                                case '2':
                                    $url = 'https://www.metrocuadrado.com/inmueble/domus/'.$cod;
                                    break;
                                case '7':
                                    $cod2 =  substr($cod, 3);
                                    $url = 'https://'.$responseB2b['data'][0]['type'].'.mercadolibre.com.co/MCO-'.$cod2;
                                    break;
                                case '8':
                                    $cod2 =  substr($cod, 3);
                                    $url = 'https://viviendo.la/property/search/'.$cod;
                                    break;
                                case '10':
                                    $url = 'https://www.olx.com.co/items/q-cod'.$cod;
                                    break;
                                case '15':
                                    $url = 'https://fincaraiz.elpais.com.co/aviso/domus-vp'.$cod;
                                    break;
                                case '16':
                                    $url = 'https://www.ciencuadras.com/inmueble/'.$cod;
                                    break;                    
                                default:
                                    $url = $portal['url_portal'];
                                    break;
                            }
                            $searchPortal = InmueblePortal::where('inmueble_id', $inmueble['id'])
                            ->where('nombre_portal',$nombre)
                            ->first();
            
                            $portal= empty($searchPortal)? new InmueblePortal: $searchPortal ;
                            
                            $portal->nombre_portal = $nombre;
                            $portal->codigo_portal = $cod;
                            $portal->url_portal = $url;
                            $portal->fecha_actualizacion = $actualizacion;
                            $portal->fecha_subida = $publicacion;
                            $portal->estado = $estado;
                            $portal->inmueble_id = $inmueble['id'];
        
                            $portal->save();
                        }
                    }
                    $searchPortal = InmueblePortal::where('inmueble_id', $inmueble['id'])
                    ->where('nombre_portal',"Elite")
                    ->first();
    
                    $portal= empty($searchPortal)? new InmueblePortal: $searchPortal ;
                    $inmuebleApi = $responseB2b['data'][0];
                    $clase = new GeneralController;
                    $portal->nombre_portal = 'Elite';
                    $portal->codigo_portal = '';
                    $portal->url_portal = "http://propiedades.eliteinmobiliaria.com.co/inmuebles/".$clase->replacespacios($inmuebleApi['type'])."-en-". $clase->replacespacios($inmuebleApi['biz']) ."-en-". $clase->replacespacios($inmuebleApi['neighborhood']) ."/". $inmuebleApi["codpro"] ."/". $inmuebleApi['idpro'] ;
                    $portal->fecha_actualizacion = date("Y-m-d H:i:s");
                    $portal->fecha_subida = date("Y-m-d H:i:s");
                    $portal->estado = 1;
                    $portal->inmueble_id = $inmueble['id'];
        
                    $portal->save();

                    //Creacion de portal elite sin datos
                    $searchPortal = InmueblePortal::where('inmueble_id', $inmueble['id'])
                    ->where('nombre_portal',"Elite sin datos")
                    ->first();
    
                    $portal= empty($searchPortal)? new InmueblePortal: $searchPortal ;
                    $inmuebleApi = $responseB2b['data'][0];
                    $clase = new GeneralController;
                    $portal->nombre_portal = 'Elite sin datos';
                    $portal->codigo_portal = '';
                    $portal->url_portal = "http://propiedades.eliteinmobiliaria.com.co/inmuebles/1/".$clase->replacespacios($inmuebleApi['type'])."-en-". $clase->replacespacios($inmuebleApi['biz']) ."-en-". $clase->replacespacios($inmuebleApi['neighborhood']) ."/". $inmuebleApi["codpro"] ."/". $inmuebleApi['idpro'] ;
                    $portal->fecha_actualizacion = date("Y-m-d H:i:s");
                    $portal->fecha_subida = date("Y-m-d H:i:s");
                    $portal->estado = 1;
                    $portal->inmueble_id = $inmueble['id'];
        
                    $portal->save();
        
                    $cont++;
                }
            }else{
                dump('el inm '.$inmueble['id'].', no tiene codigo MLS');
            }
        }

        dump("cantidad de inmuebles actualizados: ".$cont);
    }
}