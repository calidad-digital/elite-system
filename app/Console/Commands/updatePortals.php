<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Seguimiento;
use App\Models\Inmueble;
use App\Models\InmueblePortal;
use App\Http\Controllers\MailController;
use App\Http\Controllers\GeneralController;
use Carbon\Carbon;

class updatePortals extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'elite:portales';

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Este comando actualiza la información de los inmuebles sobre los portales de domus.';

    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Execute the console command.
    *
    * @return int
    */
    public function handle()
    {
        $endDate = Carbon::now();
        $initDate = Carbon::now()->addDays(-5)->startOfDay();
        $endpoint = "http://api.domus.la/";

        $seguimientos = Seguimiento::select('inmueble_id')
        ->where('estado_inmueble_id',5)
        // ->where('fecha_registro','>', $initDate)
        // ->where('fecha_registro','<', $endDate)
        ->orderBy('id','DESC')
        ->groupBy('inmueble_id')
        ->get();

        $cont = 0;
        foreach ($seguimientos  as $seg) {
            $inmueble = Inmueble::with('tipos_inmueble','gestiones')->find($seg['inmueble_id']);
            if(isset($inmueble->codigoMLS)){
                dump('Actualizando el inmueble: '.$seg['inmueble_id']);

                $url = "pro-detail/{$inmueble->codigoMLS}";
                $response = GeneralController::getRequest("{$endpoint}{$url}", [
                    "Authorization" => 'APP_USR_74e5d19f88cc25a46c2fa04c6e2ce80d',
                ]);
                if(isset($response['data'][0]['idpro'])){

                    $id= $response['data'][0]['idpro'];
                    $responseB2b = GeneralController::getRequest("https://api.domus.la/3.0/properties/portals/".$id, [
                        "Authorization" => 'APP_USR_74e5d19f88cc25a46c2fa04c6e2ce80d',
                    ]);
                    foreach($responseB2b as $portal){
                        $nombre = $portal['portal_name']['name'];
                        $estado = $portal['status'];
                        if($nombre != ''){

                            $cod = $portal['property_code_in_portal']??'';
                            $valDatePub = substr($portal['sync_date'], 0,4);

                            $actualizacion = $portal['last_update_date'] == '0000-00-00 00:00:00' ? Carbon::now() : new Carbon($portal['last_update_date']);
                            $publicacion = $portal['sync_date'] == '0000-00-00 00:00:00' ? Carbon::now() : new Carbon($portal['sync_date']);
                            switch ($portal['portal_name']['code']) {
                                case '1':
                                $url = 'https://www.fincaraiz.com.co/detail.aspx?a='.$cod;
                                break;
                                case '2':
                                $url = 'https://www.metrocuadrado.com/inmueble/domus/'.$cod;
                                break;
                                case '7':
                                $cod2 =  substr($cod, 3);
                                $url = 'https://'.$inmueble->tipos_inmueble->nombre.'.mercadolibre.com.co/MCO-'.$cod2;
                                break;
                                case '8':
                                $cod2 =  substr($cod, 3);
                                $url = 'https://viviendo.la/property/search/'.$cod;
                                break;
                                case '10':
                                $url = 'https://www.olx.com.co/items/q-cod'.$cod;
                                break;
                                case '15':
                                $url = 'https://fincaraiz.elpais.com.co/aviso/domus-vp'.$cod;
                                break;
                                case '16':
                                // dd($portal);
                                $url = $portal['comment'];
                                break;
                                default:
                                $url = $portal['portal_name']['url'];
                                break;
                            }

                            $searchPortal = InmueblePortal::where('inmueble_id', $seg['inmueble_id'])
                            ->where('nombre_portal',$nombre)
                            ->first();

                            $portal= empty($searchPortal)? new InmueblePortal: $searchPortal ;
                            $clase = new GeneralController;
                            $portal->nombre_portal = $nombre;
                            $portal->codigo_portal = $cod;
                            $portal->url_portal = $url;
                            $portal->fecha_actualizacion = date("Y-m-d H:i:s");
                            $portal->fecha_subida = date("Y-m-d H:i:s");
                            $portal->estado = $estado ;
                            $portal->inmueble_id = $seg['inmueble_id'];

                            $portal->save();

                        }
                    }
                    $searchPortal = InmueblePortal::where('inmueble_id', $seg['inmueble_id'])
                    ->where('nombre_portal',"Elite")
                    ->first();

                    $portal = empty($searchPortal) ? new InmueblePortal : $searchPortal;
                    $complemento = "propiedad";
                    if (isset($responseB2b[0])) {
                        $inmuebleApi = $responseB2b[0];
                        $clase = new GeneralController;
                        $complemento =$clase->replacespacios($inmueble->tipos_inmueble->nombre)."-en-". $clase->replacespacios($inmueble->gestiones->nombre) ."-en-". $clase->replacespacios($inmueble->barrio_comun);
                    }

                    $portal->nombre_portal = 'Elite';
                    $portal->codigo_portal = '';
                    $portal->url_portal = "http://propiedades.eliteinmobiliaria.com.co/inmuebles/". $complemento ."/". $inmuebleApi["property_code"] ."/". $inmuebleApi['property_id'] ;
                    $portal->fecha_actualizacion = date("Y-m-d H:i:s");
                    $portal->fecha_subida = date("Y-m-d H:i:s");
                    $portal->estado = 1;
                    $portal->inmueble_id = $seg['inmueble_id'];

                    $portal->save();

                    //Creacion de portal elite sin datos
                    $searchPortal = InmueblePortal::where('inmueble_id', $seg['inmueble_id'])
                    ->where('nombre_portal',"Elite sin datos")
                    ->first();

                    $portal= empty($searchPortal)? new InmueblePortal: $searchPortal ;

                    $portal->nombre_portal = 'Elite sin datos';
                    $portal->codigo_portal = '';
                    $portal->url_portal = "http://propiedades.eliteinmobiliaria.com.co/inmuebles/1/". $complemento."/". $inmuebleApi["property_code"]  ."/".$inmuebleApi['property_id'];
                    $portal->fecha_actualizacion = date("Y-m-d H:i:s");
                    $portal->fecha_subida = date("Y-m-d H:i:s");
                    $portal->estado = 1;
                    $portal->inmueble_id = $seg['inmueble_id'];

                    $portal->save();

                    $cont++;
                }
            }
        }

        dump("cantidad de inmuebles actualizados: " . $cont);
    }
}
