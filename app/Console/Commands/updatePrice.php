<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Inmueble;
use App\Http\Controllers\ApiController;
class updatePrice extends Command
{
    /**
    * The name and signature of the console command.
    *
    * @var string
    */
    protected $signature = 'elite:price';

    /**
    * The console command description.
    *
    * @var string
    */
    protected $description = 'Metodo encargado de semanalmente aumentar el precio de los inmuebles en 1 y luego disminuirlo para posicionamiento de portales';

    /**
    * Create a new command instance.
    *
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Execute the console command.
    *
    * @return int
    */
    public function handle()
    {
        dump("iniciando proceso");
        $inmuebles = Inmueble::where('estado_id', 5)
        ->get();

        dump("cantidad de inmuebles por actualizar: " . $inmuebles->count());
        $api = new ApiController();

        foreach ($inmuebles as $key => $value) {
            dump('actualizando inmueble: ' . $value->id);
            if ($value->gestion_id == 1) {
                $validatePrice = substr($value->canon, -1);

                if ($validatePrice == "9") {
                    $value->canon = $value->canon + 1;
                } else if ($validatePrice == "0" || $validatePrice == "1") {
                    $value->canon = $value->canon - 1;
                }

                $value->save();
            } else {
                $validatePrice = substr($value->venta, -1);

                if ($validatePrice == "9") {
                    $value->venta = $value->venta + 1;
                } else if ($validatePrice == "0" || $validatePrice == "1") {
                    $value->venta = $value->venta - 1;
                }

                $value->save();
            }
            $respuest = $api->updateProperty($value->id);
        }
    }
}
