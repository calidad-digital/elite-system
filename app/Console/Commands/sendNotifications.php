<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Seguimiento;
use App\Models\AlertaActividad;
use App\Http\Controllers\MailController;
use Carbon\Carbon;

class sendNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elite:notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio de notificaciones por correo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        dump("Iniciando proceso de envio de mensajes...");

        $date = date('Y-m-d H:i:s');
        $tiempoMin = Carbon::create($date);
        $tiempoMin->minutes(0);
        $tiempoMin->seconds(0);


        $alertas = AlertaActividad::with('asignado','creador')
        ->where('fecha_asignada',$tiempoMin)
        ->get();

        dump('Total notificaciones a enviar: '.count($alertas));

        if(count($alertas)>0){

            foreach ($alertas as $alerta) {
                if($alerta->fue_notificado_email == 0){

                    
                    $actividad = Seguimiento::with('inmuebles')->find($alerta->actividad_id);
               
                    $message = file_get_contents(url("assets/mail-templates/notificacion.html"));
                    $message = str_replace("%activity%", $alerta->tipo_actividad??'N/A', $message);
                    $message = str_replace("%extra_info%", "Código MLS", $message);
                    $message = str_replace("%extra_info_code%", $actividad->inmuebles->codigoMLS??'N/A', $message);
                    $message = str_replace("%create_date%", $alerta->created_at??'N/A', $message);
                    $message = str_replace("%agent%", $alerta->creador->nombres??'N/A' . $alerta->creador->apellidos??'N/A', $message);
                    $message = str_replace("%comment%", $alerta->comentario??'N/A', $message);
                    
                    $destinatarios = [];
        
                    array_push($destinatarios, [
                        "name" => $alerta["asignado"]["nombres"]??'N/A' . $alerta["asignado"]["apellidos"]??'N/A',
                        "email" => $alerta->asignado->email??'N/A'
                    ]);
                    
                    $mail = new MailController();
                    dump('Enviando correo a: '.$destinatarios[0]['email']??'');
                    $mail->sendMail("Notificación elite System", $message, $destinatarios);

                    $alerta->fue_notificado_email = 1;
                    $alerta->save();
                }

            }
            
        }


    }
}