<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ventas1Back extends FormRequest
{
    /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
    public function authorize()
    {
        return true;
    }

    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules()
    {
        return [
            "comprador_1_cedula_url" => "file|max:1024",
            "comprador_2_cedula_url" => "file|max:1024",
            "comprador_3_cedula_url" => "file|max:1024",
            "comprador_4_cedula_url" => "file|max:1024",
            "clt_url" => "file|max:1024",
            "oferta_compra_url" => "file|max:1024",
            "escritura_publica_url" => "file|max:1024",
            "promesa_firmada_url" => "file|max:1024",
            "clt_parqueadero_1_url" => "file|max:1024",
            "clt_parqueadero_2_url" => "file|max:1024",
            "camara_comercio_url" => "file|max:1024",
            "clt_deposito_1_url" => "file|max:1024",
            "clt_deposito_2_url" => "file|max:1024",
            "poder_copia_diligenciada_url" => "file|max:1024"
        ];
    }

    /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
    public function messages()
    {
        return [
            "max" => ":attribute no debe ser mayor que :max kilobytes"
        ];
    }

    /**
    * Get custom attributes for validator errors.
    *
    * @return array
    */
    public function attributes()
    {
        return [
            "comprador_1_cedula_url" => "La cedula del comprador 1",
            "comprador_2_cedula_url" => "La cedula del comprador 2",
            "comprador_3_cedula_url" => "La cedula del comprador 3",
            "comprador_4_cedula_url" => "La cedula del comprador 4",
            "clt_url" => "El CLT",
            "oferta_compra_url" => "La oferta de compra",
            "escritura_publica_url" => "La escritura pública",
            "promesa_firmada_url" => "La promesa firmada",
            "clt_parqueadero_1_url" => "El CLT parqueadero",
            "clt_parqueadero_2_url" => "El CLT parqueadero 2",
            "camara_comercio_url" => "La cámara de comercio",
            "clt_deposito_1_url" => "El CLT depósito",
            "clt_deposito_2_url" => "El CLT depósito 2",
            "poder_copia_diligenciada_url" => "El poder en copia diligenciado"
        ];
    }

}
