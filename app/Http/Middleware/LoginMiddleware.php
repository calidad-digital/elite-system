<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class LoginMiddleware
{
    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
    public function handle(Request $request, Closure $next)
    {
        if ($request->session()->has("usuario")) {
            $request->session()->put("usuario", $request->session()->get("usuario"));
            return $next($request);
        } else {
            return redirect("admin/login");
        }
    }
}
