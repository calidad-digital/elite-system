<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Usuario;

class AuthenticationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->header("Authorization") != "") {
            $token = $request->header("Authorization");
            $usuario = Usuario::with(["estados", "empresas.estados"])
            ->where("token", $token)
            ->first();

            if ($usuario != null) {

                if (!$usuario->estados->is_active) {
                    return response()->json([
                        "error" => "401",
                        "mensaje" => "El usuario no se encuentra activo",
                    ], 401);
                }

                if (!$usuario->empresas->estados->is_active) {
                    return response()->json([
                        "error" => "401",
                        "mensaje" => "La empresa no se encuentra activa",
                    ], 401);
                }

                $request->mergeIfMissing(["user" => $usuario]);

                return $next($request);
            } else {
                return response()->json([
                    "error" => "404",
                    "mensaje" => "El usuario no existe",
                ], 404);
            }
        } else {
            return response()->json([
                "error" => "401",
                "mensaje" => "Debe enviar authorization header para acceder a este recurso",
            ], 401);
        }
    }
}
