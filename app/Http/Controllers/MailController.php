<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MailController extends Controller
{
    public function sendMail($asunto, $message, $destinatarios) {
        // return $destinatarios;
        try {
            $client = new \GuzzleHttp\Client();

            $res = $client->request("POST", "https://api.sendinblue.com/v3/smtp/email", [
                "headers" => [
                    "accept" => "application/json",
                    "api-key" => "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",
                    "content-type" => "application/json"
                ],
                "json" => [
                    "sender" => [
                        "name" => "Notificaciones Elite System",
                        "email" => "mercadeo@eliteinmobiliaria.com.co"
                    ],
                    "to" => $destinatarios,
                    "replyTo" => [
                        "name" => "Elite System",
                        "email" => "mercado@eliteinmobiliaria.com.co"
                    ],
                    "subject" => $asunto,
                    "htmlContent" => $message
                ]
            ]);

            return $res->getStatusCode();

        } catch (RequestException $e) {
            echo Psr7\Message::toString($e->getResponse());

            die;
        }
    }
}
