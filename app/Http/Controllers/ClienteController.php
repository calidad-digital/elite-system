<?php

namespace App\Http\Controllers;

//Modelos
use App\Models\Cliente;
use App\Models\Ciudad;
use App\Models\SubEstadoCivil;

//Paquetes
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class ClienteController extends Controller
{
    public function __construct() {
        $this->driver = config("app.files_driver");
    }

    public function generalList($content, $usuario) {
        $clientes = Cliente::with("roles", "estados")
        ->where("empresa_id", $usuario["empresa_id"])
        ->where(function($query) use ($content) {
            if (isset($content["status"]) && $content["status"] != "") {
                $query->where("estado_id", $content["status"]);
            }else{
                $query->where("estado_id", 1);

            }

        })
        ->where(function($query) use ($content) {
            if (isset($content["office"]) && $content["office"] != "") {
                $query->where("oficina_id", $content["office"]);
            }
        })
        ->where(function($query) use ($usuario) {
            if ($usuario['rol_id'] != 1) {
                $query->where("create_user_id", $usuario['id']);
            }
        })
        ->where(function($query) use ($content) {
            if (isset($content["city"]) && $content["city"] != "") {
                $query->where("ciudad_id", $content["city"]);
            }
        })
        ->where(function($query) use ($content) {
            if (isset($content["name"]) && $content["name"] != "") {
                $query->where("nombres", "like", "%" . $content["name"] . "%")
                ->orWhere("apellidos", "like", "%" . $content["name"] . "%");
            }
        })
        ->where(function($query) use ($content) {
            if (isset($content["email"]) && $content["email"] != "") {
                $query->where("email", "like", "%" . $content["email"] . "%");
            }
        })
        ->where(function($query) use ($content) {
            if (isset($content["document"]) && $content["document"] != "") {
                $query->where("documento", "like", "%" . $content["document"] . "%");
            }
        })
        ->where(function($query) use ($content) {
            if (isset($content["comment"]) && $content["comment"] != "") {
                $query->where("comentario", "like", "%" . $content["comment"] . "%");
            }
        })
        ->where(function($query) use ($content) {
            if (isset($content["phone"]) && $content["phone"] != "") {
                $query->where("telefono", "like", "%" . $content["phone"] . "%")
                ->orWhere("celular", "like", "%" . $content["phone"] . "%")
                ->orWhere("whatsapp", "like", "%" . $content["phone"] . "%");
            }
        })
        ->where(function($query) use ($content) {
            if (isset($content["rol_id"]) && $content["rol_id"] != "") {
                $query->where("rol_id", $content["rol_id"]);
            }
        })
        ->where(function($query) use ($content) {
            if (isset($content["property"]) && $content["property"] != "") {
                $query->whereHas("inmuebles", function ($query) use ($content) {
                    $query->whereHas("inmuebles", function ($query) use ($content) {
                        $query->where("codigo", $content["property"]);
                    });
                });
            }
        })

        ->orderBy("id", "desc")
        ->paginate(12);

        return $clientes;
    }

    public function index(Request $request) {
        $content = $request->all();
        return $this->generalList($content, $request["user"])->toArray();
    }

    public function show(Request $request, $id) {
        $cliente = Cliente::where("empresa_id", $request["user"]["empresa_id"])
        ->where("id", $id)
        ->orWhere("documento", $id)
        ->first();

        if ($cliente != null) {
            return $cliente;
        } else {
            return response()->json([
                "error" => "404",
                "mensaje" => "El cliente no existe",
            ], 404);
        }
    }

    public function store(Request $request) {
        $content = $request->all();

        $messages = [

            "name.required" => "El nombre es obligatorio",
            "last_name.required" => "El apellido es obligatorio",
            "document.unique" => "El documento registrado ya existe",
            "email.required" => "El email es obligatorio",
            "document_type.exists" => "El tipo de documento no existe",
            "status.exists" => "El estado seleccionado no existe",
            "role.exists" => "El rol seleccionado no existe",
        ];

        $validator = Validator::make($content, [
            "name" => "required|max:100",
            "last_name" => "required|max:100",
            "document" => "unique:clientes,documento|max:100",
            "email" => "required|max:100",
            "document_type" => "exists:tipo_documentos,id",
            "status" => "exists:estados,id",
            "role" => "exists:rols,id",
        ], $messages);

        if ($validator->fails()) {
            return response($validator->errors())->setStatusCode(401);
        }

        //Si existe el archivo enviado por api
        if ($request->hasFile("archivo_documento")) {
            $name = $content["username"] . "_cedula." . $request->file("archivo_documento")->extension();
            $path2 = $request->file("archivo_documento")->storeAs("clientes/documentos/cliente_{$content["username"]}", $name, $this->driver);

            $content["url_documento"] = "https://elitesystem1.s3.amazonaws.com/$path2";
        }

        $celular = $content["cellphone"] ?? "";

        $cliente = new Cliente();
        $cliente->username = $content["username"] ?? "";
        $cliente->password = $content["password"] ?? "";
        $cliente->nombres = $content["name"] ?? "";
        $cliente->direccion = $content["direccion"] ?? "";
        $cliente->apellidos = $content["last_name"] ?? "";
        $cliente->email = $content["email"] ?? "";
        $cliente->documento = $content["document"] ?? "";
        $cliente->telefono = $content["phone"] ?? "";
        $cliente->celular = $celular ?? "";
        $cliente->whatsapp = $content["whatsapp"] ?? $celular;
        // $cliente->foto = $content["picture"] ?? "";
        $cliente->url_documento = $content["url_documento"] ?? "";
        $cliente->comentario = $content["comment"] ?? "";
        $cliente->expedicion = $content["expedicion"] ?? "";
        $cliente->subestado_civil_id = $content["subestado_civil"] ?? NULL;
        $cliente->profesion = $content["profession"] ?? "";
        $cliente->estado_civil = $content["estado_civil"] ?? "";

        //Ciudad
        if (isset($content["ciudad"]) && $content["ciudad"] != "") {
            if($content["ciudad"] == 'ext'){
                $cliente->ciudad = $content["ciudad_ext"];
                $cliente->ciudad_ext = 1;
                $cliente->ciudad_id = $request["user"]["ciudad_id"];
                $cliente->estado_ubicacions_id = $request["user"]["estado_ubicacions_id"];
            }else{
                $ciudad = Ciudad::find($content["ciudad"]);

                if ($ciudad != null) {
                    $cliente->ciudad_id = $ciudad->id;
                    $cliente->ciudad_ext = 0;
                    $cliente->ciudad = $ciudad->nombre;
                    $cliente->estado_ubicacions_id = $ciudad->estado_ubicacions_id;
                } else {
                    return response()->json([
                        "error" => "404",
                        "mensaje" => "No se encontró la ciudad seleccionada",
                    ], 404);
                }
            }
        } else {
            $cliente->ciudad = $request["user"]["ciudad"];
            $cliente->ciudad_ext = 0;
            $cliente->ciudad_id = $request["user"]["ciudad_id"];
            $cliente->estado_ubicacions_id = $request["user"]["estado_ubicacions_id"];
        }

        $cliente->fecha_nacimiento = $content["birthdate"] ?? "1990-01-01";
        $cliente->tipo_documento_id = $content["document_type"] ?? 1;
        $cliente->estado_id = $content["status"] ?? 1;
        $cliente->rol_id = $content["role"] ?? 1;

        $cliente->empresa_id = $request["user"]["empresa_id"];
        $cliente->oficina_id = $request["user"]["oficina_id"];
        $cliente->create_user_id = $request["user"]["id"];
        $cliente->update_user_id = $request["user"]["id"];

        $cliente->save();

        return $cliente;
    }

    public function update(Request $request, $id) {
        $content = $request->all();

        $messages = [
            "username.unique" => "El usuario registrado ya existe",
            "name.required" => "El nombre es obligatorio",
            "last_name.required" => "El apellido es obligatorio",
            "document.required" => "El documento es obligatorio",
            "document.unique" => "El documento registrado ya existe",
            "email.required" => "El email es obligatorio",
            "email.unique" => "El email registrado ya existe",
            "document_type.exists" => "El tipo de documento no existe",
            "status.exists" => "El estado seleccionado no existe",
            "role.exists" => "El rol seleccionado no existe",
        ];

        $validator = Validator::make($content, [
            "username" => "sometimes|required|max:100",
            "name" => "sometimes|required|max:100",
            "last_name" => "sometimes|required|max:100",
            "document" => "sometimes|required|max:100",
            "email" => "sometimes|required|max:100",
            "document_type" => "exists:tipo_documentos,id",
            "status" => "exists:estados,id",
            "role" => "exists:rols,id",
        ], $messages);

        if ($validator->fails()) {
            return $validator->errors();
        }

        $cliente = Cliente::find($id);

        if ($cliente != null) {

            if (isset($content["name"]) && $content["name"] != "") {
                $cliente->nombres = $content["name"];
            }

            if (isset($content["last_name"]) && $content["last_name"] != "") {
                $cliente->apellidos = $content["last_name"];
            }

            if (isset($content["password"]) && $content["password"] != "") {
                $cliente->password = $content["password"];
            }

            if (isset($content["direccion"]) && $content["direccion"] != "") {
                $cliente->direccion = $content["direccion"];
            }

            if (isset($content["email"]) && $content["email"] != "") {
                $cliente->email = $content["email"];
            }

            if (isset($content["username"]) && $content["username"] != "") {
                $cliente->username = $content["username"];
            }

            if (isset($content["document"]) && $content["document"] != "") {
                $cliente->documento = $content["document"];
            }

            if (isset($content["expedicion"]) && $content["expedicion"] != "") {
                $cliente->expedicion = $content["expedicion"];
            }

            if (isset($content["phone"]) && $content["phone"] != "") {
                $cliente->telefono = $content["phone"];
            }

            if (isset($content["cellphone"]) && $content["cellphone"] != "") {
                $cliente->celular = $content["cellphone"];
                $cliente->whatsapp = $content["cellphone"];
            }

            if (isset($content["whatsapp"]) && $content["whatsapp"] != "") {
                $cliente->whatsapp = $content["whatsapp"];
            }

            if (isset($content["picture"]) && $content["picture"] != "") {
                $cliente->foto = $content["picture"];
            }

            if (isset($content["url_documento"]) && $content["url_documento"] != "") {
                $cliente->url_documento = $content["url_documento"];
            }

            if (isset($content["comment"]) && $content["comment"] != "") {
                $cliente->comentario = $content["comment"];
            }

            if (isset($content["profession"]) && $content["profession"] != "") {
                $cliente->profesion = $content["profession"];
            }

            //Ciudad
            if (isset($content["ciudad"]) && $content["ciudad"] != "") {
                if ($content["ciudad"] == "ext"){
                    $cliente->ciudad = $content["ciudad_ext"];
                    $cliente->ciudad_ext = 1;
                    $cliente->ciudad_id = $request["user"]["ciudad_id"];
                    $cliente->estado_ubicacions_id = $request["user"]["estado_ubicacions_id"];
                } else {
                    $ciudad = Ciudad::find($content["ciudad"]);

                    if ($ciudad != null) {
                        $cliente->ciudad_id = $ciudad->id;
                        $cliente->ciudad_ext = 0;
                        $cliente->ciudad = $ciudad->nombre;
                        $cliente->estado_ubicacions_id = $ciudad->estado_ubicacions_id;
                    } else {
                        return response()->json([
                            "error" => "404",
                            "mensaje" => "No se encontró la ciudad seleccionada",
                        ], 404);
                    }
                }
            }

            if (isset($content["birthdate"]) && $content["birthdate"] != "") {
                $cliente->fecha_nacimiento = $content["birthdate"];
            }

            if (isset($content["profession"]) && $content["profession"] != "") {
                $cliente->profesion = $content["profession"];
            }

            if (isset($content["document_type"]) && $content["document_type"] != "") {
                $cliente->tipo_documento_id = $content["document_type"];
            }

            if (isset($content["status"]) && $content["status"] != "") {
                $cliente->estado_id = $content["status"];
            }

            if (isset($content["role"]) && $content["role"] != "") {
                $cliente->rol_id = $content["role"];
            }

            if (isset($content["office_id"]) && $content["office_id"] != "") {
                $cliente->oficina_id = $content["office_id"];
            }

            if (isset($content["estado_civil"]) && $content["estado_civil"] != "") {
                $cliente->estado_civil = $content["estado_civil"];
            }

            if (isset($content["subestado_civil"]) && $content["subestado_civil"] != "") {
                $cliente->subestado_civil_id = $content["subestado_civil"];
            }

            $cliente->update_user_id = $request["user"]["id"];
            $cliente->save();

            return $cliente;
        } else {
            return response()->json([
                "error" => "404",
                "mensaje" => "El cliente no existe",
            ], 404);
        }
    }

    public function destroy($id) {
        $cliente = Cliente::find($id);

        if ($cliente != null) {
            $cliente->delete();

            return response()->json([
                "error" => "200",
                "mensaje" => "Cliente eliminado con éxito",
            ], 200);
        } else {
            return response()->json([
                "error" => "404",
                "mensaje" => "El cliente no existe",
            ], 404);
        }
    }

    public function dashboard(Request $request) {
        $usuario = session("usuario");
        $content = $request->all();

        $clientes = $this->generalList($content, $usuario);
        $ciudades = new CiudadController();
        $estados = new EstadoController();
        $roles = new RolController();
        $tipos_documento = new TipoDocumentoController();
        $subestado_civil = SubEstadoCivil::get();

        GeneralController::renderHeader("Clientes", $usuario, "activeClientes", true);

        echo view("components.clientes.index")
        ->with("clientes", $clientes)
        ->with("ciudades", $ciudades->generalList())
        ->with("estados", $estados->generalList())
        ->with("roles", $roles->generalList(2))
        ->with("estados_civiles", $subestado_civil)
        ->with("ext", "ext")
        ->with("tipos_documento", $tipos_documento->generalList())
        ->with("request", $content);

        GeneralController::renderFooter();
    }

    public function crearCliente(Request $request) {
        $usuario = session("usuario");
        $content = $request->all();
        // return $content;

        // return $content["url_documento"];
        $cliente = GeneralController::postRequest(url("api/clientes"), [
            "Authorization" => $usuario->token
        ], $content);
        // return $cliente;
        if ($request->file("foto")) {
            if ($request->file('foto')->isValid()) {

                $name = $cliente['id'] . "_foto." . $request->file("foto")->extension();
                $path = $request->file("foto")->storeAs("clientes/fotos/cliente_{$cliente['id']}", $name, $this->driver);

                $content["picture"] = "https://elitesystem1.s3.amazonaws.com/$path";
            }else{
                return redirect()
            ->route("listaClientes")
            ->with("mensaje", "El archivo tiene un formato invalido o esta corrupto.");
            }
        }

        if ($request->file("archivo_documento")) {
            if ($request->file('archivo_documento')->isValid()) {

                $name = $cliente['id'] . "_cedula." . $request->file("archivo_documento")->extension();
                $path2 = $request->file("archivo_documento")->storeAs("clientes/documentos/cliente_{$cliente['id']}", $name, $this->driver);

                $content["url_documento"] = "https://elitesystem1.s3.amazonaws.com/$path2";
            }else{
                return redirect()
                ->route("listaClientes")
                ->with("mensaje", "El archivo tiene un formato invalido o esta corrupto.");
            }
        }
        $archivosCliente = Cliente::find($cliente['id']);
        $archivosCliente->foto = $content["picture"] ?? ($archivosCliente->foto??'') ;
        $archivosCliente->url_documento = $content["url_documento"] ?? ($archivosCliente->url_documento??'');
        $archivosCliente->save();

        if (!isset($cliente["username"]) || $cliente["username"] != $content["username"]) {
            if (isset($path)) {
                Storage::disk($this->driver)->delete($path);
            }

            if (isset($path2)) {
                Storage::disk($this->driver)->delete($path2);
            }

            $content["picture"] = "";
            $content["foto"] = "";
            $content["archivo_documento"] = "";
            $content["url_documento"] = "";

            return redirect()
            ->route("listaClientes")
            ->with("content", $content)
            ->with("error", $cliente);
        } else {
            $request->session()->forget("error");

            return redirect()
            ->route("listaClientes")
            ->with("mensaje", "Cliente creado exitosamente");
        }
    }

    public function editarCliente(Request $request, $id) {
        $usuario = session("usuario");
        $content = $request->all();
        // return $content;

        if ($request->file("foto")) {
            $name = $content["username"] . "_foto." . $request->file("foto")->extension();
            $path = $request->file("foto")->storeAs("clientes/fotos/cliente_{$content["username"]}", $name, $this->driver);

            $content["picture"] = "https://elitesystem1.s3.amazonaws.com/$path";
        }

        if ($request->file("archivo_documento")) {
            $name = $content["username"] . "_cedula." . $request->file("archivo_documento")->extension();
            $path2 = $request->file("archivo_documento")->storeAs("clientes/documentos/cliente_{$content["username"]}", $name, $this->driver);

            $content["url_documento"] = "https://elitesystem1.s3.amazonaws.com/$path2";
        }

        $cliente = GeneralController::putRequest(url("api/clientes/$id"), [
            "Authorization" => $usuario->token
        ], $content);

        if (!isset($cliente["username"]) || $cliente["username"] != $content["username"]) {
            if (isset($path)) {
                Storage::disk($this->driver)->delete($path);
            }

            if (isset($path2)) {
                Storage::disk($this->driver)->delete($path2);
            }

            $content["picture"] = "";
            $content["foto"] = "";
            $content["archivo_documento"] = "";
            $content["url_documento"] = "";

            return redirect()
            ->route("listaClientes")
            ->with("error", $cliente);
        } else {
            $request->session()->forget("error");

            return redirect()
            ->route("listaClientes")
            ->with("mensaje", "Cliente actualizado exitosamente");
        }
    }
}
