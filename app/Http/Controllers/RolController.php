<?php

namespace App\Http\Controllers;

//Modelos
use App\Models\Rol;

//Paquetes
use Illuminate\Http\Request;

class RolController extends Controller
{
    public function generalList($tipo = 1) {
        $rol = Rol::where("tipo_rol", $tipo)->get();
        return $rol;
    }
}
