<?php

namespace App\Http\Controllers;

//Modelos
use App\Models\TipoDocumento;

//Paquetes
use Illuminate\Http\Request;

class TipoDocumentoController extends Controller
{
    public function generalList() {
        $rol = TipoDocumento::get();
        return $rol;
    }
}
