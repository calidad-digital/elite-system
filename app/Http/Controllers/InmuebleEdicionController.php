<?php

namespace App\Http\Controllers;

//Modelos
use App\Models\Inmueble;
use App\Models\TipoInmueble;
use App\Models\Gestion;
use App\Models\EstadoUbicacion;
use App\Models\Destino;
use App\Models\Ciudad;
use App\Models\Zona;
use App\Models\Estrato;
use App\Models\Usuario;
use App\Models\Dir1;
use App\Models\Dir2;
use App\Models\Dir3;
use App\Models\Dir4;
use App\Models\Oficina;
use App\Models\InmuebleCaracteristica;
use App\Models\InmuebleDireccion;
use App\Models\InmuebleImagen;
use App\Models\Cliente;
use App\Models\InmuebleCliente;
use App\Models\TipoDocumentoInmueble;
use App\Models\DocumentoInmueble;
use App\Models\SubEstadoCivil;

//Paquetes
use Illuminate\Http\Request;

class InmuebleEdicionController extends Controller
{
    public function editarInmuebleBack(Request $request, $id) {
        $usuario = session("usuario");
        $content = $request->all();

        if(isset($content['parq1_chip']) && $content['parq1_chip'] == '125AAA'){
            return $content;
        }
        //Vamos a crear el inmueble en la base de datos
        $inmueble = Inmueble::find($id);
        $inmueble->update_user_id = $usuario->id;

        if (isset($content["tipo"]) && $content["tipo"] != "" && $content["tipo"] != $inmueble->tipo_inmueble_id) {
            $inmueble->tipo_inmueble_id = $content["tipo"];
        }

        if (isset($content["agente"]) && $content["agente"] != "" && $content["agente"] != $inmueble->asesor_id) {
            $inmueble->asesor_id = $content["agente"];
        }

        if (isset($content["gestion"]) && $content["gestion"] != "" && $content["gestion"] != $inmueble->gestion_id) {
            $inmueble->gestion_id = $content["gestion"];
        }

        if (isset($content["corretaje"]) && $content["corretaje"] != "" && $content["corretaje"] != $inmueble->corretaje) {
            $inmueble->corretaje = $content["corretaje"];
        }

        if (isset($content["destino"])  && $content["destino"] != $inmueble->destino_id) {
            $inmueble->destino_id = $content["destino"];
        }

        if (isset($content["canon"]) && $content["canon"] != "0" && $content["canon"] != $inmueble->canon) {
            $inmueble->canon = str_replace(",", "", $content["canon"]);
        }

        if (isset($content["administracion"]) && $content["administracion"] != "0" && $content["administracion"] != $inmueble->administracion) {
            $inmueble->administracion = str_replace(",", "", $content["administracion"]);
        }

        if (isset($content["venta"]) && $content["venta"] != "0" && $content["venta"] != $inmueble->venta) {
            $inmueble->venta = str_replace(",", "", $content["venta"]);
        }

        if (isset($content["fecha_consignacion"]) && $content["fecha_consignacion"] != "0" && $content["fecha_consignacion"] != $inmueble->fecha_consignacion) {
            $inmueble->fecha_consignacion = $content["fecha_consignacion"];
        }

        if (isset($content["departamento"]) && $content["departamento"] != "" && $content["departamento"] != $inmueble->estado_ubicacion_id) {
            $inmueble->estado_ubicacion_id = $content["departamento"];
        }

        if (isset($content["ciudad"]) && $content["ciudad"] != "" && $content["ciudad"] != $inmueble->ciudad_id) {
            $inmueble->ciudad_id = explode(":", $content["ciudad"])[1];
        }

        if (isset($content["zona"])  && $content["zona"] != $inmueble->zona_id) {
            $inmueble->zona_id = $content["zona"];
        }

        if (isset($content["localidad"])  && $content["localidad"] != $inmueble->localidad_id) {
            $inmueble->localidad_id = explode(":", $content["localidad"])[1];
        }

        if (isset($content["estrato"])  && $content["estrato"] != $inmueble->estrato_id) {
            $inmueble->estrato_id = $content["estrato"];
        }

        if (isset($content["barrio_id"])  && $content["barrio_id"] != $inmueble->barrio_id) {
            $inmueble->barrio_id = explode(":", $content["barrio_id"])[1];
        }

        if (isset($content["barrio_comun"])  && $content["barrio_comun"] != $inmueble->barrio_comun) {
            $inmueble->barrio_comun = $content["barrio_comun"];
        }

        if (isset($content["direccion"])  && $content["direccion"] != $inmueble->direccion) {
            $inmueble->direccion = $content["direccion"];
        }

        if (isset($content["isCompleteAddress"]) && $content["isCompleteAddress"] == "on") {
            if (isset($content["complete_address"]) && $content["complete_address"] != "") {
                $inmueble->direccion = str_replace("null", "", str_replace("  ", "", $content["complete_address"]));
            }
        }

        if (isset($content["habitaciones"])  && $content["habitaciones"] != $inmueble->habitaciones) {
            $inmueble->habitaciones = $content["habitaciones"];
        }

        if (isset($content["banios"])  && $content["banios"] != $inmueble->banios) {
            $inmueble->banios = $content["banios"];
        }

        if (isset($content["depositos"])  && $content["depositos"] != $inmueble->depositos) {
            $inmueble->depositos = $content["depositos"];
        }

        if (isset($content["parqueaderos"])  && $content["parqueaderos"] != $inmueble->parqueaderos) {
            $inmueble->parqueaderos = $content["parqueaderos"];
        }

        if (isset($content["parqueaderos_cubiertos"])  && $content["parqueaderos_cubiertos"] != $inmueble->parqueaderos_cubiertos) {
            $inmueble->parqueaderos_cubiertos = $content["parqueaderos_cubiertos"];
        }

        if (isset($content["parqueaderos_descubiertos"])  && $content["parqueaderos_descubiertos"] != $inmueble->parqueaderos_descubiertos) {
            $inmueble->parqueaderos_descubiertos = $content["parqueaderos_descubiertos"];
        }

        if (isset($content["parqueaderos_dobles"])  && $content["parqueaderos_dobles"] != $inmueble->parqueaderos_dobles) {
            $inmueble->parqueaderos_dobles = $content["parqueaderos_dobles"];
        }

        if (isset($content["parqueaderos_sencillos"])  && $content["parqueaderos_sencillos"] != $inmueble->parqueaderos_sencillos) {
            $inmueble->parqueaderos_sencillos = $content["parqueaderos_sencillos"];
        }

        $inmueble->numero_parqueadero_1 = $content["parq1_numero"] ?? "";
        $inmueble->numero_parqueadero_2 = $content["parq2_numero"] ?? "";
        $inmueble->numero_parqueadero_3 = $content["parq3_numero"] ?? "";
        $inmueble->numero_deposito_1 = $content["dpto1_numero"] ?? "";
        $inmueble->numero_deposito_2 = $content["dpto2_numero"] ?? "";
        $inmueble->matricula_parqueadero_1 = $content["parq1_matricula"] ?? "";
        $inmueble->matricula_parqueadero_2 = $content["parq2_matricula"] ?? "";
        $inmueble->matricula_parqueadero_3 = $content["parq3_matricula"] ?? "";
        $inmueble->chip = $content["chip"] ?? "";
        $inmueble->cedula_catastral = $content["cedula_catastral"] ?? "";
        $inmueble->chip_parqueadero_1 = $content["parq1_chip"] ?? "";
        $inmueble->chip_parqueadero_2 = $content["parq2_chip"] ?? "";
        $inmueble->chip_parqueadero_3 = $content["parq3_chip"] ?? "";
        $inmueble->cedula_parqueadero_1 = $content["parq1_cedula"] ?? "";
        $inmueble->cedula_parqueadero_2 = $content["parq2_cedula"] ?? "";
        $inmueble->cedula_parqueadero_3 = $content["parq3_cedula"] ?? "";
        $inmueble->matricula_deposito_1 = $content["dpto1_matricula"] ?? "";
        $inmueble->matricula_deposito_2 = $content["dpto2_matricula"] ?? "";
        $inmueble->chip_deposito_1 = $content["dpto1_chip"] ?? "";
        $inmueble->chip_deposito_2 = $content["dpto2_chip"] ?? "";
        $inmueble->cedula_deposito_1 = $content["dpto1_cedula"] ?? "";
        $inmueble->cedula_deposito_2 = $content["dpto2_cedula"] ?? "";

        if (isset($content["area_lote"])  && $content["area_lote"] != $inmueble->area_lote) {
            $inmueble->area_lote = $content["area_lote"];
        }

        if (isset($content["area_cons"]) && $content["area_cons"] != $inmueble->area_construida) {
            $inmueble->area_construida = $content["area_cons"];
        }

        if (isset($content["area_privada"])  && $content["area_privada"] != $inmueble->area_privada) {
            $inmueble->area_privada = $content["area_privada"];
        }

        if (isset($content["frente"])  && $content["frente"] != $inmueble->frente) {
            $frente = str_replace("," , ".", $content["frente"]);
            $inmueble->frente = $frente;
        }

        if (isset($content["fondo"])  && $content["fondo"] != $inmueble->fondo) {
            $fondo = str_replace("," , ".", $content["fondo"]);
            $inmueble->fondo = $fondo;
        }

        if (isset($content["construccion"]) && $content["construccion"] != "" && $content["construccion"] != $inmueble->anio_construccion) {
            $inmueble->anio_construccion = $content["construccion"];
        }

        if (isset($content["nivel"])  && $content["nivel"] != $inmueble->nivel) {
            $inmueble->nivel = $content["nivel"];
        }


        $inmueble->video = $content["video"]??'';
        $inmueble->tour_virtual = $content["tour_virtual"]??'';


        if (isset($content["sucursal"])  && $content["sucursal"] != $inmueble->oficina_id) {
            $inmueble->oficina_id = $content["sucursal"];
        }

        if (isset($content["aviso"]) && $content["aviso"] == "1") {
            $inmueble->aviso_ventana = true;
        } else {
            $inmueble->aviso_ventana = false;
        }

        if (isset($content["destacado"]) && $content["destacado"] == "on") {
            $inmueble->destacado = true;
        } else {
            $inmueble->destacado = false;
        }

        if (isset($content["exclusivo"]) && $content["exclusivo"] == "on") {
            $inmueble->exclusivo = true;
        } else {
            $inmueble->exclusivo = false;
        }

        if (isset($content["remodelacion"]) && $content["remodelacion"] != $inmueble->anio_remodelacion) {
            $inmueble->anio_remodelacion = $content["remodelacion"];
        }

        if (isset($content["valor_renta"])  && $content["valor_renta"] != $inmueble->valor_renta) {
            $inmueble->valor_renta = str_replace(",", "", $content["valor_renta"]);
        }

        if (isset($content["comision"]) && $content["comision"] != $inmueble->comision) {
            $inmueble->comision = $content["comision"];
        }

        if (isset($content["matricula"])  && $content["matricula"] != $inmueble->matricula) {
            $inmueble->matricula = $content["matricula"];
        }

        if (isset($content["chip"])  && $content["chip"] != $inmueble->chip) {
            $inmueble->chip = $content["chip"];
        }

        if (isset($content["cedula_catastral"])  && $content["cedula_catastral"] != $inmueble->cedula_catastral) {
            $inmueble->cedula_catastral = $content["cedula_catastral"];
        }

        if (isset($content["descripcion"]) && $content["descripcion"] != $inmueble->descripcion) {
            $inmueble->descripcion = $content["descripcion"];
        }

        $inmueble->descripcion_metro_cuadrado = $content["descripcion"] ?? '';
        $inmueble->descripcion_mls = $content["descripcion"] ?? '';
        $inmueble->restricciones = $content["restricciones"] ?? '';
        $inmueble->comentario = $content["comentario"] ?? '';
        $inmueble->comentario2 = $content["comentario2"] ?? '';


        if (isset($content["latitude"]) && $content["latitude"] != "" && $content["latitude"] != $inmueble->latitud) {
            $inmueble->latitud = $content["latitude"];
        }

        if (isset($content["longitude"]) && $content["longitude"] != "" && $content["longitude"] != $inmueble->longitud) {
            $inmueble->longitud = $content["longitude"];
        }

        // return $content;

        //Guardando el inmueble
        $inmueble->save();

        //Características
        // return $content;
        foreach ($content as $key => $value) {
            $caract_id = explode("_", $key);
            if ($caract_id[0] == "caract") {

                $caracteristica = InmuebleCaracteristica::where("inmueble_id", $inmueble->id)
                ->where("caracteristica_id", $caract_id[1])
                ->first();

                if (empty($caracteristica) && $value == "on") {
                    $caract = new InmuebleCaracteristica();
                    $caract->inmueble_id = $inmueble->id;
                    $caract->caracteristica_id = $caract_id[1];
                    $caract->create_user_id = $usuario->id;
                    $caract->update_user_id = $usuario->id;
                    $caract->save();
                }

            }
        }

        //Dirección
        if (isset($content["isCompleteAddress"]) && $content["isCompleteAddress"] == "on") {
            $direccion = InmuebleDireccion::where("inmueble_id", $inmueble->id)->first();

            if ($direccion != null) {
                $direccion->delete();
            }
        } else {
            $direccion = InmuebleDireccion::where("inmueble_id", $inmueble->id)->first();

            if ($direccion != null) {
                $direccion->inmueble_id = $inmueble->id;

                if ($content["dir1"] != "" && $content["dir1"] != $direccion->c1_id) {
                    $direccion->c1_id = $content["dir1"];
                }

                if ($content["dir2"] != "" && $content["dir2"] != $direccion->c2) {
                    $direccion->c2 = $content["dir2"];
                }

                if ($content["dir3"] != "" && $content["dir3"] != $direccion->c3_id) {
                    $direccion->c3_id = $content["dir3"];
                }

                if ($content["dir4"] != "" && $content["dir4"] != $direccion->c4_id) {
                    $direccion->c4_id = $content["dir4"];
                }

                if ($content["dir5"] != "" && $content["dir5"] != $direccion->c5_id) {
                    $direccion->c5_id = $content["dir5"];
                }

                if ($content["dir6"] != "" && $content["dir6"] != $direccion->c6_id) {
                    $direccion->c6_id = $content["dir6"];
                }

                if ($content["dir7"] != "" && $content["dir7"] != $direccion->c7_id) {
                    $direccion->c7 = $content["dir7"];
                }

                if ($content["dir8"] != "" && $content["dir8"] != $direccion->c8_id) {
                    $direccion->c8_id = $content["dir8"];
                }

                if ($content["dir9"] != "" && $content["dir9"] != $direccion->c9_id) {
                    $direccion->c9 = $content["dir9"];
                }

                if ($content["dir10"] != "" && $content["dir10"] != $direccion->c10_id) {
                    $direccion->c10_id = $content["dir10"];
                }

                $direccion->complemento = $content["complemento"]??'';
                $direccion->complemento2 = $content["complemento2"]??'';
                $direccion->complemento3 = $content["complemento3"]??'';

                $direccion->update_user_id = $usuario->id;
                $direccion->save();
            } else {
                $direccion = new InmuebleDireccion();
                $direccion->inmueble_id = $inmueble->id;
                $direccion->c1_id = $content["dir1"];
                $direccion->c2 = $content["dir2"];
                $direccion->c3_id = $content["dir3"];
                $direccion->c4_id = $content["dir4"];
                $direccion->c5_id = $content["dir5"];
                $direccion->c6_id = $content["dir6"];
                $direccion->c7 = $content["dir7"];
                $direccion->c8_id = $content["dir8"];
                $direccion->c9 = $content["dir9"];
                $direccion->c10_id = $content["dir10"];
                $direccion->complemento = $content["complemento"] ?? "";
                $direccion->complemento2 = $content["complemento2"] ?? "";
                $direccion->create_user_id = $usuario->id;
                $direccion->update_user_id = $usuario->id;
                $direccion->save();
            }
        }

        return $inmueble;
    }

    public function editarInmueble(Request $request, $id) {
        $usuario = session("usuario");
        $content = $request->all();

        //Obtener el último codigo de inmueble
        $inmueble = Inmueble::with(["direcciones", "caracteristicas","asesores"])
        ->find($id);

        //Primera parte
        $tipos = TipoInmueble::get();
        $gestiones = Gestion::get();
        $destinos = Destino::get();
        $usuarios = Usuario::get();

        //Ubicación
        $departamentos = EstadoUbicacion::get();
        $zonas = Zona::get();
        $estratos = Estrato::get();

        //Dirección
        $dir1 = Dir1::get();
        $dir2 = Dir2::get();
        $dir3 = Dir3::get();
        $dir4 = Dir4::get();

        //Adicionales
        $sucursales = Oficina::get();

        //Características del inmueble
        $caracteristicas = [];
        foreach ($inmueble->caracteristicas as $caract) {
            array_push($caracteristicas, $caract->caracteristica_id);
        }


        GeneralController::renderHeader("Inmuebles", $usuario, "activeInmuebles", true, "expandedInmuebles");

        echo view("components.inmuebles.editar.editar")
        ->with("usuario", $usuario)
        ->with("inmueble", $inmueble)
        ->with("usuarios", $usuarios)
        ->with("tipos", $tipos)
        ->with("gestiones", $gestiones)
        ->with("destinos", $destinos)
        ->with("departamentos", $departamentos)
        ->with("zonas", $zonas)
        ->with("estratos", $estratos)
        ->with("dir1", $dir1)
        ->with("dir2", $dir2)
        ->with("dir3", $dir3)
        ->with("dir4", $dir4)
        ->with("sucursales", $sucursales)
        ->with("caracteristicas", $caracteristicas);

        GeneralController::renderFooter("inmuebles/editar");
    }

    public function editarImagenesInmueble(Request $request, $id) {
        $usuario = session("usuario");
        $content = $request->all();

        $inmueble = Inmueble::with(["imagenes" => function ($query) {
            $query->orderBy("orden", "asc");
        }])
        ->find($id);

        GeneralController::renderHeader("Inmuebles", $usuario, "activeInmuebles", true, "expandedInmuebles");

        echo view("components.inmuebles.editar.imagenes")
        ->with("inmueble", $inmueble)
        ->with("usuario", $usuario);

        GeneralController::renderFooter("inmuebles/editar");
    }

    public function editarPropietariosInmueble(Request $request, $id) {
        $usuario = session("usuario");
        $content = $request->all();

        $inmueble = Inmueble::find($id);
        $ciudades = new CiudadController();
        $estados = new EstadoController();
        $roles = new RolController();
        $tipos_documento = new TipoDocumentoController();
        $subestado_civil = SubEstadoCivil::get();
        $inmuebleClientes = Cliente::with(["inmuebles" => function ($inmueble) use ($id) {
            $inmueble->where("inmueble_id", $id);
        }])
        ->whereHas("inmuebles", function ($query) use ($id) {
            $query->where("inmueble_id", $id);
        })->get();

        GeneralController::renderHeader("Crear propietarios", $usuario, "activeInmueblesCrear", true, "expandedInmuebles");

        echo view("components.inmuebles.editar.propietarios")
        ->with("usuario", $usuario)
        ->with("inmueble", $inmueble)
        ->with("ciudades", $ciudades->generalList())
        ->with("estados", $estados->generalList())
        ->with("roles", $roles->generalList(2))
        ->with("estados_civiles", $subestado_civil)
        ->with("tipos_documento", $tipos_documento->generalList())
        ->with("inmuebleClientes", $inmuebleClientes);

        GeneralController::renderFooter("inmuebles/editar");
    }

    public function apiEditarPropietarios(Request $request, $id) {
        $content = $request->all();

        // return $content;

        $inmueble = Inmueble::find($id);

        if ($inmueble == null) {
            return response()->json([
                "status" => "404",
                "mensaje" => "Inmueble no encontrado",
            ], 404);
        }

        //Esto es solo para la creación y edición
        for ($i = 0; $i <= 30; $i++) {


            if (isset($content["owner_" . $i]) && $content["owner_" . $i] != 0) {

                $searchProspect = Cliente::find($content["owner_" . $i]);
                if($searchProspect->rol_id == 13){
                    $searchProspect->rol_id = 3;
                    $searchProspect->save();
                }

                $inmuebleHasCliente = InmuebleCliente::where("inmueble_id", $id)
                ->where("cliente_id", $content["owner_" . $i])
                ->first();

                if ($inmuebleHasCliente == null) {
                    $inmuebleCliente = new InmuebleCliente();
                    $inmuebleCliente->inmueble_id = $id;
                    $inmuebleCliente->cliente_id = $content["owner_" . $i];
                    $inmuebleCliente->rol_id = 3;
                    $inmuebleCliente->orden = $i;
                    $inmuebleCliente->porcentaje = $content["owner_percentage_" . $i];

                    $inmuebleCliente->save();
                } else {
                    $inmuebleHasCliente->orden = $i;
                    $inmuebleHasCliente->porcentaje = $content["owner_percentage_" . $i];

                    $inmuebleHasCliente->save();
                }
            }
        }

        if (isset($content["borrados"]) && $content["borrados"] != "") {
            foreach (explode(",", $content["borrados"]) as $key => $value) {
                if ($value != "") {
                    $inmuebleCliente = InmuebleCliente::find($value);
                    if ($inmuebleCliente != null) {
                        $inmuebleCliente->delete();
                    }
                }
            }
        }

        $inmuebleClientes = InmuebleCliente::where("inmueble_id", $id)
        ->get();

        return $inmuebleClientes;
    }

    public function editarDocumentosInmueble(Request $request, $id) {
        $usuario = session("usuario");
        $content = $request->all();

        $inmueble = Inmueble::with("documentos")
        ->find($id);
        // return $inmueble;

        $id_max = 23;
        if($inmueble->gestion_id != 1 || $inmueble->corretaje == 1){
            $id_max = 11;
        }
        $documentosOpcionales = TipoDocumentoInmueble::where('id','>',5)
        ->where('id','<',$id_max)
        ->Orwhere('id',20)
        ->Orwhere('id',23)
        ->get();

        $docs = [];

        foreach ($inmueble->documentos as $key => $value) {
            $docs[$value->tipo_documento_id] = $value->url;
        }

        // return $docs;

        GeneralController::renderHeader("Inmuebles", $usuario, "activeInmuebles", true, "expandedInmuebles");

        echo view("components.inmuebles.editar.documentos")
        ->with("tipos_doc", $documentosOpcionales)
        ->with("docs", $docs)
        ->with("inmueble", $inmueble);

        GeneralController::renderFooter("inmuebles/editar");
    }

    public function editarDocumentosInmuebleBack(Request $request, $id) {
        $content = $request->all();
        $usuario = session("usuario");
        $inmueble = Inmueble::find($id);

        if (isset($content["comentarios_documentos"]) && $content["comentarios_documentos"] != "" && $content["comentarios_documentos"] != $inmueble->comentarios_documentos ) {
            $inmueble->comentarios_documentos = $content["comentarios_documentos"];
            $inmueble->save();
        }

        $id_max = 23;
        if($inmueble->gestion_id != 1){
            $id_max = 11;
        }
        $documentosOpcionales = TipoDocumentoInmueble::where('id','>',5)
        ->where('id','<',$id_max)
        ->Orwhere('id',20)
        ->Orwhere('id',23)
        ->get();
        //Guardando documentos del cliente
        for ($i = 1; $i <= 5; $i++) {
            if ($request->hasFile("file_" . $i)) {

                $documento = $request->file("file_" . $i);
                // return $documento;

                switch ($i) {
                    case 1:
                    $documentName = "inmueble_" . $id . "_clt_documento." . $documento->extension();
                    $comentario = "CLT";
                    break;

                    case 2:
                    $documentName = "inmueble_" . $id . "_cedula_propietario_documento." . $documento->extension();
                    $comentario = "Cédula de propietario";
                    break;

                    case 3:
                    $documentName = "inmueble_" . $id . "_contrato_formado_documento." . $documento->extension();
                    $comentario = "Contrato firmado";
                    break;

                    case 4:
                    $documentName = "inmueble_" . $id . "_cuenta_cobro_administracion_documento." . $documento->extension();
                    $comentario = "Cuenta de cobro de administración";
                    break;

                    case 5:
                    $documentName = "inmueble_" . $id . "_docusign." . $documento->extension();
                    $comentario = "Docusign";
                    break;

                    default:
                    $documentName = "";
                    $comentario = "";
                    break;
                }

                $driver = "public";
                $url = $documento->storeAs("documentos/inmueble_" . $id, $documentName, $driver);

                //Guardando documento en base de datos
                $documentoExists = DocumentoInmueble::where("inmueble_id", $inmueble->id)
                ->where("tipo_documento_id", $i)
                ->first();

                if ($documentoExists == null) {
                    $documentoInmueble = new DocumentoInmueble();
                    $documentoInmueble->inmueble_id = $inmueble->id;
                    $documentoInmueble->tipo_documento_id = $i;
                    $documentoInmueble->is_active = true;
                    $documentoInmueble->comentario = $comentario;

                    if ($driver == "public") {
                        $documentoInmueble->url = url("storage/" . $url);
                    }

                    $documentoInmueble->save();
                } else {
                    $documentoExists->is_active = true;
                    $documentoExists->comentario = $comentario;
                    $documentoExists->url = url("storage/" . $url);
                    $documentoExists->save();
                }
            }
        }

        //Guardando documentos adicionales
        foreach ($documentosOpcionales as $key => $value) {
            if ($request->hasFile("file_" . $value->id) && $request->file("file_" . $value->id)->isValid()) {

                $documento = $request->file("file_" . $value->id);
                $comentario = $value->nombre;
                $name_format = mb_strtolower(str_replace(" " , "_", $comentario)) ;
                $documentName = "inmueble_" . $id . $name_format.'.' . $documento->extension();
                $driver = "public";
                $url = $documento->storeAs("documentos/inmueble_" . $id, $documentName, $driver);
                // guardando en base de datos
                $documentoExists = DocumentoInmueble::where("inmueble_id", $inmueble->id)
                ->where("tipo_documento_id", $value->id)
                ->first();

                if ($documentoExists == null) {
                    $documentoInmueble = new DocumentoInmueble();
                    $documentoInmueble->inmueble_id = $inmueble->id;
                    $documentoInmueble->tipo_documento_id = $value->id;
                    $documentoInmueble->is_active = true;
                    $documentoInmueble->comentario = $comentario;

                    if ($driver == "public") {
                        $documentoInmueble->url = url("storage/" . $url);
                    }

                    $documentoInmueble->save();
                } else {
                    $documentoExists->is_active = true;
                    $documentoExists->comentario = $comentario;
                    $documentoExists->url = url("storage/" . $url);
                    $documentoExists->save();
                }
            }
        }

        if (isset($content["revision"]) && $content["revision"] == 1) {
            //Vamos a crear el segundo seguimiento
            $seguimiento = [
                "property" => $inmueble->id,
                "status" => 3,
                "substatus" => 4,
                "description" => "Inmueble enviado a revisión"
            ];

            $cliente = GeneralController::postRequest(url("api/seguimientos"), [
                "Authorization" => $usuario->token
            ], $seguimiento);
        }

        // Actualizamos el inmueble en Domus
        if (isset($content["revision"]) && ($content["revision"] == 2 || $content["revision"] == 1)) {
            $api = new ApiController();
            $respuest = $api->updateProperty($inmueble->id);

            // return $respuest;
        }


        return redirect()
        ->route("listaInmuebles")
        ->with("mensaje", "Inmueble {$inmueble->codigo} actualizado exitosamente");
    }
}
