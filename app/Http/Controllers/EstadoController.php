<?php

namespace App\Http\Controllers;

//Modelos
use App\Models\Estado;

//Paquetes
use Illuminate\Http\Request;

class EstadoController extends Controller
{
    public function generalList() {
        $estado = Estado::get();
        return $estado;
    }
}
