<?php

namespace App\Http\Controllers;

//Modelos
use App\Models\Usuario;
use App\Models\Tutorial;

//Paquetes
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request) {
        $usuario = session("usuario");
        $tutoriales = Tutorial::get();

        //Header
        GeneralController::renderHeader("Inicio", $usuario, "activeDashboard");
        echo view("components.dashboard.index", [
            "tutoriales" => $tutoriales
        ]);
        GeneralController::renderFooter();
    }

    public function login(Request $request) {
        //Vista del login
        echo view("login.login")
        ->with("titulo", "Login");
    }

    public function validateLogin(Request $request) {
        $content = $request->all();

        //Validando si se envían campos

        if (isset($content["validation"]) && $content["validation"] != "") {
            return redirect()
            ->route("adminLogin")
            ->with("mensaje", "Éxito amiguito de lo ajeno");
        }

        if (!isset($content["username"]) || $content["username"] == "") {
            return redirect()
            ->route("adminLogin")
            ->with("mensaje", "Debe suministrar un nombre de usuario válido");
        }

        if (!isset($content["pwd"]) || $content["pwd"] == "") {
            return redirect()
            ->route("adminLogin")
            ->with("mensaje", "Debe suministrar una contraseña válida");
        }

        //Haciendo ahora sí el login
        $usuario = Usuario::with(["estados", "empresas.estados", "roles.permisos"])
        ->where("nombre_usuario", $content["username"])
        ->first();

        //Validación del usuario
        if ($usuario != null) {
            //Validación de la contraseña
            if ($usuario->ingreso == $content["pwd"]) {
                //Validación de estado del usuario
                if ($usuario->estados->is_active) {
                    //Validación de estado de la empresa
                    if ($usuario->empresas->estados->is_active) {
                        $request->session()->put("usuario", $usuario);
                        return redirect()
                        ->route("adminDashboard");
                    } else {
                        return redirect()
                        ->route("adminLogin")
                        ->with("mensaje", "La empresa no se encuentra activa");
                    }
                } else {
                    return redirect()
                    ->route("adminLogin")
                    ->with("mensaje", "El usuario no se encuentra activo");
                }
            } else {
                return redirect()
                ->route("adminLogin")
                ->with("mensaje", "Contraseña incorrecta");
            }
        } else {
            return redirect()
            ->route("adminLogin")
            ->with("mensaje", "El usuario no existe");
        }
    }

    public function logout(Request $request) {
        $request->session()->forget("usuario");

        return redirect()
        ->route("adminLogin");
    }
}
