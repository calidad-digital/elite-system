<?php

namespace App\Http\Controllers;

//Modelos
use App\Models\Usuario;
use App\Models\Ciudad;
use App\Models\Departamento;

//Paquetes
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class UsuarioController extends Controller
{
    public function __construct() {
        $this->driver = config("app.files_driver");
    }

    public function allUsers(){
        $usuarios = Usuario::get();

        return $usuarios;
    }

    public function altGeneralList($oficina) {
        $usuarios = Usuario::where("oficina_id", $oficina)
        ->get();

        return $usuarios;
    }

    public function generalList($content, $usuario) {
        $clientes = Usuario::with("roles", "estados", "departamentos")
        ->where("empresa_id", $usuario["empresa_id"])
        ->where(function($query) use ($content) {
            if(!isset($content["status"])){
                $query->where("estado_id", 1);
            }else if ($content["status"] != "" && $content["status"] != 0) {
                $query->where("estado_id", $content["status"]);
            }
        })
        ->where(function($query) use ($content) {
            if (isset($content["office"]) && $content["office"] != "") {
                $query->where("oficina_id", $content["office"]);
            }
        })
        ->where(function($query) use ($content) {
            if (isset($content["city"]) && $content["city"] != "") {
                $query->where("ciudad_id", $content["city"]);
            }
        })
        ->where(function($query) use ($content) {
            if (isset($content["name"]) && $content["name"] != "") {
                $query->where("nombres", "like", "%" . $content["name"] . "%")
                ->orWhere("apellidos", "like", "%" . $content["name"] . "%");
            }
        })
        ->where(function($query) use ($content) {
            if (isset($content["email"]) && $content["email"] != "") {
                $query->where("email", "like", "%" . $content["email"] . "%");
            }
        })
        ->where(function($query) use ($content) {
            if (isset($content["document"]) && $content["document"] != "") {
                $query->where("documento", "like", "%" . $content["document"] . "%");
            }
        })
        ->where(function($query) use ($content) {
            if (isset($content["comment"]) && $content["comment"] != "") {
                $query->where("comentario", "like", "%" . $content["comment"] . "%");
            }
        })
        ->where(function($query) use ($content) {
            if (isset($content["code"]) && $content["code"] != "") {
                $query->where("codigo_interno", "like", "%" . $content["code"] . "%");
            }
        })
        ->where(function($query) use ($content) {
            if (isset($content["phone"]) && $content["phone"] != "") {
                $query->where("telefono", "like", "%" . $content["phone"] . "%")
                ->orWhere("celular", "like", "%" . $content["phone"] . "%")
                ->orWhere("whatsapp", "like", "%" . $content["phone"] . "%");
            }
        })
        ->where(function($query) use ($content) {
            if (isset($content["rol_id"]) && $content["rol_id"] != "") {
                $query->where("rol_id", $content["rol_id"]);
            }
        })
        ->orderBy("id", "desc")
        ->paginate(12);

        return $clientes;
    }

    public function index(Request $request) {
        $content = $request->all();
        return $this->generalList($content, $request["user"])->toArray();
    }

    public function show(Request $request, $id) {
        $cliente = Usuario::where("empresa_id", $request["user"]["empresa_id"])
        ->where("id", $id)
        ->orWhere("documento", $id)
        ->first();

        if ($cliente != null) {
            return $cliente;
        } else {
            return response()->json([
                "error" => "404",
                "mensaje" => "El cliente no existe",
            ], 404);
        }
    }

    public function store(Request $request) {
        $content = $request->all();

        $messages = [
            "code.unique" => "El código interno registrado ya existe",
            "username.unique" => "El usuario registrado ya existe",
            "name.required" => "El nombre es obligatorio",
            "last_name.required" => "El apellido es obligatorio",
            "document.required" => "El documento es obligatorio",
            "document.unique" => "El documento registrado ya existe",
            "email.required" => "El email es obligatorio",
            "email.unique" => "El email registrado ya existe",
            "city.exists" => "La ciudad no existe",
            "document_type.exists" => "El tipo de documento no existe",
            "status.exists" => "El estado seleccionado no existe",
            "role.exists" => "El rol seleccionado no existe",
        ];

        $validator = Validator::make($content, [
            "code" => "required|unique:usuarios,codigo_interno|max:100",
            "username" => "required|unique:usuarios,nombre_usuario|max:100",
            "name" => "required|max:100",
            "last_name" => "required|max:100",
            "document" => "required|unique:usuarios,documento|max:100",
            "email" => "required|unique:usuarios,email|max:100",
            "city" => "exists:ciudads,id",
            "document_type" => "exists:tipo_documentos,id",
            "status" => "exists:estados,id",
            "role" => "exists:rols,id",
        ], $messages);

        if ($validator->fails()) {
            return $validator->errors();
        }

        $celular = $content["cellphone"] ?? "";

        $token = md5($content["username"]);

        $id_domus = null;
        if(isset($content["domus_token"])){
            $id = GeneralController::getRequest("http://api.domus.la/real-state/info", [
                "Authorization" => $content["domus_token"]
            ]);
            if(isset($id['data'][0]['broker_code']) && is_numeric($id['data'][0]['broker_code'])){
                $id_domus = $id['data'][0]['broker_code'];
            }
        }


        $cliente = new Usuario();
        $cliente->codigo_interno = $content["code"] ?? "";
        $cliente->codigo_interno_mls = $content["code"]."000" ?? "";
        $cliente->nombre_usuario = $content["username"] ?? "";
        $cliente->token = $token;
        $cliente->ingreso = $content["password"] ?? "";
        $cliente->nombres = $content["name"] ?? "";
        $cliente->apellidos = $content["last_name"] ?? "";
        $cliente->email = $content["email"] ?? "";
        $cliente->documento = $content["document"] ?? "";
        $cliente->telefono = $content["phone"] ?? "";
        $cliente->celular = $celular ?? "";
        $cliente->whatsapp = $content["whatsapp"] ?? $celular;
        $cliente->foto = $content["picture"] ?? "";
        $cliente->domus_token = $content["domus_token"] ?? "";
        $cliente->domus_id = $id_domus;
        $cliente->comentario = $content["comment"] ?? "";
        $cliente->comentario2 = $content["comment2"] ?? "";

        //Ciudad
        if (isset($content["city"]) && $content["city"] != "") {
            $ciudad = Ciudad::find($content["city"]);

            if ($ciudad != null) {
                $cliente->ciudad_id = $ciudad->id;
                $cliente->ciudad = $ciudad->nombre;
                $cliente->estado_ubicacions_id = $ciudad->estado_ubicacions_id;
            } else {
                return response()->json([
                    "error" => "404",
                    "mensaje" => "No se encontró la ciudad seleccionada",
                ], 404);
            }
        } else {
            $cliente->ciudad = $request["user"]["ciudad"];
            $cliente->ciudad_id = $request["user"]["ciudad_id"];
            $cliente->estado_ubicacions_id = $request["user"]["estado_ubicacions_id"];
        }

        //Departamento
        if (isset($content["department"]) && $content["department"] != "") {
            $departamento = Departamento::find($content["department"]);

            if ($departamento != null) {
                $cliente->departamento_id = $departamento->id;
                $cliente->departamento = $departamento->nombre;
            } else {
                return response()->json([
                    "error" => "404",
                    "mensaje" => "No se encontró el departamento seleccionado",
                ], 404);
            }
        } else {
            $cliente->departamento = $request["user"]["departamento"];
            $cliente->departamento_id = $request["user"]["departamento_id"];
        }

        $cliente->fecha_nacimiento = $content["birthdate"] ?? "1990-01-01";

        if (isset($content["entry_date"]) && $content["entry_date"] != "") {
            $cliente->fecha_ingreso = $content["entry_date"] . " 00:00:00";
        }

        if (isset($content["plan"]) && $content["plan"] != "") {
            $cliente->plan = $content["plan"];
            $cliente->fecha_ultimo_cambio_plan = date('Y-m-d H:i:s');
        }

        $cliente->tipo_documento_id = $content["document_type"] ?? 1;
        $cliente->estado_id = $content["status"] ?? 1;
        $cliente->rol_id = $content["role"] ?? 2;
        $cliente->orden = $content["order"] ?? 0;

        $cliente->empresa_id = $request["user"]["empresa_id"];
        $cliente->oficina_id = $request["user"]["oficina_id"];

        $cliente->save();

        return $cliente;
    }

    public function update(Request $request, $id) {
        $content = $request->all();

        $messages = [
            "username.unique" => "El usuario registrado ya existe",
            "name.required" => "El nombre es obligatorio",
            "last_name.required" => "El apellido es obligatorio",
            "document.required" => "El documento es obligatorio",
            "document.unique" => "El documento registrado ya existe",
            "email.required" => "El email es obligatorio",
            "email.unique" => "El email registrado ya existe",
            "city.exists" => "La ciudad no existe",
            "document_type.exists" => "El tipo de documento no existe",
            "status.exists" => "El estado seleccionado no existe",
            "role.exists" => "El rol seleccionado no existe",
        ];

        $validator = Validator::make($content, [
            "username" => "sometimes|required|max:100",
            "name" => "sometimes|required|max:100",
            "last_name" => "sometimes|required|max:100",
            "document" => "sometimes|required|max:100",
            "email" => "sometimes|required|max:100",
            "city" => "exists:ciudads,id",
            "document_type" => "exists:tipo_documentos,id",
            "status" => "exists:estados,id",
            "role" => "exists:rols,id",
        ], $messages);

        if ($validator->fails()) {
            return $validator->errors();
        }

        $cliente = Usuario::find($id);





        if ($cliente != null) {

            if (isset($content["code"]) && $content["code"] != "") {
                $cliente->codigo_interno = $content["code"];
            }

            if (isset($content["username"]) && $content["username"] != "") {
                $cliente->nombre_usuario = $content["username"];
            }

            if (isset($content["name"]) && $content["name"] != "") {
                $cliente->nombres = $content["name"];
            }

            if (isset($content["last_name"]) && $content["last_name"] != "") {
                $cliente->apellidos = $content["last_name"];
            }

            if (isset($content["password"]) && $content["password"] != "") {
                $cliente->ingreso = $content["password"];
            }

            if (isset($content["email"]) && $content["email"] != "") {
                $cliente->email = $content["email"];
            }

            if (isset($content["document"]) && $content["document"] != "") {
                $cliente->documento = $content["document"];
            }

            if (isset($content["domus_token"]) && $content["domus_token"] != "") {
                $cliente->domus_token = $content["domus_token"];
                $id_domus = '';
                $id = GeneralController::getRequest("http://api.domus.la/real-state/info", [
                    "Authorization" => $content["domus_token"]
                ]);
                if(isset($id['data'][0]['broker_code']) && is_numeric($id['data'][0]['broker_code'])){
                    $id_domus = $id['data'][0]['broker_code'];
                    $cliente->domus_id = $id_domus;
                }
            }

            if (isset($content["phone"]) && $content["phone"] != "") {
                $cliente->telefono = $content["phone"];
            }

            if (isset($content["cellphone"]) && $content["cellphone"] != "") {
                $cliente->celular = $content["cellphone"];
                $cliente->whatsapp = $content["cellphone"];
            }

            if (isset($content["whatsapp"]) && $content["whatsapp"] != "") {
                $cliente->whatsapp = $content["whatsapp"];
            }

            if (isset($content["picture"]) && $content["picture"] != "") {
                $cliente->foto = $content["picture"];
            }

            if (isset($content["comment"]) && $content["comment"] != "") {
                $cliente->comentario = $content["comment"];
            }

            if (isset($content["comment2"]) && $content["comment2"] != "") {
                $cliente->comentario2 = $content["comment2"];
            }

            //Ciudad
            if (isset($content["city"]) && $content["city"] != "") {
                $ciudad = Ciudad::find($content["city"]);

                if ($ciudad != null) {
                    $cliente->ciudad_id = $ciudad->id;
                    $cliente->ciudad = $ciudad->nombre;
                    $cliente->estado_ubicacions_id = $ciudad->estado_ubicacions_id;
                } else {
                    return response()->json([
                        "error" => "404",
                        "mensaje" => "No se encontró la ciudad seleccionada",
                    ], 404);
                }
            }

            if (isset($content["birthdate"]) && $content["birthdate"] != "") {
                $cliente->fecha_nacimiento = $content["birthdate"];
            }

            if (isset($content["entry_date"]) && $content["entry_date"] != "") {
                $cliente->fecha_ingreso = $content["entry_date"] . " 00:00:00";
            }

            if (isset($content["plan"]) && $content["plan"] != "" && $content["plan"] != $cliente->plan) {
                $cliente->plan = $content["plan"];
                $cliente->fecha_ultimo_cambio_plan = date('Y-m-d H:i:s');
            }

            if (isset($content["document_type"]) && $content["document_type"] != "") {
                $cliente->tipo_documento_id = $content["document_type"];
            }

            if (isset($content["order"]) && $content["order"] != "") {
                $cliente->orden = $content["order"];
            }

            if (isset($content["status"]) && $content["status"] != "") {
                $cliente->estado_id = $content["status"];
            }

            if (isset($content["department"]) && $content["department"] != "") {
                $cliente->departamento_id = $content["department"];
            }

            if (isset($content["role"]) && $content["role"] != "") {
                $cliente->rol_id = $content["role"];
            }

            if (isset($content["office_id"]) && $content["office_id"] != "") {
                $cliente->oficina_id = $content["office_id"];
            }

            $cliente->save();

            return $cliente;
        } else {
            return response()->json([
                "error" => "404",
                "mensaje" => "El cliente no existe",
            ], 404);
        }
    }

    public function destroy($id) {
        $cliente = Usuario::find($id);

        if ($cliente != null) {
            $cliente->delete();

            return response()->json([
                "error" => "200",
                "mensaje" => "Usuario eliminado con éxito",
            ], 200);
        } else {
            return response()->json([
                "error" => "404",
                "mensaje" => "El cliente no existe",
            ], 404);
        }
    }

    public function dashboard(Request $request) {
        $usuario = session("usuario");
        $content = $request->all();

        $usuarios = $this->generalList($content, $usuario);
        $ciudades = new CiudadController();
        $estados = new EstadoController();
        $roles = new RolController();
        $tipos_documento = new TipoDocumentoController();
        $departamentos = new DepartamentoController();

        GeneralController::renderHeader("Agentes", $usuario, "activeAgentes", true);

        echo view("components.agentes.index")
        ->with("agentes", $usuarios)
        ->with("ciudades", $ciudades->generalList())
        ->with("estados", $estados->generalList())
        ->with("roles", $roles->generalList(1))
        ->with("tipos_documento", $tipos_documento->generalList())
        ->with("departamentos", $departamentos->generalList())
        ->with("request", $content);

        GeneralController::renderFooter();
    }

    public function crearUsuario(Request $request) {
        $usuario = session("usuario");
        $content = $request->all();


        if ($request->file("foto")) {
            $name = $content["username"] . "_" . $content["name"] . "." . $request->file("foto")->extension();
            $path = $request->file("foto")->storePubliclyAs("agentes", $name, $this->driver);

            $content["picture"] = "https://elitesystem1.s3.amazonaws.com/$path";
        }

        $cliente = GeneralController::postRequest(url("api/usuarios"), [
            "Authorization" => $usuario->token
        ], $content);

        if (!isset($cliente["nombre_usuario"]) || $cliente["nombre_usuario"] != $content["username"]) {
            if (isset($path)) {
                Storage::disk($this->driver)->delete($path);
            }

            $content["picture"] = "";
            $content["foto"] = "";

            return redirect()
            ->route("listaAgentes")
            ->with("content", $content)
            ->with("error", $cliente);
        } else {
            $request->session()->forget("error");

            return redirect()
            ->route("listaAgentes")
            ->with("mensaje", "Usuario creado exitosamente");
        }
    }

    public function editarUsuario(Request $request, $id) {
        $usuario = session("usuario");
        $content = $request->all();

        if ($request->file("foto")) {
            $name = $content["username"] . "_" . $content["name"] . "." . $request->file("foto")->extension();
            $path = $request->file("foto")->storePubliclyAs("agentes", $name, $this->driver);

            $content["picture"] = "https://elitesystem1.s3.amazonaws.com/$path";
        }

        $cliente = GeneralController::putRequest(url("api/usuarios/$id"), [
            "Authorization" => $usuario->token
        ], $content);

        // return $content;
        // return $cliente;

        if (!isset($cliente["codigo_interno"]) || $cliente["codigo_interno"] != $content["code"]) {
            if (isset($path)) {
                Storage::disk("public")->delete($path);
            }

            $content["picture"] = "";
            $content["foto"] = "";

            return redirect()
            ->route("listaAgentes")
            ->with("error", $cliente);
        } else {
            $request->session()->forget("error");

            return redirect()
            ->route("listaAgentes")
            ->with("mensaje", "Usuario actualizado exitosamente");
        }
    }
}
