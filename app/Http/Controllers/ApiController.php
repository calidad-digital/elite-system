<?php

namespace App\Http\Controllers;

//Modelos
use App\Models\Inmueble;
use App\Models\Ciudad;
use App\Models\Usuario;

//Paquetes
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function __construct() {
        $this->endpoint = "http://api.domus.la/";
        $this->grupo = 21;
        // $this->grupo = 0;
    }

    public function detailUser($id) {
        $user = Usuario::find($id);

        return $user;
    }
    public function listProperties($usuario, $filter) {
        $response = GeneralController::getRequest("{$this->endpoint}pro-grid?a=1&{$filter}", [
            "Authorization" => $usuario->domus_token,
            "perpage" => 12,
            "grupo" => $this->grupo,
        ]);

        return $response;
    }

    public function propertyDetail($usuario, $cod,$id) {
        $url = isset($id) ? "pro-detail/{$cod}?idpro={$id}" : "pro-detail/{$cod}";
        $response = GeneralController::getRequest("{$this->endpoint}{$url}", [
            "Authorization" => $usuario->domus_token,
            "grupo" => $this->grupo,
        ]);

        return $response;
    }
    public function propertyDetailCode($cod) {
        $url = "pro-detail/{$cod}";
        $response = GeneralController::getRequest("{$this->endpoint}{$url}", [
            "Authorization" => 'APP_USR_74e5d19f88cc25a46c2fa04c6e2ce80d',
            "grupo" => $this->grupo,
        ]);
        $id= $response['data'][0]['idpro'];
        $responseB2b = GeneralController::getRequest("https://b2b.domus.la/b2b/detail-id/".$id, [
            "Authorization" => 'APP_USR_74e5d19f88cc25a46c2fa04c6e2ce80d',
            "grupo" => $this->grupo,
        ]);

        return $responseB2b;
    }

    public function listCiudades($usuario) {
        $response = GeneralController::getRequest("{$this->endpoint}cities", [
            "Authorization" => $usuario->domus_token,
            "perpage" => 12,
            "grupo" => $this->grupo,
        ]);

        return $response;
    }

    public function updatePortals(Request $request){
        $data = $request->all();
        // return $data;
        $response = GeneralController::getRequest("{$this->endpoint}/properties/update/portal/{$data['id']}", [
            "Authorization" => $data['usuario']['domus_token'],
            "perpage" => 12,
            "grupo" => $this->grupo,
        ]);

        return $response;
    }

    public function actualizarEstadoInmueble($usuario, $inmueble) {
        $post = [
            "status" => $inmueble["status"],
            "source" => $inmueble["source"],
            "value" => $inmueble["value"],
            "real_state"=>$inmueble["real_state"]
        ];

        // return $post;
        $response = GeneralController::putRequest("{$this->endpoint}property/update/status/{$inmueble["codigoMLS"]}", [
            "Authorization" => $usuario["domus_token"],
            "perpage" => 12
        ], $post, 1);

        return $response;
    }

    public function listGestiones($usuario) {
        $response = GeneralController::getRequest("{$this->endpoint}biz", [
            "Authorization" => $usuario->domus_token,
            "perpage" => 12,
            "grupo" => $this->grupo,
        ]);

        return $response;
    }

    public function listTiposInmueble($usuario) {
        $response = GeneralController::getRequest("{$this->endpoint}types", [
            "Authorization" => $usuario->domus_token,
            "perpage" => 12,
            "grupo" => $this->grupo,
        ]);

        return $response;
    }

    public function listEstados($usuario) {
        $response = GeneralController::getRequest("{$this->endpoint}propertystatus", [
            "Authorization" => $usuario->domus_token,
            "perpage" => 12,
            "grupo" => $this->grupo,
        ]);

        return $response;
    }

    public function createProperty($codigoInmueble) {
        $inmueble = Inmueble::with([
            "caracteristicas.caracteristicas",
            "direcciones",
            "estados",
            "asesores"
        ])
        ->with(["imagenes" => function ($query) {
            $query->orderBy("orden", "asc");
        }])
        ->find($codigoInmueble);

        $aEnviar = [
            "codpro" => $inmueble->codigoMLS,
            "address" => str_replace("null", "", $inmueble->direccion),
            "zone" => $inmueble->zona_id,
            "city" => Ciudad::find($inmueble->ciudad_id)->domus_id,
            "type" => $inmueble->tipo_inmueble_id,
            "biz" => $inmueble->gestion_id,
            "area_cons" => $inmueble->area_construida,
            "area_lot" => $inmueble->area_lote,
            "built_year" => $inmueble->anio_construccion != 0 ? $inmueble->anio_construccion : date("Y"),
            "remodeling_year" => $inmueble->anio_remodelacion != 0 ? $inmueble->anio_remodelacion : date("Y"),
            "destacado" => $inmueble->great,
            "level" => $inmueble->nivel,
            "parking" => $inmueble->parqueaderos,
            "parking_covered" => $inmueble->parqueaderos_cubiertos,
            "bedrooms" => $inmueble->habitaciones,
            "bathrooms" => $inmueble->banios,
            "stratum" => $inmueble->estrato_id,
            "administration" => $inmueble->administracion,
            "description" => $inmueble->descripcion,
            "broker" => $inmueble->asesores->domus_id,
            "catcher_broker" => $inmueble->asesores->domus_id,
            "neighborhood" => $inmueble->barrio_comun,
            "destination" => $inmueble->destino_id,
            "consignation_date" => $inmueble->fecha_consignacion,
            "latitude" => $inmueble->latitud,
            "longitude" => $inmueble->longitud,
            "status" => 1,
        ];

        if ($inmueble->gestion_id == 1) {
            $aEnviar["rent"] = $inmueble->canon;
        } else if ($inmueble->gestion_id == 2) {
            $aEnviar["saleprice"] = $inmueble->venta;
        } else {
            $aEnviar["rent"] = $inmueble->canon;
            $aEnviar["saleprice"] = $inmueble->venta;
        }

        if ($inmueble->matricula != "") {
            $aEnviar["registration"] = $inmueble->matricula;
        }
        if ($inmueble->descripcion_metro_cuadrado != "") {
            $aEnviar["descripcionmetrocuadrado"] = $inmueble->descripcion_metro_cuadrado;
        }
        if ($inmueble->aviso_ventana != "") {
            $aEnviar["window_sign"] = $inmueble->aviso_ventana;
        }

        if ($inmueble->descripcion_mls != "") {
            $aEnviar["description_mls"] = $inmueble->descripcion_mls;
        }

        if ($inmueble->restricciones != "") {
            $aEnviar["restrictions"] = $inmueble->restricciones;
        }

        if ($inmueble->comentario != "") {
            $aEnviar["comment"] = $inmueble->comentario;
        }

        if ($inmueble->comentario2 != "") {
            $aEnviar["comment2"] = $inmueble->comentario2;
        }

        if ($inmueble->video != "") {
            $aEnviar["video"] = $inmueble->video;
        }

        if ($inmueble->tour_virtual != "") {
            $aEnviar["tour3d"] = $inmueble->tour_virtual;
        }

        if ($inmueble->frente != "") {
            $aEnviar["front"] = $inmueble->frente;
        }

        if ($inmueble->fondo != "") {
            $aEnviar["rear"] = $inmueble->fondo;
        }

        if ($inmueble->area_privada != "") {
            $aEnviar["private_area"] = $inmueble->area_privada;
        }

        if ($inmueble->referencia != "") {
            $aEnviar["reference"] = $inmueble->referencia;
        }

        if ($inmueble->comision != 0) {
            $aEnviar["comission_percentage"] = $inmueble->comision;
        }

        if (count($inmueble->caracteristicas) > 0) {
            $amenities = "";
            foreach ($inmueble->caracteristicas as $caract) {
                $amenities .= "{$caract->caracteristicas->domus_id},";
            }
            $aEnviar["amenities"] = rtrim($amenities, ",");
        }

        if (isset($inmueble->direcciones->id)) {
            $aEnviar["dir_1"] = $inmueble->direcciones->c1_id;
            $aEnviar["dir_2"] = $inmueble->direcciones->c2;
            $aEnviar["dir_3"] = $inmueble->direcciones->c3_id;
            $aEnviar["dir_4"] = $inmueble->direcciones->c4_id;
            $aEnviar["dir_5"] = $inmueble->direcciones->c5_id;
            $aEnviar["dir_6"] = $inmueble->direcciones->c6_id;
            $aEnviar["dir_7"] = $inmueble->direcciones->c7;
            $aEnviar["dir_8"] = $inmueble->direcciones->c8_id;
            $aEnviar["dir_9"] = $inmueble->direcciones->c9;
            $aEnviar["dir_10"] = $inmueble->direcciones->c10_id;

            if ($inmueble->direcciones->complemento != "") {
                $aEnviar["complement_address"] = $inmueble->direcciones->complemento;
            }
        }

        if (count($inmueble->imagenes) > 0) {
            $imagenes = [];
            $imagenes360 = [];
            foreach ($inmueble->imagenes as $key => $value) {
                if (!$value->is_360) {
                    array_push($imagenes, $value);
                } else {
                    array_push($imagenes360, $value);
                }
            }

            foreach ($imagenes as $key => $value) {
                $aEnviar["image_" . ($key + 1)] = "{$value->url}";
            }

            foreach ($imagenes360 as $key => $value) {
                $aEnviar["image360_" . ($key + 1)] = "{$value->url}";
            }
        }

        // return $aEnviar;

        if ($inmueble->estados->id == 2  && $inmueble->publicado != 1 ) {

            $cliente = GeneralController::postRequest("http://api.domus.la/3.0/properties", [
                "Authorization" => $inmueble->asesores->domus_token
            ], $aEnviar, 1);

            // return $cliente;

            if (is_string($cliente)) {
                $cliente = json_decode($cliente, true);
            }

            if (is_array($cliente)) {
                if (!isset($cliente["error"]) && !isset($cliente["error"]) && count($cliente["errors"]) == 0) {

                    //Ya publicado el inmueble publicamos el mismo
                    $seguimiento = [
                        "property" => $inmueble->id,
                        "status" => 5,
                        "substatus" => 7,
                        "description" => "Inmueble publicado"
                    ];

                    $cliente2 = GeneralController::postRequest(url("api/seguimientos"), [
                        "Authorization" => $inmueble->asesores->token
                    ], $seguimiento);

                    $inmueble->publicado = 1;
                    $inmueble->save();
                } else {
                    return response()->json([
                        "error" => "401",
                        "mensaje" => $cliente,
                    ], 404);
                }
            } else {
                //Ya publicado el inmueble publicamos el mismo
                $seguimiento = [
                    "property" => $inmueble->id,
                    "status" => 5,
                    "substatus" => 7,
                    "description" => "Inmueble publicado"
                ];

                $cliente2 = GeneralController::postRequest(url("api/seguimientos"), [
                    "Authorization" => $inmueble->asesores->token
                ], $seguimiento);

                $inmueble->publicado = 1;
                $inmueble->save();
            }
        }

        return $aEnviar;
    }

    public function updateAndRedirectProperty($id){
        $update = $this->updateProperty($id);
        return redirect()
        ->route("listaInmuebles")
        ->with("mensaje", "Inmueble actualizado exitosamente");
    }

    public function updateProperty($codigoInmueble) {
        $inmueble = Inmueble::with([
            "caracteristicas.caracteristicas",
            "direcciones",
            "estados",
            "asesores"
        ])
        ->with(["imagenes" => function ($query) {
            $query->orderBy("orden", "asc");
        }])
        ->find($codigoInmueble);

        $aEnviar = [
            "codpro" => $inmueble->codigoMLS,
            "address" => str_replace("null", "", $inmueble->direccion),
            "zone" => $inmueble->zona_id,
            "city" => Ciudad::find($inmueble->ciudad_id)->domus_id,
            "type" => $inmueble->tipo_inmueble_id,
            "biz" => $inmueble->gestion_id,
            "area_cons" => $inmueble->area_construida,
            "area_lot" => $inmueble->area_lote,
            "built_year" => $inmueble->anio_construccion != 0 ? $inmueble->anio_construccion : date("Y"),
            "remodeling_year" => $inmueble->anio_remodelacion != 0 ? $inmueble->anio_remodelacion : date("Y"),
            "destacado" => $inmueble->great,
            "level" => $inmueble->nivel,
            "parking" => $inmueble->parqueaderos,
            "parking_covered" => $inmueble->parqueaderos_cubiertos,
            "bedrooms" => $inmueble->habitaciones,
            "bathrooms" => $inmueble->banios,
            "stratum" => $inmueble->estrato_id,
            "administration" => $inmueble->administracion,
            "description" => $inmueble->descripcion,
            "broker" => $inmueble->asesores->domus_id,
            "catcher_broker" => $inmueble->asesores->domus_id,
            "neighborhood" => $inmueble->barrio_comun,
            "destination" => $inmueble->destino_id,
            "consignation_date" => $inmueble->fecha_consignacion,
            "latitude" => $inmueble->latitud??'',
            "longitude" => $inmueble->longitud??'',
            "status" => 1,
        ];

        if ($inmueble->gestion_id == 1) {
            $aEnviar["rent"] = $inmueble->canon;
        } else if ($inmueble->gestion_id == 2) {
            $aEnviar["saleprice"] = $inmueble->venta;
        } else {
            $aEnviar["rent"] = $inmueble->canon;
            $aEnviar["saleprice"] = $inmueble->venta;
        }

        if ($inmueble->matricula != "") {
            $aEnviar["registration"] = $inmueble->matricula;
        }
        if ($inmueble->descripcion_metro_cuadrado != "") {
            $aEnviar["descripcionmetrocuadrado"] = $inmueble->descripcion_metro_cuadrado;
        }
        if ($inmueble->aviso_ventana != "") {
            $aEnviar["window_sign"] = $inmueble->aviso_ventana;
        }

        if ($inmueble->descripcion_mls != "") {
            $aEnviar["description_mls"] = $inmueble->descripcion_mls;
        }

        if ($inmueble->restricciones != "") {
            $aEnviar["restrictions"] = $inmueble->restricciones;
        }

        if ($inmueble->comentario != "") {
            $aEnviar["comment"] = $inmueble->comentario;
        }

        if ($inmueble->comentario2 != "") {
            $aEnviar["comment2"] = $inmueble->comentario2;
        }

        if ($inmueble->video != "") {
            $aEnviar["video"] = $inmueble->video;
        }

        if ($inmueble->tour_virtual != "") {
            $aEnviar["tour3d"] = $inmueble->tour_virtual;
        }

        if ($inmueble->frente != "") {
            $aEnviar["front"] = $inmueble->frente;
        }

        if ($inmueble->fondo != "") {
            $aEnviar["rear"] = $inmueble->fondo;
        }

        if ($inmueble->area_privada != "") {
            $aEnviar["private_area"] = $inmueble->area_privada;
        }

        if ($inmueble->referencia != "") {
            $aEnviar["reference"] = $inmueble->referencia;
        }

        if ($inmueble->comision != 0) {
            $aEnviar["comission_percentage"] = $inmueble->comision;
        }

        if (count($inmueble->caracteristicas) > 0) {
            $amenities = "";
            foreach ($inmueble->caracteristicas as $caract) {
                $amenities .= "{$caract->caracteristicas->domus_id},";
            }
            $aEnviar["amenities"] = rtrim($amenities, ",");
        }

        if (isset($inmueble->direcciones->id)) {
            $aEnviar["dir_1"] = $inmueble->direcciones->c1_id;
            $aEnviar["dir_2"] = $inmueble->direcciones->c2;
            $aEnviar["dir_3"] = $inmueble->direcciones->c3_id;
            $aEnviar["dir_4"] = $inmueble->direcciones->c4_id;
            $aEnviar["dir_5"] = $inmueble->direcciones->c5_id;
            $aEnviar["dir_6"] = $inmueble->direcciones->c6_id;
            $aEnviar["dir_7"] = $inmueble->direcciones->c7;
            $aEnviar["dir_8"] = $inmueble->direcciones->c8_id;
            $aEnviar["dir_9"] = $inmueble->direcciones->c9;
            $aEnviar["dir_10"] = $inmueble->direcciones->c10_id;

            if ($inmueble->direcciones->complemento != "") {
                $aEnviar["complement_address"] = $inmueble->direcciones->complemento;
            }
        }

        if (count($inmueble->imagenes) > 0) {
            $imagenes = [];
            $imagenes360 = [];
            foreach ($inmueble->imagenes as $key => $value) {
                if (!$value->is_360) {
                    array_push($imagenes, $value);
                } else {
                    array_push($imagenes360, $value);
                }
            }

            foreach ($imagenes as $key => $value) {
                $aEnviar["image_" . ($key + 1)] = "{$value->url}";
            }

            foreach ($imagenes360 as $key => $value) {
                $aEnviar["image360_" . ($key + 1)] = "{$value->url}";
            }
        }

        if ($inmueble->publicado == 1) {

            $cliente = GeneralController::putRequest("http://api.domus.la/3.0/properties/".$inmueble->codigoMLS, [
                "Authorization" => $inmueble->asesores->domus_token
            ], $aEnviar, 1);

            // return $cliente;

            if (is_string($cliente)) {
                $cliente = json_decode($cliente, true);
            }

            if (is_array($cliente)) {
                if (!isset($cliente["error"]) && count($cliente["errors"]) == 0) {

                    //Ya publicado el inmueble publicamos el mismo
                    $seguimiento = [
                        "property" => $inmueble->id,
                        "status" => 5,
                        "substatus" => 5,
                        "description" => "Inmueble actualizado y enviado"
                    ];

                    $cliente2 = GeneralController::postRequest(url("api/seguimientos"), [
                        "Authorization" => $inmueble->asesores->token
                    ], $seguimiento);
                } else {
                    return response()->json([
                        "error" => "401",
                        "mensaje" => $cliente,
                    ], 404);
                }
            } else {
                //Ya publicado el inmueble publicamos el mismo
                $seguimiento = [
                    "property" => $inmueble->id,
                    "status" => 5,
                    "substatus" => 5,
                    "description" => "Inmueble actualizado y enviado"
                ];

                $cliente2 = GeneralController::postRequest(url("api/seguimientos"), [
                    "Authorization" => $inmueble->asesores->token
                ], $seguimiento);
            }
        }
        return $aEnviar;
    }
}
