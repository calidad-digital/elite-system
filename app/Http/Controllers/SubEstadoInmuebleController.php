<?php

namespace App\Http\Controllers;

//Modelos
use App\Models\SubEstadoInmueble;

//Paquetes
use Illuminate\Http\Request;

class SubEstadoInmuebleController extends Controller
{
    public function generalList($estado) {
        $subestados = SubEstadoInmueble::where(function ($query) use ($estado) {
            if ($estado != "") {
                $query->where("estado_id", $estado);
            }
        })
        ->get();
        return $subestados;
    }
}
