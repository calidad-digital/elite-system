<?php

namespace App\Http\Controllers;

// Modelos
use App\Models\Tutorial;

// Paquetes
use Illuminate\Http\Request;

class ConfigController extends Controller
{
    public function tutorials(Request $request) {
        $usuario = session("usuario");
        $content = $request->all();

        $tutoriales = Tutorial::get();

        GeneralController::renderHeader("Tutoriales", $usuario, "activeConfigTutorials", true, "expandedConfig");

        echo view("components.config.tutorials", [
            "tutoriales" => $tutoriales
        ]);

        GeneralController::renderFooter();
    }

    public function tutorialBackEnd(Request $request) {
        $content = $request->all();

        if (isset($content["opperation"]) && $content["opperation"] == "create") {
            $tutorial = new Tutorial();
            $tutorial->titulo = $content["title"] ?? "";
            $tutorial->subtitulo = $content["subtitle"] ?? "";
            $tutorial->url = $content["url"] ?? "";

            switch ($content["platform"]) {
                case "1":
                $tutorial->icono = "youtube";
                break;
                case "2":
                $tutorial->icono = "vimeo";
                break;
                case "3":
                $tutorial->icono = "dailymotion";
                break;
                default:
                $tutorial->icono = "";
                break;
            }

            $tutorial->save();

            return redirect()
            ->route("configTutorials")
            ->with("mensaje", "Tutorial creado exitosamente");
        }

        if (isset($content["opperation"]) && $content["opperation"] == "delete") {
            $tutorial = Tutorial::find($content["delete"]);
            $tutorial->delete();

            return redirect()
            ->route("configTutorials")
            ->with("mensaje", "Tutorial eliminado exitosamente");
        }

        if (isset($content["opperation"]) && $content["opperation"] == "update") {
            $tutorial = Tutorial::find($content["registry"]);
            $tutorial->titulo = $content["title"] ?? "";
            $tutorial->subtitulo = $content["subtitle"] ?? "";
            $tutorial->url = $content["url"] ?? "";

            switch ($content["platform"]) {
                case "1":
                $tutorial->icono = "youtube";
                break;
                case "2":
                $tutorial->icono = "vimeo";
                break;
                case "3":
                $tutorial->icono = "dailymotion";
                break;
                default:
                $tutorial->icono = "";
                break;
            }

            $tutorial->save();

            return redirect()
            ->route("configTutorials")
            ->with("mensaje", "Tutorial actualizado exitosamente");
        }
    }
}
