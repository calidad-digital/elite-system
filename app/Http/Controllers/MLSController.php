<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MLSController extends Controller
{

    public function __construct() {
        $this->api = new ApiController();
    }

    public function dashboard(Request $request) {
        $usuario = session("usuario");
        $content = $request->all();

        $filtro = "";

        if (isset($content["page"]) && $content["page"] != "") {
            $filtro .= "&page={$content["page"]}";
        }

        if (isset($content["ciudad"]) && $content["ciudad"] != "") {
            $filtro .= "&city={$content["ciudad"]}";
        }

        if (isset($content["gestion"]) && $content["gestion"] != "") {
            $filtro .= "&biz={$content["gestion"]}";

            if ($content["gestion"] == 1) {
                if (isset($content["pricemin"]) && $content["pricemin"] != "") {
                    $filtro .= "&pcmin={$content["pricemin"]}";
                }

                if (isset($content["pricemax"]) && $content["pricemax"] != "") {
                    $filtro .= "&pcmax={$content["pricemax"]}";
                }
            } else {
                if (isset($content["pricemin"]) && $content["pricemin"] != "") {
                    $filtro .= "&pvmin={$content["pricemin"]}";
                }

                if (isset($content["pricemax"]) && $content["pricemax"] != "") {
                    $filtro .= "&pvmax={$content["pricemax"]}";
                }
            }
        } else {
            $content["pricemin"] = "";
            $content["pricemax"] = "";
        }

        if (isset($content["habmax"]) && $content["habmax"] != "") {
            $filtro .= "&maxbed={$content["habmax"]}";
        }
        if (isset($content["habmin"]) && $content["habmin"] != "") {
            $filtro .= "&minbed={$content["habmin"]}";
        }

        if (isset($content["banosmin"]) && $content["banosmin"] != "") {
            $filtro .= "&minbath={$content["banosmin"]}";
        }
        if (isset($content["banosmax"]) && $content["banosmax"] != "") {
            $filtro .= "&maxbath={$content["banosmax"]}";
        }
        if (isset($content["areamin"]) && $content["areamin"] != "") {
            $filtro .= "&minarea={$content["areamin"]}";
        }
        if (isset($content["areamax"]) && $content["areamax"] != "") {
            $filtro .= "&maxarea={$content["areamax"]}";
        }
        if (isset($content["parqmin"]) && $content["parqmin"] != "") {
            $filtro .= "&minparking={$content["parqmin"]}";
        }
        if (isset($content["parqmax"]) && $content["parqmax"] != "") {
            $filtro .= "&maxparking={$content["parqmax"]}";
        }
        if (isset($content["parqmin"]) && $content["parqmin"] != "") {
            $filtro .= "&minparking={$content["parqmin"]}";
        }
        if (isset($content["parqmax"]) && $content["parqmax"] != "") {
            $filtro .= "&maxparking={$content["parqmax"]}";
        }

        if (isset($content["tipo"]) && $content["tipo"] != "") {
            $filtro .= "&type={$content["tipo"]}";
        }

        if (isset($content["barrio"]) && $content["barrio"] != "") {
            $filtro .= "&neighborhood={$content["barrio"]}";
        }

        if (isset($content["estado"]) && $content["estado"] != "") {
            $filtro .= "&status={$content["estado"]}";
        }

        if (isset($content["orden"]) && $content["orden"] != "") {
            switch ($content["orden"]) {
                case 1:
                $filtro .= "";
                break;
                case 2:
                $filtro .= "&order=id&sort=asc";
                break;
                case 3:
                $filtro .= "&order=codpro&sort=asc";
                break;
                case 4:
                $filtro .= "&order=rent&sort=asc";
                break;
                case 5:
                $filtro .= "&order=saleprice&sort=asc";
                break;
                default:
                $filtro .= "";
                break;
            }
        }

        if (isset($content["codigo"]) && $content["codigo"] != "") {
            $filtro = "&codpro={$content["codigo"]}&nostatus=0";
        }

        $ciudades = $this->api->listCiudades($usuario);
        $gestiones = $this->api->listGestiones($usuario);
        $tipos = $this->api->listTiposInmueble($usuario);
        $estados = $this->api->listEstados($usuario);

        $inmuebles = $this->api->listProperties($usuario, $filtro);

        // return $inmuebles;

        GeneralController::renderHeader("MSL", $usuario, "activeMLS", true, "expandedInmuebles");

        echo view("components.mls.index")
        ->with("request", $content)
        ->with("inmuebles", $inmuebles)
        ->with("ciudades", $ciudades)
        ->with("gestiones", $gestiones)
        ->with("usuario", $usuario)
        ->with("tipos", $tipos)
        ->with("estados", $estados)
        ->with("clase", new GeneralController());

        GeneralController::renderFooter();
    }

    public function getPorpertyDetail ($code, $id){

        $idFinal = $id ?? null;

        $usuario = session("usuario");

        $inmueble = $this->api->propertyDetail($usuario, $code, $idFinal);

        return $inmueble;
    }
}