<?php

namespace App\Http\Controllers;

// Modelos
use App\Models\Usuario;
use App\Models\Cliente;
use App\Models\Inmueble;
use App\Models\Arriendo;
use App\Models\Codeudor;
use App\Models\Ocupante;
use App\Models\ArriendoCorreos;
use App\Models\InmuebleDireccion;

// Paquetes
use Dompdf\Dompdf;
use Dompdf\Options;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ArriendosController extends Controller
{

    public function __construct() {
        $this->driver = config("app.files_driver");
        $this->bucketUrl = "https://elitesystem1.s3.amazonaws.com/";
    }

    public function updateContract(Request $request, $id){
        $data = $request->all();

        $contrato = ArriendoCorreos::find($id);

        $contrato->aprobado = $data['like'];
        $contrato->commentario = $data['comentarios'];

        $contrato->save();

        $arriendo = Arriendo::with([
            "inmueble.destinos",
            "arrendatario",
            "creado",
            "ocupantes",
            "codeudores",
            "coordinador"
        ])
        ->find($contrato->arriendo_id);

        $encript_link = ArriendosController::encriptar($contrato->arriendo_id.'::'.$contrato->id);

        return redirect()->route("contratoGracias");
    }

    public function thanksContract() {
        echo view("components.templates.gracias", [
            "titulo" => "Gracias"
        ]);
    }

    public function showContract($id){

        $decode1 = base64_decode($id);
        $stringlisto = explode("::",$decode1);
        $idArriendo = base64_decode($stringlisto[1]);
        $variables = explode("::",$idArriendo);

        $arriendo = Arriendo::with([
            "inmueble.destinos",
            "arrendatario",
            "creado",
            "ocupantes",
            "codeudores",
            "coordinador"
        ])
        ->find($variables[0]);

        $persona = ArriendoCorreos::find($variables[1]);
        $persona->ultima_visita = date('Y-m-d H:i:s');
        $persona->save();

        echo view("components.arriendos.contrato")
        ->with("arriendo", $arriendo)
        ->with("persona", $persona);

        GeneralController::renderFooter();
    }

    public function arriendosCerrar($id){
        $arriendo = Arriendo::find($id);

        $arriendo->active = 0;
        $arriendo->save();

        return 1;
    }

    public function editarPost(Request $request){
        $data = $request->all();
        $usuario = session("usuario");

        // return $data; die;

        $arriendo = Arriendo::with('creado','coordinador','arrendatario','codeudores')->find($data['id']);
        $inmueble = Inmueble::with("direcciones")->find($arriendo->inmueble_id);

        $arriendo->coordinadora = intval($data['coordinadora'])??$arriendo->coordinadora;
        $arriendo->solicitud_aseguradora = $data['solicitud_aseguradora']??'';
        $arriendo->valor_total = str_replace(',','',$data['valor_total'])??0;
        $arriendo->canon = str_replace(',','',$data['canon'])??0;
        $inmueble->canon = str_replace(',','',$data['canon'])??0;
        $arriendo->admon = str_replace(',','',$data['admon'])??0;
        $inmueble->administracion = str_replace(',','',$data['admon'])??0;
        $arriendo->admon_no_desc = isset($data['admon_no_desc']) ?str_replace(',','',$data['admon_no_desc']):0;
        $arriendo->caldera = isset($data['caldera']) ?str_replace(',','',$data['caldera']):0;
        $arriendo->parqueadero =  isset($data['parqueadero']) ?str_replace(',','',$data['parqueadero']):0;
        $arriendo->lavanderia = isset($data['lavanderia']) ?str_replace(',','',$data['lavanderia']):0;
        $arriendo->parqueadero1 = $data['parqueadero1']??'';
        $arriendo->parqueadero2 = $data['parqueadero2']??'';
        $arriendo->parqueadero3 = $data['parqueadero3']??'';
        $arriendo->deposito = $data['deposito']??'';
        $arriendo->deposito2 = $data['deposito2']??'';
        $arriendo->contrato_acueducto = $data['contrato_acueducto']??'';
        $arriendo->contrato_energia = $data['contrato_energia']??'';
        $arriendo->contrato_gas = $data['contrato_gas']??'';
        $arriendo->nombre_conjunto = $data['nombre_conjunto']??'';

        if(isset($inmueble->direcciones->id)){
            $direccion = InmuebleDireccion::find($inmueble->direcciones->id);
            $direccion->complemento = $data['nombre_conjunto']??'';
            $direccion->save();
        }

        $arriendo->uso_suelo_permitido = $data['uso_suelo_permitido']??'';
        $arriendo->incremento_anual = $data['incremento_anual']??'';
        $arriendo->uso_suelo_inmueble = $data['uso_suelo_inmueble']??'';
        $arriendo->fecha_entrega = $data['fecha_entrega']??date('Y-m-d H:i:s');
        $arriendo->fecha_firma_contrato = $data['fecha_firma_contrato']??date('Y-m-d H:i:s');
        $arriendo->fecha_inicio_contrato = $data['fecha_inicio_contrato']??date('Y-m-d H:i:s');
        $arriendo->fecha_final_entrega = $data['fecha_final_entrega']??date('Y-m-d H:i:s');
        $arriendo->amparo_basico = $data['amparo_basico']??'';
        $arriendo->amparo_hogar = $data['amparo_hogar']??'';
        $arriendo->amparo_integral = $data['amparo_integral']??'';
        $arriendo->notas_seguros = $data['notas_seguros']??'';
        $arriendo->numero_contrato = $data['numero_contrato']??'';
        $arriendo->notas_servicios = $data['notas_servicios']??'';
        $arriendo->notas_precio = $data['notas_precio']??'';
        $arriendo->fecha_pago_canon = $data["fecha_pago_canon"] ?? null;
        $arriendo->fecha_pago_admon = $data["fecha_pago_admon"] ?? null;
        $arriendo->fecha_pago_caldera = $data["fecha_pago_caldera"] ?? null;
        $arriendo->fecha_pago_parqueadero = $data["fecha_pago_parqueadero"] ?? null;
        $arriendo->fecha_pago_lavanderia = $data["fecha_pago_lavanderia"] ?? null;
        $arriendo->nota_pagos_mensuales = $data["nota_pagos_mensuales"] ?? "";
        $arriendo->objeto_condiciones_contrato = $data["objeto_condiciones_contrato"] ?? "";
        $arriendo->notas_servicio_energia = $data["notas_servicio_energia"] ?? "";
        $arriendo->notas_servicio_gas = $data["notas_servicio_gas"] ?? "";
        $arriendo->texto_fecha_entrega = $data["texto_fecha_entrega"] ?? "";

        $arriendo->actualizado_por = $usuario['id'];
        $arriendo->creado_por = $data['create_user_id'];

        $arriendo->save();

        // Garaje
        for ($i = 1; $i <= 3; $i++) {
            if (isset($data["numero_parqueadero_{$i}"]) && $data["numero_parqueadero_{$i}"] != "") {
                $inmueble["numero_parqueadero_{$i}"] = $data["numero_parqueadero_{$i}"];
            }

            if (isset($data["matricula_parqueadero_{$i}"]) && $data["matricula_parqueadero_{$i}"] != "") {
                $inmueble["matricula_parqueadero_{$i}"] = $data["matricula_parqueadero_{$i}"];
            }

            if (isset($data["chip_parqueadero_{$i}"]) && $data["chip_parqueadero_{$i}"] != "") {
                $inmueble["chip_parqueadero_{$i}"] = $data["chip_parqueadero_{$i}"];
            }

            if (isset($data["cedula_parqueadero_{$i}"]) && $data["cedula_parqueadero_{$i}"] != "") {
                $inmueble["cedula_parqueadero_{$i}"] = $data["cedula_parqueadero_{$i}"];
            }
        }

        // Depósito
        for ($i = 1; $i <= 2; $i++) {
            if (isset($data["numero_deposito_{$i}"]) && $data["numero_deposito_{$i}"] != "") {
                $inmueble["numero_deposito_{$i}"] = $data["numero_deposito_{$i}"];
            }

            if (isset($data["matricula_deposito_{$i}"]) && $data["matricula_deposito_{$i}"] != "") {
                $inmueble["matricula_deposito_{$i}"] = $data["matricula_deposito_{$i}"];
            }

            if (isset($data["chip_deposito_{$i}"]) && $data["chip_deposito_{$i}"] != "") {
                $inmueble["chip_deposito_{$i}"] = $data["chip_deposito_{$i}"];
            }

            if (isset($data["cedula_deposito_{$i}"]) && $data["cedula_deposito_{$i}"] != "") {
                $inmueble["cedula_deposito_{$i}"] = $data["cedula_deposito_{$i}"];
            }
        }

        $inmueble->save();

        if ($request->hasFile("contrato_captacion_url") && $request->file("contrato_captacion_url")->isValid()) {

            $image = $request->file("contrato_captacion_url");
            $imageName = "contrato_captacion_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->contrato_captacion_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("clt_url") && $request->file("clt_url")->isValid()) {

            $image = $request->file("clt_url");
            $imageName = "clt_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->clt_url = $this->bucketUrl . $url;
        }
        if ($request->hasFile("clt_parqueadero_url") && $request->file("clt_parqueadero_url")->isValid()) {

            $image = $request->file("clt_parqueadero_url");
            $imageName = "clt_parqueadero_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->clt_parqueadero_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("clt_parqueadero2_url") && $request->file("clt_parqueadero2_url")->isValid()) {

            $image = $request->file("clt_parqueadero2_url");
            $imageName = "clt_parqueadero2_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->clt_parqueadero2_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("clt_deposito_url") && $request->file("clt_deposito_url")->isValid()) {

            $image = $request->file("clt_deposito_url");
            $imageName = "clt_deposito_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->clt_deposito_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("clt_deposito2_url") && $request->file("clt_deposito2_url")->isValid()) {

            $image = $request->file("clt_deposito2_url");
            $imageName = "clt_deposito2_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->clt_deposito2_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("entrega_inventario_url") && $request->file("entrega_inventario_url")->isValid()) {

            $image = $request->file("entrega_inventario_url");
            $imageName = "entrega_inventario_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->entrega_inventario_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("mandanto_admon_url") && $request->file("mandanto_admon_url")->isValid()) {

            $image = $request->file("mandanto_admon_url");
            $imageName = "mandanto_admon_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->mandanto_admon_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("recibo_predial_url") && $request->file("recibo_predial_url")->isValid()) {

            $image = $request->file("recibo_predial_url");
            $imageName = "recibo_predial_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->recibo_predial_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("servicios_publicos_url") && $request->file("servicios_publicos_url")->isValid()) {

            $image = $request->file("servicios_publicos_url");
            $imageName = "servicios_publicos_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->servicios_publicos_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("cuenta_cobro_admon_url") && $request->file("cuenta_cobro_admon_url")->isValid()) {

            $image = $request->file("cuenta_cobro_admon_url");
            $imageName = "cuenta_cobro_admon_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->cuenta_cobro_admon_url = $this->bucketUrl . $url;
        }
        if ($request->hasFile("escritura_linderos_url") && $request->file("escritura_linderos_url")->isValid()) {

            $image = $request->file("escritura_linderos_url");
            $imageName = "escritura_linderos_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->escritura_linderos_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("aprobacion_aseguradora_url") && $request->file("aprobacion_aseguradora_url")->isValid()) {

            $image = $request->file("aprobacion_aseguradora_url");
            $imageName = "aprobacion_aseguradora_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->aprobacion_aseguradora_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("entrega_amoblados_url") && $request->file("entrega_amoblados_url")->isValid()) {

            $image = $request->file("entrega_amoblados_url");
            $imageName = "entrega_amoblados_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->entrega_amoblados_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("camara_comercio_url") && $request->file("camara_comercio_url")->isValid()) {

            $image = $request->file("camara_comercio_url");
            $imageName = "camara_comercio_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->camara_comercio_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("leasing_url") && $request->file("leasing_url")->isValid()) {

            $image = $request->file("leasing_url");
            $imageName = "camara_comercio_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->leasing_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("usufructo_url") && $request->file("usufructo_url")->isValid()) {

            $image = $request->file("usufructo_url");
            $imageName = "usufructo_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->usufructo_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("rut_url") && $request->file("rut_url")->isValid()) {

            $image = $request->file("rut_url");
            $imageName = "rut_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->rut_url = $this->bucketUrl . $url;

        }

        $arriendo->save();

        $arrendatario = Cliente::find($data['arrendatario_id']);

        $arrendatario->username = $data['arrendatario_email']??$arrendatario->username;
        $arrendatario->password = $data['arrendatario_cedula']??$arrendatario->password;
        $arrendatario->documento = $data['arrendatario_cedula']??$arrendatario->documento;
        $arrendatario->nombres = $data['arrendatario_nombre']??$arrendatario->nombres;
        $arrendatario->apellidos = $data['arrendatario_apellido']??$arrendatario->apellidos;
        $arrendatario->email = $data['arrendatario_email']??$arrendatario->email;
        $arrendatario->celular = $data['arrendatario_celular']??$arrendatario->celular;
        $arrendatario->whatsapp = $data['arrendatario_celular']??$arrendatario->whatsapp;
        $arrendatario->direccion = $data['arrendatario_direccion']??$arrendatario->direccion;
        $arrendatario->copropiedad = $data['arrendatario_copropiedad']??$arrendatario->direccion;
        $arrendatario->direccion_comercial = $data['arrendatario_direccion_comercial']??$arrendatario->direccion;

        if ($request->hasFile("arrendatario_cedula_url")) {
            $doc_arrendatario = '';
            $name = $data["arrendatario_email"] . "_cedula." . $request->file("arrendatario_cedula_url")->extension();
            $path2 = $request->file("arrendatario_cedula_url")->storePubliclyAs("clientes/documentos/cliente_{$data["arrendatario_email"]}", $name, $this->driver);

            $doc_arrendatario = $this->bucketUrl. $path2;
            $arrendatario->url_documento = $doc_arrendatario;
        }

        $arrendatario->save();


        for ($i=1; $i < ($data['cantidad_codeudores']+1); $i++) {
            if (isset($data[$i.'_codeudor_nombre']) && $data[$i.'_codeudor_nombre'] != "") {
                if (isset($data[$i.'_codeudor_id']) && $data[$i.'_codeudor_id'] != "") {
                    $codeudor = Codeudor::find($data[$i.'_codeudor_id']);
                } else {
                    $codeudor = new Codeudor;
                }

                $codeudor->nombre = $data[$i.'_codeudor_nombre'];
                $codeudor->cedula = $data[$i.'_codeudor_cedula'];
                $codeudor->direccion = $data[$i.'_codeudor_direccion'];
                $codeudor->email = $data[$i.'_codeudor_email'];
                $codeudor->celular = $data[$i.'_codeudor_celular'];
                $codeudor->copropiedad = $data[$i.'_codeudor_copropiedad'];
                $codeudor->direccion_comercial = $data[$i.'_codeudor_direccion_comercial'];
                $codeudor->arriendo_id = $arriendo->id;
                $codeudor->save();

                if ($request->hasFile($i."_codeudor_cedula_url") && $request->file($i."_codeudor_cedula_url")->isValid()) {
                    $image = $request->file($i."_codeudor_cedula_url");
                    $imageName = "cedula_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
                    $url = $image->storeAs("arriendos/codeudores/codeudor_" . $codeudor->id, $imageName, $this->driver);
                    $codeudor->url_cedula = $this->bucketUrl . $url;
                    $codeudor->save();
                }
            }
        }

        for ($i=1; $i < ($data['cantidad_ocupantes']+1); $i++) {

            if (isset($data[$i.'_ocupante_id'])) {
                $ocupantes = Ocupante::find($data[$i.'_ocupante_id']);
            } else {
                $ocupantes = new Ocupante;
            }

            $ocupantes->nombre = $data[$i.'_ocupante_nombre'];
            $ocupantes->cedula = $data[$i.'_ocupante_documento'];
            $ocupantes->arriendo_id = $arriendo->id;
            $ocupantes->save();
        }

        $destinatarios = [];

        $correos = Usuario::where('rol_id',6)
        ->get();

        foreach($correos as $user_for_email) {
            array_push($destinatarios, [
                "name" => "{$user_for_email->nombres} {$user_for_email->apellidos}",
                "email" => $user_for_email->email
            ]);
        }

        if ($data['revision'] == 1) {
            $creador = Usuario::find($arriendo->creado_por);

            array_push($destinatarios, [
                "name" => "{$creador->nombres} {$creador->apellidos}",
                "email" => $creador->email
            ]);

            array_push($destinatarios, [
                "name" => "James Woodcock Villarejo",
                "email" => "jwoodcock@eliteinmobiliaria.com.co"
            ]);

            $asunto = "Creación";
            $message = "Se ha creado un nuevo formulario de arriendo del agente "
            . "{$usuario->nombres} {$usuario->apellidos}"
            . " ,codigo MLS "
            . ($inmueble->codigoMLS ?? '')
            . ", para más información consulta: "
            . "http://elite-system.calidad.digital/admin/arriendos/editar/"
            . $arriendo->id
            . ". <br> <br> ";

            $mail = new MailController();
            $mail->sendMail($asunto. " formulario arriendo ".date('d/m/Y'), $message, $destinatarios);

        } else if ($data['revision'] == 2) {

            //Correo agente
            $agente = [
                "tipo"=> "agente",
                "nombre"=> "{$arriendo->creado->nombres} {$arriendo->creado->apellidos}",
                "correo"=> $arriendo->creado->email,
                "arriendo"=>$arriendo->id
            ];
            $mailAgente = $this->enviarCorreo($agente);

            //Correo arrendatario
            $arrendatario = [
                "tipo"=> "arrendatario",
                "nombre"=> "{$arriendo->arrendatario->nombres} {$arriendo->arrendatario->apellidos}",
                "correo"=> $arriendo->arrendatario->email,
                "arriendo"=>$arriendo->id
            ];
            $mailArrrendatario=$this->enviarCorreo($arrendatario);

            // Correo coordinador
            if ($arriendo->coordinador != null) {
                $coordinador = [
                    "tipo"=> "coordinador",
                    "nombre"=> "{$arriendo->coordinador->nombres} {$arriendo->coordinador->apellidos}",
                    "correo"=> $arriendo->coordinador->email,
                    "arriendo"=>$arriendo->id
                ];
                $mailCoordinador= $this->enviarCorreo($coordinador);
            }

            $searchCorreo = ArriendoCorreos::where('arriendo_id',$arriendo->id)
            ->where('usuario_tipo','codeudor')
            ->get();

            foreach ($searchCorreo as $key => $value) {
                $value->delete();
            }

            foreach ($arriendo->codeudores as $key => $value) {
                //Correo codeudor
                $codeudor = [
                    "tipo"=> "codeudor",
                    "nombre"=> "{$value->nombre}",
                    "correo"=> $value->email,
                    "arriendo" => $arriendo->id
                ];
                $mailCodeudor= $this->enviarCorreo($codeudor);

            }

            //Correo director
            $director = [
                "tipo"=> "director",
                "nombre"=> "James Woodcock Villarejo",
                "correo"=> "jwoodcock@eliteinmobiliaria.com.co",
                "arriendo" => $arriendo->id
            ];
            $mailDirector= $this->enviarCorreo($director);

        }


        return redirect()
        ->route("listaArriendos");
    }

    public static function encriptar($stringToEncrypt){
        $urlToEncript = $stringToEncrypt;
        $encript1 = base64_encode($urlToEncript);
        $radnum = rand(1,100000);
        $aleadorio = uniqid($radnum, false)."::";
        $encript_link = base64_encode($aleadorio.$encript1);

        return $encript_link;
    }

    public static function enviarCorreo($info){

        if (isset($info['correo']) && isset($info['arriendo'])) {
            if ($info['tipo'] != 'codeudor') {
                $searchCorreo = ArriendoCorreos::where('arriendo_id',$info['arriendo'])
                ->where('usuario_tipo',$info['tipo'])
                ->first();
                $correo = empty($searchCorreo) ? new ArriendoCorreos : $searchCorreo ;
            } else {
                $correo =  new ArriendoCorreos;
            }

            $correo->usuario_tipo = $info['tipo'] ?? '';
            $correo->usuario_nombre = $info['nombre'] ?? '';
            $correo->usuario_correo = $info['correo'];

            if ($info["tipo"] == "director") {
                $correo->aprobado = 1;
            } else {
                $correo->aprobado = 0;
            }

            $correo->commentario = '';
            $correo->ultima_visita = $correo->ultima_visita;
            $correo->arriendo_id = $info['arriendo'];

            $correo->save();

            $encript_link = ArriendosController::encriptar($info['arriendo'].'::'.$correo->id);
            $final_link ="http://elite-system.calidad.digital/alt/contrato-arrendamiento/".$encript_link;
            // $final_link ="http://elite-system.test/alt/contrato-arrendamiento/".$encript_link;

            $correo->url_aprobacion = $final_link;
            $correo->save();

            $destinatarios = [];

            array_push($destinatarios, [
                "name" => $info['nombre']??'',
                // "email" => "desarrolloweb@domus.la"
                "email" => $info['correo']
            ]);

            $asunto = "Revisión contrato arriendo";
            $message = "En el siguiente link, encontrará toda la información correspondiente al contrato de arriendo.  <br></br>
            <a href='".$final_link."' target='_blank'>Información contrato.</a> <br></br> Por favor recuerde validarla y aprobarla, en caso de
            que encuentre alguna incongruencia, en el mismo enlace podrar rechazar y notificarle al agente el motivo para poder hacer las correcciones pertinentes";

            $mail = new MailController();
            $mail->sendMail($asunto. " ".date('d/m/Y'), $message, $destinatarios);
            return $mail;

        }

    }

    public function crear(Request $request){

        $data = $request->all();
        $usuario = session("usuario");
        $inmueble = Inmueble::with('ciudades', "direcciones")->find($data['inmueble_id']);


        $arriendo = new Arriendo;

        // return $data;


        // return $inmueble;

        $search_arrendatario = Cliente::where('documento',$data['arrendatario_cedula'])->first();

        if(empty($search_arrendatario)){
            $arrendatario = new Cliente;
            $arrendatario->username = $data['arrendatario_email']??'';
            $arrendatario->password = $data['arrendatario_cedula']??'';
            $arrendatario->nombres = $data['arrendatario_nombre']??'';
            $arrendatario->apellidos = $data['arrendatario_apellido']??'';
            $arrendatario->email = $data['arrendatario_email']??'';
            $arrendatario->documento = $data['arrendatario_cedula']??'';
            $arrendatario->telefono = $data['arrendatario_celular']??'';
            $arrendatario->celular = $data['arrendatario_celular']??'';
            $arrendatario->whatsapp = $data['arrendatario_celular']??'';
            $arrendatario->comentario = 'Creado desde el modulo de arriendos';
            $arrendatario->profesion = '';
            $arrendatario->fecha_nacimiento = date('Y-m-d H:i:s');
            $arrendatario->estado_ubicacions_id = 1;

            if ($request->hasFile("arrendatario_cedula_url")) {
                $doc_arrendatario = '';
                $name = $data["arrendatario_email"] . "_cedula." . $request->file("arrendatario_cedula_url")->extension();
                $path2 = $request->file("arrendatario_cedula_url")->storePubliclyAs("clientes/documentos/cliente_{$data["arrendatario_email"]}", $name, $this->driver);

                $doc_arrendatario = $this->bucketUrl . $path2;
            }
            $arrendatario->url_documento = $doc_arrendatario ?? "";
            $arrendatario->direccion = $data['arrendatario_direccion']??'';
            $arrendatario->tipo_documento_id = 1;
            $arrendatario->ciudad = $inmueble->ciudades->nombre;
            $arrendatario->ciudad_id = $inmueble->ciudad_id;
            $arrendatario->empresa_id = 1;
            $arrendatario->oficina_id = 1;
            $arrendatario->estado_id = 1;
            $arrendatario->rol_id = 4;
            $arrendatario->create_user_id = $usuario['id'];
            $arrendatario->update_user_id = $usuario['id'];

            $arrendatario->save();

        }else{
            $arrendatario = $search_arrendatario;
        }

        $arriendo->coordinadora = $data['coordinadora']??null;
        $arriendo->solicitud_aseguradora = $data['solicitud_aseguradora']??'';
        $arriendo->valor_total = str_replace(',','',$data['valor_total'])??0;
        $arriendo->canon = str_replace(',','',$data['canon'])??0;
        $inmueble->canon = str_replace(',','',$data['canon'])??0;
        $arriendo->admon = str_replace(',','',$data['admon'])??0;
        $inmueble->administracion = str_replace(',','',$data['admon'])??0;
        $arriendo->admon_no_desc = isset($data['admon_no_desc']) ?str_replace(',','',$data['admon_no_desc']):0;
        $arriendo->caldera = isset($data['caldera']) ?str_replace(',','',$data['caldera']):0;
        $arriendo->parqueadero =  isset($data['parqueadero']) ?str_replace(',','',$data['parqueadero']):0;
        $arriendo->lavanderia = isset($data['lavanderia']) ?str_replace(',','',$data['lavanderia']):0;
        $arriendo->parqueadero1 = $data['parqueadero1']??'';
        $arriendo->parqueadero2 = $data['parqueadero2']??'';
        $arriendo->parqueadero3 = $data['parqueadero3']??'';
        $arriendo->deposito = $data['deposito']??'';
        $arriendo->deposito2 = $data['deposito2']??'';
        $arriendo->contrato_acueducto = $data['contrato_acueducto']??'';
        $arriendo->contrato_energia = $data['contrato_energia']??'';
        $arriendo->contrato_gas = $data['contrato_gas']??'';
        $arriendo->nombre_conjunto = $data['nombre_conjunto']??'';

        if (isset($inmueble->direcciones->id) && $inmueble->direcciones->id != null) {
            $direccion = InmuebleDireccion::find($inmueble->direcciones->id);
            $direccion->complemento = $data['nombre_conjunto']??'';
            $direccion->save();
        }

        $arriendo->uso_suelo_permitido = $data['uso_suelo_permitido']??'';
        $arriendo->incremento_anual = $data['incremento_anual']??'';
        $arriendo->uso_suelo_inmueble = $data['uso_suelo_inmueble']??'';
        $arriendo->fecha_entrega = $data['fecha_entrega']??date('Y-m-d H:i:s');
        $arriendo->fecha_firma_contrato = $data['fecha_firma_contrato']??date('Y-m-d H:i:s');
        $arriendo->fecha_inicio_contrato = $data['fecha_inicio_contrato']??date('Y-m-d H:i:s');
        $arriendo->fecha_final_entrega = $data['fecha_final_entrega']??date('Y-m-d H:i:s');
        $arriendo->amparo_basico = $data['amparo_basico']??'';
        $arriendo->amparo_hogar = $data['amparo_hogar']??'';
        $arriendo->amparo_integral = $data['amparo_integral']??'';
        $arriendo->notas_seguros = $data['notas_seguros']??'';
        $arriendo->numero_contrato = $data['numero_contrato']??'';

        $arriendo->inmueble_id = $data['inmueble_id'];
        $arriendo->arrendatario_id = $arrendatario->id;
        // $arriendo->creado_por = $usuario['id'];
        $arriendo->actualizado_por = $usuario['id'];
        $arriendo->creado_por = $data['create_user_id'];

        $arriendo->save();

        // Garaje
        for ($i = 1; $i <= 3; $i++) {
            if (isset($data["numero_parqueadero_{$i}"]) && $data["numero_parqueadero_{$i}"] != "") {
                $inmueble["numero_parqueadero_{$i}"] = $data["numero_parqueadero_{$i}"];
            }

            if (isset($data["matricula_parqueadero_{$i}"]) && $data["matricula_parqueadero_{$i}"] != "") {
                $inmueble["matricula_parqueadero_{$i}"] = $data["matricula_parqueadero_{$i}"];
            }

            if (isset($data["chip_parqueadero_{$i}"]) && $data["chip_parqueadero_{$i}"] != "") {
                $inmueble["chip_parqueadero_{$i}"] = $data["chip_parqueadero_{$i}"];
            }

            if (isset($data["cedula_parqueadero_{$i}"]) && $data["cedula_parqueadero_{$i}"] != "") {
                $inmueble["cedula_parqueadero_{$i}"] = $data["cedula_parqueadero_{$i}"];
            }
        }

        // Depósito
        for ($i = 1; $i <= 2; $i++) {
            if (isset($data["numero_deposito_{$i}"]) && $data["numero_deposito_{$i}"] != "") {
                $inmueble["numero_deposito_{$i}"] = $data["numero_deposito_{$i}"];
            }

            if (isset($data["matricula_deposito_{$i}"]) && $data["matricula_deposito_{$i}"] != "") {
                $inmueble["matricula_deposito_{$i}"] = $data["matricula_deposito_{$i}"];
            }

            if (isset($data["chip_deposito_{$i}"]) && $data["chip_deposito_{$i}"] != "") {
                $inmueble["chip_deposito_{$i}"] = $data["chip_deposito_{$i}"];
            }

            if (isset($data["cedula_deposito_{$i}"]) && $data["cedula_deposito_{$i}"] != "") {
                $inmueble["cedula_deposito_{$i}"] = $data["cedula_deposito_{$i}"];
            }
        }

        $inmueble->save();


        if ($request->hasFile("contrato_captacion_url") && $request->file("contrato_captacion_url")->isValid()) {

            $image = $request->file("contrato_captacion_url");
            $imageName = "contrato_captacion_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->contrato_captacion_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("clt_url") && $request->file("clt_url")->isValid()) {

            $image = $request->file("clt_url");
            $imageName = "clt_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->clt_url = $this->bucketUrl . $url;
        }
        if ($request->hasFile("clt_parqueadero_url") && $request->file("clt_parqueadero_url")->isValid()) {

            $image = $request->file("clt_parqueadero_url");
            $imageName = "clt_parqueadero_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->clt_parqueadero_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("clt_parqueadero2_url") && $request->file("clt_parqueadero2_url")->isValid()) {

            $image = $request->file("clt_parqueadero2_url");
            $imageName = "clt_parqueadero2_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->clt_parqueadero2_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("clt_deposito_url") && $request->file("clt_deposito_url")->isValid()) {

            $image = $request->file("clt_deposito_url");
            $imageName = "clt_deposito_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->clt_deposito_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("clt_deposito2_url") && $request->file("clt_deposito2_url")->isValid()) {

            $image = $request->file("clt_deposito2_url");
            $imageName = "clt_deposito2_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->clt_deposito2_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("entrega_inventario_url") && $request->file("entrega_inventario_url")->isValid()) {

            $image = $request->file("entrega_inventario_url");
            $imageName = "entrega_inventario_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->entrega_inventario_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("mandanto_admon_url") && $request->file("mandanto_admon_url")->isValid()) {

            $image = $request->file("mandanto_admon_url");
            $imageName = "mandanto_admon_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->mandanto_admon_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("recibo_predial_url") && $request->file("recibo_predial_url")->isValid()) {

            $image = $request->file("recibo_predial_url");
            $imageName = "recibo_predial_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->recibo_predial_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("servicios_publicos_url") && $request->file("servicios_publicos_url")->isValid()) {

            $image = $request->file("servicios_publicos_url");
            $imageName = "servicios_publicos_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->servicios_publicos_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("cuenta_cobro_admon_url") && $request->file("cuenta_cobro_admon_url")->isValid()) {

            $image = $request->file("cuenta_cobro_admon_url");
            $imageName = "cuenta_cobro_admon_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->cuenta_cobro_admon_url = $this->bucketUrl . $url;
        }
        if ($request->hasFile("escritura_linderos_url") && $request->file("escritura_linderos_url")->isValid()) {

            $image = $request->file("escritura_linderos_url");
            $imageName = "escritura_linderos_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->escritura_linderos_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("aprobacion_aseguradora_url") && $request->file("aprobacion_aseguradora_url")->isValid()) {

            $image = $request->file("aprobacion_aseguradora_url");
            $imageName = "aprobacion_aseguradora_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->aprobacion_aseguradora_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("entrega_amoblados_url") && $request->file("entrega_amoblados_url")->isValid()) {

            $image = $request->file("entrega_amoblados_url");
            $imageName = "entrega_amoblados_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->entrega_amoblados_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("camara_comercio_url") && $request->file("camara_comercio_url")->isValid()) {

            $image = $request->file("camara_comercio_url");
            $imageName = "camara_comercio_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->camara_comercio_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("leasing_url") && $request->file("leasing_url")->isValid()) {

            $image = $request->file("leasing_url");
            $imageName = "camara_comercio_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->leasing_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("usufructo_url") && $request->file("usufructo_url")->isValid()) {

            $image = $request->file("usufructo_url");
            $imageName = "usufructo_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->usufructo_url = $this->bucketUrl . $url;

        }
        if ($request->hasFile("rut_url") && $request->file("rut_url")->isValid()) {

            $image = $request->file("rut_url");
            $imageName = "rut_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("arriendos/arriendo_" . $arriendo->id, $imageName, $this->driver);
            $arriendo->rut_url = $this->bucketUrl . $url;

        }

        $arriendo->save();


        for ($i=1; $i < ($data['cantidad_codeudores']+1); $i++) {
            if (isset($data[$i.'_codeudor_nombre']) && $data[$i.'_codeudor_nombre'] != "") {

                $codeudor = new Codeudor;

                $codeudor->nombre = $data[$i.'_codeudor_nombre'];
                $codeudor->cedula = $data[$i.'_codeudor_cedula'];
                $codeudor->direccion = $data[$i.'_codeudor_direccion'];
                $codeudor->email = $data[$i.'_codeudor_email'];
                $codeudor->celular = $data[$i.'_codeudor_celular'];

                $codeudor->arriendo_id = $arriendo->id;

                $codeudor->save();

                if ($request->hasFile($i."_codeudor_cedula_url") && $request->file($i."_codeudor_cedula_url")->isValid()) {

                    $image = $request->file($i."_codeudor_cedula_url");
                    $imageName = "cedula_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
                    $url = $image->storeAs("arriendos/codeudores/codeudor_" . $codeudor->id, $imageName, $this->driver);
                    $codeudor->url_cedula = $this->bucketUrl . $url;

                    $codeudor->save();
                }
            }

        }



        for ($i=1; $i < ($data['cantidad_ocupantes']+1); $i++) {
            $ocupantes = new Ocupante;

            $ocupantes->nombre = $data[$i.'_ocupante_nombre'];
            $ocupantes->cedula = $data[$i.'_ocupante_documento'];
            $ocupantes->arriendo_id = $arriendo->id;

            $ocupantes->save();
        }

        $api = new ApiController;
        $detalle = $api->propertyDetail($usuario, $inmueble['codigoMLS'],null);

        // return $usuario;

        $property= [
            'codigoMLS'=>$inmueble['codigoMLS'],
            'status'=>6,
            'source'=>10845,
            'value'=>$data['canon'],
            'real_state'=>52,
        ];


        $destinatarios = [];

        $correos = Usuario::where('rol_id',6)
        ->get();

        foreach($correos as $user_for_email){

            array_push($destinatarios, [
                "name" => "{$user_for_email->nombres} {$user_for_email->apellidos}",
                "email" => $user_for_email->email
            ]);
        }


        if($data['revision'] == 1  ){

            $inmueble->estado_id = 9;
            $inmueble->save();

            $act = $api->actualizarEstadoInmueble($usuario, $property);

            array_push($destinatarios, [
                "name" => "{$usuario['nombres']} {$usuario['apellidos']}",
                "email" => $usuario['email']
            ]);
            array_push($destinatarios, [
                "name" => "James Woodcock Villarejo",
                "email" => "jwoodcock@eliteinmobiliaria.com.co"
            ]);

            $asunto = "Creación";
            $message= "Se ha creado un nuevo formulario de arriendo del agente ". "{$usuario->nombres} {$usuario->apellidos}". " ,codigo MLS ".
            $inmueble->codigoMLS.", para más información consulta: " . url("/") . "/admin/arriendos/editar/".$arriendo->id.". <br> <br> ";

            $mail = new MailController();
            $mail->sendMail($asunto. " formulario arriendo ".date('d/m/Y'), $message, $destinatarios);

        }


        return redirect()
        ->route("listaArriendos");
    }

    public function arriendos1(Request $request, $inmueble_id) {
        $usuario = session("usuario");
        $agentes = Usuario::get();
        $inmueble = Inmueble::with("clientes.clientes","asesores","destinos")->find($inmueble_id);
        $coordinadoras = Usuario::where('rol_id',10)->get();

        $arriendo = Arriendo::where("inmueble_id", $inmueble->id)->orderBy("id", "desc")->first();

        if ($arriendo != null) {
            return $this->arriendosEditar($request, $arriendo->id);
        }

        // return $inmueble;
        GeneralController::renderHeader("Arriendos", $usuario, "activeArriendos", true, "expandedInmuebles");

        echo view("components.arriendos.1")
        ->with("coordinadoras", $coordinadoras)
        ->with("usuario", $usuario)
        ->with("agentes", $agentes)
        ->with("inmueble", $inmueble);

        GeneralController::renderFooter();
    }

    public function arriendosEditar(Request $request, $id) {
        $usuario = session("usuario");
        $coordinadoras = Usuario::where('rol_id',10)->get();
        $agentes = Usuario::get();
        $arriendo = Arriendo::with(
            "inmueble",
            "arrendatario",
            "creado",
            "ocupantes",
            "codeudores",
            "correos",
            "coordinador")->find($id);

            $inmueble = Inmueble::with("clientes.clientes","asesores","destinos")->find($arriendo->inmueble_id);

            // return $arriendo;
            GeneralController::renderHeader("Arriendos", $usuario, "activeArriendos", true, "expandedInmuebles");

            echo view("components.arriendos.edit")
            ->with("coordinadoras", $coordinadoras)
            ->with("arriendo", $arriendo)
            ->with("usuario", $usuario)
            ->with("agentes", $agentes)
            ->with("inmueble", $inmueble);

            GeneralController::renderFooter();
        }

        public function lista(Request $request) {
            $content = $request->all();
            $usuario = session("usuario");

            $agentes = Usuario::get();
            $arriendos = Arriendo::with('inmueble.tipos_inmueble',
            'arrendatario',
            'creado',
            'ocupantes',
            'codeudores')
            ->where(function($query) use ($usuario,$content) {
                if(isset($content['usuario']) && $content['usuario'] != ''){
                    $query->where("creado_por", $content['usuario']);
                }else{
                    $query->where("creado_por", $usuario['id']);
                }

            })
            ->orderBy('id','desc')
            ->paginate(10);


            GeneralController::renderHeader("Arriendos", $usuario, "activeArriendos", true, "expandedInmuebles");
            echo view("components.arriendos.lista")
            ->with("request", $content)
            ->with("agentes", $agentes)
            ->with("arriendos", $arriendos)
            ->with("usuario", $usuario);

            GeneralController::renderFooter();
        }

        public function arriendos2(Request $request) {
            $usuario = session("usuario");
            $inmueble = Inmueble::with("clientes.clientes")->find(1);
            GeneralController::renderHeader("Arriendos", $usuario, "activeArriendos", true, "expandedInmuebles");

            echo view("components.arriendos.2")
            ->with("inmueble", $inmueble);

            GeneralController::renderFooter();
        }

        public function arriendos3(Request $request) {
            $usuario = session("usuario");
            $inmueble = Inmueble::with("clientes.clientes")->find(1);
            GeneralController::renderHeader("Arriendos", $usuario, "activeArriendos", true, "expandedInmuebles");

            echo view("components.arriendos.3")
            ->with("inmueble", $inmueble);

            GeneralController::renderFooter();
        }

        public function generarContrato(Request $request, $id) {
            $arriendo = Arriendo::with([
                "arrendatario",
                "codeudores",
                "ocupantes",
                "coordinador",
                "inmueble" => function ($query) {
                    $query->with("ciudades.departamentos", "direcciones", "asesores");
                }
            ])
            ->find($id);

            $arriendo->cantidad_veces_que_se_genera_el_contrato = $arriendo->cantidad_veces_que_se_genera_el_contrato + 1;
            $arriendo->save();

            if ($arriendo->cantidad_veces_que_se_genera_el_contrato == 1) {
                $mail = new MailController();

                $mensaje = "<p>Buen día</p>";
                $mensaje .= "<p>Elite system le informa que ha sido generado por primera vez el pdf para el arriendo código {$arriendo->id} correspondiente al inmueble código mls {$arriendo->inmueble->codigoMLS}.</p>";
                $mensaje .= "<p>Con número de contrato {$arriendo->numero_contrato}</p>";
                $mensaje .= "<p>Se recomienda revisar la información para proceder al cierre del negocio.</p>";

                $destinatarios = [];
                array_push($destinatarios, [
                    "name" => "Elite System",
                    "email" => "jwoodcock@eliteinmobiliaria.com.co"
                ]);

                array_push($destinatarios, [
                    "name" => "Abogado",
                    "email" => "juridico1@eliteinmobiliaria.com.co"
                ]);

                $mail->sendMail("Se ha generado el contrato de arriendo ({$arriendo->numero_contrato}) para el inmueble {$arriendo->inmueble->codigoMLS}", $mensaje, $destinatarios);
            }

            $general = new GeneralController();

            // return $arriendo->inmueble;

            // Información general
            $contrato = view("components.arriendos.contrato-arriendo", [
                "arriendo" => $arriendo,
                "general" => $general
            ]);

            $telefonosArrendatario = "";

            if ($arriendo->arrendatario->telefono != "") {
                $telefonosArrendatario .= $arriendo->arrendatario->telefono;
            }

            if ($arriendo->arrendatario->celular != "") {
                if ($arriendo->arrendatario->telefono != "") {
                    $telefonosArrendatario .= ", ";
                }
                $telefonosArrendatario .= $arriendo->arrendatario->celular;
            }

            $contrato = str_replace("Nombre_Arrendatario_Var", "{$general->mayus($arriendo->arrendatario->nombres)} {$general->mayus($arriendo->arrendatario->apellidos)}", $contrato);
            $contrato = str_replace("CCArrendatario_Var", "{$general->toDottedNumber($arriendo->arrendatario->documento)}", $contrato);
            $contrato = str_replace("DireccionArrendatario_Var", "{$general->mayus($arriendo->arrendatario->direccion)}", $contrato);
            $contrato = str_replace("CopropiedadArrendatario_Var", "{$general->mayus($arriendo->arrendatario->copropiedad)}", $contrato);
            $contrato = str_replace("TelefonosArrendatario_Var", "{$telefonosArrendatario}", $contrato);
            $contrato = str_replace("DirArrendatario_Var", "{$general->mayus($arriendo->arrendatario->direccion_comercial)}", $contrato);
            $contrato = str_replace("EmailArrendatario_Var", "{$arriendo->arrendatario->email}", $contrato);

            // Codeudores
            for ($i = 1; $i <= 3; $i++) {
                if (isset($arriendo->codeudores[$i - 1]) && $arriendo->codeudores[$i - 1]->id != null) {
                    $contrato = str_replace("Nombre_Deudor{$i}_Var", "{$general->mayus($arriendo->codeudores[$i - 1]->nombre)}", $contrato);
                    $contrato = str_replace("CCDeudor{$i}_Var", "{$general->toDottedNumber($arriendo->codeudores[$i - 1]->cedula)}", $contrato);
                    $contrato = str_replace("DireccionDeudor{$i}_Var", "{$general->mayus($arriendo->codeudores[$i - 1]->direccion)}", $contrato);
                    $contrato = str_replace("CopropiedadDeudor{$i}_Var", "{$general->mayus($arriendo->codeudores[$i - 1]->copropiedad)}", $contrato);
                    $contrato = str_replace("TelefonosDeudor{$i}_Var", "{$general->mayus($arriendo->codeudores[$i - 1]->celular)}", $contrato);
                    $contrato = str_replace("DirDeudor{$i}_Var", "{$general->mayus($arriendo->codeudores[$i - 1]->direccion_comercial)}", $contrato);
                    $contrato = str_replace("EmailDeudor{$i}_Var", "{$arriendo->codeudores[$i - 1]->email}", $contrato);
                } else {
                    $contrato = str_replace("Nombre_Deudor{$i}_Var", "N/A", $contrato);
                    $contrato = str_replace("CCDeudor{$i}_Var", "N/A", $contrato);
                    $contrato = str_replace("DireccionDeudor{$i}_Var", "N/A", $contrato);
                    $contrato = str_replace("CopropiedadDeudor{$i}_Var", "N/A", $contrato);
                    $contrato = str_replace("TelefonosDeudor{$i}_Var", "N/A", $contrato);
                    $contrato = str_replace("DirDeudor{$i}_Var", "N/A", $contrato);
                    $contrato = str_replace("EmailDeudor{$i}_Var", "N/A", $contrato);
                }
            }

            // Ocupantes
            for ($i = 1; $i <= 6; $i++) {
                if (isset($arriendo->ocupantes[$i - 1]) && $arriendo->ocupantes[$i - 1]->id != null) {
                    $contrato = str_replace("NombreAutorizado{$i}_Var", "{$general->mayus($arriendo->ocupantes[$i - 1]->nombre)}", $contrato);
                    $contrato = str_replace("CcAutorizado{$i}_Var", "{$general->toDottedNumber($arriendo->ocupantes[$i - 1]->cedula)}", $contrato);
                } else {
                    $contrato = str_replace("NombreAutorizado{$i}_Var", "N/A", $contrato);
                    $contrato = str_replace("CcAutorizado{$i}_Var", "N/A", $contrato);
                }
            }

            $contrato = str_replace("fecha_inicio_contrato", GeneralController::formatFecha($arriendo->fecha_inicio_contrato), $contrato);
            $contrato = str_replace("fecha_final_entrega", GeneralController::formatFecha($arriendo->fecha_final_entrega), $contrato);

            // Inmueble
            $contrato = str_replace("Direccion1_Var", $general->mayus(str_replace("null", "", str_replace("  ", "", $arriendo->inmueble->direccion))), $contrato);
            $contrato = str_replace("Ciudad_Var", "{$general->mayus($arriendo->inmueble->ciudades->nombre)}, " . $general->mayus($arriendo->inmueble->ciudades->departamentos->nombre) . ", COLOMBIA", $contrato);
            $direccion2 = "";
            $direccion3 = "";

            if ($arriendo->inmueble->direcciones != null) {
                if ($arriendo->inmueble->direcciones->complemento != "" || $arriendo->inmueble->direcciones->complemento != 0) {
                    $direccion3 .= "{$arriendo->inmueble->direcciones->complemento} ";
                }
                if ($arriendo->inmueble->direcciones->complemento2 != "" || $arriendo->inmueble->direcciones->complemento2 != 0) {
                    $direccion2 .= "{$arriendo->inmueble->direcciones->complemento2} ";
                }
                if ($arriendo->inmueble->direcciones->complemento3 != "" || $arriendo->inmueble->direcciones->complemento3 != 0) {
                    $direccion2 .= "{$arriendo->inmueble->direcciones->complemento3} ";
                }
            }

            $contrato = str_replace("Direccion2_Var", "{$general->mayus($direccion2)}", $contrato);
            $contrato = str_replace("Direccion3_Var", "{$general->mayus($direccion3)}", $contrato);
            $contrato = str_replace("MatriculaVarAp1", $arriendo->inmueble->matricula != 0 ? $general->mayus($arriendo->inmueble->matricula) : "N/A", $contrato);
            $contrato = str_replace("CedulaCatastral1", "{$general->mayus($arriendo->inmueble->cedula_catastral)}", $contrato);
            $contrato = str_replace("matricula_parqueadero_1", $arriendo->inmueble->matricula_parqueadero_1 != "0" ? $general->mayus($arriendo->inmueble->matricula_parqueadero_1) : "N/A", $contrato);
            $contrato = str_replace("matricula_parqueadero_2", $arriendo->inmueble->matricula_parqueadero_2 != "0" ? $general->mayus($arriendo->inmueble->matricula_parqueadero_2) : "N/A", $contrato);
            $contrato = str_replace("matricula_parqueadero_3", $arriendo->inmueble->matricula_parqueadero_3 != "0" ? $general->mayus($arriendo->inmueble->matricula_parqueadero_3) : "N/A", $contrato);
            $contrato = str_replace("cedula_parqueadero_1", $arriendo->inmueble->cedula_parqueadero_1 != "0" ? $general->mayus($arriendo->inmueble->cedula_parqueadero_1) : "N/A", $contrato);
            $contrato = str_replace("cedula_parqueadero_2", $arriendo->inmueble->cedula_parqueadero_2 != "0" ? $general->mayus($arriendo->inmueble->cedula_parqueadero_2) : "N/A", $contrato);
            $contrato = str_replace("cedula_parqueadero_3", $arriendo->inmueble->cedula_parqueadero_3 != "0" ? $general->mayus($arriendo->inmueble->cedula_parqueadero_3) : "N/A", $contrato);
            $contrato = str_replace("chip_parqueadero_1", $arriendo->inmueble->chip_parqueadero_1 != "0" ? $general->mayus($arriendo->inmueble->chip_parqueadero_1) : "N/A", $contrato);
            $contrato = str_replace("chip_parqueadero_2", $arriendo->inmueble->chip_parqueadero_2 != "0" ? $general->mayus($arriendo->inmueble->chip_parqueadero_2) : "N/A", $contrato);
            $contrato = str_replace("chip_parqueadero_3", $arriendo->inmueble->chip_parqueadero_3 != "0" ? $general->mayus($arriendo->inmueble->chip_parqueadero_3) : "N/A", $contrato);
            $contrato = str_replace("matricula_deposito_1", $arriendo->inmueble->matricula_deposito_1 != "0" ? $general->mayus($arriendo->inmueble->matricula_deposito_1) : "N/A", $contrato);
            $contrato = str_replace("matricula_deposito_2", $arriendo->inmueble->matricula_deposito_2 != "0" ? $general->mayus($arriendo->inmueble->matricula_deposito_2) : "N/A", $contrato);
            $contrato = str_replace("chip_deposito_1", $arriendo->inmueble->chip_deposito_1 != "0" ? $general->mayus($arriendo->inmueble->chip_deposito_1) : "N/A", $contrato);
            $contrato = str_replace("chip_deposito_2", $arriendo->inmueble->chip_deposito_2 != "0" ? $general->mayus($arriendo->inmueble->chip_deposito_2) : "N/A", $contrato);
            $contrato = str_replace("cedula_deposito_1", $arriendo->inmueble->cedula_deposito_1 != "0" ? $general->mayus($arriendo->inmueble->cedula_deposito_1) : "N/A", $contrato);
            $contrato = str_replace("cedula_deposito_2", $arriendo->inmueble->cedula_deposito_2 != "0" ? $general->mayus($arriendo->inmueble->cedula_deposito_2) : "N/A", $contrato);

            $formatterES = new \NumberFormatter("es", \NumberFormatter::SPELLOUT);
            $contrato = str_replace("fecha_entrega", GeneralController::formatFecha($arriendo->fecha_entrega), $contrato);
            $contrato = str_replace("fecha_firma_contrato", GeneralController::formatFecha($arriendo->fecha_firma_contrato), $contrato);
            $contrato = str_replace("PrecioCanonNumeros_Var", "$ " . number_format($arriendo->canon, 0, ",", "."), $contrato);
            $contrato = str_replace("PrecioCanonLetras_Var", $general->mayus($formatterES->format($arriendo->canon)), $contrato);
            $contrato = str_replace("FechaDePagoCanon_Var", GeneralController::formatFecha($arriendo->fecha_pago_canon), $contrato);
            $contrato = str_replace("PrecioAdmonNumeros_Var", "$ " . number_format($arriendo->admon, 0, ",", "."), $contrato);
            $contrato = str_replace("PrecioAdmonLetras_Var", $general->mayus($formatterES->format($arriendo->admon)), $contrato);
            $contrato = str_replace("PrecioAdmonCompletoNumeros_Var", "$ " . number_format($arriendo->admon_no_desc, 0, ",", "."), $contrato);
            $contrato = str_replace("PrecioAdmonCompletoLetras_Var", $general->mayus($formatterES->format($arriendo->admon_no_desc)), $contrato);
            $contrato = str_replace("FechaDePagoAdmon_Var", GeneralController::formatFecha($arriendo->fecha_pago_admon), $contrato);
            $contrato = str_replace("PrecioCalderaNumeros_Var", "$ " . number_format($arriendo->caldera, 0, ",", "."), $contrato);
            $contrato = str_replace("PrecioCalderaLetras_Var", $general->mayus($formatterES->format($arriendo->caldera)), $contrato);
            $contrato = str_replace("FechaDePagoCaldera_Var", GeneralController::formatFecha($arriendo->fecha_pago_caldera), $contrato);
            $contrato = str_replace("PrecioParqueaderoNumeros_Var", "$ " . number_format($arriendo->parqueadero, 0, ",", "."), $contrato);
            $contrato = str_replace("PrecioParqueaderoLetras_Var", $general->mayus($formatterES->format($arriendo->parqueadero)), $contrato);
            $contrato = str_replace("FechaDePagoParqueadero_Var", GeneralController::formatFecha($arriendo->fecha_pago_parqueadero), $contrato);
            $contrato = str_replace("PrecioLavanderiaNumeros_Var", "$ " . number_format($arriendo->lavanderia, 0, ",", "."), $contrato);
            $contrato = str_replace("PrecioLavanderiaLetras_Var", $general->mayus($formatterES->format($arriendo->lavanderia)), $contrato);
            $contrato = str_replace("FechaDePagoLavanderia_Var", GeneralController::formatFecha($arriendo->fecha_pago_lavanderia), $contrato);
            $contrato = str_replace("notas_precio", $general->mayus($arriendo->notas_precio), $contrato);

            $contrato = str_replace("contrato_acueducto", $general->mayus($arriendo->contrato_acueducto), $contrato);
            $contrato = str_replace("notas_servicios", $general->mayus($arriendo->notas_servicios), $contrato);
            $contrato = str_replace("contrato_energia", $general->mayus($arriendo->contrato_energia), $contrato);
            $contrato = str_replace("notas_servicio_energia", $general->mayus($arriendo->notas_servicio_energia), $contrato);
            $contrato = str_replace("contrato_gas", $general->mayus($arriendo->contrato_gas), $contrato);
            $contrato = str_replace("notas_servicio_gas", $general->mayus($arriendo->notas_servicio_gas), $contrato);

            // echo $contrato; die;

            $options = new Options();
            $options->setIsHtml5ParserEnabled(true);
            $options->setChroot(__DIR__);
            $options->set("isRemoteEnabled", true);
            $options->set("isFontSubsettingEnabled", true);

            $dompdf = new Dompdf();
            $dompdf->setOptions($options);
            $dompdf->setPaper("a4", "portrait");
            $dompdf->loadHtml($contrato);
            $dompdf->render();
            $dompdf->stream("contrato.pdf", [
                "Attachment" => false
            ]);

        }

    }