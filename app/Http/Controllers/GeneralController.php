<?php

namespace App\Http\Controllers;

// Modelos
use App\Models\Tutorial;

// Paquetes
use Illuminate\Http\Request;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class GeneralController extends Controller
{

    public static function test() {
        phpinfo();
    }

    public static function renderHeader($titulo, $usuario, $activeName = "", $expanded = false, $expandedName = "expanded", $photoSphere = "") {
        $tutoriales = Tutorial::get();

        echo view("components.templates.header")
        ->with("titulo", $titulo)
        ->with("usuario", $usuario)
        ->with($expandedName, $expanded)
        ->with($activeName, "nav-active")
        ->with("photoSphere", $photoSphere)
        ->with("tutoriales", $tutoriales);
    }

    public static function renderFooter($script = "", $photoSphere = "") {
        echo view("components.templates.footer")
        ->with("script", $script)
        ->with("photoSphere", $photoSphere);
    }

    public static function getRequest($url, $headers) {
        try {
            $client = new \GuzzleHttp\Client();
            $res = $client->request("GET", $url, ["headers" => $headers]);
            return json_decode($res->getBody(), true);
        } catch (RequestException $e) {
            return Psr7\Message::toString($e->getResponse());
        }
    }

    public static function getPermission($usuario, $permiso) {
        $validarPermiso = 0;

        foreach ($usuario->roles->permisos as $permisos) {
            if ($permisos->permiso_id == $permiso) {
                $validarPermiso = 1;
            }
        }

        return $validarPermiso;
    }

    public static function postRequest($url, $headers, $body, $json = 0) {

        try {
            $client = new \GuzzleHttp\Client();

            $res = $client->request("POST", $url, [
                "headers" => $headers,
                "form_params" => $body
            ]);

            if ($json == 1) {
                return $res;
            } else {
                return json_decode($res->getBody(), true);
            }
        } catch (RequestException $e) {
            // return Psr7\Message::toString($e->getResponse());
            return Psr7\Message::parseMessage(Psr7\Message::toString($e->getResponse()))["body"];
            die;
        }
    }

    public static function putRequest($url, $headers, $body, $json = 0) {

        try {
            $client = new \GuzzleHttp\Client();

            $res = $client->request("PUT", $url, [
                "headers" => $headers,
                "form_params" => $body
            ]);

            if ($json == 1) {
                return $res;
            } else {
                return json_decode($res->getBody(), true);
            }
        } catch (RequestException $e) {
            // return Psr7\Message::toString($e->getResponse());
            return Psr7\Message::parseMessage(Psr7\Message::toString($e->getResponse()))["body"];
            die;
        }
    }

    public function replacespacios($string) {
        $characters = array(
            "Á" => "A", "Ç" => "c", "É" => "e", "Í" => "i", "Ñ" => "n", "Ó" => "o", "Ú" => "u",
            "á" => "a", "ç" => "c", "é" => "e", "í" => "i", "ñ" => "n", "ó" => "o", "ú" => "u",
            "à" => "a", "è" => "e", "ì" => "i", "ò" => "o", "ù" => "u"
        );

        $string = strtr($string, $characters);
        $string = strtolower(trim($string));
        $string = preg_replace("/[^a-z0-9-]/", "-", $string);
        $string = preg_replace("/-+/", "-", $string);

        if(substr($string, strlen($string) - 1, strlen($string)) === "-") {
            $string = substr($string, 0, strlen($string) - 1);
        }

        return str_replace("/", "-", str_replace(" ", "-", strtolower(trim($string, " "))));
    }

    public function cletter($string) {
        return ucwords(mb_strtolower(trim($string, " ")));
    }

    public function mayus($string) {
        return mb_strtoupper(mb_strtolower(trim($string, " ")));
    }

    public function toDottedNumber($string) {
        $string = str_replace(".", "", $string);
        $string = str_replace(",", "", $string);
        $string = intval($string);

        if ($string == 0) {
            return "N/A";
        } else {
            return number_format($string, 0, ",", ".");
        }
    }

    public static function transformImageFromBase64($base64_code, $path, $image_name) {
        if (!empty($base64_code) && !empty($path)):
            // split the string to get extension and remove not required part
            // $string_pieces[0] = to get image extension
            // $string_pieces[1] = actual string to convert into image
            $string_pieces = explode(";base64,", $base64_code);
            /*@ Get type of image ex. png, jpg, etc. */
            // $image_type[1] will return type
            $image_type_pieces = explode("image/", $string_pieces[0]);
            $image_type = $image_type_pieces[1];

            /*@ If image name available then use that  */
            if (!empty($image_name) ) :
                $name = $path . "/" . $image_name . "." . $image_type;
            endif;
            $decoded_string = base64_decode($string_pieces[1]);

            Storage::disk(config("app.files_driver"))->put($name, $decoded_string);

            return Storage::url($name);
        endif;
    }

    public static function generalSaveImage($image, $path, $image_name) {
        $image_type = $image->extension();

        if (!empty($image_name) ) :
            $name = $path . "/" . $image_name . "." . $image_type;
        endif;

        return $name;

        Storage::disk("public")->put($name, $image);

        return Storage::url($name);
    }

    public static function generalDeleteImage($name) {
        Storage::disk(config("app.files_driver"))->delete($name);
    }

    public function getInitials($string = null) {
        return array_reduce(
            explode(" ", $string),
            function ($initials, $word) {
                return sprintf("%s%s", $initials, substr($word, 0, 1));
            },
            ""
        );
    }

    public static function formatFecha($fecha_formato) {
        $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        $fecha = Carbon::parse($fecha_formato);
        $mes = $meses[($fecha->format("n")) - 1];
        return $fecha->format("d") . " de " . $mes . " de " . $fecha->format("Y");
    }
}
