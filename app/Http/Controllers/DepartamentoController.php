<?php

namespace App\Http\Controllers;

//Modelos
use App\Models\Departamento;

//Paquetes
use Illuminate\Http\Request;

class DepartamentoController extends Controller
{
    public function generalList() {
        $estado = Departamento::get();
        return $estado;
    }
}
