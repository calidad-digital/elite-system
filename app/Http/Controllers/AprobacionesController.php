<?php

namespace App\Http\Controllers;

// Modelos
use App\Models\Venta1;
use App\Models\VentaCorreo;
use App\Models\ArriendoCorreos;

// Paquetes
use Illuminate\Http\Request;

class AprobacionesController extends Controller
{
    public function index (Request $request) {
        $content = $request->all();

        if ($content["type"] == "Venta") {
            $venta = VentaCorreo::find($content["record"]);

            if (isset($content["aprobar"]) && $content["aprobar"] == 1) {
                $venta->aprobado = 1;
                $venta->comentario = "Aprobado forzadamente por el administrador";
            }

            if (isset($content["rechazar"]) && $content["rechazar"] == 1) {
                $venta->aprobado = 0;
                $venta->comentario = "Rechazado forzadamente por el administrador";
            }

            $venta->save();
        }

        if ($content["type"] == "Arriendo") {
            $venta = ArriendoCorreos::find($content["record"]);

            if (isset($content["aprobar"]) && $content["aprobar"] == 1) {
                $venta->aprobado = 1;
                $venta->commentario = "Aprobado forzadamente por el administrador";
            }

            if (isset($content["rechazar"]) && $content["rechazar"] == 1) {
                $venta->aprobado = 0;
                $venta->commentario = "Rechazado forzadamente por el administrador";
            }

            $venta->save();
        }

        return redirect()->back();
    }
}
