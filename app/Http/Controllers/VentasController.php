<?php

namespace App\Http\Controllers;

// Modelos
use App\Models\Usuario;
use App\Models\Venta1;
use App\Models\Venta1Extended;
use App\Models\ClienteVenta;
use App\Models\Cliente;
use App\Models\TipoDocumento;
use App\Models\SubEstadoCivil;
use App\Models\ApoderadoClienteVenta;
use App\Models\InmuebleCliente;
use App\Models\Rol;
use App\Models\VentaCorreo;

// Paquetes
use Illuminate\Http\Request;
use App\Models\Inmueble;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\InmuebleController;
use App\Http\Controllers\ApiController;
use Dompdf\Dompdf;
use Dompdf\Options;

// Requests
use App\Http\Requests\ventas1Back;
use Carbon\Carbon;

class VentasController extends Controller
{
    public function __construct() {
        $this->driver = config("app.files_driver");
        $this->bucketUrl = "https://elitesystem1.s3.amazonaws.com/";
    }

    public function updateClienteVentas($venta) {
        $propietarios = Cliente::with("subestadociviles")
        ->whereHas("inmuebles", function ($query) use ($venta) {
            $query->where("inmueble_id", $venta->inmueble_id)
            ->where("rol_id", 3);
        })
        ->get();

        $arrayPropietarios = [];

        // Agregamos propietarios nuevos si los hay
        foreach ($propietarios as $propietario) {
            array_push($arrayPropietarios, $propietario->documento);

            $vendedorExist = ClienteVenta::where("venta1_id", $venta->id)
            ->where("tipo_cliente", "vendedor")
            ->where("inmueble_id", $venta->inmueble_id)
            ->where("numero_documento", $propietario->documento)
            ->first();

            if ($vendedorExist == null) {
                $vendedor = new ClienteVenta();
                $vendedor->inmueble_id = $venta->inmueble_id;
                $vendedor->tipo_cliente = "vendedor";
                $vendedor->nombre = "{$propietario->nombres} {$propietario->apellidos}";
                $vendedor->ciudad = $propietario->ciudad;
                $vendedor->numero_documento = $propietario->documento;
                $vendedor->lugar_expedicion_documento = $propietario->expedicion;
                $vendedor->fecha_expedicion_documento = Carbon::now();
                $vendedor->fecha_nacimiento = $propietario->fecha_nacimiento;
                $vendedor->estado_civil = $propietario->estado_civil;
                $vendedor->telefono = $propietario->telefono;
                $vendedor->celular = $propietario->celular;
                $vendedor->email = $propietario->email;
                $vendedor->direccion = $propietario->direccion;
                $vendedor->profesion = $propietario->profesion;
                $vendedor->tipo_documento_id = $propietario->tipo_documento_id;
                $vendedor->subestado_civil_id = $propietario->subestado_civil_id != 0 ? $propietario->subestado_civil_id : null;
                $vendedor->venta1_id = $venta->id;

                $vendedor->save();
            } else {
                if ($vendedorExist->email == "" || $vendedorExist->email == null) {
                    $vendedorExist->email = $propietario->email;
                }

                if ($vendedorExist->email == "" || $vendedorExist->email == null) {
                    $vendedorExist->url_documento = $propietario->url_documento;
                }

                $vendedorExist->save();
            }
        }

        $vendedores = ClienteVenta::where("venta1_id", $venta->id)
        ->where("tipo_cliente", "vendedor")
        ->where("inmueble_id", $venta->inmueble_id)
        ->get();

        $arrayExistentes = [];

        foreach ($vendedores as $vendedor) {
            array_push($arrayExistentes, $vendedor->numero_documento);
        }

        $difference = array_values(array_diff($arrayExistentes, $arrayPropietarios));

        foreach ($difference as $documentToDelete) {
            $vendedorToDelete = ClienteVenta::where("venta1_id", $venta->id)
            ->where("tipo_cliente", "vendedor")
            ->where("inmueble_id", $venta->inmueble_id)
            ->where("numero_documento", $documentToDelete)
            ->first();

            $vendedorToDelete->delete();
        }

        $vendedores = ClienteVenta::with("apoderados", "subestadociviles")
        ->where("venta1_id", $venta->id)
        ->where("tipo_cliente", "vendedor")
        ->where("inmueble_id", $venta->inmueble_id)
        ->get();

        return $vendedores;
    }

    public function addCompradorToClientes($usuario, $cliente) {
        // Validación de si el comprador existe en la tabla clientes
        $roles = Rol::where("nombre", "Comprador")->first();
        if ($roles == null) {
            $rol = new Rol();
            $rol->tipo_rol = 2;
            $rol->nombre = "Comprador";
            $rol->descripcion = "Comprador de un inmueble";
            $rol->save();

            $roles = $rol;

            $comprador = null;
        } else {
            // Si existe el rol, entonces buscamos este comprador
            $comprador = Cliente::where("documento", $cliente->numero_documento)
            ->orWhere("email", $cliente->email)
            ->where("rol_id", $roles->id)
            ->first();
        }

        if ($comprador == null) {
            $comprador = new Cliente();
            $comprador->username = $cliente->numero_documento;
            $comprador->password = $cliente->numero_documento;
            $comprador->documento = $cliente->numero_documento;
            $comprador->tipo_documento_id = $cliente->tipo_documento_id ?? 1;
            $comprador->rol_id = $roles->id;
            $comprador->nombres = $cliente->nombre;
            $comprador->apellidos = "";
            $comprador->email = $cliente->email ?? "";
            $comprador->expedicion = $cliente->lugar_expedicion_documento ?? "";
            $comprador->telefono = $cliente->telefono ?? "";
            $comprador->celular = $cliente->celular ?? "";
            $comprador->whatsapp = $cliente->celular ?? "";
            $comprador->url_documento = $cliente->url_documento ?? "";
            $comprador->ciudad = $cliente->ciudad ?? "";
            $comprador->direccion = $cliente->direccion ?? "";
            $comprador->estado_ubicacions_id = 1;
            $comprador->ciudad_id = 1;
            $comprador->estado_id = 1;
            $comprador->empresa_id = 1;
            $comprador->oficina_id = 1;
            $comprador->comentario = "";
            $comprador->profesion = "";
            $comprador->fecha_nacimiento = Carbon::now();
            $comprador->create_user_id = $usuario["id"];
            $comprador->update_user_id = $usuario["id"];

            $comprador->save();
        }
    }

    public function addCompradorToPropietarios(Request $request, $usuario, $cliente, $inmueble) {
        // Validación de si el comprador existe en la tabla clientes
        $vendedor = Cliente::where("documento", $cliente->numero_documento)
        ->orWhere("email", $cliente->email)
        ->where("rol_id", 3)
        ->first();

        if ($vendedor == null) {
            $vendedor = new Cliente();
            $vendedor->username = $cliente->numero_documento;
            $vendedor->password = $cliente->numero_documento;
            $vendedor->documento = $cliente->numero_documento;
            $vendedor->tipo_documento_id = $cliente->tipo_documento_id;
            $vendedor->rol_id = 3;
            $vendedor->nombres = $cliente->nombre;
            $vendedor->apellidos = "";
            $vendedor->email = $cliente->email ?? "";
            $vendedor->expedicion = $cliente->lugar_expedicion_documento ?? "";
            $vendedor->telefono = $cliente->telefono ?? "";
            $vendedor->celular = $cliente->celular ?? "";
            $vendedor->whatsapp = $cliente->celular ?? "";
            $vendedor->url_documento = $cliente->url_documento;
            $vendedor->ciudad = $cliente->ciudad;
            $vendedor->direccion = $cliente->direccion;
            $vendedor->estado_ubicacions_id = 1;
            $vendedor->ciudad_id = 1;
            $vendedor->estado_id = 1;
            $vendedor->empresa_id = 1;
            $vendedor->oficina_id = 1;
            $vendedor->comentario = "";
            $vendedor->profesion = "";
            $vendedor->fecha_nacimiento = Carbon::now();
            $vendedor->create_user_id = $usuario["id"];
            $vendedor->update_user_id = $usuario["id"];

            $vendedor->save();
        } else {
            $vendedor->username = $cliente->numero_documento;
            $vendedor->password = $cliente->numero_documento;
            $vendedor->documento = $cliente->numero_documento;
            $vendedor->tipo_documento_id = $cliente->tipo_documento_id;
            $vendedor->nombres = $cliente->nombre;
            $vendedor->apellidos = "";
            $vendedor->email = $cliente->email ?? "";
            $vendedor->expedicion = $cliente->lugar_expedicion_documento ?? "";
            $vendedor->telefono = $cliente->telefono ?? "";
            $vendedor->celular = $cliente->celular ?? "";
            $vendedor->whatsapp = $cliente->celular ?? "";

            if ($cliente->url_documento != "") {
                $vendedor->url_documento = $cliente->url_documento;
            }

            $vendedor->ciudad = $cliente->ciudad;
            $vendedor->direccion = $cliente->direccion ?? "";
            $vendedor->update_user_id = $usuario["id"];

            $vendedor->save();
        }

        // Ahora asociamos el inmueble al cliente
        $inmuebleCliente = InmuebleCliente::where("inmueble_id", $inmueble)
        ->where("cliente_id", $vendedor->id)
        ->where("rol_id", 3)
        ->first();

        if ($inmuebleCliente == null) {
            $inmuebleCliente = new InmuebleCliente();
            $inmuebleCliente->inmueble_id = $inmueble;
            $inmuebleCliente->cliente_id = $vendedor->id;
            $inmuebleCliente->rol_id = 3;
            $inmuebleCliente->orden = 1;
            $inmuebleCliente->porcentaje = 50;
            $inmuebleCliente->save();
        }
    }

    public function removeVendedorFromPropietarios($cliente, $inmueble) {
        $vendedor = Cliente::where("documento", $cliente->numero_documento)
        ->orWhere("email", $cliente->email)
        ->where("rol_id", 3)
        ->first();

        if ($vendedor != null) {
            $inmuebleCliente = InmuebleCliente::where("inmueble_id", $inmueble)
            ->where("cliente_id", $vendedor->id)
            ->where("rol_id", 3)
            ->orderBy("id", "desc")
            ->first();

            if ($inmuebleCliente != null) {
                $inmuebleCliente->delete();
            }
        }
    }

    public function ventas1(Request $request, $code) {
        $usuario = session("usuario");
        $agentes = Usuario::get();
        $inmueble = Inmueble::with("clientes.clientes")->find($code);

        GeneralController::renderHeader("Ventas", $usuario, "activeVentas", true, "expandedInmuebles");

        $venta = Venta1::with("usuarios")
        ->where("inmueble_id", $inmueble->id)
        ->orderBy("id", "desc")
        ->first();

        if ($venta == null) {
            $venta = new Venta1();
            $venta->inmueble_id = $inmueble->id;
            $venta->estado_venta_id = 1;
            $venta->create_user_id = $usuario->id;
            $venta->update_user_id = $usuario->id;

            $venta->save();

            $venta = Venta1::with("usuarios")
            ->find($venta->id);
        }

        $extensiones = Venta1Extended::where("venta_id", $venta->id)->first();

        if ($extensiones != null) {
            foreach ($extensiones->toArray() as $key => $value) {
                if ($key != "id") {
                    $venta[$key] = $value;
                }
            }
        }

        $compradores = ClienteVenta::where("venta1_id", $venta->id)
        ->where("tipo_cliente", "comprador")
        ->where("inmueble_id", $venta->inmueble_id)
        ->get();

        $tipos_documento = TipoDocumento::get();
        $estados_civiles = SubEstadoCivil::get();
        $roles = SubEstadoCivil::get();

        echo view("components.ventas.edit")
        ->with("venta",$venta)
        ->with("usuario", $usuario)
        ->with("agentes", $agentes)
        ->with("vendedores", $this->updateClienteVentas($venta))
        ->with("compradores", $compradores)
        ->with("tipos_documento", $tipos_documento)
        ->with("estados_civiles", $estados_civiles);

        GeneralController::renderFooter();
    }

    public function replaceNumbers(String $text){
        $text =   str_replace(',', '',$text);
        $text =   str_replace('$', '',$text);
        if (strstr($text, '.', true) === false) {
            return $text;

        } else {
            $text = strstr($text, '.', true);
            return $text;
        }
    }

    public function ventas1Back(Request $request, $id) {
        $usuario = session("usuario");
        $content = $request->all();

        $inm = Inmueble::find($content['inmueble_id']);

        if (!isset($content['venta_id'])) {
            $ventas = new Venta1();
            $ventas->estado_venta_id = 1;
            $ventas->inmueble_id = $id;
            $ventas->create_user_id = $usuario["id"];
        }else{
            $ventas = Venta1::find($content['venta_id']);
        }

        $ventas->update_user_id = $usuario["id"];

        if (isset($content["create_user_id"]) && $content["create_user_id"] != "") {
            $ventas->create_user_id = $content["create_user_id"];
        } else {
            $ventas->create_user_id = $usuario["id"];
        }

        // Garaje
        for ($i = 1; $i <= 3; $i++) {
            if (isset($content["numero_parqueadero_{$i}"]) && $content["numero_parqueadero_{$i}"] != "") {
                $inm["numero_parqueadero_{$i}"] = $content["numero_parqueadero_{$i}"];
            }

            if (isset($content["matricula_parqueadero_{$i}"]) && $content["matricula_parqueadero_{$i}"] != "") {
                $inm["matricula_parqueadero_{$i}"] = $content["matricula_parqueadero_{$i}"];
            }

            if (isset($content["chip_parqueadero_{$i}"]) && $content["chip_parqueadero_{$i}"] != "") {
                $inm["chip_parqueadero_{$i}"] = $content["chip_parqueadero_{$i}"];
            }

            if (isset($content["cedula_parqueadero_{$i}"]) && $content["cedula_parqueadero_{$i}"] != "") {
                $inm["cedula_parqueadero_{$i}"] = $content["cedula_parqueadero_{$i}"];
            }
        }

        // Depósito
        for ($i = 1; $i <= 2; $i++) {
            if (isset($content["numero_deposito_{$i}"]) && $content["numero_deposito_{$i}"] != "") {
                $inm["numero_deposito_{$i}"] = $content["numero_deposito_{$i}"];
            }

            if (isset($content["matricula_deposito_{$i}"]) && $content["matricula_deposito_{$i}"] != "") {
                $inm["matricula_deposito_{$i}"] = $content["matricula_deposito_{$i}"];
            }

            if (isset($content["chip_deposito_{$i}"]) && $content["chip_deposito_{$i}"] != "") {
                $inm["chip_deposito_{$i}"] = $content["chip_deposito_{$i}"];
            }

            if (isset($content["cedula_deposito_{$i}"]) && $content["cedula_deposito_{$i}"] != "") {
                $inm["cedula_deposito_{$i}"] = $content["cedula_deposito_{$i}"];
            }
        }

        $inm->save();

        $clientesQueEstan = [];
        // Vendedores con apoderados
        for ($i = 0; $i <= $content["cantidad_vendedores"]; $i++) {
            if (isset($content["{$i}_vendedor_nombre"]) && $content["{$i}_vendedor_nombre"] != "") {
                if (isset($content["{$i}_vendedor_id"]) && $content["{$i}_vendedor_id"] != "") {
                    $cliente = ClienteVenta::where("inmueble_id", $ventas->inmueble_id)
                    ->where("tipo_cliente", "vendedor")
                    ->where(function ($query) use ($content, $i) {
                        $query->where("numero_documento", $content["{$i}_vendedor_cedula"])
                        ->orWhere("id", $content["{$i}_vendedor_id"]);
                    })
                    ->first();
                } else {
                    $cliente = null;
                }

                if ($cliente == null) {
                    $cliente = new ClienteVenta();
                    $cliente->venta1_id = $ventas->id;
                    $cliente->inmueble_id = $ventas->inmueble_id;
                    $cliente->tipo_cliente = "vendedor";
                }

                $cliente->nombre = $content["{$i}_vendedor_nombre"];
                $cliente->ciudad = $content["{$i}_vendedor_ciudad"];
                $cliente->tipo_documento_id = $content["{$i}_vendedor_tipo_doc"];
                $cliente->numero_documento = $content["{$i}_vendedor_cedula"];
                $cliente->lugar_expedicion_documento = $content["{$i}_vendedor_expedicion"];
                $cliente->estado_civil = $content["{$i}_vendedor_estado_civil"];
                $cliente->subestado_civil_id = $content["{$i}_vendedor_subestado_civil"];
                $cliente->telefono = $content["{$i}_vendedor_telefono"];
                $cliente->celular = $content["{$i}_vendedor_celular"];
                $cliente->email = $content["{$i}_vendedor_email"];
                $cliente->direccion = $content["{$i}_vendedor_direccion"];
                $cliente->save();

                if ($request->hasFile("{$i}_vendedor_archivo_cedula")) {
                    $image = $request->file("{$i}_vendedor_archivo_cedula");
                    $imageName = "cedula_vendedor_" . mb_strtolower(str_replace(" " , "-", $cliente->numero_documento)) . "." . $image->extension();
                    $url = $image->storeAs("ventas/inmuebles_" . $content["inmueble_id"], $imageName, $this->driver);
                    $cliente->url_documento = $this->bucketUrl . $url;
                    $cliente->save();
                }

                array_push($clientesQueEstan, $cliente->id);

                $this->addCompradorToPropietarios($request, $usuario, $cliente, $ventas->inmueble_id);

                if (isset($content["{$i}_vendedor_apoderado_nombre"]) && $content["{$i}_vendedor_apoderado_nombre"] != "") {
                    $apoderado = ApoderadoClienteVenta::where("cliente_venta_id", $cliente->id)
                    ->first();

                    if ($content["{$i}_vendedor_apoderado"] == "apoderado") {
                        if ($apoderado == null) {
                            $apoderado = new ApoderadoClienteVenta();
                            $apoderado->venta1_id = $ventas->id;
                            $apoderado->inmueble_id = $ventas->inmueble_id;
                            $apoderado->cliente_venta_id = $cliente->id;
                        }

                        $apoderado->nombre = $content["{$i}_vendedor_apoderado_nombre"];
                        $apoderado->ciudad = $content["{$i}_vendedor_apoderado_ciudad"];
                        $apoderado->tipo_documento_id = $content["{$i}_vendedor_apoderado_tipo_doc"];
                        $apoderado->numero_documento = $content["{$i}_vendedor_apoderado_cedula"];
                        $apoderado->lugar_expedicion_documento = $content["{$i}_vendedor_apoderado_expedicion"];
                        $apoderado->save();

                        if ($request->hasFile("{$i}_vendedor_apoderado_archivo_cedula")) {
                            $image = $request->file("{$i}_vendedor_apoderado_archivo_cedula");
                            $imageName = "cedula_vendedor_apoderado_" . mb_strtolower(str_replace(" " , "-", $apoderado->numero_documento)) . "." . $image->extension();
                            $url = $image->storeAs("ventas/inmuebles_" . $content["inmueble_id"], $imageName, $this->driver);
                            $apoderado->url_documento = $this->bucketUrl . $url;
                            $apoderado->save();
                        }
                    } else {
                        if ($apoderado != null) {
                            $apoderado->delete();
                        }
                    }
                }
            }
        }

        // Validación de qué vendedores se borraron
        $clientesQueNoEstan = ClienteVenta::where("venta1_id", $ventas->id)
        ->where("inmueble_id", $ventas->inmueble_id)
        ->where("tipo_cliente", "vendedor")
        ->whereNotIn("id", $clientesQueEstan)
        ->get();

        if (count($clientesQueNoEstan) > 0) {
            foreach ($clientesQueNoEstan as $value) {
                $apoderados = ApoderadoClienteVenta::where("cliente_venta_id", $value->id)->first();
                if ($apoderados != null) {
                    $apoderados->delete();
                }

                $vendedores = ClienteVenta::find($value->id);

                if ($vendedores != null) {
                    $this->removeVendedorFromPropietarios($vendedores, $ventas->inmueble_id);
                    $vendedores->delete();
                }
            }
        }

        $clientesQueEstan = [];
        // Compradores con apoderados
        for ($i = 0; $i <= $content["cantidad_compradores"]; $i++) {
            if (isset($content["{$i}_comprador_nombre"]) && $content["{$i}_comprador_nombre"] != "") {
                if (isset($content["{$i}_comprador_id"]) && $content["{$i}_comprador_id"] != "") {
                    $cliente = ClienteVenta::where("inmueble_id", $ventas->inmueble_id)
                    ->where("tipo_cliente", "comprador")
                    ->where(function ($query) use ($content, $i) {
                        $query->where("numero_documento", $content["{$i}_comprador_cedula"])
                        ->orWhere("id", $content["{$i}_comprador_id"]);
                    })
                    ->first();
                } else {
                    $cliente = null;
                }

                if ($cliente == null) {
                    $cliente = new ClienteVenta();
                    $cliente->venta1_id = $ventas->id;
                    $cliente->inmueble_id = $ventas->inmueble_id;
                    $cliente->tipo_cliente = "comprador";
                }

                $cliente->nombre = $content["{$i}_comprador_nombre"];
                $cliente->ciudad = $content["{$i}_comprador_ciudad"];
                $cliente->tipo_documento_id = $content["{$i}_comprador_tipo_doc"];
                $cliente->numero_documento = $content["{$i}_comprador_cedula"];
                $cliente->lugar_expedicion_documento = $content["{$i}_comprador_expedicion"];
                $cliente->estado_civil = $content["{$i}_comprador_estado_civil"];
                $cliente->subestado_civil_id = $content["{$i}_comprador_subestado_civil"];
                $cliente->telefono = $content["{$i}_comprador_telefono"];
                $cliente->celular = $content["{$i}_comprador_celular"];
                $cliente->email = $content["{$i}_comprador_email"];
                $cliente->direccion = $content["{$i}_comprador_direccion"];
                $cliente->save();

                if ($request->hasFile("{$i}_comprador_archivo_cedula")) {
                    $image = $request->file("{$i}_comprador_archivo_cedula");
                    $imageName = "cedula_comprador_" . mb_strtolower(str_replace(" " , "-", $cliente->numero_documento)) . "." . $image->extension();
                    $url = $image->storeAs("ventas/inmuebles_" . $content["inmueble_id"], $imageName, $this->driver);
                    $cliente->url_documento = $this->bucketUrl . $url;
                    $cliente->save();
                }

                array_push($clientesQueEstan, $cliente->id);

                $this->addCompradorToClientes($usuario, $cliente);

                if (isset($content["{$i}_comprador_apoderado_nombre"]) && $content["{$i}_comprador_apoderado_nombre"] != "") {
                    $apoderadoExists = ApoderadoClienteVenta::where("cliente_venta_id", $cliente->id)
                    ->first();

                    if ($content["{$i}_comprador_apoderado"] == "apoderado") {

                        if ($apoderadoExists == null) {
                            $apoderado = new ApoderadoClienteVenta();
                            $apoderado->venta1_id = $ventas->id;
                            $apoderado->inmueble_id = $ventas->inmueble_id;
                            $apoderado->cliente_venta_id = $cliente->id;
                        } else {
                            $apoderado = ApoderadoClienteVenta::find($apoderadoExists->id);
                        }

                        $apoderado->nombre = $content["{$i}_comprador_apoderado_nombre"];
                        $apoderado->ciudad = $content["{$i}_comprador_apoderado_ciudad"];
                        $apoderado->tipo_documento_id = $content["{$i}_comprador_apoderado_tipo_doc"];
                        $apoderado->numero_documento = $content["{$i}_comprador_apoderado_cedula"];
                        $apoderado->lugar_expedicion_documento = $content["{$i}_comprador_apoderado_expedicion"];
                        $apoderado->save();

                        if ($request->hasFile("{$i}_comprador_apoderado_archivo_cedula")) {
                            $image = $request->file("{$i}_comprador_apoderado_archivo_cedula");
                            $imageName = "cedula_comprador_apoderado_" . mb_strtolower(str_replace(" " , "-", $apoderado->numero_documento)) . "." . $image->extension();
                            $url = $image->storeAs("ventas/inmuebles_" . $content["inmueble_id"], $imageName, $this->driver);
                            $apoderado->url_documento = $this->bucketUrl . $url;
                            $apoderado->save();
                        }
                    } else {
                        if ($apoderadoExists != null) {
                            $apoderadoExists->delete();
                        }
                    }
                }
            }
        }

        // Validación de qué compradores se borraron
        $clientesQueNoEstan = ClienteVenta::where("venta1_id", $ventas->id)
        ->where("inmueble_id", $ventas->inmueble_id)
        ->where("tipo_cliente", "comprador")
        ->whereNotIn("id", $clientesQueEstan)
        ->get();

        if (count($clientesQueNoEstan) > 0) {
            foreach ($clientesQueNoEstan as $value) {
                $apoderados = ApoderadoClienteVenta::where("cliente_venta_id", $value->id)->first();
                if ($apoderados != null) {
                    $apoderados->delete();
                }

                $compradores = ClienteVenta::find($value->id);

                if ($compradores != null) {
                    $compradores->delete();
                }
            }
        }

        // Cláusulas del contrato
        $conditionalClausula = 5;
        $clausulas = "";
        for ($i = 1; $i <= $conditionalClausula + 1; $i++) {
            if (isset($content["clausula_{$i}"]) && $content["clausula_{$i}"] != "") {
                $clausulas .= ".ñ.{$content["clausula_{$i}"]}";
            } else {
                $clausulas .= ".ñ.";
            }
        }

        $ventas->clausulas = $clausulas;

        // return "Funciona";
        //Deposito
        if (isset($content["matricula_deposito"]) && $content["matricula_deposito"] != "") {
            $ventas->matricula_deposito = $content["matricula_deposito"];
        }

        if (isset($content["chip_deposito"]) && $content["chip_deposito"] != "") {
            $ventas->chip_deposito = $content["chip_deposito"];
        }

        if (isset($content["cedula_catastral_deposito"]) && $content["cedula_catastral_deposito"] != "") {
            $ventas->cedula_catastral_deposito = $content["cedula_catastral_deposito"];
        }

        //Negociacion
        if (isset($content["negociacion_precio"]) && $content["negociacion_precio"] != "") {
            $ventas->negociacion_precio = $this->replaceNumbers($content["negociacion_precio"]);
        }
        if (isset($content["negociacion_pagado_promesa"]) && $content["negociacion_pagado_promesa"] != "") {
            $ventas->negociacion_pagado_promesa = $this->replaceNumbers($content["negociacion_pagado_promesa"]);
        }

        if (isset($content["negociacion_saldo_pendiente"]) && $content["negociacion_saldo_pendiente"] != "") {
            $ventas->negociacion_saldo_pendiente = $this->replaceNumbers($content["negociacion_saldo_pendiente"]);
        }

        if (isset($content["negociacion_arras"]) && $content["negociacion_arras"] != "") {
            $ventas->negociacion_arras = $this->replaceNumbers($content["negociacion_arras"]);
        }

        if (isset($content["negociacion_arras_favor"]) && $content["negociacion_arras_favor"] != "") {
            $ventas->negociacion_arras_favor = $content["negociacion_arras_favor"];
        }

        if (isset($content["show_paragrafo_unico"]) && $content["show_paragrafo_unico"] != "") {
            $ventas->mostrar_paragrafo_unico = $content["show_paragrafo_unico"];
        }

        // Pagos
        for ($i= 1; $i <= 8; $i++) {

            if ($i >= 5) {
                $pagoExist = Venta1Extended::where("venta_id", $ventas->id)->first();

                if ($pagoExist == null) {
                    $pagos = new Venta1Extended();
                    $pagos->venta_id = $ventas->id;
                } else {
                    $pagos = $pagoExist;
                }
            } else {
                $pagos = Venta1::find($ventas->id);
            }

            if (isset($content["pago_{$i}_fecha_pago"]) && $content["pago_{$i}_fecha_pago"] != "") {
                $pagos["pago_{$i}_fecha_pago"] = $content["pago_{$i}_fecha_pago"];
            } else {
                $pagos["pago_{$i}_fecha_pago"] = null;
            }

            if ($i == 1) {
                if (isset($content["pago_{$i}_notaria"]) && $content["pago_{$i}_notaria"] != "") {
                    $pagos["pago_{$i}_notaria"] = $content["pago_{$i}_notaria"];
                } else {
                    $pagos["pago_{$i}_notaria"] = null;
                }
            }

            if ($i != 1) {
                if (isset($content["pago_{$i}_pago_favor"]) && $content["pago_{$i}_pago_favor"] != "") {
                    $pagos["pago_{$i}_pago_favor"] = $content["pago_{$i}_pago_favor"];
                } else {
                    $pagos["pago_{$i}_pago_favor"] = null;
                }
            }

            if (isset($content["pago_{$i}_medio_pago"]) && $content["pago_{$i}_medio_pago"] != "") {
                $pagos["pago_{$i}_medio_pago"] = $content["pago_{$i}_medio_pago"];
            } else {
                $pagos["pago_{$i}_medio_pago"] = null;
            }

            if (isset($content["pago_{$i}_valor_pago"]) && $content["pago_{$i}_valor_pago"] != "") {
                $pagos["pago_{$i}_valor_pago"] = $this->replaceNumbers($content["pago_{$i}_valor_pago"]);
            } else {
                $pagos["pago_{$i}_valor_pago"] = null;
            }

            if (isset($content["pago_{$i}_cheque_transferencia"]) && $content["pago_{$i}_cheque_transferencia"] != "") {
                $pagos["pago_{$i}_cheque_transferencia"] = $content["pago_{$i}_cheque_transferencia"];
            } else {
                $pagos["pago_{$i}_cheque_transferencia"] = null;
            }

            if (isset($content["pago_{$i}_origen_recurso"]) && $content["pago_{$i}_origen_recurso"] != "") {
                $pagos["pago_{$i}_origen_recurso"] = $content["pago_{$i}_origen_recurso"];
            } else {
                $pagos["pago_{$i}_origen_recurso"] = null;
            }

            $pagos->save();
        }

        //Escrituracion
        if (isset($content["escritura_fecha_publica"]) && $content["escritura_fecha_publica"] != "") {
            $ventas->escritura_fecha_publica = $content["escritura_fecha_publica"];
        }

        if (isset($content["escritura_notaria"]) && $content["escritura_notaria"] != "") {
            $ventas->escritura_notaria = $content["escritura_notaria"];
        }

        if (isset($content["notaria_circulo"]) && $content["notaria_circulo"] != "") {
            $ventas->notaria_circulo = $content["notaria_circulo"];
        }

        if (isset($content["hora_escrituracion"]) && $content["hora_escrituracion"] != "") {
            $ventas->hora_escrituracion = $content["hora_escrituracion"];
        }

        if (isset($content["notas_escrituracion"]) && $content["notas_escrituracion"] != "") {
            $ventas->notas_escrituracion = $content["notas_escrituracion"];
        }

        if (isset($content["escritura_fecha_entrega_inmueble"]) && $content["escritura_fecha_entrega_inmueble"] != "") {
            $ventas->escritura_fecha_entrega_inmueble = $content["escritura_fecha_entrega_inmueble"];
        }

        if (isset($content["escritura_predial_asumido"]) && $content["escritura_predial_asumido"] != "") {
            $ventas->escritura_predial_asumido = $content["escritura_predial_asumido"];
        }

        if (isset($content["escritura_gasto_notarial_asumido"]) && $content["escritura_gasto_notarial_asumido"] != "") {
            $ventas->escritura_gasto_notarial_asumido = $content["escritura_gasto_notarial_asumido"];
        }

        if (isset($content["escritura_beneficiado_instrumento_publico"]) && $content["escritura_beneficiado_instrumento_publico"] != "") {
            $ventas->escritura_beneficiado_instrumento_publico = $content["escritura_beneficiado_instrumento_publico"];
        }

        if (isset($content["escritura_negociacion_adicional"]) && $content["escritura_negociacion_adicional"] != "") {
            $ventas->escritura_negociacion_adicional = $content["escritura_negociacion_adicional"];
        }

        if (isset($content["escritura_pago_comision_elite"]) && $content["escritura_pago_comision_elite"] != "") {
            $ventas->escritura_pago_comision_elite = $content["escritura_pago_comision_elite"];
        } else {
            $ventas->escritura_pago_comision_elite = "";
        }

        if (isset($content["escritura_observacion"]) && $content["escritura_observacion"] != "") {
            $ventas->escritura_observacion = $content["escritura_observacion"];
        }

        if (isset($content["fecha_firma_promesa"]) && $content["fecha_firma_promesa"] != "") {
            $ventas->fecha_firma_promesa = $content["fecha_firma_promesa"];
        }

        if (isset($content["mostrar_paragrafo_segundo"]) && $content["mostrar_paragrafo_segundo"] != "") {
            $ventas->mostrar_paragrafo_segundo = $content["mostrar_paragrafo_segundo"];
        }

        if (isset($content["paragrafo_segundo_texto"]) && $content["paragrafo_segundo_texto"] != "") {
            $ventas->paragrafo_segundo_texto = $content["paragrafo_segundo_texto"];
        }

        if (isset($content["mostrar_clausula_covid"]) && $content["mostrar_clausula_covid"] != "") {
            $pagoExist = Venta1Extended::where("venta_id", $ventas->id)->first();

            if ($pagoExist == null) {
                $pagos = new Venta1Extended();
                $pagos->venta_id = $ventas->id;
            } else {
                $pagos = $pagoExist;
            }

            $pagos->mostrar_clausula_covid = $content["mostrar_clausula_covid"] == "1" ? true : false;

            $pagos->save();
        }

        if (isset($content["mostrar_firma_electronica"]) && $content["mostrar_firma_electronica"] != "") {
            $pagoExist = Venta1Extended::where("venta_id", $ventas->id)->first();

            if ($pagoExist == null) {
                $pagos = new Venta1Extended();
                $pagos->venta_id = $ventas->id;
            } else {
                $pagos = $pagoExist;
            }

            $pagos->mostrar_firma_electronica = $content["mostrar_firma_electronica"] == "1" ? true : false;

            $pagos->save();
        }

        if ($request->hasFile("clt_url")) {
            $image = $request->file("clt_url");
            $imageName = "clt_url_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("ventas/inmuebles_" . $content["inmueble_id"], $imageName, $this->driver);
            $ventas->clt_url = $this->bucketUrl . $url;

        }

        if ($request->hasFile("oferta_compra_url")) {
            $image = $request->file("oferta_compra_url");
            $imageName = "oferta_compra_url_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("ventas/inmuebles_" . $content["inmueble_id"], $imageName, $this->driver);
            $ventas->oferta_compra_url = $this->bucketUrl . $url;

        }

        if ($request->hasFile("escritura_publica_url")) {
            $image = $request->file("escritura_publica_url");
            $imageName = "escritura_publica_url_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("ventas/inmuebles_" . $content["inmueble_id"], $imageName, $this->driver);
            $ventas->escritura_publica_url = $this->bucketUrl . $url;

        }

        if ($request->hasFile("promesa_firmada_url")) {
            $image = $request->file("promesa_firmada_url");
            $imageName = "promesa_firmada_url_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("ventas/inmuebles_" . $content["inmueble_id"], $imageName, $this->driver);
            $ventas->promesa_firmada_url = $this->bucketUrl . $url;

        }

        if ($request->hasFile("clt_parqueadero_1_url")) {
            $image = $request->file("clt_parqueadero_1_url");
            $imageName = "clt_parqueadero_1_url_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("ventas/inmuebles_" . $content["inmueble_id"], $imageName, $this->driver);
            $ventas->clt_parqueadero_1_url = $this->bucketUrl . $url;

        }

        if ($request->hasFile("clt_parqueadero_2_url")) {
            $image = $request->file("clt_parqueadero_2_url");
            $imageName = "clt_parqueadero_2_url_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("ventas/inmuebles_" . $content["inmueble_id"], $imageName, $this->driver);
            $ventas->clt_parqueadero_2_url = $this->bucketUrl . $url;

        }

        if ($request->hasFile("camara_comercio_url")) {
            $image = $request->file("camara_comercio_url");
            $imageName = "camara_comercio_url_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("ventas/inmuebles_" . $content["inmueble_id"], $imageName, $this->driver);
            $ventas->camara_comercio_url = $this->bucketUrl . $url;

        }

        if ($request->hasFile("clt_deposito_1_url")) {
            $image = $request->file("clt_deposito_1_url");
            $imageName = "clt_deposito_1_url_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("ventas/inmuebles_" . $content["inmueble_id"], $imageName, $this->driver);
            $ventas->clt_deposito_1_url = $this->bucketUrl . $url;

        }

        if ($request->hasFile("clt_deposito_2_url")) {
            $image = $request->file("clt_deposito_2_url");
            $imageName = "clt_deposito_2_url_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("ventas/inmuebles_" . $content["inmueble_id"], $imageName, $this->driver);
            $ventas->clt_deposito_2_url = $this->bucketUrl . $url;

        }

        if ($request->hasFile("poder_copia_diligenciada_url")) {
            $image = $request->file("poder_copia_diligenciada_url");
            $imageName = "poder_copia_diligenciada_url_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
            $url = $image->storeAs("ventas/inmuebles_" . $content["inmueble_id"], $imageName, $this->driver);
            $ventas->poder_copia_diligenciada_url = $this->bucketUrl . $url;

        }

        $ventas->save();

        // Cambio de estado en Domus

        // Armando array para enviar el inmueble al API
        $api = new ApiController;
        $seguimiento = new SeguimientosVentasController;
        $detalle = $api->propertyDetail($usuario, $inm['codigoMLS'],null);

        // return $usuario;

        $property= [
            'codigoMLS'=>$inm['codigoMLS'],
            'status'=>6,
            'source'=>10845,
            'value'=>$content['negociacion_precio'],
            'real_state'=>52,
        ];

        if ($content["revision"] == 1) {
            $this->revisionGeneral($content, $usuario, $ventas);
            // return "Final";
        }

        if ($inm->estado_id != 9 && $content['revision'] == 1) {
            $inm->estado_id = 9;
            $inm->save();

            $requestSeguimiento = new \Illuminate\Http\Request();

            $requestSeguimiento->replace([
                "venta_id" => $ventas->id,
                "estado_venta_id" => 1,
                "usuario_encargado" => $usuario->id,
                "comentario" => "Creación promesa de compraventa",
                "commentario_email" => $content['comment_email']??'',
                "fecha_seguimiento" => date('Y-m-d H:i:s')
            ]);

            $seg = $seguimiento->crearSeguimiento($requestSeguimiento);
            $act = $api->actualizarEstadoInmueble($usuario, $property);
        }

        return redirect()
        ->route("ventas3");
    }

    public function ventas2(Request $request, $id) {
        $usuario = session("usuario");
        $content = $request->all();
        $venta = Venta1::with("usuarios")
        ->with('inmuebles.asesores')
        ->with(['inmuebles.clientes' => function ($owner) {
            $owner
            ->where('rol_id',3)
            ->orderBy('porcentaje','desc')
            ->with('clientes');
        }])
        ->find($id);

        // return $venta->inmuebles;

        $agentes = Usuario::get();

        GeneralController::renderHeader("Ventas", $usuario, "activeVentas", true, "expandedInmuebles");

        echo view("components.ventas.2")
        ->with("usuario", $usuario)
        ->with("venta", $venta)
        ->with("agentes", $agentes);

        GeneralController::renderFooter("ventas/2");
    }

    public function edit(Request $request, $id) {
        $usuario = session("usuario");
        $content = $request->all();

        $agentes = Usuario::get();
        $venta = Venta1::with("usuarios")
        ->find($id);

        $extensiones = Venta1Extended::where("venta_id", $id)->first();

        if ($extensiones != null) {
            foreach ($extensiones->toArray() as $key => $value) {
                if ($key != "id") {
                    $venta[$key] = $value;
                }
            }
        }

        $compradores = ClienteVenta::with("apoderados")
        ->where("venta1_id", $venta->id)
        ->where("tipo_cliente", "comprador")
        ->where("inmueble_id", $venta->inmueble_id)
        ->get();

        $tipos_documento = TipoDocumento::get();
        $estados_civiles = SubEstadoCivil::get();
        $roles = SubEstadoCivil::get();

        // Vamos a obtener el último seguimiento
        $ventaCorreo = VentaCorreo::where("venta_id", $id)
        ->orderBy("id", "desc")
        ->where("usuario_id", $usuario["id"])
        ->first();

        GeneralController::renderHeader("Ventas", $usuario, "activeVentas", true, "expandedInmuebles");

        echo view("components.ventas.edit")
        ->with("venta",$venta)
        ->with("usuario", $usuario)
        ->with("agentes", $agentes)
        ->with("vendedores", $this->updateClienteVentas($venta))
        ->with("compradores", $compradores)
        ->with("tipos_documento", $tipos_documento)
        ->with("ventaCorreo", $ventaCorreo)
        ->with("estados_civiles", $estados_civiles);

        GeneralController::renderFooter();
    }

    public function ventas3(Request $request) {
        $usuario = session("usuario");
        $content = $request->all();


        // Lista de ventas
        $ventas = Venta1::with("usuarios")
        ->with("aprobaciones")
        ->with(["inmuebles" => function ($query) {
            $query->with("estados", "tipos_inmueble","ciudades",
            "zonas",
            "localidades",
            "imagenes",
            "estratos",
            "destinos");
        }])
        ->where(function($query) use ($usuario,$content) {
            if (!isset($content["mls"]) || $content["mls"] == "") {
                if (isset($content['usuario']) && $content['usuario'] != '') {
                    $query->where("create_user_id", $content['usuario']);
                } else {
                    $query->where("create_user_id", $usuario['id']);
                }
            }

        })
        ->WhereHas('inmuebles', function ($inm) use ($content) {
            $inm->where(function ($query) use ($content) {
                if (isset($content['mls']) && $content['mls'] != '') {
                    $query->where('codigoMLS', 'like', '%' . $content['mls'] . '%');
                }
            });
        })
        ->orderBy("updated_at", "desc")
        ->paginate(5);

        $agentes = Usuario::get();

        GeneralController::renderHeader("Ventas", $usuario, "activeVentas", true, "expandedInmuebles");

        echo view("components.ventas.3")
        ->with("agentes", $agentes)
        ->with("usuario", $usuario)
        ->with("request", $request)
        ->with("ventas", $ventas);


        GeneralController::renderFooter();
    }

    public function ventas4(Request $request, $id) {
        $usuario = session("usuario");
        $content = $request->all();

        $venta = Venta1::with("inmuebles","usuarios")->find($id);
        GeneralController::renderHeader("Ventas", $usuario, "activeVentas", true);
        echo view("components.ventas.4")
        ->with("venta", $venta)
        ->with("usuario",$usuario);

        GeneralController::renderFooter();
    }

    public function ventaCruzada(Request $request) {
        $usuario = session("usuario");
        $content = $request->all();

        $agentes = Usuario::get();

        GeneralController::renderHeader("Ventas", $usuario, "activeVentas", true, "expandedInmuebles");

        echo view("components.ventas.cruzada")
        ->with("usuario", $usuario)
        ->with("agentes", $agentes);

        GeneralController::renderFooter();
    }

    public function revisarContrato(Request $request, $id, $tipo = "agente") {
        $partes = explode("_", base64_decode($id));

        $venta = Venta1::with("inmuebles")
        ->find($partes[0]);

        $extensiones = Venta1Extended::where("venta_id", $venta->id)->first();

        if ($extensiones != null) {
            foreach ($extensiones->toArray() as $key => $value) {
                if ($key != "id") {
                    $venta[$key] = $value;
                }
            }
        }

        $ventaCorreo = VentaCorreo::where("venta_id", $venta->id)
        ->where("usuario_id", $partes[1])
        ->where(function ($query) use ($tipo) {
            if ($tipo == "cliente") {
                $query->where("usuario_tipo", "Cliente");
            }
        })
        ->first();

        $aprobacionesRechazadas = VentaCorreo::where("venta_id", $venta->id)
        ->where("usuario_tipo", "Cliente")
        ->where("aprobado", 0)
        ->where("comentario", "!=", "")
        ->get();

        $generalController = new GeneralController();

        // Vendedores
        $vendedores = ClienteVenta::with("apoderados", "subestadociviles", "tipos_documento")
        ->where("venta1_id", $venta->id)
        ->where("tipo_cliente", "vendedor")
        ->where("inmueble_id", $venta->inmueble_id)
        ->get();

        // Compradores
        $compradores = ClienteVenta::with("apoderados.tipos_documento", "subestadociviles", "tipos_documento")
        ->where("venta1_id", $venta->id)
        ->where("tipo_cliente", "comprador")
        ->where("inmueble_id", $venta->inmueble_id)
        ->get();

        $formatterES = new \NumberFormatter("es", \NumberFormatter::SPELLOUT);
        $lastClausula = 4;
        $clausulas = explode(".ñ.", $venta->clausulas);

        $contrato = view("components.ventas.revisar-contrato-venta", [
            "venta" => $venta,
            "vendedores" => $vendedores,
            "compradores" => $compradores,
            "formater" => $formatterES,
            "clausulas" => $clausulas,
            "general" => $generalController,
            "ventaCorreo" => $ventaCorreo,
            "aprobacionesRechazadas" => $aprobacionesRechazadas
        ]);

        return $contrato;
    }

    public function revisionGeneral($content, $usuario, $ventas) {
        /**
        * Solo se van a crear dos registros, el registro del agente y el
        * registro del abogado y/o administrador, estos dos registros
        * se manejan hasta que ambos estén aprobados
        * El primer registro es el del agente y el segundo es el del abogado
        */
        $mail = new MailController();
        $destinatarios = [];

        // Inmueble
        $inmueble = Inmueble::find($ventas->inmueble_id);
        // Esto es por si se actualiza una ya existente por parte de un cliente
        if (isset($content["tipo"]) && $content["tipo"] == "Cliente") {
            $ventaCorreo = VentaCorreo::where("venta_id", $ventas->id)
            ->where("usuario_tipo", "Cliente")
            ->where("usuario_id", $usuario->id)
            ->first();

            if ($ventaCorreo != null) {
                $ventaCorreo->comentario = $content["comment_email"] ?? "";

                $agente = Usuario::where("id", $ventas->create_user_id)->first();
                // Se envia el correo al agente
                array_push($destinatarios, [
                    "name" => "Agente",
                    "email" => $agente->email
                    // "email" => "desarrolloweb@domus.la"
                ]);

                if (isset($content["aprobar"]) && $content["aprobar"] == 1) {
                    $ventaCorreo->aprobado = 1;
                    $ventaCorreo->save();

                    $mensaje = "<p>Buen día</p>";
                    $mensaje .= "<p>Elite system le informa que el cliente ha aprobado el contrato de venta para la venta: {$inmueble->codigoMLS} ";
                    $mensaje .= "con el comentario:</p>";
                    $mensaje .= "<p>" . $content["comment_email"] ?? "" . "</p>";

                    $asunto = "Ha sido aprobado el contrato de venta por parte de un cliente";
                }

                if (isset($content["rechazar"]) && $content["rechazar"] == 1) {
                    $ventaCorreo->aprobado = 0;
                    $ventaCorreo->comentario = $content["comment_email"] ?? "";
                    $ventaCorreo->save();

                    $ventaCorreo = VentaCorreo::where("venta_id", $ventas->id)
                    ->where("usuario_tipo", "Agente")
                    ->orderBy("id", "desc")
                    ->first();

                    $ventaCorreo->aprobado = 0;
                    $ventaCorreo->comentario = "El cliente ha enviado de nuevo a revisión el contrato y dejó el siguiente comentario: " . $content["comment_email"] ?? "";
                    $ventaCorreo->save();

                    $mensaje = "<p>Buen día</p>";
                    $mensaje .= "<p>Elite system le informa que se ha enviado de nuevo a revisión el contrato de venta para la venta: {$inmueble->codigoMLS} ";
                    $mensaje .= "con el comentario:</p>";
                    $mensaje .= "<p>" . $content["comment_email"] ?? "" . "</p>";
                    $mensaje .= "<p>Se recomienda revisar la información para proceder al cierre del negocio.</p>";

                    $asunto = "Se ha enviando de nuevo a revisión una venta por parte del cliente";
                }

                $mail->sendMail($asunto, $mensaje, $destinatarios);

                return $ventaCorreo;
            }
        }

        $ventaExists = VentaCorreo::where("venta_id", $ventas->id)
        ->orderBy("id", "desc")
        ->first();

        // Paso 1, creación de registros
        if ($ventaExists == null) {

            // Registro para el agente
            $ventaCorreo = new VentaCorreo();
            $ventaCorreo->usuario_tipo = "Agente";
            $ventaCorreo->usuario_nombre = $usuario["nombres"] . " " . $usuario["apellidos"];
            $ventaCorreo->usuario_id = $usuario["id"];
            $ventaCorreo->comentario = $content["comment_email"] ?? "";
            $ventaCorreo->venta_id = $ventas->id;
            $urlContrato = url("/") . "/alt/contrato-venta/" . base64_encode($ventas->id . "_" . $usuario["id"]);
            $ventaCorreo->url_aprobacion = $urlContrato;
            $ventaCorreo->save();

            // Registro para el abogado
            $ventaCorreo = new VentaCorreo();
            $ventaCorreo->usuario_tipo = "Juridico";
            $ventaCorreo->usuario_nombre = "Jorge Andrés Chois";
            $ventaCorreo->usuario_id = 47;
            $ventaCorreo->comentario = "";
            $ventaCorreo->venta_id = $ventas->id;
            $urlContrato = url("/") . "/alt/contrato-venta/" . base64_encode($ventas->id . "_" . 47);
            $ventaCorreo->url_aprobacion = $urlContrato;
            $ventaCorreo->save();

            // Url de la venta
            $urlEmail = url("/") . "/ventas/editar/{$ventas->id}";

            // Se envia el correo al abogado
            array_push($destinatarios, [
                "name" => "Abogado",
                "email" => "juridico1@eliteinmobiliaria.com.co"
                // "email" => "desarrolloweb@domus.la"
            ]);

            $mensaje = "<p>Buen día</p>";
            $mensaje .= "<p>Elite system le informa que se ha enviado a revisión la venta {$inmueble->codigoMLS}</p>";
            $mensaje .= "<p>En la siguiente url {$urlEmail} con el comentario:</p>";
            $mensaje .= "<p>" . $content["comment_email"] ?? "" . "</p>";
            $mensaje .= "<p>Se recomienda revisar la información para proceder al cierre del negocio.</p>";

            $asunto = "Se ha generado la revisión de una venta";

        } else {
            // Paso 2
            if ($usuario["roles"]["id"] == 6 || $usuario["roles"]["id"] == 1) {

                // Actualización por parte del abogado
                $ventaCorreo = VentaCorreo::where("venta_id", $ventas->id)
                ->where("usuario_tipo", "Juridico")
                ->orderBy("id", "desc")
                ->first();

                $ventaCorreo->usuario_nombre = $usuario["nombres"] . " " . $usuario["apellidos"];
                $ventaCorreo->usuario_id = $usuario["id"];
                $ventaCorreo->comentario = $content["comment_email"] ?? "";

                if (isset($content["aprobar"]) && $content["aprobar"] == 1) {
                    $ventaCorreo->aprobado = 1;
                }

                if (isset($content["rechazar"]) && $content["rechazar"] == 1) {
                    $ventaCorreo->aprobado = 0;
                }

                $ventaCorreo->save();

                $agente = Usuario::where("id", $ventas->create_user_id)->first();

                // Se envia el correo al agente
                array_push($destinatarios, [
                    "name" => "Agente",
                    "email" => $agente->email
                    // "email" => "desarrolloweb@domus.la"
                ]);

                // Obtenemos la venta del asesor para enviar la url de aprobación
                $ventaCorreo = VentaCorreo::where("venta_id", $ventas->id)
                ->where("usuario_tipo", "Agente")
                ->orderBy("id", "desc")
                ->first();

                $mensaje = "<p>Buen día</p>";
                $mensaje .= "<p>Elite system le informa que se ha revisado la venta {$inmueble->codigoMLS}</p>";
                $mensaje .= "<p>En la siguiente url {$ventaCorreo->url_aprobacion} con el comentario:</p>";
                $mensaje .= "<p>" . $content["comment_email"] ?? "" . "</p>";
                $mensaje .= "<p>Se recomienda revisar la información para proceder al cierre del negocio.</p>";

                $asunto = "Se ha generado la revisión de una venta";

            } else {
                $ventaCorreo = VentaCorreo::where("venta_id", $ventas->id)
                ->where("usuario_tipo", "Agente")
                ->orderBy("id", "desc")
                ->first();

                $ventaCorreo->comentario = $content["comment_email"] ?? "";

                if (isset($content["aprobar"]) && $content["aprobar"] == 1) {
                    $ventaCorreo->aprobado = 1;
                }

                if (isset($content["rechazar"]) && $content["rechazar"] == 1) {
                    $ventaCorreo->aprobado = 0;
                }

                $ventaCorreo->save();

                $urlEmail = url("/") . "/ventas/editar/{$ventas->id}";

                // Se envia el correo al abogado
                array_push($destinatarios, [
                    "name" => "Abogado",
                    "email" => "juridico1@eliteinmobiliaria.com.co"
                    // "email" => "desarrolloweb@domus.la"
                ]);

                $mensaje = "<p>Buen día</p>";
                $mensaje .= "<p>Elite system le informa que se ha enviado a revisión la venta {$inmueble->codigoMLS}</p>";
                $mensaje .= "<p>En la siguiente url {$urlEmail} con el comentario:</p>";
                $mensaje .= "<p>" . $content["comment_email"] ?? "" . "</p>";
                $mensaje .= "<p>Se recomienda revisar la información para proceder al cierre del negocio.</p>";

                $asunto = "Se ha generado la revisión de una venta";
            }
        }

        $mail->sendMail($asunto, $mensaje, $destinatarios);

        // Famos con la fase 3
        $ventaAprobadaExists = VentaCorreo::where("venta_id", $ventas->id)
        ->orderBy("id", "desc")
        ->where("aprobado", 1)
        ->count();

        $destinatarios = [];

        if ($ventaAprobadaExists == 2) {
            // Si tanto el agente como el abogado aprobaron entonces se envía esta wea a todo el mundo
            $clientes = ClienteVenta::where("venta1_id", $ventas->id)
            ->where("inmueble_id", $ventas->inmueble_id)
            ->get();

            foreach ($clientes as $cliente) {
                if ($cliente->email != null && $cliente->email != "") {
                    $destinatarios = [
                        [
                            "name" => $cliente->nombre,
                            "email" => $cliente->email
                            // "email" => "desarrolloweb@domus.la"
                        ]
                    ];
                }

                $urlContrato = url("/") . "/alt/contrato-venta/" . base64_encode($ventas->id . "_" . $cliente->id) . "/cliente";

                $ventaCorreo = new VentaCorreo();
                $ventaCorreo->usuario_tipo = "Cliente";
                $ventaCorreo->usuario_nombre = $cliente->nombre;
                $ventaCorreo->usuario_correo = $cliente->email ?? "";
                $ventaCorreo->usuario_id = $cliente->id;
                $ventaCorreo->comentario = "";
                $ventaCorreo->venta_id = $ventas->id;
                $ventaCorreo->url_aprobacion = $urlContrato;
                $ventaCorreo->save();

                $mensaje = "<p>Buen día</p>";
                $mensaje .= "<p>A continuación encontrará una versión web del contrato de compraventa, favor revisar el mismo con el propósito de evitar errores menores ";
                $mensaje .= "(números de documento o nombres erróneos). Al final del contrato podrá encontrar una sección donde es posible aprobar o rechazar la información.</p>";
                $mensaje .= "<p>{$urlContrato}</p>";

                $asunto = "Solicitud de revisión de contrato de compraventa";

                if (count($destinatarios) > 0) {
                    $mail->sendMail($asunto, $mensaje, $destinatarios);
                }
            }
        }

        $aprobacionesRechazadas = VentaCorreo::where("venta_id", $ventas->id)
        ->where("usuario_tipo", "Cliente")
        ->where("aprobado", 0)
        ->where("comentario", "!=", "")
        ->get();

        $destinatarios = [];

        if (count($aprobacionesRechazadas) > 0) {
            foreach ($aprobacionesRechazadas as $aprobacion) {
                $clientes = ClienteVenta::where("venta1_id", $ventas->id)
                ->where("id", $aprobacion->usuario_id)
                ->get();

                foreach ($clientes as $cliente) {
                    if ($cliente->email != null && $cliente->email != "") {
                        $destinatarios = [
                            [
                                "name" => $cliente->nombre,
                                "email" => $cliente->email
                                // "email" => "desarrolloweb@domus.la"
                            ]
                        ];
                    }

                    $urlContrato = url("/") . "/alt/contrato-venta/" . base64_encode($ventas->id . "_" . $cliente->id) . "/cliente";

                    $mensaje = "<p>Buen día</p>";
                    $mensaje .= "<p>El contrato de compraventa ha sido aprobado por el equipo de Elite Inmobiliaria y se ha enviado nuevamente a revisión, ";
                    $mensaje .= "puede verlo y aprobarlo en el siguiente enlace:</p>";
                    $mensaje .= "<p>{$urlContrato}</p>";

                    $asunto = "Solicitud de revisión de contrato de compraventa";

                    if (count($destinatarios) > 0) {
                        $mail->sendMail($asunto, $mensaje, $destinatarios);
                    }
                }
            }
        }
    }

    public function revisarContratoPost(Request $request) {
        $content = $request->all();

        if ($content["tipo"] == "Cliente") {
            $usuario = ClienteVenta::find($content["agente"]);
        } else {
            $usuario = Usuario::find($content["agente"]);
        }

        $venta = Venta1::find($content["venta"]);
        $content["comment_email"] = $content["comment"];

        $this->revisionGeneral($content, $usuario, $venta);

        return redirect()->route("contratoGracias");
    }

    public function contratoVenta(Request $request, $id) {
        // Venta
        $venta = Venta1::with("inmuebles")
        ->find($id);

        $generalController = new GeneralController();

        $venta->cantidad_veces_que_se_genera_el_contrato = $venta->cantidad_veces_que_se_genera_el_contrato + 1;

        $venta->save();

        if ($venta->cantidad_veces_que_se_genera_el_contrato == 1) {
            $mail = new MailController();

            $mensaje = "<p>Buen día</p>";
            $mensaje .= "<p>Elite system le informa que ha sido generado por primera vez el pdf para la venta código {$venta->id} correspondiente al inmueble código mls {$venta->inmuebles->codigoMLS}.</p>";
            $mensaje .= "<p>Se recomienda revisar la información para proceder al cierre del negocio.</p>";

            $destinatarios = [];
            array_push($destinatarios, [
                "name" => "Elite System",
                "email" => "jwoodcock@eliteinmobiliaria.com.co"
            ]);

            array_push($destinatarios, [
                "name" => "Abogado",
                "email" => "juridico1@eliteinmobiliaria.com.co"
            ]);

            $mail->sendMail("Se ha generado el contrato de venta para el inmueble {$venta->inmuebles->codigoMLS}", $mensaje, $destinatarios);
        }

        $extensiones = Venta1Extended::where("venta_id", $venta->id)->first();

        if ($extensiones != null) {
            foreach ($extensiones->toArray() as $key => $value) {
                if ($key != "id") {
                    $venta[$key] = $value;
                }
            }
        }

        // Vendedores
        $vendedores = ClienteVenta::with("apoderados", "subestadociviles", "tipos_documento")
        ->where("venta1_id", $venta->id)
        ->where("tipo_cliente", "vendedor")
        ->where("inmueble_id", $venta->inmueble_id)
        ->get();

        if (count($vendedores) == 0) {
            $vendedores = Cliente::with("subestadociviles")
            ->whereHas("inmuebles", function ($query) use ($venta) {
                $query->where("inmueble_id", $venta->inmueble_id)
                ->where("rol_id", 3);
            })
            ->get();

            foreach ($vendedores as $propietario) {
                $vendedor = new ClienteVenta();
                $vendedor->inmueble_id = $venta->inmueble_id;
                $vendedor->tipo_cliente = "vendedor";
                $vendedor->nombre = "{$propietario->nombres} {$propietario->apellidos}";
                $vendedor->ciudad = $propietario->ciudad;
                $vendedor->numero_documento = $propietario->documento;
                $vendedor->fecha_expedicion_documento = $propietario->expedicion;
                $vendedor->fecha_nacimiento = $propietario->fecha_nacimiento;
                $vendedor->estado_civil = $propietario->estado_civil;
                $vendedor->telefono = $propietario->telefono;
                $vendedor->celular = $propietario->celular;
                $vendedor->direccion = $propietario->direccion;
                $vendedor->profesion = $propietario->profesion;
                $vendedor->tipo_documento_id = $propietario->tipo_documento_id;
                $vendedor->subestado_civil_id = $propietario->subestado_civil_id != 0 ? $propietario->subestado_civil_id : null;
                $vendedor->venta1_id = $venta->id;

                $vendedor->save();
            }

            $vendedores = ClienteVenta::with("apoderados.tipos_documento", "subestadociviles", "tipos_documento")
            ->where("venta1_id", $venta->id)
            ->where("tipo_cliente", "vendedor")
            ->where("inmueble_id", $venta->inmueble_id)
            ->get();
        }

        // Compradores
        $compradores = ClienteVenta::with("apoderados.tipos_documento", "subestadociviles", "tipos_documento")
        ->where("venta1_id", $venta->id)
        ->where("tipo_cliente", "comprador")
        ->where("inmueble_id", $venta->inmueble_id)
        ->get();

        $formatterES = new \NumberFormatter("es", \NumberFormatter::SPELLOUT);
        $lastClausula = 4;
        $clausulas = explode(".ñ.", $venta->clausulas);

        $contrato = view("components.ventas.contrato-venta", [
            "venta" => $venta,
            "vendedores" => $vendedores,
            "compradores" => $compradores,
            "formater" => $formatterES,
            "clausulas" => $clausulas,
            "general" => $generalController
        ]);

        // return $contrato;

        $options = new Options();
        $options->setIsHtml5ParserEnabled(true);
        $options->setChroot(__DIR__);
        $options->set("isRemoteEnabled", true);

        $dompdf = new Dompdf();
        $dompdf->setOptions($options);
        $dompdf->setPaper("a4", "portrait");
        $dompdf->loadHtml($contrato);
        $dompdf->render();
        $dompdf->stream("contrato.pdf", [
            "Attachment" => false
        ]);
    }
}
