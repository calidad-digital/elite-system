<?php

namespace App\Http\Controllers;

//Modelos
use App\Models\Caracteristica;

//Paquetes
use Illuminate\Http\Request;

class CaracteristicaController extends Controller
{
    public function altGeneralList($tipo = "", $caracteristica = "") {
        $caracteristicas = Caracteristica::where(function ($query) use ($tipo) {
            if ($tipo != "") {
                $query->where("tipo_inmueble_id", $tipo);
            }
        })
        ->where(function ($query) use ($caracteristica) {
            if ($caracteristica != "") {
                $query->where("tipo_caracteristica_id", $caracteristica);
            }
        })
        ->get();
        return $caracteristicas;
    }
}
