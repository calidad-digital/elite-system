<?php

namespace App\Http\Controllers;

//Modelos
use App\Models\Barrio;

//Paquetes
use Illuminate\Http\Request;

class BarrioController extends Controller
{
    public function generalList($localidad) {
        $barrio = Barrio::where(function ($query) use ($localidad) {
            if ($localidad != "") {
                $query->where("localidad_id", $localidad);
            }
        })
        ->get();
        return $barrio;
    }
}
