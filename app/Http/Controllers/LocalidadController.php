<?php

namespace App\Http\Controllers;

//Modelos
use App\Models\Localidad;

//Paquetes
use Illuminate\Http\Request;

class LocalidadController extends Controller
{
    public function generalList($ciudad = "", $zona = "") {
        $localidad = Localidad::where(function ($query) use ($ciudad) {
            if ($ciudad != "") {
                $query->where("ciudad_id", $ciudad);
            }
        })
        ->where(function ($query) use ($zona) {
            if ($zona != "" && $zona != "0") {
                $query->where("zona_id", $zona);
            }
        })
        ->get();
        return $localidad;
    }
}
