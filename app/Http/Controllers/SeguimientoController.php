<?php

namespace App\Http\Controllers;

//Modelos
use App\Models\Inmueble;
use App\Models\EstadoInmueble;
use App\Models\SubEstadoInmueble;
use App\Models\Seguimiento;
use App\Models\AlertaActividad;
use App\Models\Usuario;

//Paquetes
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class SeguimientoController extends Controller
{

    public function store(Request $request) {
        $content = $request->all();

        $messages = [
            "property.unique" => "Debe enviar un inmueble para realizar un seguimiento",
            "status.required" => "El estado es obligatorio",
            "property.exists" => "El inmueble no existe",
            "status.exists" => "El estado de seguimiento no existe",
            "substatus.exists" => "El subestado de seguimiento no existe"
        ];

        $validator = Validator::make($content, [
            "property" => "required|exists:inmuebles,id",
            "status" => "required|exists:estado_inmuebles,id",
            "substatus" => "required|exists:sub_estado_inmuebles,id"
        ], $messages);

        if ($validator->fails()) {
            return $validator->errors();
        }

        // return $content;

        $seguimiento = new Seguimiento();
        $seguimiento->inmueble_id = $content["property"];
        $seguimiento->estado_inmueble_id = $content["status"];
        $seguimiento->sub_estado_inmueble_id = $content["substatus"];
        $seguimiento->is_active = false;
        $seguimiento->comentario = $content["description"] ?? "";
        $seguimiento->fecha_registro = $content["date"] ?? date('Y-m-d H:i:s');
        $seguimiento->create_user_id = $request["user"]["id"];
        $seguimiento->update_user_id = $request["user"]["id"];
        $seguimiento->save();

        // Creamos alerta si es necesario
        if (isset($content["has_alert"]) && $content["has_alert"] == "on") {
            $alerta = new AlertaActividad();

            $alerta->tipo_actividad = "Seguimiento inmueble";
            $alerta->actividad_id = $seguimiento->id;
            $alerta->estado_actividad_id = $content["status"];
            $alerta->comentario = $content["description_alert"];
            $alerta->fecha_asignada = $content["date_alert"].' '.$content["hour_alert"];
            $alerta->fecha_vencimiento = $content["date_alert"].' '.$content["hour_alert"];
            $alerta->fue_notificado_email = 0;
            $alerta->fue_notificado_sms = 0;
            $alerta->usuario_creador_id = $request["user"]["id"];
            $alerta->usuario_asignado_id = $request["user"]["id"];

            $alerta->save();
        }


        //Actualizamos el inmueble también
        $inmueble = Inmueble::with("asesores")->find($content["property"]);

        $preState = $inmueble->estado_id;

        $inmueble->estado_id = $content["status"];
        $inmueble->sub_estado_id = $content["substatus"];
        $inmueble->save();

        if ($content["status"] > 4) {
            switch ($content["status"]) {
                case 5:
                $nuevoEstado = 1;
                break;
                case 6:
                $nuevoEstado = 2;
                break;
                case 7:
                $nuevoEstado = 3;
                break;
                case 8:
                $nuevoEstado = 4;
                break;
                case 9:
                $nuevoEstado = 6;
                break;
                default:
                $nuevoEstado = 4;
                break;
            }

            $api = new ApiController();
            $usuario = Usuario::find($inmueble->asesores->id);
            $property = [
                "codigoMLS" => $inmueble["codigoMLS"],
                "status" => $nuevoEstado,
                "source" => 10845,
                "value" => $inmueble->canon,
                "real_state" => 52,
            ];
            $act = $api->actualizarEstadoInmueble($usuario, $property);
        }

        if ($seguimiento->estado_inmueble_id == 2 && $inmueble->publicado != 1) {

            if ($inmueble->codigoMLS == "" || $inmueble->codigoMLS == null) {
                $inmueble->codigoMLS = ($inmueble->asesores->codigo_interno_mls + 1);
                $inmueble->save();

                $usuario = Usuario::find($inmueble->asesores->id);
                $usuario->codigo_interno_mls = $inmueble->codigoMLS;
                $usuario->save();
            }

            // $api = new ApiController();
            // $publicado = $api->createProperty($seguimiento->inmueble_id);
        }

        $estado = EstadoInmueble::find($content["status"]);
        $subestado = SubEstadoInmueble::find($content["substatus"]);

        $fecha = date("m-d-Y");

        if($inmueble->estado_id == 2 && $content["status"] == 5){
            $content["description"] .= "<br><br> Recuerde que tiene 2 días para revisar el inmueble Disñponible, ya que enviaremos la información a los propietarios para su verificación.";
        }

        $message = file_get_contents(url("assets/mail-templates/seguimiento_send.html"));
        $message = str_replace("%inmueble%", $inmueble->codigo, $message);
        $message = str_replace("%direccion%", $inmueble->direccion, $message);
        $message = str_replace("%fecha%", $fecha, $message);
        $message = str_replace("%name%", $request["user"]["nombres"] . $request["user"]["apellidos"], $message);
        $message = str_replace("%email%", $request["user"]["email"], $message);
        $message = str_replace("%agent%", $inmueble->asesores->nombres??''.' '.$inmueble->asesores->apellidos??'', $message);
        $message = str_replace("%estado%", $estado->nombre, $message);
        $message = str_replace("%subestado%", $subestado->nombre, $message);
        $message = str_replace("%code%",$inmueble->codigoMLS??'', $message);
        $message = str_replace("%exclusivo%",$inmueble->exclusivo == 0 ? 'No' : 'Si', $message);
        $message = str_replace("%comentario%", $content["description"]??'', $message);

        //Correos

        $destinatarios = [];

        array_push($destinatarios, [
            "name" => "{$inmueble->asesores->nombres} {$inmueble->asesores->apellidos}",
            "email" => $inmueble->asesores->email
        ]);

        if ($subestado->id_notificacion_asesores != "" && $subestado->id_notificacion_asesores != 0) {
            foreach (explode(",", $subestado->id_notificacion_asesores) as $user_for_email) {
                $usuario = Usuario::where("codigo_interno", $user_for_email)
                ->first();

                array_push($destinatarios, [
                    "name" => "{$usuario->nombres} {$usuario->apellidos}",
                    "email" => $usuario->email
                ]);
            }
        } else {
            // array_push($destinatarios, [
            //     "name" => "Elite System",
            //     "email" => "desarrolloweb@domus.la"
            // ]);
        }

        if($inmueble->estado_id != 5 && $content["status"] != 5){
            $mail = new MailController();
            $mail->sendMail("Actualización inmueble - ".$estado->nombre, $message, $destinatarios);
        }

        return $seguimiento;
    }

    public function seguimientoInmuebleBack(Request $request) {
        $usuario = session("usuario");
        $content = $request->all();


        $cliente = GeneralController::postRequest(url("api/seguimientos"), [
            "Authorization" => $usuario->token
        ], $content);

        $inmueble = Inmueble::find($content["property"]);

        if (!isset($cliente["id"])) {
            return redirect()
            ->route("seguimientoInmueble", ["code" => $inmueble->codigo])
            ->with("error", $cliente);
        } else {
            $request->session()->forget("error");

            return redirect()
            ->route("seguimientoInmueble", ["code" => $inmueble->codigo])
            ->with("mensaje", "Seguimiento creado exitosamente");
        }
    }

    public function seguimientoInmueble(Request $request) {
        $usuario = session("usuario");
        $content = $request->all();



        $seguimientos = [];
        $inmueble = [];

        if (isset($request["code"]) && $request["code"] != "") {
            $inmueble = Inmueble::where("codigo", $request["code"])->first();

            if ($inmueble != null) {
                $seguimientos = Seguimiento::with("estados", "agentes", "subestados","alertas")
                ->where("inmueble_id", $inmueble->id)
                ->where(function($query) use ($usuario) {
                    if ($usuario['rol_id'] != 1) {
                        $query->where("create_user_id", $usuario['id']);
                    }
                })
                ->orderBy("id","desc")
                ->get();
            }
        }

        // return $seguimientos;

        $estados = EstadoInmueble::get();
        $agentes = Usuario::get();

        GeneralController::renderHeader("Inmuebles", $usuario, "activeInmueblesSeguimiento", true, "expandedInmuebles");

        echo view("components.inmuebles.seguimiento.index")
        ->with("request", $content)
        ->with("seguimientos", $seguimientos)
        ->with("inmueble", $inmueble)
        ->with("estados", $estados)
        ->with("usuario", $usuario)
        ->with("agentes", $agentes);

        GeneralController::renderFooter();
    }
}
