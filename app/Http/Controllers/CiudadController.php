<?php

namespace App\Http\Controllers;

//Modelos
use App\Models\Ciudad;

//Paquetes
use Illuminate\Http\Request;

class CiudadController extends Controller
{
    public function generalList($departamento = "") {
        $ciudad = Ciudad::where(function ($query) use ($departamento) {
            if ($departamento != "") {
                $query->where("estado_ubicacions_id", $departamento);
            }
        })
        ->get();
        return $ciudad;
    }
}
