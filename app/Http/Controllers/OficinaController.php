<?php

namespace App\Http\Controllers;

//Modelos
use App\Models\Oficina;

//Paquetes
use Illuminate\Http\Request;

class OficinaController extends Controller
{
    public function index(Request $request) {
        $oficinas = Oficina::paginate(12);
        return $oficinas;
    }

    public function show(Request $request, $id) {
        $oficina = Oficina::find($id);

        if ($oficina != null) {
            return $oficina;
        } else {
            return response()->json([
                "error" => "404",
                "mensaje" => "La oficina no existe",
            ], 404);
        }
    }

    public function store(Request $request) {
        $content = $request->all();

        $oficina = new Oficina();
        $oficina->nombre = $content["name"] ?? "";
        $oficina->email = $content["email"] ?? "";
        $oficina->telefono = $content["phone"] ?? "";
        $oficina->celular = $content["cellphone"] ?? "";
        $oficina->whatsapp = $content["whatsapp"] ?? "";
        $oficina->empresa_id = $request["user"]["empresa_id"];
        $oficina->ciudad_id = $content["city"] ?? $request["user"]["ciudad_id"];

        $oficina->save();

        return $oficina;
    }

    public function update(Request $request, $id) {
        $content = $request->all();

        $oficina = Oficina::find($id);

        if ($oficina != null) {
            $oficina->nombre = $content["name"] ?? "";
            $oficina->email = $content["email"] ?? "";
            $oficina->telefono = $content["phone"] ?? "";
            $oficina->celular = $content["cellphone"] ?? "";
            $oficina->whatsapp = $content["whatsapp"] ?? "";
            $oficina->empresa_id = $request["user"]["empresa_id"];
            $oficina->ciudad_id = $content["city"] ?? $request["user"]["ciudad_id"];

            $oficina->save();

            return $oficina;
        } else {
            return response()->json([
                "error" => "404",
                "mensaje" => "La oficina no existe",
            ], 404);
        }
    }

    public function destroy($id) {
        $oficina = Oficina::find($id);

        if ($oficina != null) {
            $oficina->delete();

            return response()->json([
                "error" => "200",
                "mensaje" => "Oficina eliminada con éxito",
            ], 200);
        } else {
            return response()->json([
                "error" => "404",
                "mensaje" => "La oficina no existe",
            ], 404);
        }
    }
}
