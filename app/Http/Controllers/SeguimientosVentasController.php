<?php

namespace App\Http\Controllers;

// Modelos
use App\Models\EstadoSeguimientosVenta;
use App\Models\SeguimientoVenta;
use App\Models\Venta1;
use App\Models\Inmueble;
use App\Models\Usuario;

use Illuminate\Http\Request;
use Carbon\Carbon;

class SeguimientosVentasController extends Controller
{
    public function listaEstados (){
        $estados = EstadoSeguimientosVenta::get();

        return $estados;
    }

    public function lista ($id){
        $seguimientos = SeguimientoVenta::with('estados',
        'ventas',
        'encargado',
        'creador')->where('venta_id',$id)->get();

        return $seguimientos;
    }

    public function crearSeguimiento(Request $request){
        $data = $request->all();
        $usuario = session("usuario");

        //validación campos obligatorios

        if(!isset($data['venta_id']) || !isset($data['estado_venta_id']) || !isset($data['usuario_encargado']) ){
            $respuesta = [
                "status" => 0,
                "message" => "Error, favor revise el formulario"
            ];

            return $respuesta;
        }

        $encargado = Usuario::find($data['usuario_encargado']);

        //Creando registro de nuevo seguimiento

        $seguimiento = new SeguimientoVenta;

        $seguimiento->venta_id = $data['venta_id'];
        $seguimiento->estado_venta_id = $data['estado_venta_id'];
        $seguimiento->comentario = $data['comentario']??'';
        $seguimiento->documento_soporte_url = '';
        $seguimiento->fecha_seguimiento = $data['fecha_seguimiento']??date('Y-m-d H:i:s');
        $seguimiento->usuario_encargado = $data['usuario_encargado'];
        $seguimiento->create_user_id = $usuario->id;
        $seguimiento->update_user_id = $usuario->id;
        $seguimiento->created_at = date('Y-m-d H:i:s');
        $seguimiento->updated_at = date('Y-m-d H:i:s');

        $seguimiento->save();


        //Validación documento de soporte

        if ($request->hasFile("soporte")) {
            $documento = $request->file("soporte");
            $documentName = "Venta" . $data['venta_id'] . "_seguimiento_" .$seguimiento->id.".". $documento->extension();

            $driver = "public";
            $url = $documento->storeAs("documentos/ventas/seguimientos/" . $seguimiento->id, $documentName, $driver);

            $seguimiento->documento_soporte_url = url("storage/" . $url);
            $seguimiento->save();
        }

        //enviando correo(s) notificando del seguimiento

        $estado = EstadoSeguimientosVenta::find($data['estado_venta_id']);
        $idInm = Venta1::select("inmueble_id")->find($data['venta_id']);
        $codigoInm = Inmueble::select("codigoMLS")->find($idInm);



        $correos= explode(',',$estado->enviar_a);
        $destinatarios = [];

        if($data['estado_venta_id'] == 3){

            array_push($destinatarios, [
                "name" => "{$encargado->nombres} {$encargado->apellidos}",
                "email" => $encargado->email
            ]);
        }


        foreach($correos as $user_for_email){

            $user = Usuario::where("id", $user_for_email)
                ->first();


            array_push($destinatarios, [
                "name" => "{$user->nombres} {$user->apellidos}",
                "email" => $user->email
            ]);
        }

        $bienvenida = $data['estado_venta_id'] == 1 ? "Se ha creado": "Se ha actualizado";
        $asunto = $data['estado_venta_id'] == 1 ? "Creación" : "Seguimiento";
        $comentario = $data['commentario_email']??'';
        $message= $bienvenida." una promesa de compraventa del agente ". "{$encargado->nombres} {$encargado->apellidos}". " ,codigo MLS ".
        $codigoInm[0]->codigoMLS.", para más información consulta: " . url("/") . "/admin/ventas/editar/".$data['venta_id'] ." <br> <br> ". $comentario;

        $mail = new MailController();
        $mail->sendMail($asunto. " promesa de compraventa ".date('d/m/Y'), $message, $destinatarios);

        return redirect()
        ->route("ventas4", ["id" => $data['venta_id']]);

    }
}
