<?php

namespace App\Http\Controllers;

//Modelos
use App\Models\Inmueble;
use App\Models\TipoInmueble;
use App\Models\Gestion;
use App\Models\EstadoUbicacion;
use App\Models\Destino;
use App\Models\Ciudad;
use App\Models\Zona;
use App\Models\Estrato;
use App\Models\Dir1;
use App\Models\Dir2;
use App\Models\Dir3;
use App\Models\Dir4;
use App\Models\Oficina;
use App\Models\InmuebleCaracteristica;
use App\Models\InmuebleDireccion;
use App\Models\InmuebleImagen;
use App\Models\Cliente;
use App\Models\InmuebleCliente;
use App\Models\DocumentoInmueble;
use App\Models\TipoDocumento;
use App\Models\TipoDocumentoInmueble;
use App\Models\EstadoInmueble;
use App\Models\SubEstadoInmueble;
use App\Models\SubEstadoCivil;
use App\Models\Usuario;
use Storage;
use File;
//Paquetes
use Illuminate\Http\Request;
use Carbon\Carbon;

class InmuebleController extends Controller
{
    public function __construct() {
        $this->driver = config("app.files_driver");
    }

    public function generalList($content, $usuario = null) {
        // $priceMin  = '';
        // $priceMax  = '';
        // if(isset( $content["pricemin"]) && $content["pricemin"] != ''){
        //   $priceMin =  str_replace('%2C','',$content["pricemin"]);
        // }
        // if(isset( $content["pricemax"]) && $content["pricemax"] != ''){
        //     $priceMax =  str_replace('%2C','',$content["pricemax"]);
        // }
        $usuario = session("usuario");
        $inmuebles = Inmueble::with([
            "tipos_inmueble",
            "gestiones",
            "imagenes",
            "ciudades",
            "zonas",
            "localidades",
            "estratos",
            "destinos",
            "direcciones",
            "caracteristicas.caracteristicas",
            "asesores",
            "estados",
            "portales",
            "documentos.tipos"
        ])
        ->with(["clientes.clientes" => function ($query) {
            $query->with([
                "tipos_documento"
            ]);
        }])
        ->where(function ($query) use ($content, $usuario) {
            if (isset($content["departamento"]) && $content["departamento"] != "") {
                $query->where("estado_ubicacion_id", $content["departamento"]);
            }

            if (isset($content["ciudad"]) && $content["ciudad"] != "") {
                $query->where("ciudad_id", $content["ciudad"]);
            }

            if (isset($content["gestion"]) && $content["gestion"] != "") {
                $query->where("gestion_id", $content["gestion"]);

                // if ($priceMin != '') {
                //     if ($content["gestion"] == "1") {
                //         $query->where("canon", ">=",  0 );
                //     } else {
                //         $query->where("venta", ">=",  0 );
                //     }
                // }

                // if ($priceMax!= "") {
                //     if ($content["gestion"] == "1") {
                //         $query->where("canon", "<=",  1000000);
                //     } else {
                //         $query->where("venta", "<=",  1000000);
                //     }
                // }
            }

            if (isset($content["tipo"]) && $content["tipo"] != "") {
                $query->where("tipo_inmueble_id", $content["tipo"]);
            }

            if (isset($content["codigo"]) && $content["codigo"] != "") {
                $query->where("codigo", $content["codigo"]);
            }

            if (isset($content["mls"]) && $content["mls"] != "") {
                $query->where("codigoMLS", $content["mls"]);
            }

            if (isset($content["barrio"]) && $content["barrio"] != "") {
                $query->where("barrio_comun", "like", "%{$content["barrio"]}%");
            }

            if (isset($content["estado"]) && $content["estado"] != "0") {
                $query->where("estado_id", $content["estado"]);
            }

            if (isset($content["subestado"]) && $content["subestado"] != "") {
                $query->where("sub_estado_id", $content["subestado"]);
            }

            if (isset($content["agente"])) {
                if( $content["agente"] != "0"){
                    $query->where("asesor_id", $content["agente"]);
                }
            }else{
                $query->where("asesor_id", $usuario["id"]);
            }
        })
        ->orderBy($content["order"], $content["sort"])
        ->paginate(12);



        return $inmuebles;
    }

    public function crearInmuebleBack(Request $request) {
        $usuario = session("usuario");
        $content = $request->all();

        // return $content;

        //Vamos a crear el inmueble en la base de datos
        $inmueble = new Inmueble();
        $inmueble->create_user_id = $usuario->id;
        $inmueble->update_user_id = $usuario->id;
        $inmueble->empresa_id = $usuario->empresa_id;
        $inmueble->estado_id = 1;
        $inmueble->sub_estado_id = 3;
        $inmueble->estado_id = 4;

        if (isset($content["referencia"]) && $content["referencia"] != "") {
            //Buscamos si ya existe un inmueble con esta referencia
            $property = Inmueble::where("referencia", $content["referencia"])
            ->first();

            if ($property != null) {
                return response()->json([
                    "error" => "401",
                    "mensaje" => "El inmueble ya existe",
                ], 401);
            }

            $inmueble->referencia = $content["referencia"];
        }

        if (isset($content["tipo"]) && $content["tipo"] != "") {
            $inmueble->tipo_inmueble_id = $content["tipo"];
        }

        if (isset($content["gestion"]) && $content["gestion"] != "") {
            $inmueble->gestion_id = $content["gestion"];
        }
        if (isset($content["corretaje"]) && $content["corretaje"] != "") {
            $inmueble->corretaje = $content["corretaje"];
        }

        if (isset($content["destino"]) && $content["destino"] != "") {
            $inmueble->destino_id = $content["destino"];
        }

        if (isset($content["canon"]) && $content["canon"] != "0") {
            $inmueble->canon = str_replace(",", "", $content["canon"]);
        } else {
            $inmueble->canon = 0;
        }

        if (isset($content["administracion"]) && $content["administracion"] != "0") {
            $inmueble->administracion = str_replace(",", "", $content["administracion"]);
        } else {
            $inmueble->administracion = 0;
        }

        if (isset($content["venta"]) && $content["venta"] != "0") {
            $inmueble->venta = str_replace(",", "", $content["venta"]);
        } else {
            $inmueble->venta = 0;
        }

        if (isset($content["fecha_consignacion"]) && $content["fecha_consignacion"] != "0") {
            $inmueble->fecha_consignacion = $content["fecha_consignacion"];
        }

        if (isset($content["departamento"]) && $content["departamento"] != "") {
            $inmueble->estado_ubicacion_id = $content["departamento"];
        }

        if (isset($content["ciudad"]) && $content["ciudad"] != "") {
            $inmueble->ciudad_id = explode(":", $content["ciudad"])[1];
        }

        if (isset($content["zona"]) && $content["zona"] != "") {
            $inmueble->zona_id = $content["zona"];
        }

        if (isset($content["localidad"]) && $content["localidad"] != "") {
            $inmueble->localidad_id = explode(":", $content["localidad"])[1];
        }

        if (isset($content["estrato"]) && $content["estrato"] != "") {
            $inmueble->estrato_id = $content["estrato"];
        }

        if (isset($content["barrio_id"]) && $content["barrio_id"] != "") {
            $inmueble->barrio_id = explode(":", $content["barrio_id"])[1];
        }

        if (isset($content["barrio_comun"]) && $content["barrio_comun"] != "") {
            $inmueble->barrio_comun = $content["barrio_comun"];
        }

        if (isset($content["direccion"]) && $content["direccion"] != "") {
            $inmueble->direccion = $content["direccion"];
        }

        if (isset($content["isCompleteAddress"]) && $content["isCompleteAddress"] == "on") {
            if (isset($content["complete_address"]) && $content["complete_address"] != "") {
                $inmueble->direccion = str_replace("null", "", str_replace("  ", "", $content["complete_address"]));
            }
        }

        if (isset($content["habitaciones"]) && $content["habitaciones"] != "") {
            $inmueble->habitaciones = $content["habitaciones"];
        } else {
            $inmueble->habitaciones = 0;
        }

        if (isset($content["banios"]) && $content["banios"] != "") {
            $inmueble->banios = $content["banios"];
        } else {
            $inmueble->banios = 0;
        }

        if (isset($content["parqueaderos"]) && $content["parqueaderos"] != "") {
            $inmueble->parqueaderos = $content["parqueaderos"];
        } else {
            $inmueble->parqueaderos = 0;
        }

        if (isset($content["parqueaderos_cubiertos"]) && $content["parqueaderos_cubiertos"] != "") {
            $inmueble->parqueaderos_cubiertos = $content["parqueaderos_cubiertos"];
        } else {
            $inmueble->parqueaderos_cubiertos = 0;
        }

        if (isset($content["parqueaderos_descubiertos"]) && $content["parqueaderos_descubiertos"] != "") {
            $inmueble->parqueaderos_descubiertos = $content["parqueaderos_descubiertos"];
        } else {
            $inmueble->parqueaderos_descubiertos = 0;
        }

        if (isset($content["parqueaderos_dobles"]) && $content["parqueaderos_dobles"] != "") {
            $inmueble->parqueaderos_dobles = $content["parqueaderos_dobles"];
        } else {
            $inmueble->parqueaderos_dobles = 0;
        }


        if (isset($content["parq1_matricula"]) && $content["parq1_matricula"] != "") {
            $inmueble->matricula_parqueadero_1 = $content["parq1_matricula"];
        } else {
            $inmueble->matricula_parqueadero_1 = 0;
        }
        if (isset($content["parq2_matricula"]) && $content["parq2_matricula"] != "") {
            $inmueble->matricula_parqueadero_2 = $content["parq2_matricula"];
        } else {
            $inmueble->matricula_parqueadero_2 = 0;
        }
        if (isset($content["parq3_matricula"]) && $content["parq3_matricula"] != "") {
            $inmueble->matricula_parqueadero_3 = $content["parq3_matricula"];
        } else {
            $inmueble->matricula_parqueadero_3 = 0;
        }

        if (isset($content["chip"]) && $content["chip"] != "") {
            $inmueble->chip = $content["chip"];
        }
        if (isset($content["depositos"]) && $content["depositos"] != "") {
            $inmueble->depositos = $content["depositos"];
        }
        if (isset($content["cedula_catastral"]) && $content["cedula_catastral"] != "") {
            $inmueble->cedula_catastral = $content["cedula_catastral"];
        }

        if (isset($content["parq1_chip"]) && $content["parq1_chip"] != "") {
            $inmueble->chip_parqueadero_1 = $content["parq1_chip"];
        } else {
            $inmueble->chip_parqueadero_1 = 0;
        }
        if (isset($content["parq2_chip"]) && $content["parq2_chip"] != "") {
            $inmueble->chip_parqueadero_2 = $content["parq2_chip"];
        } else {
            $inmueble->chip_parqueadero_2 = 0;
        }
        if (isset($content["parq3_chip"]) && $content["parq3_chip"] != "") {
            $inmueble->chip_parqueadero_3 = $content["parq3_chip"];
        } else {
            $inmueble->chip_parqueadero_3 = 0;
        }

        if (isset($content["parq1_cedula"]) && $content["parq1_cedula"] != "") {
            $inmueble->cedula_parqueadero_1 = $content["parq1_cedula"];
        } else {
            $inmueble->cedula_parqueadero_1 = 0;
        }
        if (isset($content["parq2_cedula"]) && $content["parq2_cedula"] != "") {
            $inmueble->cedula_parqueadero_2 = $content["parq2_cedula"];
        } else {
            $inmueble->cedula_parqueadero_2 = 0;
        }
        if (isset($content["parq3_cedula"]) && $content["parq3_cedula"] != "") {
            $inmueble->cedula_parqueadero_3 = $content["parq3_cedula"];
        } else {
            $inmueble->cedula_parqueadero_3 = 0;
        }

        if (isset($content["dpto1_matricula"]) && $content["dpto1_matricula"] != "") {
            $inmueble->matricula_deposito_1 = $content["dpto1_matricula"];
        } else {
            $inmueble->matricula_deposito_1 = 0;
        }
        if (isset($content["dpto2_matricula"]) && $content["dpto2_matricula"] != "") {
            $inmueble->matricula_deposito_2 = $content["dpto2_matricula"];
        } else {
            $inmueble->matricula_deposito_2 = 0;
        }

        if (isset($content["dpto1_chip"]) && $content["dpto1_chip"] != "") {
            $inmueble->chip_deposito_1 = $content["dpto1_chip"];
        } else {
            $inmueble->chip_deposito_1 = 0;
        }
        if (isset($content["dpto2_chip"]) && $content["dpto2_chip"] != "") {
            $inmueble->chip_deposito_2 = $content["dpto2_chip"];
        } else {
            $inmueble->chip_deposito_2 = 0;
        }

        if (isset($content["dpto1_cedula"]) && $content["dpto1_cedula"] != "") {
            $inmueble->cedula_deposito_1 = $content["dpto1_cedula"];
        } else {
            $inmueble->cedula_deposito_1 = 0;
        }
        if (isset($content["dpto2_cedula"]) && $content["dpto2_cedula"] != "") {
            $inmueble->cedula_deposito_2 = $content["dpto2_cedula"];
        } else {
            $inmueble->cedula_deposito_2 = 0;
        }

        if (isset($content["parqueaderos_sencillos"]) && $content["parqueaderos_sencillos"] != "") {
            $inmueble->parqueaderos_sencillos = $content["parqueaderos_sencillos"];
        } else {
            $inmueble->parqueaderos_sencillos = 0;
        }

        if (isset($content["area_lote"]) && $content["area_lote"] != "") {
            $inmueble->area_lote = str_replace(",", ".", $content["area_lote"]);
        } else {
            $inmueble->area_lote = 0;
        }

        if (isset($content["area_cons"]) && $content["area_cons"] != "") {
            $inmueble->area_construida = str_replace(",", ".", $content["area_cons"]);
        } else {
            $inmueble->area_construida = 0;
        }

        if (isset($content["area_privada"]) && $content["area_privada"] != "") {
            $inmueble->area_privada = str_replace(",", ".", $content["area_privada"]);
        } else {
            $inmueble->area_privada = 0;
        }

        if (isset($content["frente"]) && $content["frente"] != "") {
            $frente = str_replace("," , ".", $content["frente"]);
            $inmueble->frente = $frente;
        } else {
            $inmueble->frente = 0;
        }

        if (isset($content["fondo"]) && $content["fondo"] != "") {
            $fondo = str_replace("," , ".", $content["fondo"]);
            $inmueble->fondo = $fondo;
        } else {
            $inmueble->fondo = 0;
        }

        if (isset($content["construccion"]) && $content["construccion"] != "") {
            $inmueble->anio_construccion = $content["construccion"];
        } else {
            $inmueble->anio_construccion = 0;
        }

        if (isset($content["nivel"]) && $content["nivel"] != "") {
            $inmueble->nivel = $content["nivel"];
        } else {
            $inmueble->nivel = 0;
        }

        if (isset($content["video"]) && $content["video"] != "") {
            $inmueble->video = $content["video"];
        } else {
            $inmueble->video = "";
        }

        if (isset($content["tour_virtual"]) && $content["tour_virtual"] != "") {
            $inmueble->tour_virtual = $content["tour_virtual"];
        } else {
            $inmueble->tour_virtual = "";
        }

        if (isset($content["sucursal"]) && $content["sucursal"] != "") {
            $inmueble->oficina_id = $content["sucursal"];
        } else {
            $inmueble->oficina_id = $usuario->oficina_id;
        }

        if (isset($content["aviso"]) && $content["aviso"] == "1") {
            $inmueble->aviso_ventana = true;
        } else {
            $inmueble->aviso_ventana = false;
        }

        if (isset($content["destacado"]) && $content["destacado"] == "on") {
            $inmueble->destacado = true;
        } else {
            $inmueble->destacado = false;
        }

        if (isset($content["exclusivo"]) && $content["exclusivo"] == "on") {
            $inmueble->exclusivo = true;
        } else {
            $inmueble->exclusivo = false;
        }

        if (isset($content["remodelacion"]) && $content["remodelacion"] != "") {
            $inmueble->anio_remodelacion = $content["remodelacion"];
        } else {
            $inmueble->anio_remodelacion = 0;
        }

        if (isset($content["valor_renta"]) && $content["valor_renta"] != "") {
            $inmueble->valor_renta = str_replace(",", "", $content["valor_renta"]);
        } else {
            $inmueble->valor_renta = 0;
        }

        if (isset($content["comision"]) && $content["comision"] != "") {
            $inmueble->comision = $content["comision"];
        } else {
            $inmueble->comision = 0;
        }

        if (isset($content["matricula"]) && $content["matricula"] != "") {
            $inmueble->matricula = $content["matricula"];
        } else {
            $inmueble->matricula = "";
        }

        if (isset($content["chip"]) && $content["chip"] != "") {
            $inmueble->chip = $content["chip"];
        }

        if (isset($content["cedula_catastral"]) && $content["cedula_catastral"] != "") {
            $inmueble->cedula_catastral = $content["cedula_catastral"];
        }

        if (isset($content["asesor"]) && $content["asesor"] != "") {
            $inmueble->asesor_id = $content["asesor"];
        } else {
            $inmueble->asesor_id = $usuario->id;
        }

        if (isset($content["descripcion"]) && $content["descripcion"] != "") {
            $inmueble->descripcion = $content["descripcion"];
        } else {
            $inmueble->descripcion = "";
        }

        if (isset($content["descripcion"]) && $content["descripcion"] != "") {
            $inmueble->descripcion_metro_cuadrado = $content["descripcion"];
        } else {
            $inmueble->descripcion_metro_cuadrado = "";
        }

        if (isset($content["descripcion"]) && $content["descripcion"] != "") {
            $inmueble->descripcion_mls = $content["descripcion"];
        } else {
            $inmueble->descripcion_mls = "";
        }

        if (isset($content["restricciones"]) && $content["restricciones"] != "") {
            $inmueble->restricciones = $content["restricciones"];
        } else {
            $inmueble->restricciones = "";
        }

        if (isset($content["comentario"]) && $content["comentario"] != "") {
            $inmueble->comentario = $content["comentario"];
        } else {
            $inmueble->comentario = "";
        }

        if (isset($content["comentario2"]) && $content["comentario2"] != "") {
            $inmueble->comentario2 = $content["comentario2"];
        } else {
            $inmueble->comentario2 = "";
        }

        if (isset($content["latitude"]) && $content["latitude"] != "") {
            $inmueble->latitud = $content["latitude"];
        } else {
            $inmueble->latitud = "";
        }

        if (isset($content["longitude"]) && $content["longitude"] != "") {
            $inmueble->longitud = $content["longitude"];
        } else {
            $inmueble->longitud = "";
        }


        //Guardando el inmueble
        $inmueble->save();
        $inmueble->codigo = $inmueble->id;
        $inmueble->save();

        //Características
        foreach ($content as $key => $value) {
            $caract_id = explode("_", $key);
            if ($caract_id[0] == "caract") {

                $caracteristica = InmuebleCaracteristica::where("inmueble_id", $inmueble->id)
                ->where("caracteristica_id", $caract_id[1])
                ->first();


                if (empty($caracteristica) && $value == "on") {
                    $caract = new InmuebleCaracteristica();
                    $caract->inmueble_id = $inmueble->id;
                    $caract->caracteristica_id = $caract_id[1];
                    $caract->create_user_id = $usuario->id;
                    $caract->update_user_id = $usuario->id;
                    $caract->save();
                }

            }
        }

        // Dirección
        if (!isset($content["complete_address"]) || $content["complete_address"] == "") {
            $direccion = new InmuebleDireccion();
            $direccion->inmueble_id = $inmueble->id;
            $direccion->c1_id = $content["dir1"];
            $direccion->c2 = $content["dir2"];
            $direccion->c3_id = $content["dir3"];
            $direccion->c4_id = $content["dir4"];
            $direccion->c5_id = $content["dir5"];
            $direccion->c6_id = $content["dir6"];
            $direccion->c7 = $content["dir7"];
            $direccion->c8_id = $content["dir8"];
            $direccion->c9 = $content["dir9"];
            $direccion->c10_id = $content["dir10"];
            $direccion->complemento = $content["complemento"] ?? "";
            $direccion->complemento2 = $content["complemento2"] ?? "";
            $direccion->create_user_id = $usuario->id;
            $direccion->update_user_id = $usuario->id;
            $direccion->save();
        }

        //Vamos a crear el primer seguimiento
        $seguimiento = [
            "property" => $inmueble->id,
            "status" => 1,
            "substatus" => 1,
            "description" => "Inmueble creado"
        ];

        $cliente = GeneralController::postRequest(url("api/seguimientos"), [
            "Authorization" => $usuario->token
        ], $seguimiento);

        return $inmueble;
    }

    public function crearInmueble(Request $request) {
        $usuario = session("usuario");
        $content = $request->all();

        //Obtener el último codigo de inmueble
        $ultimoCodigo = Inmueble::orderBy("id", "desc")->first();
        $codigo = 1;

        if ($ultimoCodigo != null) {
            $codigo = $ultimoCodigo->codigo + 1;
        }

        //Primera parte
        $tipos = TipoInmueble::get();
        $gestiones = Gestion::get();
        $destinos = Destino::get();

        //Ubicación
        $departamentos = EstadoUbicacion::get();
        $zonas = Zona::get();
        $estratos = Estrato::get();

        //Dirección
        $dir1 = Dir1::get();
        $dir2 = Dir2::get();
        $dir3 = Dir3::get();
        $dir4 = Dir4::get();

        //Adicionales
        $sucursales = Oficina::get();

        GeneralController::renderHeader("Inmuebles", $usuario, "activeInmueblesCrear", true, "expandedInmuebles");

        echo view("components.inmuebles.crear")
        ->with("usuario", $usuario)
        ->with("codigo", $codigo)
        ->with("tipos", $tipos)
        ->with("gestiones", $gestiones)
        ->with("destinos", $destinos)
        ->with("departamentos", $departamentos)
        ->with("zonas", $zonas)
        ->with("estratos", $estratos)
        ->with("dir1", $dir1)
        ->with("dir2", $dir2)
        ->with("dir3", $dir3)
        ->with("dir4", $dir4)
        ->with("sucursales", $sucursales);

        GeneralController::renderFooter();
    }

    public function dashboard(Request $request) {
        $usuario = session("usuario");
        $content = $request->all();

        // return $content;
        if (isset($content["ciudad"]) && $content["ciudad"] != "") {
            $content["ciudad"] = str_replace("number:", "", $content["ciudad"]);
        }

        // validaciones usuarios
        if(!isset($content['estado'])){
            switch ($usuario['rol_id']) {
                case 1:
                case 2:
                $content['estado'] = 5;
                break;
                case 5:
                $content['estado'] = 2;
                break;
                case 6:
                $content['estado'] = 3;
                break;

                default:
                # code...
                break;
            }
        }

        if (isset($content["orden"]) && $content["orden"] != "") {
            switch ($content["orden"]) {
                case 1:
                $content["order"] = "id";
                $content["sort"] = "desc";
                break;
                case 2:
                $content["order"] = "id";
                $content["sort"] = "asc";
                case 3:
                $content["order"] = "updated_at";
                $content["sort"] = "desc";
                case 4:
                $content["order"] = "updated_at";
                $content["sort"] = "asc";
                case 5:
                $content["order"] = "codigo";
                $content["sort"] = "asc";
                case 6:
                $content["order"] = "codigoMLS";
                $content["sort"] = "asc";
                case 7:
                $content["order"] = "canon";
                $content["sort"] = "asc";
                case 8:
                $content["order"] = "venta";
                $content["sort"] = "asc";
                break;
            }
        } else {
            $content["order"] = "id";
            $content["sort"] = "desc";
            $content["orden"] = 1;
            $content["agente"]=($usuario['rol_id'] ==1 || $usuario['rol_id'] ==5|| $usuario['rol_id'] ==6)?'0':  $usuario["id"];
        }

        // return $content;

        $inmuebles = $this->generalList($content, $usuario);
        // return $inmuebles;

        $departamentos = EstadoUbicacion::get();
        $gestiones = Gestion::get();
        $tipos = TipoInmueble::get();
        $estados = EstadoInmueble::get();
        $subestados = SubEstadoInmueble::get();
        $usuarios = Usuario::wherein('rol_id',[1,2])->where('estado_id',1)->get();

        // return $content;

        GeneralController::renderHeader("Inmuebles", $usuario, "activeInmuebles", true, "expandedInmuebles", 1);

        echo view("components.inmuebles.index")
        ->with("inmuebles", $inmuebles)
        ->with("departamentos", $departamentos)
        ->with("gestiones", $gestiones)
        ->with("tipos", $tipos)
        ->with("estados", $estados)
        ->with("subestados", $subestados)
        ->with("agente", $usuario)
        ->with("usuarios", $usuarios)
        ->with("general", new GeneralController())
        ->with("request", $content);

        GeneralController::renderFooter("", 1);
    }

    public function crearImagenesInmuebleBack(Request $request, $id) {
        //Aquí se subirá el archivo a amazon y a la base de datos
        $content = $request->all();

        if (isset($content["primera_subida"]) == 1) {
            return $content;
        } else {

            $other_part = "";
            if (isset($content["are_360"]) && $content["are_360"] == true) {
                $other_part = "360";
            }
            for ($i = 1; $i <= 30 ; $i++) {
                if (isset($content["imagen" . $i]) && $content["imagen" . $i] != "") {
                    $url = GeneralController::transformImageFromBase64($content["imagen" . $i], "inmuebles", "inmueble_" . $id . "_" . $i . ($other_part == "360" ? "_" . "360" : "") . "_imagen");
                    $imagenInmueble = new InmuebleImagen();
                    $imagenInmueble->inmueble_id = $id;
                    $imagenInmueble->is_360 = $other_part == "360";
                    $imagenInmueble->url = $url;
                    $imagenInmueble->thumb_url = $url;
                    $imagenInmueble->orden = $i;
                    $imagenInmueble->comentario = "Desde el gestor personal";
                    $imagenInmueble->save();
                }
            }

            return response()->json([
                "status" => "200",
                "mensaje" => "Imágenes subidas con éxito",
            ], 200);
        }
    }

    public function crearImagenesInmueble(Request $request, $id) {
        $usuario = session("usuario");
        $content = $request->all();

        $inmueble = Inmueble::find($id);

        GeneralController::renderHeader("Inmuebles", $usuario, "activeInmueblesCrear", true, "expandedInmuebles");

        echo view("components.inmuebles.crear-imagenes")
        ->with("inmueble", $inmueble)
        ->with("usuario", $usuario);

        GeneralController::renderFooter();
    }

    public function crearPropietariosInmueble(Request $request, $id) {
        $usuario = session("usuario");
        $content = $request->all();

        $inmueble = Inmueble::find($id);
        $ciudades = new CiudadController();
        $estados = new EstadoController();
        $roles = new RolController();
        $tipos_documento = new TipoDocumentoController();
        $subestado_civil = SubEstadoCivil::get();
        $inmuebleClientes = Cliente::whereHas("inmuebles", function ($query) use ($id) {
            $query->where("inmueble_id", $id);
        })->get();

        GeneralController::renderHeader("Crear propietarios", $usuario, "activeInmueblesCrear", true, "expandedInmuebles");

        echo view("components.inmuebles.crear-propietarios")
        ->with("usuario", $usuario)
        ->with("inmueble", $inmueble)
        ->with("ciudades", $ciudades->generalList())
        ->with("estados", $estados->generalList())
        ->with("roles", $roles->generalList(2))
        ->with("estados_civiles", $subestado_civil)
        ->with("tipos_documento", $tipos_documento->generalList())
        ->with("inmuebleClientes", $inmuebleClientes);

        GeneralController::renderFooter();
    }

    public function crearDocumentosInmuebleBack(Request $request, $id) {
        $content = $request->all();
        $usuario = session("usuario");
        $inmueble = Inmueble::find($id);

        if (isset($content["comentarios_documentos"]) && $content["comentarios_documentos"] != "") {
            $inmueble->comentarios_documentos = $content["comentarios_documentos"];
        } else {
            $inmueble->comentarios_documentos = "";
        }
        $inmueble->save();

        $errorFiles = '';
        $id_max = 23;
        if($inmueble->gestion_id != 1){
            $id_max = 11;
        }
        $documentosOpcionales = TipoDocumentoInmueble::where('id','>',5)
        ->where('id','<',$id_max)
        ->Orwhere('id',20)
        ->Orwhere('id',23)
        ->get();


        //Guardando documentos del cliente
        for ($i = 1; $i <= 5; $i++) {
            if ($request->hasFile("file_" . $i)) {
                if($request->file("file_" . $i)->isValid()){

                    $documento = $request->file("file_" . $i);

                    switch ($i) {
                        case 1:
                        $documentName = "inmueble_" . $id . "_clt_documento." . $documento->extension();
                        $comentario = "CLT";
                        break;

                        case 2:
                        $documentName = "inmueble_" . $id . "_cedula_propietario_documento." . $documento->extension();
                        $comentario = "Cédula de propietario";
                        break;

                        case 3:
                        $documentName = "inmueble_" . $id . "_contrato_formado_documento." . $documento->extension();
                        $comentario = "Contrato firmado";
                        break;

                        case 4:
                        $documentName = "inmueble_" . $id . "_cuenta_cobro_administracion_documento." . $documento->extension();
                        $comentario = "Cuenta de cobro de administración";
                        break;

                        case 5:
                        $documentName = "inmueble_" . $id . "_docusign." . $documento->extension();
                        $comentario = "Docusign";
                        break;

                        default:
                        $documentName = "";
                        $comentario = "";
                        break;
                    }

                    $driver = "public";
                    $url = $documento->storeAs("documentos/inmueble_" . $id, $documentName, $driver);

                    //Guardando documento en base de datos
                    $documentoExists = DocumentoInmueble::where("inmueble_id", $inmueble->id)
                    ->where("tipo_documento_id", $i)
                    ->first();


                    if ($documentoExists == null) {
                        $documentoInmueble = new DocumentoInmueble();
                        $documentoInmueble->inmueble_id = $inmueble->id;
                        $documentoInmueble->tipo_documento_id = $i;
                        $documentoInmueble->is_active = true;
                        $documentoInmueble->comentario = $comentario;

                        if ($driver == "public") {
                            $documentoInmueble->url = url("storage/" . $url);
                        }

                        $documentoInmueble->save();
                    } else {
                        $documentoExists->url = url("storage/" . $url);
                        $documentoExists->save();
                    }
                }


                else{
                    switch ($i) {
                        case 1:
                        $errorFiles =  $request->file("file_" . $i)->isValid() ? $errorFiles : $errorFiles.' CLT ';

                        break;

                        case 2:
                        $errorFiles =  $request->file("file_" . $i)->isValid() ? $errorFiles : $errorFiles.' Cédula de propietario ';

                        break;

                        case 3:
                        $errorFiles =  $request->file("file_" . $i)->isValid() ? $errorFiles : $errorFiles.' Contrato firmado ';

                        break;

                        case 4:
                        $errorFiles =  $request->file("file_" . $i)->isValid() ? $errorFiles : $errorFiles.' Cuenta de cobro de administración ';

                        break;

                        case 5:
                        $errorFiles =  $request->file("file_" . $i)->isValid() ? $errorFiles : $errorFiles.' Docusign ';
                        ;
                        break;

                        default:
                        break;
                    }
                }
            }
        }

        //Guardando documentos adicionales
        foreach ($documentosOpcionales as $key => $value) {
            if ($request->hasFile("file_" . $value->id)) {
                if($request->file("file_" . $value->id)->isValid()){
                    $documento = $request->file("file_" . $value->id);
                    $comentario = $value->nombre;
                    $name_format = mb_strtolower(str_replace(" " , "_", $comentario)) ;
                    $documentName = "inmueble_" . $id . $name_format.'.' . $documento->extension();
                    $driver = "public";
                    $url = $documento->storeAs("documentos/inmueble_" . $id, $documentName, $driver);
                    // guardando en base de datos
                    $documentoExists = DocumentoInmueble::where("inmueble_id", $inmueble->id)
                    ->where("tipo_documento_id", $value->id)
                    ->first();


                    if ($documentoExists == null) {
                        $documentoInmueble = new DocumentoInmueble();
                        $documentoInmueble->inmueble_id = $inmueble->id;
                        $documentoInmueble->tipo_documento_id = $value->id;
                        $documentoInmueble->is_active = true;
                        $documentoInmueble->comentario = $comentario;

                        if ($driver == "public") {
                            $documentoInmueble->url = url("storage/" . $url);
                        }

                        $documentoInmueble->save();
                    } else {
                        $documentoExists->url = url("storage/" . $url);
                        $documentoExists->save();
                    }
                }else{
                    $errorFiles =  $request->file("file_" . $value->id)->isValid() ? $errorFiles : $errorFiles.$value->nombre;
                }
            }
        }

        if (isset($content["revision"]) && $content["revision"] == 1) {
            //Vamos a crear el segundo seguimiento
            $seguimiento = [
                "property" => $inmueble->id,
                "status" => 3,
                "substatus" => 4,
                "description" => "Inmueble enviado a revisión"
            ];

            $cliente = GeneralController::postRequest(url("api/seguimientos"), [
                "Authorization" => $usuario->token
            ], $seguimiento);
        }

        if($errorFiles == ''){

            return redirect()
            ->route("listaInmuebles")
            ->with("mensaje", "Inmueble creado exitosamente");
        }else{
            $errorFiles = "Los siguientes archivos no se subieron correctamente, pueden tener un formato incorrecto o estar
            corruptos: ". $errorFiles. ".";
            return redirect()
            ->route("listaInmuebles")
            ->with("mensaje", $errorFiles);
        }

    }

    public function crearDocumentosInmueble(Request $request, $id) {
        $usuario = session("usuario");
        $content = $request->all();

        $inmueble = Inmueble::find($id);
        //se traen los tipos de documento para adjuntarlos en la vista, se traeran solo depositos y parquqeaderos en caso de que el inmueble sea diferente a arriendo
        $id_max = 23;
        if($inmueble->gestion_id != 1 || $inmueble->corretaje == 1){
            $id_max = 11;
        }
        $documentosOpcionales = TipoDocumentoInmueble::where('id','>',5)
        ->where('id','<',$id_max)
        ->Orwhere('id',20)
        ->Orwhere('id',23)
        ->get();



        GeneralController::renderHeader("Inmuebles", $usuario, "activeInmueblesCrear", true, "expandedInmuebles");

        echo view("components.inmuebles.crear-documentos")
        ->with("tipos_doc", $documentosOpcionales)
        ->with("inmueble", $inmueble);

        GeneralController::renderFooter();
    }

    //Manejo de Imágenes
    public function saveImage(Request $request) {
        $content = $request->all();

        //Guardando imagen en disco duro
        $image = $request->file("image");
        $imageName = "inmueble_" . $content["property"] . "_" . $content["imageNumber"] . ($content["is_360"] == "true" ? "_" . "360" : "") . "_" . mb_strtolower(str_replace(" " , "-", $image->getClientOriginalName())) . "_" . "_imagen." . $image->extension();
        $driver = $this->driver;
        $url = $image->storeAs("inmuebles/inmueble_" . $content["property"], $imageName, $this->driver);

        //Guardando imagen en base de datos
        $imagenInmueble = new InmuebleImagen();
        $imagenInmueble->inmueble_id = $content["property"];
        $imagenInmueble->is_360 = $content["is_360"] == "true";

        if ($driver == "s3") {
            $imagenInmueble->url = "https://elitesystem1.s3.amazonaws.com/$url";
        }

        $imagenInmueble->thumb_url = $url;
        $imagenInmueble->orden = $content["imageNumber"];
        $imagenInmueble->comentario = "Desde el gestor personal";
        $imagenInmueble->save();

        return response($imagenInmueble->id);
    }

    public function saveImageOrder(Request $request, $id) {
        //Turn input "imageIdOrder" from string to array with "explode"
        //function then saved as $imageIdOrder.
        $imageIdOrder = explode(',', $request->input('imageIdOrder'));
        //For each $imageIdOrder element find record in "image” table with "id"
        //equal $imageIdOrder[$i] then update "image_number" equal i + 1.
        for ($i = 0; $i < count($imageIdOrder); $i++) {
            $image = InmuebleImagen::where("inmueble_id", $id)
            ->where("id", $imageIdOrder[$i])
            ->update(["orden" => $i + 1]);
        }
    }

    public function deleteImage(Request $request) {
        //Find in database table "image" the image with "id" equal request
        //input "imageId" then delete it.
        $image = InmuebleImagen::where("id", $request->input("imageId"))->first();
        $imageName = $image->thumb_url;
        // Vamos a borrar la imagen del API
        $inmueble = Inmueble::with("asesores")->find($image->inmueble_id);
        $aEnviar = [];
        if ($inmueble->codigoMLS != "" && $inmueble->publicado == 1) {
            if ($image->is_360) {
                $aEnviar["image_delete_{$image->orden}"] = 1;
            } else {
                $aEnviar["image360_delete_{$image->orden}"] = 1;
            }
            $cliente = GeneralController::putRequest("http://api.domus.la/3.0/properties/". $inmueble->codigoMLS, [
                "Authorization" => $inmueble->asesores->domus_token
            ], $aEnviar, 1);
        }

        $image->delete();
        // In server storage delete image with name equal $imageName.
        GeneralController::generalDeleteImage($imageName);
    }

    public function apiCrearPropietarios(Request $request, $id) {
        $content = $request->all();

        $inmueble = Inmueble::find($id);

        if ($inmueble == null) {
            return response()->json([
                "status" => "404",
                "mensaje" => "Inmueble no encontrado",
            ], 404);
        }

        //Esto es solo para la creación y edición
        for ($i = 0; $i <= 30; $i++) {
            if (isset($content["owner_" . $i]) && $content["owner_" . $i] != 0) {

                $searchProspect = Cliente::find($content["owner_" . $i]);
                if($searchProspect->rol_id == 13){
                    $searchProspect->rol_id = 3;
                    $searchProspect->save();
                }

                $inmuebleHasCliente = InmuebleCliente::where("inmueble_id", $id)
                ->where("cliente_id", $content["owner_" . $i])
                ->first();

                if ($inmuebleHasCliente == null) {
                    $inmuebleCliente = new InmuebleCliente();
                    $inmuebleCliente->inmueble_id = $id;
                    $inmuebleCliente->cliente_id = $content["owner_" . $i];
                    $inmuebleCliente->rol_id = 3;
                    $inmuebleCliente->orden = $i;
                    $inmuebleCliente->porcentaje = $content["owner_percentage_" . $i];

                    $inmuebleCliente->save();
                } else {
                    $inmuebleHasCliente->orden = $i;
                    $inmuebleHasCliente->porcentaje = $content["owner_percentage_" . $i];

                    $inmuebleHasCliente->save();
                }
            }
        }

        $inmuebleClientes = InmuebleCliente::where("inmueble_id", $id)
        ->get();

        return $inmuebleClientes;
    }

    public function duplicateInm($id){
        $originalInm = Inmueble::with('direcciones','caracteristicas','imagenes','documentos')->find($id);
        $originalDireccion = InmuebleDireccion::find($originalInm->direcciones->id );

        // return url("/");

        $newInm = $originalInm->replicate();
        $newInm->save();

        $newDireccion = $originalDireccion->replicate();
        $newDireccion->inmueble_id = $newInm->id;
        $newDireccion->save();

        foreach ($originalInm->caracteristicas  as $key => $value) {
            $value->replicate();
            $value->inmueble_id = $newInm->id;
            $value->save();
        }


        foreach ($originalInm->imagenes as $key => $value) {
            $baseUrl = "https://elitesystem1.s3.amazonaws.com";
            $inmName = "inmueble_".$originalInm->id;
            $newName = "inmueble_".$newInm->id;

            $newUrl = str_replace($inmName,$newName,$value->url);
            $newUrl = str_replace($baseUrl,'',$newUrl);

            $actualUrl = str_replace($baseUrl,'',$value->url);
            // return $value;
            File::copy($actualUrl, $newUrl);
            $value->replicate();
            $value->inmueble_id = $newInm->id;
            $value->url = $newUrl;
            $value->save();

        }

        $newFullInm = Inmueble::with('direcciones','caracteristicas','imagenes','documentos')->find($newInm->id);
        return $newFullInm;
    }
}
