<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ["deleted_at"];

    public function estados() {
        return $this->belongsTo("App\Models\Estado", "estado_id", "id");
    }

    public function roles() {
        return $this->belongsTo("App\Models\Rol", "rol_id", "id");
    }

    public function tipos_documento() {
        return $this->belongsTo("App\Models\TipoDocumento", "tipo_documento_id", "id");
    }

    public function inmuebles() {
        return $this->hasMany("App\Models\InmuebleCliente", "cliente_id", "id");
    }

    // Belongs To
    public function subestadociviles() {
        return $this->belongsTo("App\Models\SubEstadoCivil", "subestado_civil_id", "id");
    }
}
