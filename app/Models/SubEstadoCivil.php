<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubEstadoCivil extends Model
{
    use HasFactory;
    protected $table = 'subestado_civiles';
}