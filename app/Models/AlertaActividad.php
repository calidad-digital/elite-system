<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlertaActividad extends Model
{
    use HasFactory;

    public function asignado() {
        return $this->belongsTo("App\Models\Usuario", "usuario_asignado_id", "id");
    }
    public function creador() {
        return $this->belongsTo("App\Models\Usuario", "usuario_creador_id", "id");
    }
}