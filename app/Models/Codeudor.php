<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Codeudor extends Model
{
    use HasFactory;
    protected $table = 'codeudores';
}