<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocumentoInmueble extends Model
{
    use HasFactory;

    public function tipos() {
        return $this->belongsTo("App\Models\TipoDocumentoInmueble", "tipo_documento_id", "id");
    }
}
