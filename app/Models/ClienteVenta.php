<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClienteVenta extends Model
{
    use HasFactory;

    // Has One
    public function apoderados() {
        return $this->hasOne("App\Models\ApoderadoClienteVenta", "cliente_venta_id", "id");
    }

    // Belongs To
    public function subestadociviles() {
        return $this->belongsTo("App\Models\SubEstadoCivil", "subestado_civil_id", "id");
    }

    public function tipos_documento() {
        return $this->belongsTo("App\Models\TipoDocumento", "tipo_documento_id", "id");
    }
}
