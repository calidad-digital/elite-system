<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Venta1Extended extends Model
{
    use HasFactory;

    protected $table = "venta1_extended";
}
