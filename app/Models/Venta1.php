<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Venta1 extends Model
{
    use HasFactory;

    public function inmuebles() {
        return $this->belongsTo("App\Models\Inmueble", "inmueble_id", "id");
    }

    public function usuarios() {
        return $this->belongsTo("App\Models\Usuario", "create_user_id", "id");
    }

    public function aprobaciones() {
        return $this->hasMany("App\Models\VentaCorreo", "venta_id", "id");
    }

    public function extensions() {
        return $this->hasOne("App\Models\Venta1Extended", "venta_id", "id");
    }
}
