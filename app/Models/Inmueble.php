<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inmueble extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dates = ["deleted_at"];

    public function tipos_inmueble() {
        return $this->belongsTo("App\Models\TipoInmueble", "tipo_inmueble_id", "id");
    }

    public function gestiones() {
        return $this->belongsTo("App\Models\Gestion", "gestion_id", "id");
    }

    public function direcciones() {
        return $this->hasOne("App\Models\InmuebleDireccion", "inmueble_id", "id");
    }

    public function portales() {
        return $this->hasMany("App\Models\InmueblePortal", "inmueble_id", "id");
    }

    public function caracteristicas() {
        return $this->hasMany("App\Models\InmuebleCaracteristica", "inmueble_id", "id");
    }

    public function imagenes() {
        return $this->hasMany("App\Models\InmuebleImagen", "inmueble_id", "id")->orderBy('orden','ASC');
    }

    public function documentos() {
        return $this->hasMany("App\Models\DocumentoInmueble", "inmueble_id", "id");
    }

    public function clientes() {
        return $this->hasMany("App\Models\InmuebleCliente", "inmueble_id", "id");
    }

    public function ciudades() {
        return $this->belongsTo("App\Models\Ciudad", "ciudad_id", "id");
    }

    public function zonas() {
        return $this->belongsTo("App\Models\Zona", "zona_id", "id");
    }

    public function localidades() {
        return $this->belongsTo("App\Models\Localidad", "localidad_id", "id");
    }

    public function estratos() {
        return $this->belongsTo("App\Models\Estrato", "estrato_id", "id");
    }

    public function destinos() {
        return $this->belongsTo("App\Models\Destino", "destino_id", "id");
    }

    public function asesores() {
        return $this->belongsTo("App\Models\Usuario", "asesor_id", "id");
    }

    public function estados() {
        return $this->belongsTo("App\Models\EstadoInmueble", "estado_id", "id");
    }
}