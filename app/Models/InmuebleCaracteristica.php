<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InmuebleCaracteristica extends Model
{
    use HasFactory;

    protected $fillable = ["id", "inmueble_id"];

    public function caracteristicas() {
        return $this->hasOne("App\Models\Caracteristica", "id", "caracteristica_id");
    }
}