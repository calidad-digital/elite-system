<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    use HasFactory;

    // Has Many

    public function permisos() {
        return $this->hasMany("App\Models\RolPermiso", "rol_id", "id");
    }
}
