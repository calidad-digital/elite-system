<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    use HasFactory;

    // Belongs To
    public function departamentos() {
        return $this->belongsTo("App\Models\EstadoUbicacion", "estado_ubicacions_id", "id");
    }
}
