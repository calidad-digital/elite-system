<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApoderadoClienteVenta extends Model
{
    use HasFactory;

    // Belongs To
    public function tipos_documento() {
        return $this->belongsTo("App\Models\TipoDocumento", "tipo_documento_id", "id");
    }
}
