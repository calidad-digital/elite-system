<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeguimientoVenta extends Model
{
    use HasFactory;

    //Belongs to
    public function estados() {
        return $this->belongsTo("App\Models\EstadoSeguimientosVenta", "estado_venta_id", "id");
    }

    public function ventas() {
        return $this->belongsTo("App\Models\Venta1", "venta_id", "id");
    }

    public function encargado() {
        return $this->belongsTo("App\Models\Usuario", "usuario_encargado", "id");
    }
    public function creador() {
        return $this->belongsTo("App\Models\Usuario", "create_user_id", "id");
    }
}