<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Arriendo extends Model
{
    use HasFactory;

    public function inmueble() {
        return $this->belongsTo("App\Models\Inmueble", "inmueble_id", "id");
    }

    public function arrendatario() {
        return $this->belongsTo("App\Models\Cliente", "arrendatario_id", "id");
    }

    public function creado() {
        return $this->belongsTo("App\Models\Usuario", "creado_por", "id");
    }

    public function actualizado() {
        return $this->belongsTo("App\Models\Usuario", "actualizado_por", "id");
    }

    public function ocupantes() {
        return $this->hasMany("App\Models\Ocupante", "arriendo_id", "id");
    }

    public function correos() {
        return $this->hasMany("App\Models\ArriendoCorreos", "arriendo_id", "id");
    }

    public function codeudores() {
        return $this->hasMany("App\Models\Codeudor", "arriendo_id", "id");
    }

    public function coordinador() {
        return $this->belongsTo("App\Models\Usuario", "coordinadora", "id");
    }

}
