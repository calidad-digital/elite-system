<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\AlertaActividad;

class Seguimiento extends Model
{
    use HasFactory;

    //Belongs to
    public function estados() {
        return $this->belongsTo("App\Models\EstadoInmueble", "estado_inmueble_id", "id");
    }

    public function subestados() {
        return $this->belongsTo("App\Models\SubEstadoInmueble", "sub_estado_inmueble_id", "id");
    }

    public function inmuebles() {
        return $this->belongsTo("App\Models\Inmueble", "inmueble_id", "id");
    }

    public function agentes() {
        return $this->belongsTo("App\Models\Usuario", "create_user_id", "id");
    }
    // alertas
    public function alertas(){
        // $alertas = AlertaActividad::get();
        return $this->hasOne("App\Models\AlertaActividad", "actividad_id", "id");
    }
}