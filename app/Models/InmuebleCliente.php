<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InmuebleCliente extends Model
{
    use HasFactory;

    protected $fillable = ["id", "inmueble_id"];

    // Belongs To
    public function clientes() {
        return $this->belongsTo("App\Models\Cliente", "cliente_id", "id");
    }

    public function inmuebles() {
        return $this->belongsTo("App\Models\Inmueble", "inmueble_id", "id");
    }
}
