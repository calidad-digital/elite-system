<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Usuario extends Model
{
    use HasFactory;

    protected $dates = ["deleted_at"];

    // Belongs To

    public function estados() {
        return $this->belongsTo("App\Models\Estado", "estado_id", "id");
    }

    public function empresas() {
        return $this->belongsTo("App\Models\Empresa", "empresa_id", "id");
    }

    public function departamentos() {
        return $this->belongsTo("App\Models\Departamento", "departamento_id", "id");
    }

    public function roles() {
        return $this->belongsTo("App\Models\Rol", "rol_id", "id");
    }
}
