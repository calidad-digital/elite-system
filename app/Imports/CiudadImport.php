<?php

namespace App\Imports;

use App\Models\EstadoUbicacion;
use App\Models\Ciudad;
use Maatwebsite\Excel\Concerns\ToModel;

class CiudadImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if ($row[0] != "codigo_domus") {
            $departamento = EstadoUbicacion::where("domus_id", $row[2])
            ->get()->toArray();

            return new Ciudad([
                "domus_id" => $row[0],
                "nombre" => $row[1],
                "estado_ubicacions_id" => $departamento[0]["id"]
            ]);
        } else {
            return null;
        }
    }
}
