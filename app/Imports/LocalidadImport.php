<?php

namespace App\Imports;

use App\Models\Localidad;
use App\Models\Ciudad;
use App\Models\Zona;
use Maatwebsite\Excel\Concerns\ToModel;

class LocalidadImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if ($row[0] != "id") {
            $ciudad = Ciudad::where("domus_id", $row[2])
            ->get()->toArray();

            $zona = Zona::where("id", $row[3])
            ->get()->toArray();

            return new Localidad([
                "nombre" => $row[1],
                "ciudad_id" => $ciudad[0]["id"],
                "zona_id" => $zona[0]["id"]
            ]);
        } else {
            return null;
        }
    }
}
