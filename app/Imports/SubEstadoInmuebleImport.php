<?php

namespace App\Imports;

use App\Models\SubEstadoInmueble;
use Maatwebsite\Excel\Concerns\ToModel;

class SubEstadoInmuebleImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if ($row[0] != "id") {
            return new SubEstadoInmueble([
                "nombre" => $row[1],
                "estado_id" => $row[2],
                "id_notificacion_asesores" => $row[3]
            ]);
        } else {
            return null;
        }
    }
}
