<?php

namespace App\Imports;

use App\Models\EstadoUbicacion;
use Maatwebsite\Excel\Concerns\ToModel;

class EstadoUbicacionImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if ($row[0] != "nombre") {
            return new EstadoUbicacion([
                "nombre" => $row[0],
                "domus_id" => $row[1]
            ]);
        } else {
            return null;
        }
    }
}
