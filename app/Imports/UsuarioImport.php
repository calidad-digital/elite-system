<?php

namespace App\Imports;

//Modelos
use App\Models\Usuario;

//Paquetes
use Maatwebsite\Excel\Concerns\ToModel;
use Carbon\Carbon;

class UsuarioImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if ($row[0] != "id" && $row[0] != null) {

            return new Usuario([
                "codigo_interno" => $row[0],
                "codigo_interno_mls" => $row[1],
                "nombres" => $row[2],
                "apellidos" => $row[3],
                "nombre_usuario" => $row[4],
                "ingreso" => $row[5],
                "token" => $row[6],
                "email" => $row[7],
                "telefono" => $row[8] ?? "0",
                "celular" => $row[9] ?? "0",
                "whatsapp" => $row[10] ?? "0",
                "documento" => $row[11] ?? "0",
                "foto" => $row[12] ?? "",
                "domus_token" => $row[13] ?? "",
                "domus_id" => $row[14] ?? 1,
                "comentario" => "",
                "comentario2" => "",
                "ciudad" => $row[15] ?? "",
                "departamento" => $row[16] ?? "",
                "orden" => $row[17] ?? 0,
                "departamento_id" => $row[18],
                "estado_ubicacions_id" => $row[19],
                "ciudad_id" => $row[20],
                "tipo_documento_id" => $row[21],
                "empresa_id" => $row[22],
                "oficina_id" => $row[23],
                "estado_id" => $row[24],
                "rol_id" => $row[25],
                "fecha_ingreso" => Carbon::parse($row[26] . " 00:00:00"),
                "fecha_nacimiento" => Carbon::parse($row[27] . " 00:00:00"),
                "fecha_ultimo_cambio_plan" => Carbon::parse($row[28] . " 00:00:00"),
                "plan" => $row[29]
            ]);
        } else {
            return null;
        }
    }
}
