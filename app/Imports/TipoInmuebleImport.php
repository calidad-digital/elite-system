<?php

namespace App\Imports;

use App\Models\TipoInmueble;
use Maatwebsite\Excel\Concerns\ToModel;

class TipoInmuebleImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if ($row[0] != "id") {
            return new TipoInmueble([
                "id" => $row[0],
                "nombre" => $row[1]
            ]);
        } else {
            return null;
        }
    }
}
