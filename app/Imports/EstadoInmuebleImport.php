<?php

namespace App\Imports;

use App\Models\EstadoInmueble;
use Maatwebsite\Excel\Concerns\ToModel;

class EstadoInmuebleImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if ($row[0] != "id") {
            return new EstadoInmueble([
                "nombre" => $row[1],
                "clase" => $row[2]
            ]);
        } else {
            return null;
        }
    }
}
