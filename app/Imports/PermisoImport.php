<?php

namespace App\Imports;

use App\Models\Permiso;
use Maatwebsite\Excel\Concerns\ToModel;

class PermisoImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if ($row[0] != "id") {
            return new Permiso([
                "nombre" => $row[1],
                "descripcion" => $row[2],
                "modulo_id" => $row[3]
            ]);
        } else {
            return null;
        }
    }
}
