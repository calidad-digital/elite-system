<?php

namespace App\Imports;

use App\Models\Rol;
use Maatwebsite\Excel\Concerns\ToModel;

class RolImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if ($row[0] != "id") {
            return new Rol([
                "tipo_rol" => $row[1],
                "nombre" => $row[2],
                "descripcion" => $row[3]
            ]);
        } else {
            return null;
        }
    }
}
