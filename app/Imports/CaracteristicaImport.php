<?php

namespace App\Imports;

//Modelos
use App\Models\Caracteristica;
use App\Models\TipoCaracteristica;
use App\Models\TipoInmueble;

//Paquetes
use Maatwebsite\Excel\Concerns\ToModel;

class CaracteristicaImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        if ($row[0] != "domus_id") {
            $tipo_caracteristica = TipoCaracteristica::where("id", $row[2])
            ->get()->toArray();

            $tipo_inmueble = TipoInmueble::where("id", $row[4])
            ->get()->toArray();

            return new Caracteristica([
                "domus_id" => $row[0],
                "nombre" => $row[1],
                "tipo_caracteristica_id" => $row[2],
                "orden" => $row[3],
                "tipo_inmueble_id" => $row[4],
                "tipo_inmueble" => $tipo_inmueble[0]["nombre"],
                "tipo" => $tipo_caracteristica[0]["nombre"]
            ]);
        } else {
            return null;
        }
    }
}
