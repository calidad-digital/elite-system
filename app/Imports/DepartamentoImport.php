<?php

namespace App\Imports;

use App\Models\Departamento;
use Maatwebsite\Excel\Concerns\ToModel;

class DepartamentoImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if ($row[0] != "id") {

            return new Departamento([
                "nombre" => $row[1],
                "orden" => $row[0],
                "rol_id" => $row[0] == 1 ? 1 : 2,
            ]);
        } else {
            return null;
        }
    }
}
