<?php

namespace App\Imports;

use App\Models\RolPermiso;
use Maatwebsite\Excel\Concerns\ToModel;

class RolPermisoImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if ($row[0] != "id") {
            return new RolPermiso([
                "rol_id" => $row[1],
                "permiso_id" => $row[2]
            ]);
        } else {
            return null;
        }
    }
}
