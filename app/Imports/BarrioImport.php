<?php

namespace App\Imports;

use App\Models\Barrio;
use App\Models\Localidad;
use App\Models\Ciudad;
use Maatwebsite\Excel\Concerns\ToModel;

class BarrioImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if ($row[0] != "nombre") {
            $ciudad = Ciudad::where("id", $row[2])
            ->get()->toArray();

            $localidad = Localidad::where("id", $row[1])
            ->get()->toArray();

            return new Barrio([
                "nombre" => $row[0],
                "ciudad_id" => $ciudad[0]["id"],
                "localidad_id" => $localidad[0]["id"]
            ]);
        } else {
            return null;
        }
    }
}
