<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\LoginMiddleware;
use Illuminate\Http\Request;

//Controladores
use App\Http\Controllers\UserController;
use App\Http\Controllers\SeguimientosVentasController;
use App\Http\Controllers\OficinaController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\InmuebleController;
use App\Http\Controllers\InmuebleEdicionController;
use App\Http\Controllers\CiudadController;
use App\Http\Controllers\LocalidadController;
use App\Http\Controllers\BarrioController;
use App\Http\Controllers\CaracteristicaController;
use App\Http\Controllers\SeguimientoController;
use App\Http\Controllers\SubEstadoInmuebleController;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\VentasController;
use App\Http\Controllers\ArriendosController;
use App\Http\Controllers\MLSController;
use App\Http\Controllers\ConfigController;
use App\Http\Controllers\GeneralController;
use App\Http\Controllers\AprobacionesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", function () {
    return redirect("admin");
});

//Login
Route::get("admin/login", [UserController::class, "login"])->name("adminLogin");
Route::post("admin/login", [UserController::class, "validateLogin"]);

//Logout
Route::get("admin/logout", [UserController::class, "logout"]);

//Vistas del administrador
Route::prefix("admin")->middleware([LoginMiddleware::class])->group(function () {

    //Ruta general
    Route::get("/", [UserController::class, "index"])->name("adminDashboard");
    Route::post("/aprobar-cualquier-cosa", [AprobacionesController::class, "index"]);

    //Rutas de clientes
    Route::group(["prefix" => "clientes"], function () {
        Route::get("/", [ClienteController::class, "dashboard"])->name("listaClientes");
        Route::post("/", [ClienteController::class, "crearCliente"]);
        Route::post("/{id}", [ClienteController::class, "editarCliente"]);
    });

    //Rutas de agentes
    Route::group(["prefix" => "agentes"], function () {
        Route::get("/", [UsuarioController::class, "dashboard"])->name("listaAgentes");
        Route::post("/", [UsuarioController::class, "crearUsuario"]);
        Route::post("/{id}", [UsuarioController::class, "editarUsuario"]);
    });

    //Rutas de inmuebles
    Route::group(["prefix" => "inmuebles"], function () {
        Route::get("", [InmuebleController::class, "dashboard"])->name("listaInmuebles");

        //Crear inmuebles
        Route::group(["prefix" => "crear"], function () {
            Route::get("/", [InmuebleController::class, "crearInmueble"])->name("crearInmueble");
            Route::post("/", [InmuebleController::class, "crearInmuebleBack"])->name("crearInmuebleBack");
            //Imágenes de inmuebles
            Route::get("{id}/imagenes", [InmuebleController::class, "crearImagenesInmueble"])->name("crearImagenesInmueble");
            Route::post("{id}/imagenes/save-image", [InmuebleController::class, "saveImage"])->name("saveImage");
            Route::post("{id}/imagenes/save-image-order", [InmuebleController::class, "saveImageOrder"])->name("saveImageOrder");
            Route::post("{id}/imagenes/delete-image", [InmuebleController::class, "deleteImage"])->name("deleteImage");

            //Propietarios de inmuebles
            Route::get("{id}/propietarios", [InmuebleController::class, "crearPropietariosInmueble"])->name("crearPropietariosInmueble");
            Route::get("{id}/documentos", [InmuebleController::class, "crearDocumentosInmueble"])->name("crearDocumentosInmueble");
            Route::post("{id}/documentos", [InmuebleController::class, "crearDocumentosInmuebleBack"])->name("crearDocumentosInmuebleBack");
        });

        //Editar inmuebles
        Route::group(["prefix" => "editar"], function () {
            Route::get("{id}", [InmuebleEdicionController::class, "editarInmueble"])->name("editarInmueble");
            Route::post("{id}", [InmuebleEdicionController::class, "editarInmuebleBack"])->name("editarInmuebleBack");
            Route::get("{id}/imagenes", [InmuebleEdicionController::class, "editarImagenesInmueble"])->name("editarImagenesInmueble");
            Route::get("{id}/propietarios", [InmuebleEdicionController::class, "editarPropietariosInmueble"])->name("editarPropietariosInmueble");
            Route::get("{id}/documentos", [InmuebleEdicionController::class, "editarDocumentosInmueble"])->name("editarDocumentosInmueble");
            Route::post("{id}/documentos", [InmuebleEdicionController::class, "editarDocumentosInmuebleBack"])->name("editarDocumentosInmuebleBack");
        });
    });

    //Seguimiento de inmmuebles
    Route::group(["prefix" => "seguimientos"], function () {
        Route::get("/", [SeguimientoController::class, "seguimientoInmueble"])->name("seguimientoInmueble");
        Route::post("/", [SeguimientoController::class, "seguimientoInmuebleBack"])->name("seguimientoInmuebleBack");
    });



    //ventas de inmuebles
    Route::group(["prefix" => "ventas"], function () {
        Route::get("/1/{code}", [VentasController::class, "ventas1"])->name("ventas1");
        Route::post("/1/{code}", [VentasController::class, "ventas1Back"])->name("ventas1Back");

        Route::get("/2/{id}", [VentasController::class, "ventas2"])->name("ventas2");
        Route::get("/3", [VentasController::class, "ventas3"])->name("ventas3");
        Route::get("/4/{id}", [VentasController::class, "ventas4"])->name("ventas4");
        Route::get("/editar/{id}", [VentasController::class, "edit"])->name("edit");
        Route::get("/cruzada", [VentasController::class, "ventaCruzada"])->name("ventaCruzada");
        Route::get("/contrato/{id}", [VentasController::class, "contratoVenta"]);

         //Seguimiento de ventas
        Route::group(["prefix" => "seguimiento"], function () {
            Route::get("/listaEstados", [SeguimientosVentasController::class, "listaEstados"])->name("listaEstados");
            Route::get("/lista/{id}", [SeguimientosVentasController::class, "lista"])->name("lista");
            Route::post("/crear", [SeguimientosVentasController::class, "crearSeguimiento"])->name("crearSeguimiento");
        });
    });

    //arriendo de inmuebles
    Route::group(["prefix" => "arriendos"], function () {
        Route::get("/lista", [ArriendosController::class, "lista"])->name("listaArriendos");
        Route::get("/1/{id}", [ArriendosController::class, "arriendos1"])->name("arriendos1");
        Route::post("/crear", [ArriendosController::class, "crear"])->name("crearArriendo");
        Route::post("/editar/{id}", [ArriendosController::class, "editarPost"]);

        Route::get("/editar/{id}", [ArriendosController::class, "arriendosEditar"])->name("editarArriendo");
        Route::get("/cerrar/{id}", [ArriendosController::class, "arriendosCerrar"])->name("cerrarArriendo");
        Route::get("/3", [ArriendosController::class, "arriendos3"])->name("arriendos3");
        Route::post("/generar-contrato/{id}", [ArriendosController::class, "generarContrato"]);

    });

    //Rutas de MLS
    Route::group(["prefix" => "mls"], function () {
        Route::get("/", [MLSController::class, "dashboard"])->name("listaMLS");
        Route::get("getPorpertyDetail/{code}/{id}", [MLSController::class, "getPorpertyDetail"]);
    });

    //Rutas de Configuración
    Route::group(["prefix" => "config"], function () {
        Route::get("tutorials", [ConfigController::class, "tutorials"])->name("configTutorials");
        Route::post("tutorials", [ConfigController::class, "tutorialBackEnd"]);
    });
});


//Rutas del API
Route::prefix("api")->middleware(["token"])->group(function () {

    //Oficinas
    Route::resource("oficinas", OficinaController::class);
    //Clientes
    Route::resource("clientes", ClienteController::class);
    //Usuarios
    Route::resource("usuarios", UsuarioController::class);
    //Inmuebles
    Route::group(["prefix" => "inmuebles"], function () {

        //Rutas GET
        Route::get("/", function (Request $request) {
            $content = $request->all();
            $inmuebles = new InmuebleController();
            return $inmuebles->generalList($content);
        })->name("apiListaInmuebles");

        //Rutas POST
        Route::post("crear/{id}/propietarios", [InmuebleController::class, "apiCrearPropietarios"])->name("apiCrearPropietarios");
        Route::post("editar/{id}/propietarios", [InmuebleEdicionController::class, "apiEditarPropietarios"])->name("apiEditarPropietarios");
    });

    //Seguimientos
    Route::resource("seguimientos", SeguimientoController::class);
});

//Rutas alternativas
Route::prefix("alt")->group(function () {

    // Pruebas
    Route::get("test", [GeneralController::class, "test"]);
    //Ciudades
    Route::get("ciudades/{departamento}", [CiudadController::class, "generalList"]);
    //Localidades
    Route::get("localidades/{ciudad}/{zona?}", [LocalidadController::class, "generalList"]);
    //Barrios
    Route::get("barrios/{localidad}", [BarrioController::class, "generalList"]);
    //Sub estados de inmuebles
    Route::get("sub-estados/{estado}", [SubEstadoInmuebleController::class, "generalList"]);
    //Usuarios
    Route::get("agentes/{sucursal}", [UsuarioController::class, "altGeneralList"]);
    Route::get("allUsers", [UsuarioController::class, "allUsers"]);
    //Caracteristicas
    Route::get("caracteristicas/{tipo_inmueble}/{tipo_caracteristica?}", [CaracteristicaController::class, "altGeneralList"]);
    //Publicar en el api
    Route::get("api-publish/{id}", [ApiController::class, "createProperty"]);
    //Actualizar inmueble en el api
    Route::get("api-update/{id}", [ApiController::class, "updateProperty"]);
    //Mostrar un usuario especifico del api
    Route::get("detalle-usuario/{id}", [ApiController::class, "detailUser"]);
    //Actualizar portales
    Route::get("detalle-inmueble/{cod}", [ApiController::class, "propertyDetailCode"]);
    //Actualizar inmueble en domus e ir a lista
    Route::get("actualizar-inmueble/{id}", [ApiController::class, "updateAndRedirectProperty"]);
    // Aceptación Arriendo
    Route::get("contrato-arrendamiento/{id}", [ArriendosController::class, "showContract"])->name("contratoArriendo");
    // Aceptación contrato venta
    Route::get("contrato-venta/{id}/{tipo?}", [VentasController::class, "revisarContrato"]);
    Route::post("contrato-venta/make-comment", [VentasController::class, "revisarContratoPost"]);
    // Mensaje de agradecimiento
    Route::get("contrato-gracias", [ArriendosController::class, "thanksContract"])->name("contratoGracias");

    Route::post("update-contract/{id}", [ArriendosController::class, "updateContract"]);

    Route::get("duplicate/{id}", [InmuebleController::class, "duplicateInm"]);

});
